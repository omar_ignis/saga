
# Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
# Click nbfs://nbhost/SystemFileSystem/Templates/Other/Dockerfile to edit this template
FROM adoptopenjdk/openjdk11:alpine
ARG HOME_DIR=/home/inventory
ARG MAVEN_VERSION=3.6.3
ARG MAVEN_URL=https://mirrors.estointernet.in/apache/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
RUN wget ${MAVEN_URL} -P /home
RUN tar -xvf apache-maven-${MAVEN_VERSION}-bin.tar.gz -C /opt
ENV MAVEN_HOME /opt/apache-maven-${MAVEN_VERSION}
ENV PATH="$MAVEN_HOME/bin::${PATH}"
WORKDIR ${HOME_DIR}
RUN mvn -version
COPY src ./
COPY pom.xml ./
RUN mvn clean package -Dmaven.test.skip=true 
EXPOSE 9191
#ENTRYPOINT ["java","-Xmx512M","-Djava.security.egd=file:/dev/./urandom","-jar","${HOME}/inventory-item.jar"]