/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.factory.impl;

import java.util.List;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.document.impl.SaleReturnDocumentImpl;
import com.inventory.item.repository.SalesReturnDocumentRepository;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class SalesReturnDocumentFactoryImplTest {
    
    @Mock
    private SalesReturnDocumentRepository salesReturnDocumentRepository;

    @InjectMocks
    private SaleReturnDocumentImpl salesReturnDocumentFactoryImpl;

    private Item itemOne;
    private Item itemTwo;

    private Warehouse warehouseOne;

    private SalesReturnDocument salesReturnDocumentOne;
    private SalesReturnDocument salesReturnDocumentTwo;

    private SalesReturnDetail detailOne;
    private SalesReturnDetail detailTwo;
    private SalesReturnDetail detailThree;

    private LocalDateTime now = LocalDateTime.now();
    private final String itemNameOne = "sales return item name one";
    private final String itemNameTwo = "sales return item name two";
    private final String itemDescriptionOne = "sales return item description One";
    private final String itemDescriptionTwo = "sales return item description Two";
    private final String detailDescriptionOne = "sales return detail one description";
    private final String detailDescriptionTwo = "sales return detail two description";
    private final String detailDescriptionThree = "sales return detail three description";
    private final String documentDescriptionOne = "sales return document one description";

    @BeforeEach
    void init() {

        warehouseOne = new Warehouse("Warehouse one");
        warehouseOne.setId(1L);

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription(itemDescriptionOne);
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription(itemDescriptionTwo);
        itemTwo.setValuationType(ValuationType.AVERAGE);

    }

    @Test
    public void getTypeTest() {

        DocumentType result = salesReturnDocumentFactoryImpl.getType();

        assertEquals(DocumentType.SALES_RETURN, result);

    }

    @Test
    public void getNextIdTest() {

        when(salesReturnDocumentRepository.getNextId()).thenReturn(2L);

        String result = salesReturnDocumentFactoryImpl.getNextId();
        assertEquals("SR0000000002", result);

    }

    @Test
    public void createDocumentTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        salesReturnDocumentOne.setWarehouse(warehouseOne);

        when(salesReturnDocumentRepository.getNextId()).thenReturn(1L);

        SalesReturnDocument result = salesReturnDocumentFactoryImpl.createDocument(salesReturnDocumentOne);
        assertEquals("SR0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());

    }

    @Test
    public void updateDocumentTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(500));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(30));
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        
        SalesReturnDocument result = salesReturnDocumentFactoryImpl.updateDocument(salesReturnDocumentOne);
        
        assertEquals("SR0000000001", salesReturnDocumentOne.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(500), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(30), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
    }

    @Test
    public void createDetailTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        salesReturnDocumentOne.setWarehouse(warehouseOne);

        detailOne = new SalesReturnDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setSalesReturnDocument(salesReturnDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        SalesReturnDetail result = salesReturnDocumentFactoryImpl.createDetail(detailOne);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(null, result.getSalesReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

    }
    
    @Test
    public void updateDetailTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new SalesReturnDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setSalesReturnDocument(salesReturnDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));
        
        SalesReturnDetail result = salesReturnDocumentFactoryImpl.updateDetail(detailOne);
        
        assertEquals(1L, result.getId());
        assertEquals(1, detailOne.getLineNumber());
        assertEquals(null, result.getSalesReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
    }

    @Test
    public void updateDetailsTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new SalesReturnDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new SalesReturnDetail();
        detailTwo.setId(2L);
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new SalesReturnDetail();
        detailThree.setId(3L);
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));
        
        List<SalesReturnDetail> details = List.of(detailOne, detailTwo, detailThree);
        
        List<SalesReturnDetail> list = salesReturnDocumentFactoryImpl.updateDetails(salesReturnDocumentOne, details);
        
        assertEquals(3, list.size());
        
        SalesReturnDetail result = list.get(0);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
        result = list.get(1);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());
        
        result = list.get(2);
        
        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
        
    }
    
    @Test
    public void generateDetailsTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        salesReturnDocumentOne.setWarehouse(warehouseOne);

        detailOne = new SalesReturnDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new SalesReturnDetail();
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new SalesReturnDetail();
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));

        List<SalesReturnDetail> details = List.of(detailOne, detailTwo, detailThree);

        List<SalesReturnDetail> list = salesReturnDocumentFactoryImpl.generateDetails(salesReturnDocumentOne, details);

        assertEquals(3, list.size());

        SalesReturnDetail result = list.get(0);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

        result = list.get(1);

        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());

        result = list.get(2);

        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(salesReturnDocumentOne, result.getSalesReturnDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
    }

    @Test
    public void releaseTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription(documentDescriptionOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        salesReturnDocumentOne.setWarehouse(warehouseOne);

        SalesReturnDocument result = salesReturnDocumentFactoryImpl.release(salesReturnDocumentOne);

        assertEquals("SR0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(Utils.getAsBigDecimal(80), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(25), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());
    }
}
