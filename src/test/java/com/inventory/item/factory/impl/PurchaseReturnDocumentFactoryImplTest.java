/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.factory.impl;

import java.util.List;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.document.impl.PurchaseReturnDocumentImpl;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class PurchaseReturnDocumentFactoryImplTest {
    
    @Mock
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    @InjectMocks
    private PurchaseReturnDocumentImpl purchaseReturnDocumentFactoryImpl;

    private Item itemOne;
    private Item itemTwo;

    private Warehouse warehouseOne;

    private PurchaseReturnDocument purchaseReturnDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentTwo;

    private PurchaseReturnDetail detailOne;
    private PurchaseReturnDetail detailTwo;
    private PurchaseReturnDetail detailThree;

    private LocalDateTime now = LocalDateTime.now();
    private final String itemNameOne = "purchase return item name one";
    private final String itemNameTwo = "purchase return item name two";
    private final String itemDescriptionOne = "purchase return item description One";
    private final String itemDescriptionTwo = "purchase return item description Two";
    private final String detailDescriptionOne = "purchase return detail one description";
    private final String detailDescriptionTwo = "purchase return detail two description";
    private final String detailDescriptionThree = "purchase return detail three description";
    private final String documentDescriptionOne = "purchase return document one description";

    @BeforeEach
    void init() {

        warehouseOne = new Warehouse("Warehouse one");
        warehouseOne.setId(1L);

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription(itemDescriptionOne);
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription(itemDescriptionTwo);
        itemTwo.setValuationType(ValuationType.AVERAGE);

    }

    @Test
    public void getTypeTest() {

        DocumentType result = purchaseReturnDocumentFactoryImpl.getType();

        assertEquals(DocumentType.PURCHASE_RETURN, result);

    }

    @Test
    public void getNextIdTest() {

        when(purchaseReturnDocumentRepository.getNextId()).thenReturn(2L);

        String result = purchaseReturnDocumentFactoryImpl.getNextId();
        assertEquals("PR0000000002", result);

    }

    @Test
    public void createDocumentTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);

        when(purchaseReturnDocumentRepository.getNextId()).thenReturn(1L);

        PurchaseReturnDocument result = purchaseReturnDocumentFactoryImpl.createDocument(purchaseReturnDocumentOne);
        assertEquals("PR0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());

    }

    @Test
    public void updateDocumentTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(500));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(30));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        
        PurchaseReturnDocument result = purchaseReturnDocumentFactoryImpl.updateDocument(purchaseReturnDocumentOne);
        
        assertEquals("PR0000000001", purchaseReturnDocumentOne.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(500), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(30), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
    }

    @Test
    public void createDetailTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);

        detailOne = new PurchaseReturnDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        PurchaseReturnDetail result = purchaseReturnDocumentFactoryImpl.createDetail(detailOne);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(null, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

    }
    
    @Test
    public void updateDetailTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new PurchaseReturnDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));
        
        PurchaseReturnDetail result = purchaseReturnDocumentFactoryImpl.updateDetail(detailOne);
        
        assertEquals(1L, result.getId());
        assertEquals(1, detailOne.getLineNumber());
        assertEquals(null, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
    }

    @Test
    public void updateDetailsTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new PurchaseReturnDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new PurchaseReturnDetail();
        detailTwo.setId(2L);
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new PurchaseReturnDetail();
        detailThree.setId(3L);
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));
        
        List<PurchaseReturnDetail> details = List.of(detailOne, detailTwo, detailThree);
        
        List<PurchaseReturnDetail> list = purchaseReturnDocumentFactoryImpl.updateDetails(purchaseReturnDocumentOne, details);
        
        assertEquals(3, list.size());
        
        PurchaseReturnDetail result = list.get(0);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
        result = list.get(1);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());
        
        result = list.get(2);
        
        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
        
    }
    
    @Test
    public void generateDetailsTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);

        detailOne = new PurchaseReturnDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new PurchaseReturnDetail();
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new PurchaseReturnDetail();
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));

        List<PurchaseReturnDetail> details = List.of(detailOne, detailTwo, detailThree);

        List<PurchaseReturnDetail> list = purchaseReturnDocumentFactoryImpl.generateDetails(purchaseReturnDocumentOne, details);

        assertEquals(3, list.size());

        PurchaseReturnDetail result = list.get(0);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

        result = list.get(1);

        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());

        result = list.get(2);

        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(purchaseReturnDocumentOne, result.getPurchaseReturnDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
    }

    @Test
    public void releaseTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription(documentDescriptionOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);

        PurchaseReturnDocument result = purchaseReturnDocumentFactoryImpl.release(purchaseReturnDocumentOne);

        assertEquals("PR0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(Utils.getAsBigDecimal(80), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(25), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());
    }
}
