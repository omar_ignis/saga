/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.factory.impl;

import java.util.List;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.document.impl.InputDocumentImpl;
import com.inventory.item.repository.InputDocumentRepository;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class InputDocumentFactoryImplTest {

    @Mock
    private InputDocumentRepository inputDocumentRepository;

    @InjectMocks
    private InputDocumentImpl inputDocumentFactoryImpl;

    private Item itemOne;
    private Item itemTwo;

    private Warehouse warehouseOne;

    private InputDocument inputDocumentOne;
    private InputDocument inputDocumentTwo;

    private InputDetail detailOne;
    private InputDetail detailTwo;
    private InputDetail detailThree;

    private LocalDateTime now = LocalDateTime.now();
    private final String itemNameOne = "input item name one";
    private final String itemNameTwo = "input item name two";
    private final String itemDescriptionOne = "input item description One";
    private final String itemDescriptionTwo = "input item description Two";
    private final String detailDescriptionOne = "input detail one description";
    private final String detailDescriptionTwo = "input detail two description";
    private final String detailDescriptionThree = "input detail three description";
    private final String documentDescriptionOne = "input document one description";

    @BeforeEach
    void init() {

        warehouseOne = new Warehouse("Warehouse one");
        warehouseOne.setId(1L);

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription(itemDescriptionOne);
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription(itemDescriptionTwo);
        itemTwo.setValuationType(ValuationType.AVERAGE);

    }

    @Test
    public void getTypeTest() {

        DocumentType result = inputDocumentFactoryImpl.getType();

        assertEquals(DocumentType.INPUT, result);

    }

    @Test
    public void getNextIdTest() {

        when(inputDocumentRepository.getNextId()).thenReturn(2L);

        String result = inputDocumentFactoryImpl.getNextId();
        assertEquals("IN0000000002", result);

    }

    @Test
    public void createDocumentTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        inputDocumentOne.setWarehouse(warehouseOne);

        when(inputDocumentRepository.getNextId()).thenReturn(1L);

        InputDocument result = inputDocumentFactoryImpl.createDocument(inputDocumentOne);
        assertEquals("IN0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());

    }

    @Test
    public void updateDocumentTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(500));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(30));
        inputDocumentOne.setWarehouse(warehouseOne);
        
        InputDocument result = inputDocumentFactoryImpl.updateDocument(inputDocumentOne);
        
        assertEquals("IN0000000001", inputDocumentOne.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(500), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(30), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
    }

    @Test
    public void createDetailTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        inputDocumentOne.setWarehouse(warehouseOne);

        detailOne = new InputDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setInputDocument(inputDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        InputDetail result = inputDocumentFactoryImpl.createDetail(detailOne);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(null, result.getInputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

    }
    
    @Test
    public void updateDetailTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        inputDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new InputDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setInputDocument(inputDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));
        
        InputDetail result = inputDocumentFactoryImpl.updateDetail(detailOne);
        
        assertEquals(1L, result.getId());
        assertEquals(1, detailOne.getLineNumber());
        assertEquals(null, result.getInputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
    }

    @Test
    public void updateDetailsTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        inputDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new InputDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new InputDetail();
        detailTwo.setId(2L);
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new InputDetail();
        detailThree.setId(3L);
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));
        
        List<InputDetail> details = List.of(detailOne, detailTwo, detailThree);
        
        List<InputDetail> list = inputDocumentFactoryImpl.updateDetails(inputDocumentOne, details);
        
        assertEquals(3, list.size());
        
        InputDetail result = list.get(0);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
        result = list.get(1);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());
        
        result = list.get(2);
        
        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
        
    }
    
    @Test
    public void generateDetailsTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        inputDocumentOne.setWarehouse(warehouseOne);

        detailOne = new InputDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new InputDetail();
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new InputDetail();
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));

        List<InputDetail> details = List.of(detailOne, detailTwo, detailThree);

        List<InputDetail> list = inputDocumentFactoryImpl.generateDetails(inputDocumentOne, details);

        assertEquals(3, list.size());

        InputDetail result = list.get(0);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

        result = list.get(1);

        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());

        result = list.get(2);

        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(inputDocumentOne, result.getInputDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
    }

    @Test
    public void releaseTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription(documentDescriptionOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        inputDocumentOne.setWarehouse(warehouseOne);

        InputDocument result = inputDocumentFactoryImpl.release(inputDocumentOne);

        assertEquals("IN0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(Utils.getAsBigDecimal(80), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(25), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());
    }
    
    
}
