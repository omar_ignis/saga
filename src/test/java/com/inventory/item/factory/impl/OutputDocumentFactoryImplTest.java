/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.factory.impl;

import java.util.List;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.document.impl.OutputDocumentImpl;
import com.inventory.item.repository.OutputDocumentRepository;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class OutputDocumentFactoryImplTest {

    @Mock
    private OutputDocumentRepository outputDocumentRepository;

    @InjectMocks
    private OutputDocumentImpl outputDocumentFactoryImpl;

    private Item itemOne;
    private Item itemTwo;

    private Warehouse warehouseOne;

    private OutputDocument outputDocumentOne;
    private OutputDocument outputDocumentTwo;

    private OutputDetail detailOne;
    private OutputDetail detailTwo;
    private OutputDetail detailThree;

    private LocalDateTime now = LocalDateTime.now();
    private final String itemNameOne = "output item name one";
    private final String itemNameTwo = "output item name two";
    private final String itemDescriptionOne = "output item description One";
    private final String itemDescriptionTwo = "output item description Two";
    private final String detailDescriptionOne = "output detail one description";
    private final String detailDescriptionTwo = "output detail two description";
    private final String detailDescriptionThree = "output detail three description";
    private final String documentDescriptionOne = "output document one description";

    @BeforeEach
    void init() {

        warehouseOne = new Warehouse("Warehouse one");
        warehouseOne.setId(1L);

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription(itemDescriptionOne);
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription(itemDescriptionTwo);
        itemTwo.setValuationType(ValuationType.AVERAGE);

    }

    @Test
    public void getTypeTest() {

        DocumentType result = outputDocumentFactoryImpl.getType();

        assertEquals(DocumentType.OUTPUT, result);

    }

    @Test
    public void getNextIdTest() {

        when(outputDocumentRepository.getNextId()).thenReturn(2L);

        String result = outputDocumentFactoryImpl.getNextId();
        assertEquals("OU0000000002", result);

    }

    @Test
    public void createDocumentTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        outputDocumentOne.setWarehouse(warehouseOne);

        when(outputDocumentRepository.getNextId()).thenReturn(1L);

        OutputDocument result = outputDocumentFactoryImpl.createDocument(outputDocumentOne);
        assertEquals("OU0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());

    }

    @Test
    public void updateDocumentTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(500));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(30));
        outputDocumentOne.setWarehouse(warehouseOne);
        
        OutputDocument result = outputDocumentFactoryImpl.updateDocument(outputDocumentOne);
        
        assertEquals("OU0000000001", outputDocumentOne.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(500), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(30), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
    }

    @Test
    public void createDetailTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        outputDocumentOne.setWarehouse(warehouseOne);

        detailOne = new OutputDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setOutputDocument(outputDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        OutputDetail result = outputDocumentFactoryImpl.createDetail(detailOne);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(null, result.getOutputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

    }
    
    @Test
    public void updateDetailTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        outputDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new OutputDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setOutputDocument(outputDocumentOne);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));
        
        OutputDetail result = outputDocumentFactoryImpl.updateDetail(detailOne);
        
        assertEquals(1L, result.getId());
        assertEquals(1, detailOne.getLineNumber());
        assertEquals(null, result.getOutputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
    }

    @Test
    public void updateDetailsTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(200));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        outputDocumentOne.setWarehouse(warehouseOne);
        
        detailOne = new OutputDetail();
        detailOne.setId(1L);
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new OutputDetail();
        detailTwo.setId(2L);
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new OutputDetail();
        detailThree.setId(3L);
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));
        
        List<OutputDetail> details = List.of(detailOne, detailTwo, detailThree);
        
        List<OutputDetail> list = outputDocumentFactoryImpl.updateDetails(outputDocumentOne, details);
        
        assertEquals(3, list.size());
        
        OutputDetail result = list.get(0);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());
        
        result = list.get(1);
        
        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());
        
        result = list.get(2);
        
        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
        
    }
    
    @Test
    public void generateDetailsTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        outputDocumentOne.setWarehouse(warehouseOne);

        detailOne = new OutputDetail();
        detailOne.setItem(itemOne);
        detailOne.setLineNumber(1);
        detailOne.setDescription(detailDescriptionOne);
        detailOne.setQuantity(Utils.getAsBigDecimal(5));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(2));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(10));

        detailTwo = new OutputDetail();
        detailTwo.setItem(itemOne);
        detailTwo.setLineNumber(2);
        detailTwo.setDescription(detailDescriptionTwo);
        detailTwo.setQuantity(Utils.getAsBigDecimal(10));
        detailTwo.setUnitPrice(Utils.getAsBigDecimal(2));
        detailTwo.setTotalPrice(Utils.getAsBigDecimal(20));

        detailThree = new OutputDetail();
        detailThree.setItem(itemTwo);
        detailThree.setLineNumber(3);
        detailThree.setDescription(detailDescriptionThree);
        detailThree.setQuantity(Utils.getAsBigDecimal(10));
        detailThree.setUnitPrice(Utils.getAsBigDecimal(5));
        detailThree.setTotalPrice(Utils.getAsBigDecimal(50));

        List<OutputDetail> details = List.of(detailOne, detailTwo, detailThree);

        List<OutputDetail> list = outputDocumentFactoryImpl.generateDetails(outputDocumentOne, details);

        assertEquals(3, list.size());

        OutputDetail result = list.get(0);

        assertEquals(itemOne, result.getItem());
        assertEquals(1, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionOne, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(5), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(10), result.getTotalPrice());

        result = list.get(1);

        assertEquals(itemOne, result.getItem());
        assertEquals(2, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionTwo, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(2), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(20), result.getTotalPrice());

        result = list.get(2);

        assertEquals(itemTwo, result.getItem());
        assertEquals(3, result.getLineNumber());
        assertEquals(outputDocumentOne, result.getOutputDocument());
        assertEquals(detailDescriptionThree, result.getDescription());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(50), result.getTotalPrice());
    }

    @Test
    public void releaseTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription(documentDescriptionOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(80));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(25));
        outputDocumentOne.setWarehouse(warehouseOne);

        OutputDocument result = outputDocumentFactoryImpl.release(outputDocumentOne);

        assertEquals("OU0000000001", result.getId());
        assertEquals(now, result.getDate());
        assertEquals(documentDescriptionOne, result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(Utils.getAsBigDecimal(80), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(25), result.getTotalQuantity());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(1L, result.getWarehouse().getId());
    }
    
}
