/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

import com.inventory.item.enums.DocumentType;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author ignis
 */
public class UtilTest {

    @Test
    public void testReadDataFromBarcode() {
        
        String inputBarcode = "00000000001000300003";

        DocumentType resultDocType = Util.getDocTypeFromBarcode(inputBarcode);

        assertEquals(DocumentType.INPUT, resultDocType);
    }

    @Test
    public void testWriteBarcodeFromData() {
        String docType = Constants.INPUT;
        String refNbr = Constants.INPUT_PREFIX + "00145789";
        int lineNbr = 12;
        Long itemId = 3456L;

        String result = Util.buildBarcode(docType, refNbr, lineNbr, itemId);
        assertEquals(19, result.length());
        assertEquals("RCP0014578900123456", result);
    }
}
