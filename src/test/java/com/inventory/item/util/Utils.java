/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

import java.util.Map;
import java.util.List;
import java.math.BigDecimal;
import java.math.RoundingMode;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.security.entity.Permission;

import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.util.MultiValueMap;
import org.springframework.util.LinkedMultiValueMap;

import org.apache.commons.codec.binary.Base64;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.test.web.client.TestRestTemplate;


/**
 * @author ignis
 */
public class Utils {
    
    private final static String path = "/inventory-item/oauth/token"; 
    private final static String credentials = "InventoryStudio:secreto";
    
    public static String createURLWithPort(int port, String uri) {
        return "http://localhost:" + port + uri;
    }

    public static BigDecimal getAsBigDecimal(float value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_DOWN);
    }

    public static BigDecimal getAsBigDecimal(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_DOWN);
    }

    public static String buildBarcode(DocumentType type, String documentId, int lineNumber, long itemId) {

        String lineNbrFormatted = String.format("%04d", lineNumber);
        String itemIdFormatted = String.format("%05d", itemId);
        String refNbrFormatted = documentId.substring(Constants.DOCUMENT_TYPE_LENGTH);
        return type.ordinal() + refNbrFormatted + lineNbrFormatted + itemIdFormatted;
    }

    public static Permission getPermissionByResource(String name, List<Permission> list) {

        var result = list.stream()
                .filter(permission -> permission.getResource().getResourceName().equals(name))
                .findFirst()
                .orElse(null);

        return result;
    }
    
    public static String getToken(TestRestTemplate restTemplate, int port, String username, String password) throws Exception {
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        
        String base64Credentials = new String(Base64.encodeBase64(credentials.getBytes()));
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Authorization", "Basic " + base64Credentials);
    
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", username);
        body.add("password", password);
        body.add("grant_type", "password");
        body.add("scopes", "write read");
        
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(body, headers);
        
        ResponseEntity<String> response = restTemplate
                .exchange(createURLWithPort(port, path),
                        HttpMethod.POST, entity, String.class);

        Map<String, String> json = mapper.readValue(response.getBody(), Map.class);
        
        String token = json.get("access_token");
        
        return token;
    }
    
    public static HttpHeaders getHeaders(HttpHeaders headers, String token){
        
        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        return headers;
    }
}
