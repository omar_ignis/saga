/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.validation;

import java.time.LocalDateTime;


import com.inventory.item.util.Utils;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.exception.NotEnoughInventoryException;
import com.inventory.item.validation.impl.WarehouseExitValidationImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class WarehouseExitValidationImplTest {
    
    @Mock
    private WarehouseRepository warehouseRepository;
    
    @Mock
    private ItemSummaryRepository itemSummaryRepository;
    
    @InjectMocks
    private WarehouseExitValidationImpl warehouseExitValidationImpl;
    
    private Item itemOne;
    private Warehouse warehouseOne;
    private OutputDetail detailOne;
    private ItemSummary itemSummaryOne; 
    
    @BeforeEach
    void init() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        warehouseOne.setUsed(true);
    }
    
    @Test
    public void validateItemSummaryTest() throws Exception{
        
        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(1000));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());
        
        when(itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemOne))
                .thenReturn(itemSummaryOne);
        
        ItemSummary result = warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemOne);
        
        assertEquals(itemOne.getId(), result.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), result.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(100), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(10), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1000), result.getTotalPrice());
        
    }
    
    @Test
    public void validateItemSummaryWithExceptionTest() throws Exception{
        
        when(itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemOne))
                .thenReturn(null);
        
        Exception exception = assertThrows(Exception.class, () -> {
            warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemOne);
        });
        
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("no se encuentra en el almacen"));
    }
    
    @Test
    public void validateInventoryBeforeUpdateTest() {

        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(1000));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        detailOne = new OutputDetail();
        detailOne.setId(1L);
        detailOne.setLineNumber(1);
        detailOne.setItem(itemOne);
        detailOne.setDescription("detail description");
        detailOne.setQuantity(Utils.getAsBigDecimal(50));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(10));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(500));

        assertDoesNotThrow(() -> {
            warehouseExitValidationImpl.validateInventoryBeforeUpdate(itemSummaryOne, detailOne);
        });

    }
    
    @Test
    public void validateInventoryBeforeUpdateWithExceptionTest() {
        
        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(30));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(300));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());
        
        detailOne = new OutputDetail();
        detailOne.setId(1L);
        detailOne.setLineNumber(1);
        detailOne.setItem(itemOne);
        detailOne.setDescription("detail description");
        detailOne.setQuantity(Utils.getAsBigDecimal(50));
        detailOne.setUnitPrice(Utils.getAsBigDecimal(10));
        detailOne.setTotalPrice(Utils.getAsBigDecimal(500));
        
        Exception exception = assertThrows(NotEnoughInventoryException.class, () -> {
             warehouseExitValidationImpl.validateInventoryBeforeUpdate(itemSummaryOne, detailOne);
        });
        
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("No hay suficiente inventario para el articulo"));
        
    }
}
