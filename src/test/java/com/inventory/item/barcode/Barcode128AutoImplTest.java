/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;
import java.util.ArrayList;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.model.RowSheet;
import com.inventory.item.model.RowOD5160;
import com.inventory.item.model.BarcodeLabel;
import com.inventory.item.barcode.impl.Barcode128AutoImpl;
import com.inventory.item.barcode.sheet.factory.SheetFactory;
import com.inventory.item.barcode.sheet.impl.OD5160SheetImpl;

import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class Barcode128AutoImplTest {

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;

    private InputDocument inputDocumentOne;

    @Mock
    private OD5160SheetImpl oD5160SheetImpl;

    @Mock
    private SheetFactory sheetFactoryService;

    @InjectMocks
    private Barcode128AutoImpl barcode128AutoImpl;

    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

    }

    @Test
    public void getTypeTest() {
        BarcodeType result = barcode128AutoImpl.getType();

        assertEquals(BarcodeType.CODE128, result);
    }

    @Test
    public void buildBarcodeTest() {

        String result = barcode128AutoImpl.buildBarcode(DocumentType.INPUT, "IN0000000121", 5, 14L);

        assertEquals(20, result.length());
        assertEquals("00000000121000500014", result);
    }

    @Test
    public void generateBarcodeLabelTest() {

        inputDetailOne.setQuantity(Utils.getAsBigDecimal(5));
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(3));
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(2));

        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<BarcodeLabel> result = barcode128AutoImpl.generateBarcodeLabel(inputDocumentOne, list);

        assertEquals(3, result.size());

        BarcodeLabel barcodeLabel = result.get(0);
        assertEquals("00000000001000100001", barcodeLabel.getBarcode());
        assertEquals("Item Eins", barcodeLabel.getLabel());
        assertEquals(5, barcodeLabel.getQuantity());

        barcodeLabel = result.get(1);
        assertEquals("00000000001000200002", barcodeLabel.getBarcode());
        assertEquals("Item Zwei", barcodeLabel.getLabel());
        assertEquals(3, barcodeLabel.getQuantity());

        barcodeLabel = result.get(2);
        assertEquals("00000000001000300003", barcodeLabel.getBarcode());
        assertEquals("Item Drei", barcodeLabel.getLabel());
        assertEquals(2, barcodeLabel.getQuantity());

    }

    @Test
    public void generateFullSheetLabelTest() {

        inputDetailOne.setQuantity(Utils.getAsBigDecimal(5));
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(3));
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(2));

        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<BarcodeLabel> barcodeLabelList = barcode128AutoImpl.generateBarcodeLabel(inputDocumentOne, list);

        RowSheet rowSheetOne = new RowOD5160();
        rowSheetOne.fillRow("00000000001000100001", "Item Eins");

        RowSheet rowSheetTwo = new RowOD5160();
        rowSheetTwo.setBarcode(0, "00000000001000100001");
        rowSheetTwo.setBarcode(1, "00000000001000100001");
        rowSheetTwo.setLabel(0, "Item Eins");
        rowSheetTwo.setLabel(1, "Item Eins");
        rowSheetTwo.setBarcode(2, "00000000001000200002");
        rowSheetTwo.setLabel(2, "Item Zwei");

        RowSheet rowSheetThree = new RowOD5160();
        rowSheetThree.setBarcode(0, "00000000001000200002");
        rowSheetThree.setBarcode(1, "00000000001000200002");
        rowSheetThree.setLabel(0, "Item Zwei");
        rowSheetThree.setLabel(1, "Item Zwei");
        rowSheetThree.setBarcode(2, "00000000001000300003");
        rowSheetThree.setLabel(2, "Item Drei");

        RowSheet rowSheetFour = new RowOD5160();
        rowSheetFour.setBarcode(0, "00000000001000300003");
        rowSheetFour.setBarcode(1, "00000000001000300003");
        rowSheetFour.setLabel(0, "Item Drei");
        rowSheetFour.setLabel(1, "Item Drei");

        List<RowSheet> rowList = List.of(rowSheetOne, rowSheetTwo, rowSheetThree, rowSheetFour);

        when(sheetFactoryService.getSheetImpl(SheetType.OD5160)).thenReturn(oD5160SheetImpl);
        when(oD5160SheetImpl.createFullSheet(barcodeLabelList)).thenReturn(rowList);
        List<RowSheet> result = barcode128AutoImpl.generateFullSheetLabel(barcodeLabelList, SheetType.OD5160);
        assertEquals(4, result.size());

        RowOD5160 rowSheet = (RowOD5160) result.get(0);
        assertEquals("00000000001000100001", rowSheet.getBarcodeOne());
        assertEquals("00000000001000100001", rowSheet.getBarcodeTwo());
        assertEquals("00000000001000100001", rowSheet.getBarcodeThree());
        assertEquals("Item Eins", rowSheet.getLabelOne());
        assertEquals("Item Eins", rowSheet.getLabelTwo());
        assertEquals("Item Eins", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(1);
        assertEquals("00000000001000100001", rowSheet.getBarcodeOne());
        assertEquals("00000000001000100001", rowSheet.getBarcodeTwo());
        assertEquals("00000000001000200002", rowSheet.getBarcodeThree());
        assertEquals("Item Eins", rowSheet.getLabelOne());
        assertEquals("Item Eins", rowSheet.getLabelTwo());
        assertEquals("Item Zwei", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(2);
        assertEquals("00000000001000200002", rowSheet.getBarcodeOne());
        assertEquals("00000000001000200002", rowSheet.getBarcodeTwo());
        assertEquals("00000000001000300003", rowSheet.getBarcodeThree());
        assertEquals("Item Zwei", rowSheet.getLabelOne());
        assertEquals("Item Zwei", rowSheet.getLabelTwo());
        assertEquals("Item Drei", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(3);
        assertEquals("00000000001000300003", rowSheet.getBarcodeOne());
        assertEquals("00000000001000300003", rowSheet.getBarcodeTwo());
        assertEquals("Item Drei", rowSheet.getLabelOne());
        assertEquals("Item Drei", rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());
    }

    @Test
    public void generatePartialSheetLabelTest() {

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(5));
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(3));
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(2));

        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<Integer> positionList = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);
        List<BarcodeLabel> barcodeLabelList = barcode128AutoImpl.generateBarcodeLabel(inputDocumentOne, list);
        when(sheetFactoryService.getSheetImpl(SheetType.OD5160)).thenReturn(oD5160SheetImpl);

        List<RowSheet> rowSheetList = new ArrayList<>();
        RowSheet rowSheet = new RowOD5160();
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000100001");
        rowSheet.setLabel(2, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000200002");
        rowSheet.setLabel(0, "Item Zwei");
        rowSheet.setBarcode(1, "00000000001000200002");
        rowSheet.setLabel(1, "Item Zwei");
        rowSheet.setBarcode(2, "00000000001000200002");
        rowSheet.setLabel(2, "Item Zwei");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000300003");
        rowSheet.setLabel(0, "Item Drei");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000300003");
        rowSheet.setLabel(2, "Item Drei");
        rowSheetList.add(rowSheet);

        when(oD5160SheetImpl.createCustomSheet(barcodeLabelList, positionList)).thenReturn(rowSheetList);

        List<RowSheet> result = barcode128AutoImpl.generatePartialSheetLabel(barcodeLabelList, positionList, SheetType.OD5160);
        assertEquals(20, result.size());

        RowOD5160 rowSheetOne = (RowOD5160) result.get(0);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(1);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(2);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeThree());
        assertEquals("Item Eins", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(3);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(4);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(5);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(6);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(7);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(8);
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeOne());
        assertEquals("Item Zwei", rowSheetOne.getLabelOne());
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Zwei", rowSheetOne.getLabelTwo());
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeThree());
        assertEquals("Item Zwei", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(9);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(10);
        assertEquals("00000000001000300003", rowSheetOne.getBarcodeOne());
        assertEquals("Item Drei", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(11);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(12);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(13);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(14);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(15);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(16);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(17);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(18);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(19);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000300003", rowSheetOne.getBarcodeThree());
        assertEquals("Item Drei", rowSheetOne.getLabelThree());
    }

    @Test
    public void readDocumenTypeTest() {

        String barcode = "00000000121000500014";
        DocumentType result = barcode128AutoImpl.readDocumenType(barcode);
        assertEquals(DocumentType.INPUT, result);

        barcode = "10000000121000500014";
        result = barcode128AutoImpl.readDocumenType(barcode);
        assertEquals(DocumentType.OUTPUT, result);

        barcode = "20000000121000500014";
        result = barcode128AutoImpl.readDocumenType(barcode);
        assertEquals(DocumentType.PURCHASE_RETURN, result);

        barcode = "30000000121000500014";
        result = barcode128AutoImpl.readDocumenType(barcode);
        assertEquals(DocumentType.SALES_RETURN, result);
    }

    @Test
    public void readDocumentIdTest() {

        String barcode = "10000000121000500014";
        String result = barcode128AutoImpl.readDocumentId(barcode);
        assertEquals(10, result.length());
        assertEquals("0000000121", result);
    }

    @Test
    public void readLineNumberTest() {

        String barcode = "10000000121000500014";
        int result = barcode128AutoImpl.readLineNumber(barcode);
        assertEquals(5, result);
    }

    @Test
    public void readItemIdTest() {

        String barcode = "10000000121000500014";
        long result = barcode128AutoImpl.readItemId(barcode);
        assertEquals(14, result);

    }
}
