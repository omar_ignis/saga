/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.sheet;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.RowOD5160;
import com.inventory.item.model.BarcodeLabel;

import com.inventory.item.enums.SheetType;
import com.inventory.item.barcode.sheet.impl.OD5160SheetImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class OD5160SheetImplTest {

    @InjectMocks
    private OD5160SheetImpl oD5160SheetImpl;

    @Test
    public void getTypeTest() {

        SheetType result = oD5160SheetImpl.getType();

        assertEquals(SheetType.OD5160, result);
    }

    @Test
    public void fillRowsTest() {

        List<RowSheet> accumulator = new ArrayList<>();
        oD5160SheetImpl.fillRows(3, "00000000001000100001", "itemOne", accumulator);
        RowOD5160 result = (RowOD5160) accumulator.get(0);
        assertEquals(3, accumulator.size());
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100001", result.getBarcodeTwo());
        assertEquals("itemOne", result.getLabelTwo());
        assertEquals("00000000001000100001", result.getBarcodeThree());
        assertEquals("itemOne", result.getLabelThree());

    }

    @Test
    public void fillPendingRowTest() {

        RowOD5160 result = (RowOD5160) oD5160SheetImpl.fillPendingRow(1, "00000000001000100001", "itemOne");
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());

        result = (RowOD5160) oD5160SheetImpl.fillPendingRow(2, "00000000001000100002", "itemTwo");
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
    }

    @Test
    public void fillTest() {

        RowOD5160 rowSheet = null;
        List<RowSheet> accumulator = new ArrayList<>();
        BarcodeLabel barcodeLabel = new BarcodeLabel("itemOne", "00000000001000100001", 30);
        RowOD5160 result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNull(result);
        assertEquals(10, accumulator.size());

        rowSheet = null;
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemTwo", "00000000001000100002", 9);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNull(result);
        assertEquals(3, accumulator.size());

        //El row incompleto se devuelve no se agrgar a la lista
        rowSheet = null;
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemThree", "00000000001000100003", 8);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNotNull(result);
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100003", result.getBarcodeTwo());
        assertEquals("itemThree", result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());
        assertEquals(2, accumulator.size());

        rowSheet = null;
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemFour", "00000000001000100004", 7);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNotNull(result);
        assertEquals("00000000001000100004", result.getBarcodeOne());
        assertEquals("itemFour", result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());
        assertEquals(2, accumulator.size());

        //Here there are three completed rows addes to the accumulator
        //and one uncompleted but it was not added to accumulator
        rowSheet = new RowOD5160();
        rowSheet.setBarcodeOne("00000000001000100001");
        rowSheet.setLabelOne("itemOne");
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemTwo", "00000000001000100002", 9);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNotNull(result);
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());
        assertEquals(3, accumulator.size());

        //Here there are three completed rows addes to the accumulator
        //and one uncompleted but it was not added to accumulator
        rowSheet = new RowOD5160();
        rowSheet.setBarcodeOne("00000000001000100001");
        rowSheet.setLabelOne("itemOne");
        rowSheet.setBarcodeTwo("00000000001000100001");
        rowSheet.setLabelTwo("itemOne");
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemTwo", "00000000001000100002", 9);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNotNull(result);
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());
        assertEquals(3, accumulator.size());

        //Here all rows are completed
        rowSheet = new RowOD5160();
        rowSheet.setBarcodeOne("00000000001000100001");
        rowSheet.setLabelOne("itemOne");
        rowSheet.setBarcodeTwo("00000000001000100001");
        rowSheet.setLabelTwo("itemOne");
        accumulator = new ArrayList<>();
        barcodeLabel = new BarcodeLabel("itemTwo", "00000000001000100002", 10);
        result = (RowOD5160) oD5160SheetImpl.fill(rowSheet, barcodeLabel, accumulator);
        assertNull(result);
        assertEquals(4, accumulator.size());
    }

    @Test
    public void createSheetCompletedRowTest() {

        BarcodeLabel barcodeLabelOne = new BarcodeLabel("itemOne", "00000000001000100001", 3);
        BarcodeLabel barcodeLabelTwo = new BarcodeLabel("itemTwo", "00000000001000100002", 3);
        BarcodeLabel barcodeLabelThree = new BarcodeLabel("itemThree", "00000000001000100003", 3);

        List<BarcodeLabel> list = List.of(barcodeLabelOne, barcodeLabelTwo, barcodeLabelThree);

        List<RowSheet> resultList = oD5160SheetImpl.createFullSheet(list);
        assertEquals(3, resultList.size());

        RowOD5160 result = (RowOD5160) resultList.get(0);
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100001", result.getBarcodeTwo());
        assertEquals("itemOne", result.getLabelTwo());
        assertEquals("00000000001000100001", result.getBarcodeThree());
        assertEquals("itemOne", result.getLabelThree());

        result = (RowOD5160) resultList.get(1);
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        result = (RowOD5160) resultList.get(2);
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100003", result.getBarcodeTwo());
        assertEquals("itemThree", result.getLabelTwo());
        assertEquals("00000000001000100003", result.getBarcodeThree());
        assertEquals("itemThree", result.getLabelThree());

        barcodeLabelOne = new BarcodeLabel("itemOne", "00000000001000100001", 1);
        barcodeLabelTwo = new BarcodeLabel("itemTwo", "00000000001000100002", 1);
        barcodeLabelThree = new BarcodeLabel("itemThree", "00000000001000100003", 1);

        list = List.of(barcodeLabelOne, barcodeLabelTwo, barcodeLabelThree);

        resultList = oD5160SheetImpl.createFullSheet(list);
        assertEquals(1, resultList.size());

        result = (RowOD5160) resultList.get(0);
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertEquals("00000000001000100003", result.getBarcodeThree());
        assertEquals("itemThree", result.getLabelThree());

        barcodeLabelOne = new BarcodeLabel("itemOne", "00000000001000100001", 2);
        barcodeLabelTwo = new BarcodeLabel("itemTwo", "00000000001000100002", 1);
        barcodeLabelThree = new BarcodeLabel("itemThree", "00000000001000100003", 3);

        list = List.of(barcodeLabelOne, barcodeLabelTwo, barcodeLabelThree);

        resultList = oD5160SheetImpl.createFullSheet(list);
        assertEquals(2, resultList.size());

        result = (RowOD5160) resultList.get(0);
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100001", result.getBarcodeTwo());
        assertEquals("itemOne", result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        result = (RowOD5160) resultList.get(1);
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100003", result.getBarcodeTwo());
        assertEquals("itemThree", result.getLabelTwo());
        assertEquals("00000000001000100003", result.getBarcodeThree());
        assertEquals("itemThree", result.getLabelThree());

    }

    @Test
    public void createSheetUncompletedRowTest() {

        BarcodeLabel barcodeLabelOne = new BarcodeLabel("itemOne", "00000000001000100001", 2);
        BarcodeLabel barcodeLabelTwo = new BarcodeLabel("itemTwo", "00000000001000100002", 3);
        BarcodeLabel barcodeLabelThree = new BarcodeLabel("itemThree", "00000000001000100003", 2);
        BarcodeLabel barcodeLabelFour = new BarcodeLabel("itemFour", "00000000001000100004", 4);

        List<BarcodeLabel> list = List.of(barcodeLabelOne, barcodeLabelTwo, barcodeLabelThree, barcodeLabelFour);

        List<RowSheet> resultList = oD5160SheetImpl.createFullSheet(list);
        assertEquals(4, resultList.size());

        RowOD5160 result = (RowOD5160) resultList.get(0);
        result = (RowOD5160) resultList.get(0);
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100001", result.getBarcodeTwo());
        assertEquals("itemOne", result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        result = (RowOD5160) resultList.get(1);
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertEquals("00000000001000100003", result.getBarcodeThree());
        assertEquals("itemThree", result.getLabelThree());

        result = (RowOD5160) resultList.get(2);
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100004", result.getBarcodeTwo());
        assertEquals("itemFour", result.getLabelTwo());
        assertEquals("00000000001000100004", result.getBarcodeThree());
        assertEquals("itemFour", result.getLabelThree());

        result = (RowOD5160) resultList.get(3);
        assertEquals("00000000001000100004", result.getBarcodeOne());
        assertEquals("itemFour", result.getLabelOne());
        assertEquals("00000000001000100004", result.getBarcodeTwo());
        assertEquals("itemFour", result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());

        barcodeLabelOne = new BarcodeLabel("itemOne", "00000000001000100001", 1);
        barcodeLabelTwo = new BarcodeLabel("itemTwo", "00000000001000100002", 5);
        barcodeLabelThree = new BarcodeLabel("itemThree", "00000000001000100003", 2);
        barcodeLabelFour = new BarcodeLabel("itemFour", "00000000001000100004", 2);

        list = List.of(barcodeLabelOne, barcodeLabelTwo, barcodeLabelThree, barcodeLabelFour);

        resultList = oD5160SheetImpl.createFullSheet(list);
        assertEquals(4, resultList.size());

        result = (RowOD5160) resultList.get(0);
        assertEquals("00000000001000100001", result.getBarcodeOne());
        assertEquals("itemOne", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        result = (RowOD5160) resultList.get(1);
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertEquals("00000000001000100002", result.getBarcodeTwo());
        assertEquals("itemTwo", result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        result = (RowOD5160) resultList.get(2);
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100003", result.getBarcodeTwo());
        assertEquals("itemThree", result.getLabelTwo());
        assertEquals("00000000001000100004", result.getBarcodeThree());
        assertEquals("itemFour", result.getLabelThree());

        result = (RowOD5160) resultList.get(3);
        assertEquals("00000000001000100004", result.getBarcodeOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());
    }

    @Test
    public void getRowPositionMapTest() {

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        List<Integer> list = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);
        Map<Integer, List<Integer>> map = oD5160SheetImpl.getRowPositionMap(list);
        assertEquals(7, map.size());

        List<Integer> result = map.get(1);
        assertEquals(1, result.size());
        assertEquals(1, result.get(0));

        result = map.get(3);
        assertEquals(1, result.size());
        assertEquals(2, result.get(0));

        result = map.get(4);
        assertEquals(1, result.size());
        assertEquals(0, result.get(0));

        result = map.get(6);
        assertEquals(2, result.size());
        assertEquals(0, result.get(0));
        assertEquals(1, result.get(1));

        result = map.get(9);
        assertEquals(3, result.size());
        assertEquals(0, result.get(0));
        assertEquals(1, result.get(1));
        assertEquals(2, result.get(2));

        result = map.get(11);
        assertEquals(1, result.size());
        assertEquals(0, result.get(0));

        result = map.get(20);
        assertEquals(1, result.size());
        assertEquals(2, result.get(0));
    }

    @Test
    public void customFillTest() {

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        List<Integer> list = List.of(1);
        BarcodeLabel barcodeLabelOne = new BarcodeLabel();
        barcodeLabelOne.setBarcode("00000000001000100001");
        barcodeLabelOne.setLabel("itemOne");
        barcodeLabelOne.setQuantity(1);

        BarcodeLabel barcodeLabelTwo = new BarcodeLabel();
        barcodeLabelTwo.setBarcode("00000000001000100002");
        barcodeLabelTwo.setLabel("itemTwo");
        barcodeLabelTwo.setQuantity(2);

        BarcodeLabel barcodeLabelThree = new BarcodeLabel();
        barcodeLabelThree.setBarcode("00000000001000100003");
        barcodeLabelThree.setLabel("itemThree");
        barcodeLabelThree.setQuantity(2);

        BarcodeLabel barcodeLabelFour = new BarcodeLabel();
        barcodeLabelFour.setBarcode("00000000001000100004");
        barcodeLabelFour.setLabel("itemFour");
        barcodeLabelFour.setQuantity(5);

        List<BarcodeLabel> barcodeLabelList = new ArrayList();
        barcodeLabelList.add(barcodeLabelOne);
        barcodeLabelList.add(barcodeLabelTwo);
        barcodeLabelList.add(barcodeLabelThree);
        barcodeLabelList.add(barcodeLabelFour);

        RowOD5160 result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(3, barcodeLabelList.size());
        assertNull(result.getBarcodeOne());
        assertNull(result.getLabelOne());
        assertEquals("00000000001000100001", result.getBarcodeTwo());
        assertEquals("itemOne", result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());

        list = List.of(2);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(3, barcodeLabelList.size());
        assertNull(result.getBarcodeOne());
        assertNull(result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertEquals("00000000001000100002", result.getBarcodeThree());
        assertEquals("itemTwo", result.getLabelThree());

        list = List.of(0);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(2, barcodeLabelList.size());
        assertEquals("00000000001000100002", result.getBarcodeOne());
        assertEquals("itemTwo", result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());

        list = List.of(0, 1);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(1, barcodeLabelList.size());
        assertEquals("00000000001000100003", result.getBarcodeOne());
        assertEquals("itemThree", result.getLabelOne());
        assertEquals("00000000001000100003", result.getBarcodeTwo());
        assertEquals("itemThree", result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());

        list = List.of(0, 1, 2);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(1, barcodeLabelList.size());
        assertEquals("00000000001000100004", result.getBarcodeOne());
        assertEquals("itemFour", result.getLabelOne());
        assertEquals("00000000001000100004", result.getBarcodeTwo());
        assertEquals("itemFour", result.getLabelTwo());
        assertEquals("00000000001000100004", result.getBarcodeThree());
        assertEquals("itemFour", result.getLabelThree());

        list = List.of(0);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(1, barcodeLabelList.size());
        assertEquals("00000000001000100004", result.getBarcodeOne());
        assertEquals("itemFour", result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertNull(result.getBarcodeThree());
        assertNull(result.getLabelThree());

        list = List.of(2);
        result = (RowOD5160) oD5160SheetImpl.fillCustomRow(barcodeLabelList, list);
        assertEquals(0, barcodeLabelList.size());
        assertNull(result.getBarcodeOne());
        assertNull(result.getLabelOne());
        assertNull(result.getBarcodeTwo());
        assertNull(result.getLabelTwo());
        assertEquals("00000000001000100004", result.getBarcodeThree());
        assertEquals("itemFour", result.getLabelThree());
    }

    @Test
    public void getTotalPagesTest() {

        List<Integer> list = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);
        int result = oD5160SheetImpl.getTotalRows(list);
        assertEquals(20, result);

        list = List.of(24, 25, 26, 30, 59, 65);
        result = oD5160SheetImpl.getTotalRows(list);
        assertEquals(30, result);
    }

    @Test
    public void createCustomSheetTest() {

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        
        BarcodeLabel barcodeLabelOne = new BarcodeLabel();
        barcodeLabelOne.setBarcode("00000000001000100001");
        barcodeLabelOne.setLabel("itemOne");
        barcodeLabelOne.setQuantity(1);

        BarcodeLabel barcodeLabelTwo = new BarcodeLabel();
        barcodeLabelTwo.setBarcode("00000000001000100002");
        barcodeLabelTwo.setLabel("itemTwo");
        barcodeLabelTwo.setQuantity(2);

        BarcodeLabel barcodeLabelThree = new BarcodeLabel();
        barcodeLabelThree.setBarcode("00000000001000100003");
        barcodeLabelThree.setLabel("itemThree");
        barcodeLabelThree.setQuantity(2);

        BarcodeLabel barcodeLabelFour = new BarcodeLabel();
        barcodeLabelFour.setBarcode("00000000001000100004");
        barcodeLabelFour.setLabel("itemFour");
        barcodeLabelFour.setQuantity(5);

        BarcodeLabel barcodeLabelFive = new BarcodeLabel();
        barcodeLabelFive.setBarcode("00000000001000100005");
        barcodeLabelFive.setLabel("itemFour");
        barcodeLabelFive.setQuantity(10);

        List<BarcodeLabel> barcodeLabelList = new ArrayList();
        barcodeLabelList.add(barcodeLabelOne);
        barcodeLabelList.add(barcodeLabelTwo);
        barcodeLabelList.add(barcodeLabelThree);
        barcodeLabelList.add(barcodeLabelFour);
        barcodeLabelList.add(barcodeLabelFive);

        List<Integer> list = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);

        List<RowSheet> result = oD5160SheetImpl.createCustomSheet(barcodeLabelList, list);
        assertEquals(20, result.size());

        RowOD5160 rowSheet = (RowOD5160) result.get(0);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertEquals("00000000001000100001", rowSheet.getBarcodeTwo());
        assertEquals("itemOne", rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(1);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(2);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertEquals("00000000001000100002", rowSheet.getBarcodeThree());
        assertEquals("itemTwo", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(3);
        assertEquals("00000000001000100002", rowSheet.getBarcodeOne());
        assertEquals("itemTwo", rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(4);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(5);
        assertEquals("00000000001000100003", rowSheet.getBarcodeOne());
        assertEquals("itemThree", rowSheet.getLabelOne());
        assertEquals("00000000001000100003", rowSheet.getBarcodeTwo());
        assertEquals("itemThree", rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(6);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(7);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(8);
        assertEquals("00000000001000100004", rowSheet.getBarcodeOne());
        assertEquals("itemFour", rowSheet.getLabelOne());
        assertEquals("00000000001000100004", rowSheet.getBarcodeTwo());
        assertEquals("itemFour", rowSheet.getLabelTwo());
        assertEquals("00000000001000100004", rowSheet.getBarcodeThree());
        assertEquals("itemFour", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(9);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(10);
        assertEquals("00000000001000100004", rowSheet.getBarcodeOne());
        assertEquals("itemFour", rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(11);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(12);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(13);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(14);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(15);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(16);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(17);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(18);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(19);
        assertNull(rowSheet.getBarcodeOne());
        assertNull(rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertEquals("00000000001000100004", rowSheet.getBarcodeThree());
        assertEquals("itemFour", rowSheet.getLabelThree());

        assertEquals(1, barcodeLabelList.size());
    }
}
