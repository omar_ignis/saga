/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;

import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.util.Utils;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.barcode.impl.Barcode128AutoImpl;
import com.inventory.item.barcode.impl.OutputDocumentBarcodeImpl;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.OutputDocumentRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class OutputDocumentBarcodeImplTest {

    private static BarcodeFactory barcodeFactoryService;

    private static OutputDetailRepository outputDetailRepository;

    private static OutputDocumentRepository outputDocumentRepository;

    private static OutputDocumentBarcodeImpl outputDocumentBarcodeImpl;

    private static List<BarcodeWriter> barcodeWriters = List.of(mock(Barcode128AutoImpl.class));

    private static List<BarcodeReader> barcodeReaders = List.of(mock(Barcode128AutoImpl.class));

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;

    private OutputDocument outputDocumentOne;

    @BeforeAll
    static void setup() {

        when(barcodeWriters.get(0).getType()).thenReturn(BarcodeType.CODE128);
        when(barcodeReaders.get(0).getType()).thenReturn(BarcodeType.CODE128);

        barcodeFactoryService = new BarcodeFactory(barcodeWriters, barcodeReaders);

        outputDetailRepository = mock(OutputDetailRepository.class);

        outputDocumentRepository = mock(OutputDocumentRepository.class);

        outputDocumentBarcodeImpl
                = new OutputDocumentBarcodeImpl(barcodeFactoryService, outputDetailRepository, outputDocumentRepository);
    }

    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);

        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);

        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);

    }

    @Test
    public void getType() {

        DocumentType result = outputDocumentBarcodeImpl.getType();
        assertEquals(DocumentType.OUTPUT, result);
    }

    @Test
    public void readLabelTest() throws Exception {

        String barcode = "00000000001000100001";
        when(barcodeReaders.get(0).readItemId(barcode)).thenReturn(1l);
        when(barcodeReaders.get(0).readLineNumber(barcode)).thenReturn(1);
        when(barcodeReaders.get(0).readDocumentId(barcode)).thenReturn("0000000001");
        when(outputDocumentRepository.findById("OU0000000001"))
                .thenReturn(Optional.of(outputDocumentOne));
        when(outputDetailRepository.findByOutputDocumentAndLineNumberAndItemId(outputDocumentOne, 1, 1l))
                .thenReturn(outputDetailOne);
        var result = outputDocumentBarcodeImpl.readLabel(1L, barcode, BarcodeType.CODE128);

        assertEquals(1L, result.getId());
        assertEquals("detail one", result.getDescription());
        assertEquals(1L, result.getItem().getId());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalPrice());
    }
}
