/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.model.RowSheet;
import com.inventory.item.model.RowOD5160;
import com.inventory.item.model.BarcodeLabel;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.barcode.impl.Barcode128AutoImpl;
import com.inventory.item.barcode.impl.InputDocumentBarcodeImpl;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class InputDocumentBarcodeImplTest {

    private static BarcodeFactory barcodeFactoryService;

    private static InputDetailRepository inputDetailRepository;

    private static InputDocumentRepository inputDocumentRepository;

    private static InputDocumentBarcodeImpl inputDocumentBarcodeImpl;

    private static List<BarcodeWriter> barcodeWriters = List.of(mock(Barcode128AutoImpl.class));

    private static List<BarcodeReader> barcodeReaders = List.of(mock(Barcode128AutoImpl.class));

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;

    private InputDocument inputDocumentOne;

    @BeforeAll
    static void setup() {

        when(barcodeWriters.get(0).getType()).thenReturn(BarcodeType.CODE128);
        when(barcodeReaders.get(0).getType()).thenReturn(BarcodeType.CODE128);

        barcodeFactoryService = new BarcodeFactory(barcodeWriters, barcodeReaders);

        inputDetailRepository = mock(InputDetailRepository.class);

        inputDocumentRepository = mock(InputDocumentRepository.class);

        inputDocumentBarcodeImpl
                = new InputDocumentBarcodeImpl(barcodeFactoryService, inputDetailRepository, inputDocumentRepository);
    }

    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(10));
        inputDocumentOne.setStatus(Status.RELEASED);

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(3));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(2));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(5));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

    }

    @Test
    public void getType() {

        DocumentType result = inputDocumentBarcodeImpl.getType();
        assertEquals(DocumentType.INPUT, result);
    }

    @Test
    public void generateFullSheetLabelsTest() throws Exception {

        BarcodeLabel labelOne = new BarcodeLabel();
        labelOne.setLabel("Item Eins");
        labelOne.setQuantity(3);
        labelOne.setBarcode("00000000001000100001");

        BarcodeLabel labelTwo = new BarcodeLabel();
        labelTwo.setLabel("Item Zwei");
        labelTwo.setQuantity(2);
        labelTwo.setBarcode("00000000001000200002");

        BarcodeLabel labelThree = new BarcodeLabel();
        labelThree.setLabel("Item Drei");
        labelThree.setQuantity(5);
        labelThree.setBarcode("00000000001000300003");

        RowSheet rowSheetOne = new RowOD5160();
        rowSheetOne.fillRow("00000000001000100001", "Item Eins");

        RowSheet rowSheetTwo = new RowOD5160();
        rowSheetTwo.setBarcode(0, "00000000001000200002");
        rowSheetTwo.setBarcode(1, "00000000001000200002");
        rowSheetTwo.setLabel(0, "Item Zwei");
        rowSheetTwo.setLabel(1, "Item Zwei");
        rowSheetTwo.setBarcode(2, "00000000001000300003");
        rowSheetTwo.setLabel(2, "Item Drei");

        RowSheet rowSheetThree = new RowOD5160();
        rowSheetThree.setBarcode(0, "00000000001000300003");
        rowSheetThree.setBarcode(1, "00000000001000300003");
        rowSheetThree.setLabel(0, "Item Drei");
        rowSheetThree.setLabel(1, "Item Drei");
        rowSheetThree.setBarcode(2, "00000000001000300003");
        rowSheetThree.setLabel(2, "Item Drei");

        RowSheet rowSheetFour = new RowOD5160();
        rowSheetFour.setBarcode(0, "00000000001000300003");
        rowSheetFour.setLabel(0, "Item Drei");

        List<RowSheet> rowSheetList = List.of(rowSheetOne, rowSheetTwo, rowSheetThree, rowSheetFour);
        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<BarcodeLabel> barcodeLabelList = List.of(labelOne, labelTwo, labelThree);
        List<Integer> positionList = List.of();

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        when(inputDetailRepository.findByInputDocument(inputDocumentOne))
                .thenReturn(list);

        when(barcodeWriters.get(0).generateBarcodeLabel(inputDocumentOne, list))
                .thenReturn(barcodeLabelList);

        when(barcodeWriters.get(0).generateFullSheetLabel(barcodeLabelList, SheetType.OD5160))
                .thenReturn(rowSheetList);

        List<RowSheet> result = inputDocumentBarcodeImpl
                .generateLabels(positionList, "IN0000000001", BarcodeType.CODE128, SheetType.OD5160);

        assertEquals(4, result.size());

        RowOD5160 rowSheet = (RowOD5160) result.get(0);
        assertEquals("00000000001000100001", rowSheet.getBarcodeOne());
        assertEquals("Item Eins", rowSheet.getLabelOne());
        assertEquals("00000000001000100001", rowSheet.getBarcodeTwo());
        assertEquals("Item Eins", rowSheet.getLabelTwo());
        assertEquals("00000000001000100001", rowSheet.getBarcodeThree());
        assertEquals("Item Eins", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(1);
        assertEquals("00000000001000200002", rowSheet.getBarcodeOne());
        assertEquals("Item Zwei", rowSheet.getLabelOne());
        assertEquals("00000000001000200002", rowSheet.getBarcodeTwo());
        assertEquals("Item Zwei", rowSheet.getLabelTwo());
        assertEquals("00000000001000300003", rowSheet.getBarcodeThree());
        assertEquals("Item Drei", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(2);
        assertEquals("00000000001000300003", rowSheet.getBarcodeOne());
        assertEquals("Item Drei", rowSheet.getLabelOne());
        assertEquals("00000000001000300003", rowSheet.getBarcodeTwo());
        assertEquals("Item Drei", rowSheet.getLabelTwo());
        assertEquals("00000000001000300003", rowSheet.getBarcodeThree());
        assertEquals("Item Drei", rowSheet.getLabelThree());

        rowSheet = (RowOD5160) result.get(3);
        assertEquals("00000000001000300003", rowSheet.getBarcodeOne());
        assertEquals("Item Drei", rowSheet.getLabelOne());
        assertNull(rowSheet.getBarcodeTwo());
        assertNull(rowSheet.getLabelTwo());
        assertNull(rowSheet.getBarcodeThree());
        assertNull(rowSheet.getLabelThree());
    }

    @Test
    public void generatePartialSheetLabelsTest() throws Exception {

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        BarcodeLabel labelOne = new BarcodeLabel();
        labelOne.setLabel("Item Eins");
        labelOne.setQuantity(3);
        labelOne.setBarcode("00000000001000100001");

        BarcodeLabel labelTwo = new BarcodeLabel();
        labelTwo.setLabel("Item Zwei");
        labelTwo.setQuantity(2);
        labelTwo.setBarcode("00000000001000200002");

        BarcodeLabel labelThree = new BarcodeLabel();
        labelThree.setLabel("Item Drei");
        labelThree.setQuantity(5);
        labelThree.setBarcode("00000000001000300003");

        List<RowSheet> rowSheetList = new ArrayList<>();
        RowSheet rowSheet = new RowOD5160();
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000100001");
        rowSheet.setLabel(2, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000200002");
        rowSheet.setLabel(0, "Item Zwei");
        rowSheet.setBarcode(1, "00000000001000200002");
        rowSheet.setLabel(1, "Item Zwei");
        rowSheet.setBarcode(2, "00000000001000200002");
        rowSheet.setLabel(2, "Item Zwei");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000300003");
        rowSheet.setLabel(0, "Item Drei");
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000300003");
        rowSheet.setLabel(2, "Item Drei");
        rowSheetList.add(rowSheet);

        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<BarcodeLabel> barcodeLabelList = List.of(labelOne, labelTwo, labelThree);
        List<Integer> positionList = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        when(inputDetailRepository.findByInputDocument(inputDocumentOne))
                .thenReturn(list);

        when(barcodeWriters.get(0).generateBarcodeLabel(inputDocumentOne, list))
                .thenReturn(barcodeLabelList);

        when(barcodeWriters.get(0).generatePartialSheetLabel(barcodeLabelList, positionList, SheetType.OD5160))
                .thenReturn(rowSheetList);

        List<RowSheet> result = inputDocumentBarcodeImpl
                .generateLabels(positionList, "IN0000000001", BarcodeType.CODE128, SheetType.OD5160);

        assertEquals(20, result.size());

        RowOD5160 rowSheetOne = (RowOD5160) result.get(0);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(1);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(2);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeThree());
        assertEquals("Item Eins", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(3);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(4);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(5);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(6);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(7);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(8);
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeOne());
        assertEquals("Item Zwei", rowSheetOne.getLabelOne());
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Zwei", rowSheetOne.getLabelTwo());
        assertEquals("00000000001000200002", rowSheetOne.getBarcodeThree());
        assertEquals("Item Zwei", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(9);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(10);
        assertEquals("00000000001000300003", rowSheetOne.getBarcodeOne());
        assertEquals("Item Drei", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(11);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(12);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(13);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(14);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(15);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(16);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(17);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(18);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(19);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000300003", rowSheetOne.getBarcodeThree());
        assertEquals("Item Drei", rowSheetOne.getLabelThree());
    }

    @Test
    public void generateMixedSheetPartialAndFullTest() throws Exception {

        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));

        // positions
        //  0,1,2
        // [ ,1, ]
        // [ , , ]
        // [ , ,8]
        // [9, , ]
        // [ , , ]
        // [15,16, ]
        // [ , , ]
        // [ , , ]
        // [24,25,26]
        // [ , , ]
        // [30, , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , , ]
        // [ , ,59]
        BarcodeLabel labelOne = new BarcodeLabel();
        labelOne.setLabel("Item Eins");
        labelOne.setQuantity(10);
        labelOne.setBarcode("00000000001000100001");

        BarcodeLabel labelTwo = new BarcodeLabel();
        labelTwo.setLabel("Item Zwei");
        labelTwo.setQuantity(20);
        labelTwo.setBarcode("00000000001000200002");

        BarcodeLabel labelThree = new BarcodeLabel();
        labelThree.setLabel("Item Drei");
        labelThree.setQuantity(100);
        labelThree.setBarcode("00000000001000300003");

        List<RowSheet> partialRowSheetList = new ArrayList<>();
        RowSheet rowSheet = new RowOD5160();
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000100001");
        rowSheet.setLabel(2, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        rowSheet.setBarcode(1, "00000000001000100001");
        rowSheet.setLabel(1, "Item Eins");
        rowSheet.setBarcode(2, "00000000001000100001");
        rowSheet.setLabel(2, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000100001");
        rowSheet.setLabel(0, "Item Eins");
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        partialRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(2, "00000000001000100001");
        rowSheet.setLabel(2, "Item Eins");
        partialRowSheetList.add(rowSheet);

        List<RowSheet> fullRowSheetList = new ArrayList<>();
        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.fillRow("00000000001000200002", "Item Zwei");
        fullRowSheetList.add(rowSheet);

        rowSheet = new RowOD5160();
        rowSheet.setBarcode(0, "00000000001000200002");
        rowSheet.setLabel(0, "Item Zwei");
        rowSheet.setBarcode(1, "00000000001000200002");
        rowSheet.setLabel(1, "Item Zwei");
        rowSheet.setBarcode(1, "00000000001000300003");
        rowSheet.setLabel(1, "Item Drei");
        fullRowSheetList.add(rowSheet);

        int counter = 33;
        while (counter > 0) {
            rowSheet = new RowOD5160();
            rowSheet.fillRow("00000000001000300003", "Item Drei");
            fullRowSheetList.add(rowSheet);
            counter--;
        }

        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        List<BarcodeLabel> barcodeLabelList = new ArrayList<>();
        barcodeLabelList.add(labelOne);
        barcodeLabelList.add(labelTwo);
        barcodeLabelList.add(labelThree);

        List<Integer> positionList = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        when(inputDetailRepository.findByInputDocument(inputDocumentOne))
                .thenReturn(list);

        when(barcodeWriters.get(0).generateBarcodeLabel(inputDocumentOne, list))
                .thenReturn(barcodeLabelList);

        when(barcodeWriters.get(0).generatePartialSheetLabel(barcodeLabelList, positionList, SheetType.OD5160))
                .thenReturn(partialRowSheetList);

        when(barcodeWriters.get(0).generateFullSheetLabel(barcodeLabelList, SheetType.OD5160))
                .thenReturn(fullRowSheetList);

        List<RowSheet> result = inputDocumentBarcodeImpl
                .generateLabels(positionList, "IN0000000001", BarcodeType.CODE128, SheetType.OD5160);

        assertEquals(60, result.size());
        //here just test partial rowsheet
        RowOD5160 rowSheetOne = (RowOD5160) result.get(0);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(1);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(2);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeThree());
        assertEquals("Item Eins", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(3);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(4);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(5);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(6);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(7);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(8);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeTwo());
        assertEquals("Item Eins", rowSheetOne.getLabelTwo());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeThree());
        assertEquals("Item Eins", rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(9);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(10);
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeOne());
        assertEquals("Item Eins", rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(11);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(12);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(13);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(14);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(15);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(16);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(17);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(18);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertNull(rowSheetOne.getBarcodeThree());
        assertNull(rowSheetOne.getLabelThree());

        rowSheetOne = (RowOD5160) result.get(19);
        assertNull(rowSheetOne.getBarcodeOne());
        assertNull(rowSheetOne.getLabelOne());
        assertNull(rowSheetOne.getBarcodeTwo());
        assertNull(rowSheetOne.getLabelTwo());
        assertEquals("00000000001000100001", rowSheetOne.getBarcodeThree());
        assertEquals("Item Eins", rowSheetOne.getLabelThree());
    }

    @Test
    public void readLabelTest() throws Exception {

        String barcode = "00000000001000100001";
        BarcodeType barcodeType = BarcodeType.CODE128;

        when(barcodeReaders.get(0).readItemId(barcode)).thenReturn(1L);
        when(barcodeReaders.get(0).readLineNumber(barcode)).thenReturn(1);
        when(barcodeReaders.get(0).readDocumentId(barcode)).thenReturn("0000000001");
        when(inputDocumentRepository.findById("IN0000000001")).thenReturn(Optional.of(inputDocumentOne));
        when(inputDetailRepository.findByInputDocumentAndLineNumberAndItemId(inputDocumentOne, 1, 1L))
                .thenReturn(inputDetailOne);
        
        var detail = (InputDetail) inputDocumentBarcodeImpl.readLabel(1L, barcode, barcodeType);
        
        assertEquals(inputDetailOne.getId(), detail.getId());
        assertEquals(itemOne.getId(), detail.getItem().getId());
        assertEquals(1, detail.getLineNumber());
        assertEquals(inputDetailOne.getDescription(), detail.getDescription());
        assertEquals(inputDetailOne.getQuantity(), detail.getQuantity());
        assertEquals(inputDetailOne.getUnitPrice(), detail.getUnitPrice());
        assertEquals(inputDetailOne.getTotalPrice(), detail.getTotalPrice());
    }
}
