/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;

import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.util.Utils;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.barcode.impl.Barcode128AutoImpl;
import com.inventory.item.barcode.impl.PurchaseReturnDocumentBarcodeImpl;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class PurchaseReturnDocumentBarcodeImplTest {

    private static BarcodeFactory barcodeFactoryService;

    private static PurchaseReturnDetailRepository purchaseReturnDetailRepository;

    private static PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    private static PurchaseReturnDocumentBarcodeImpl purchaseReturnDocumentBarcodeImpl;

    private static List<BarcodeWriter> barcodeWriters = List.of(mock(Barcode128AutoImpl.class));

    private static List<BarcodeReader> barcodeReaders = List.of(mock(Barcode128AutoImpl.class));

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;

    private PurchaseReturnDocument purchaseReturnDocumentOne;

    @BeforeAll
    static void setup() {

        when(barcodeWriters.get(0).getType()).thenReturn(BarcodeType.CODE128);
        when(barcodeReaders.get(0).getType()).thenReturn(BarcodeType.CODE128);

        barcodeFactoryService = new BarcodeFactory(barcodeWriters, barcodeReaders);

        purchaseReturnDetailRepository = mock(PurchaseReturnDetailRepository.class);

        purchaseReturnDocumentRepository = mock(PurchaseReturnDocumentRepository.class);

        purchaseReturnDocumentBarcodeImpl
                = new PurchaseReturnDocumentBarcodeImpl(barcodeFactoryService, purchaseReturnDetailRepository, purchaseReturnDocumentRepository);
    }

    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);

        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);

        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);

    }

    @Test
    public void getType() {

        DocumentType result = purchaseReturnDocumentBarcodeImpl.getType();
        assertEquals(DocumentType.PURCHASE_RETURN, result);
    }

    @Test
    public void readLabelTest() throws Exception {

        String barcode = "20000000001000100001";
        when(barcodeReaders.get(0).readItemId(barcode)).thenReturn(1l);
        when(barcodeReaders.get(0).readLineNumber(barcode)).thenReturn(1);
        when(barcodeReaders.get(0).readDocumentId(barcode)).thenReturn("0000000001");
        when(purchaseReturnDocumentRepository.findById("PR0000000001"))
                .thenReturn(Optional.of(purchaseReturnDocumentOne));

        when(purchaseReturnDetailRepository
                .findByPurchaseReturnDocumentAndLineNumberAndItemId(purchaseReturnDocumentOne, 1, 1l))
                .thenReturn(purchaseReturnDetailOne);
        
        var result = purchaseReturnDocumentBarcodeImpl
                .readLabel(1L, barcode, BarcodeType.CODE128);

        assertEquals(1L, result.getId());
        assertEquals("detail one", result.getDescription());
        assertEquals(1L, result.getItem().getId());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalPrice());
    }
}
