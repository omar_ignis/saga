/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service;

import java.util.Set;
import java.util.List;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;
import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.UserAccountRepository;

import org.springframework.security.core.userdetails.User;
import com.inventory.item.security.exception.UnauthorizedException;
import com.inventory.item.security.service.impl.AuthorizationServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class AuthorizationServiceImplTest {

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private UserAccountRepository userAccountRepository;

    @InjectMocks
    private AuthorizationServiceImpl authorizationServiceImpl;

    private User userOne;
    private User userTwo;
    
    private Role roleOne;
    private Role roleTwo;
    private Role roleThree;

    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;

    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private Permission permissionFour;

    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    private UserAccount userAccountThree;

    private Organization organizationOne;
    private Organization organizationTwo;

    @BeforeEach
    void init() {

        organizationOne = new Organization("OrganizationOne", "description");
        organizationOne.setId(1L);

        organizationTwo = new Organization("OrganizationTwo", "description");
        organizationTwo.setId(2L);

        resourceOne = new Resource("createItem","items");
        resourceTwo = new Resource("getAllItems","items");
        resourceThree = new Resource("getItem","items");

        permissionOne = new Permission(resourceOne, organizationOne);
        permissionOne.setId(1L);
        permissionTwo = new Permission(resourceTwo, organizationOne);
        permissionTwo.setId(2L);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionThree.setId(3L);
        permissionFour = new Permission(resourceOne, organizationTwo);
        permissionFour.setId(4L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);
        roleOne.addPermission(permissionThree);

        roleTwo = new Role("Staff", organizationOne);
        roleTwo.addPermission(permissionTwo);
        roleTwo.addPermission(permissionThree);

        roleThree = new Role("Seller", organizationTwo);
        roleThree.addPermission(permissionFour);

        userAccountOne = new UserAccount("Gael", "Alvarez", "Esponjoso", "elchiqui", "gael@chiqui.com", "chiqui123", true);
        userAccountOne.setOrganization(organizationOne);
        userAccountOne.addRole(roleOne);

        userAccountTwo = new UserAccount("Alexis", "Alvarez", "Esponjoso", "pumbin", "alexis@pumbin.com", "pumbin123", true);
        userAccountTwo.setOrganization(organizationOne);
        userAccountTwo.addRole(roleOne);
        userAccountTwo.addRole(roleTwo);

        userAccountThree = new UserAccount("Alejandra", "Andrade", "Esponjosa", "lapingui", "lapingui@josita.com", "josita123", true);
        userAccountThree.setOrganization(organizationOne);
        userAccountThree.addRole(roleTwo);
        
        userOne = new User("omar@alvarez.com","password123",List.of(roleOne));
        userTwo = new User("gael@alvarez.com","password456",List.of(roleTwo));

    }

    @Test
    public void getRolesByResourceAndOrganizationTest() {

        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain()))
                .thenReturn(List.of(roleOne));

        Set<String> result = authorizationServiceImpl
                .getRolesByResourceAndOrganization(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain());

        assertEquals(1, result.size());
        assertTrue(result.contains("Admin"));

        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceTwo.getResourceName(), resourceTwo.getResourceDomain()))
                .thenReturn(List.of(roleOne, roleTwo));

        result = authorizationServiceImpl
                .getRolesByResourceAndOrganization(
                        organizationOne.getId(), resourceTwo.getResourceName(), resourceTwo.getResourceDomain());

        assertEquals(2, result.size());
        assertTrue(result.contains("Admin"));
        assertTrue(result.contains("Staff"));

        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationTwo.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain()))
                .thenReturn(List.of(roleThree));

        result = authorizationServiceImpl
                .getRolesByResourceAndOrganization(
                        organizationTwo.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain());

        assertEquals(1, result.size());
        assertTrue(result.contains("Seller"));

    }

    @Test
    public void getRolesByResourceAndOrganizationWithExceptionTest() {

        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain()))
                .thenReturn(List.of());

        Exception exception = assertThrows(UnauthorizedException.class, () -> {
            authorizationServiceImpl
                    .getRolesByResourceAndOrganization(
                            organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain());
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Recurso denegado. El usuario no cuenta con permisos para realizar esta accion"));

    }

    @Test
    public void getUserRolesTest() {

        Set<String> result = authorizationServiceImpl.getUserRoles(userAccountOne);

        assertEquals(1, result.size());
        assertTrue(result.contains("Admin"));

        result = authorizationServiceImpl.getUserRoles(userAccountTwo);

        assertEquals(2, result.size());
        assertTrue(result.contains("Admin"));
        assertTrue(result.contains("Staff"));
    }

    @Test
    public void getUserRolesWithExceptionTest() {

        userAccountOne.getRoles().clear();

        Exception exception = assertThrows(UnauthorizedException.class, () -> {
            authorizationServiceImpl.getUserRoles(userAccountOne);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Recurso denegado. El usuario no cuenta con permisos para realizar esta accion"));
    }

    @Test
    public void hasUserPermissionTest() {
        
        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain()))
                .thenReturn(List.of(roleOne));
        
        when(userAccountRepository.findByEmail("omar@alvarez.com")).thenReturn(userAccountOne);

        boolean result = authorizationServiceImpl
                .hasUserPermission(userOne, resourceOne.getResourceName(), resourceOne.getResourceDomain());

        assertEquals(true, result);

        when(userAccountRepository.findByEmail("gael@alvarez.com")).thenReturn(userAccountThree);
        result = authorizationServiceImpl
                .hasUserPermission(userTwo, resourceOne.getResourceName(), resourceOne.getResourceDomain());

        assertEquals(false, result);
    }

    @Test
    public void hasUserPermissionWithExceptionTest() {

        when(userAccountRepository.findByEmail("omar@alvarez.com")).thenReturn(userAccountOne);
        
        when(roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain()))
                .thenReturn(List.of());

        Exception exception = assertThrows(UnauthorizedException.class, () -> {
            authorizationServiceImpl
                    .hasUserPermission(userOne, resourceOne.getResourceName(), resourceOne.getResourceDomain());
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Recurso denegado. El usuario no cuenta con permisos para realizar esta accion"));

        //this does not required when sentence because it will throw the exception in the first method
        //when the user account has no roles
        
        userOne = new User("omar@alvarez.com","password123",List.of());

        exception = assertThrows(UnauthorizedException.class, () -> {
            authorizationServiceImpl
                    .hasUserPermission(userOne, resourceTwo.getResourceName(), resourceTwo.getResourceDomain());
        });

        actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Recurso denegado. El usuario no cuenta con permisos para realizar esta accion"));

    }

}
