/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service;

import java.util.List;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;
import com.inventory.item.security.validation.SecurityValidation;
import com.inventory.item.security.service.impl.SecurityServiceImpl;
import com.inventory.item.security.exception.UserAccountNotFoundException;
import com.inventory.item.security.exception.OrganizationNotFoundException;

import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.ResourceRepository;
import com.inventory.item.security.repository.PermissionRepository;
import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class SecurityServiceImplTest {
    
    @Mock
    private RoleRepository roleRepository;

    @Mock
    private ResourceRepository resourceRepository;
    
    @Mock
    private SecurityValidation securityValidation;
    
    @Mock
    private PermissionRepository permissionRepository;
    
    @Mock
    private UserAccountRepository userAccountRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @InjectMocks
    private SecurityServiceImpl securityServiceImpl;
    
    private Role roleOne;
    private Role roleTwo;
    private Role roleThree;
    
    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    
    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private Permission permissionFour;
    private Permission permissionFive;
    private Permission permissionSix;
    
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    
    private Organization organizationOne;
    
    @BeforeEach
    void init(){
        
        organizationOne = new Organization("ROD","Republic of devs");
        organizationOne.setId(1L);
        
        resourceOne = new Resource("resourceOne","domainOne");
        resourceTwo = new Resource("resourceTwo","domainOne");
        resourceThree = new Resource("resourceThree","domainOne");
        
        permissionOne = new Permission(resourceOne, organizationOne);
        permissionTwo = new Permission(resourceTwo, organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);
    }
    
    @Test
    public void createResourcesTest() {
        
        List<Resource> list = List.of(resourceOne, resourceTwo, resourceThree);
        
        when(resourceRepository.saveAll(list)).thenReturn(list);
        
        List<Resource> result = securityServiceImpl.createResources(list);
        
        assertEquals(3, result.size());
    }
    
    @Test
    public void createPermissionsTest() {
        
        List<Permission> list = List.of(permissionOne, permissionTwo, permissionThree);
        
        permissionFour = new Permission(resourceOne, organizationOne);
        permissionFour.setId(1L);
        permissionFive = new Permission(resourceTwo, organizationOne);
        permissionFive.setId(2L);
        permissionSix = new Permission(resourceThree, organizationOne);
        permissionSix.setId(3L);
        
        when(permissionRepository.saveAll(list))
                .thenReturn(List.of(permissionFour, permissionFive, permissionSix));
        
        List<Permission> resultOne = securityServiceImpl.createPermissions(list);
        
        assertEquals(3, resultOne.size());
        
        Permission resultTwo = resultOne.get(0);
        
        assertEquals("domainOne",resultTwo.getResource().getResourceDomain());
        assertEquals("resourceOne", resultTwo.getResource().getResourceName());
        assertEquals("ROD", resultTwo.getOrganization().getName());
        
        resultTwo = resultOne.get(1);
        
        assertEquals("domainOne",resultTwo.getResource().getResourceDomain());
        assertEquals("resourceTwo", resultTwo.getResource().getResourceName());
        assertEquals("ROD", resultTwo.getOrganization().getName());
        
        resultTwo = resultOne.get(2);
        
        assertEquals("domainOne",resultTwo.getResource().getResourceDomain());
        assertEquals("resourceThree", resultTwo.getResource().getResourceName());
        assertEquals("ROD", resultTwo.getOrganization().getName());
        
    }
    
    @Test
    public void createOrganizationTest() {
        
        when(organizationRepository.save(any(Organization.class))).thenReturn(organizationOne);
        
        Organization result = securityServiceImpl.createOrganization("ROD", "Republic of devs");
        
        assertEquals(1L, result.getId());
        assertEquals("ROD", result.getName());
        assertEquals("Republic of devs", result.getDescription());
    }
    
    @Test
    public void createRoleTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(roleRepository.save(any(Role.class))).thenReturn(roleOne);

        Role result = securityServiceImpl.createRole("Admin", organizationOne.getId());
        
        assertNotNull(result);
        assertEquals("Admin", result.getRoleName());
        assertEquals(organizationOne.getId(), result.getKey().getOrganization().getId());
        assertEquals(organizationOne.getName(), result.getKey().getOrganization().getName());
        assertEquals(0, result.getPermissions().size());
        
    }
    
    @Test
    public void createRoleOrganizationExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenThrow(new OrganizationNotFoundException("1"));

        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            securityServiceImpl.createRole("Admin",organizationOne.getId());
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
    }
    
    @Test
    public void updateRoleNewRoleTest() {
        

        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleTwo = new Role("Admin", organizationOne);
        roleTwo.addPermissions(List.of(permissionOne, permissionTwo, permissionThree));

        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateRoleBeforeUpdate("Admin", organizationOne.getId())).thenReturn(roleOne);
        when(roleRepository.save(roleOne)).thenReturn(roleTwo);

        var permissions = List.of(permissionOne, permissionTwo, permissionThree);

        var resultOne = securityServiceImpl.updateRole("Admin", organizationOne.getId(), permissions);

        assertEquals(3, resultOne.getPermissions().size());

        var resultTwo = resultOne.getPermissions().get(0);
        assertNotNull(resultTwo);
        assertEquals(1L, resultTwo.getId());
        assertEquals(1L, resultTwo.getOrganization().getId());
        assertEquals("resourceOne", resultTwo.getResource().getResourceName());
        assertEquals("domainOne", resultTwo.getResource().getResourceDomain());

        resultTwo = resultOne.getPermissions().get(1);
        assertNotNull(resultTwo);
        assertEquals(2L, resultTwo.getId());
        assertEquals(1L, resultTwo.getOrganization().getId());
        assertEquals("resourceTwo", resultTwo.getResource().getResourceName());
        assertEquals("domainOne", resultTwo.getResource().getResourceDomain());

        resultTwo = resultOne.getPermissions().get(2);
        assertNotNull(resultTwo);
        assertEquals(3L, resultTwo.getId());
        assertEquals(1L, resultTwo.getOrganization().getId());
        assertEquals("resourceThree", resultTwo.getResource().getResourceName());
        assertEquals("domainOne", resultTwo.getResource().getResourceDomain());

    }
    
    @Test
    public void updateRoleRemoveAndAddPermissionsTest() {

        permissionOne.setId(1L);
        permissionOne.setDeleted(true);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleTwo = new Role("Admin", organizationOne);
        roleTwo.addPermissions(List.of(permissionTwo, permissionThree));

        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateRoleBeforeUpdate("Admin", organizationOne.getId())).thenReturn(roleOne);
        when(roleRepository.save(roleOne)).thenReturn(roleTwo);

        var permissions = List.of(permissionOne, permissionTwo, permissionThree);

        var resultOne = securityServiceImpl.updateRole("Admin", organizationOne.getId(), permissions);

        assertEquals(2, resultOne.getPermissions().size());

        var resultTwo = resultOne.getPermissions().get(0);
        assertNotNull(resultTwo);
        assertEquals(2L, resultTwo.getId());
        assertEquals(1L, resultTwo.getOrganization().getId());
        assertEquals("resourceTwo", resultTwo.getResource().getResourceName());
        assertEquals("domainOne", resultTwo.getResource().getResourceDomain());

        resultTwo = resultOne.getPermissions().get(1);
        assertNotNull(resultTwo);
        assertEquals(3L, resultTwo.getId());
        assertEquals(1L, resultTwo.getOrganization().getId());
        assertEquals("resourceThree", resultTwo.getResource().getResourceName());
        assertEquals("domainOne", resultTwo.getResource().getResourceDomain());

    }
    
    @Test
    public void createUserAccountTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        userAccountOne = new UserAccount();
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne));
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne));
        userAccountTwo.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        
        when(userAccountRepository.save(any())).thenReturn(userAccountTwo);
        
        UserAccount result = securityServiceImpl
                .createUserAccount(organizationOne.getId(), userAccountOne);
        
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("Gael", result.getName());
        assertEquals("Alvarez",result.getPaternalLastName());
        assertEquals("Esponjoso",result.getMaternalLastName());
        assertEquals("elchiqui",result.getUsername());
        assertEquals("chiqui@sponjoso.com",result.getEmail());
        assertEquals("password123", result.getPassword());
        assertEquals(0, result.getRoles().size());
        assertEquals(1L, result.getOrganization().getId());
        assertEquals(false, result.isActive());
    }
    
    @Test
    public void createUserAccountOrganizationExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        userAccountOne = new UserAccount();
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne));
        
        when(securityValidation.validateOrganization(1L)).thenThrow(new OrganizationNotFoundException("1"));

        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            securityServiceImpl
                .createUserAccount(organizationOne.getId(), userAccountOne);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
    }
    
    @Test
    public void updateUserAccountTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael2");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui2@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleOne));
        userAccountTwo.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccountBeforeUpdate(1L, "chiqui2@sponjoso.com")).thenReturn(userAccountTwo);
        
        when(userAccountRepository.save(any())).thenReturn(userAccountTwo);
        
        UserAccount result = securityServiceImpl
                .updateUserAccount(organizationOne.getId(), userAccountOne);
        
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("Gael2", result.getName());
        assertEquals("Alvarez",result.getPaternalLastName());
        assertEquals("Esponjoso",result.getMaternalLastName());
        assertEquals("elchiqui",result.getUsername());
        assertEquals("chiqui2@sponjoso.com",result.getEmail());
        assertEquals("password123", result.getPassword());
        assertEquals(1, result.getRoles().size());
        assertEquals(1L, result.getOrganization().getId());
        assertEquals(false, result.isActive());
    }
    
    @Test
    public void updatedUserAccountOrganizationExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleOne));
        userAccountTwo.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenThrow(new OrganizationNotFoundException("1"));

        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            
            securityServiceImpl.updateUserAccount(organizationOne.getId(), userAccountOne);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
        
    }
    
    @Test
    public void updateUserAccountUserExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael2");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui2@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleOne));
        userAccountTwo.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccountBeforeUpdate(1L,"chiqui2@sponjoso.com")).thenThrow(
                new UserAccountNotFoundException("chiqui2@sponjoso.com"));
        
        Exception exception = assertThrows(UserAccountNotFoundException.class, () -> {
            
            securityServiceImpl.updateUserAccount(organizationOne.getId(), userAccountOne);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("chiqui2@sponjoso.com no es un usuario valido"));
        
    }
    
    @Test
    public void enableUserAccountTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleOne));
        userAccountTwo.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccount(1L)).thenReturn(userAccountTwo);
        
        userAccountTwo.setActive(true);
        when(userAccountRepository.save(userAccountTwo)).thenReturn(userAccountTwo);
        
        UserAccount result = securityServiceImpl
                .enableUserAcccount(organizationOne.getId(), 1L, true);
        
        
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("Gael", result.getName());
        assertEquals("Alvarez",result.getPaternalLastName());
        assertEquals("Esponjoso",result.getMaternalLastName());
        assertEquals("elchiqui",result.getUsername());
        assertEquals("chiqui@sponjoso.com",result.getEmail());
        assertEquals("password123", result.getPassword());
        assertEquals(1, result.getRoles().size());
        assertEquals(1L, result.getOrganization().getId());
        assertEquals(true, result.isActive());
    }
    
    @Test
    public void enableUserAccountOrganizationExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenThrow(new OrganizationNotFoundException("1"));

        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            
            securityServiceImpl.enableUserAcccount(organizationOne.getId(), 1L, true);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
    }
    
    @Test
    public void enableUserAccountUserExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);
        
        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccount(1L)).thenThrow(new UserAccountNotFoundException());
        
        Exception exception = assertThrows(UserAccountNotFoundException.class, () -> {
            
            securityServiceImpl.enableUserAcccount(organizationOne.getId(), 1L, true);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("Usuario valido"));
    }
    
    @Test
    public void updateUsersRolesTest() {

        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));

        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));

        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setOrganization(organizationOne);

        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleOne, roleTwo));
        userAccountTwo.setOrganization(organizationOne);

        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccount(1L)).thenReturn(userAccountOne);
        when(userAccountRepository.save(userAccountOne)).thenReturn(userAccountTwo);

        UserAccount resultOne = securityServiceImpl
                .updateUserRoles(organizationOne.getId(), 1L, List.of(roleOne, roleTwo));
        
        assertNotNull(resultOne);
        assertEquals(1L, resultOne.getId());
        assertEquals("Gael", resultOne.getName());
        assertEquals("Alvarez",resultOne.getPaternalLastName());
        assertEquals("Esponjoso",resultOne.getMaternalLastName());
        assertEquals("elchiqui",resultOne.getUsername());
        assertEquals("chiqui@sponjoso.com",resultOne.getEmail());
        assertEquals("password123", resultOne.getPassword());
        assertEquals(2, resultOne.getRoles().size());
        assertEquals(1L, resultOne.getOrganization().getId());
        assertEquals(false, resultOne.isActive());
        
        Role resultTwo = resultOne.getRoles().get(0);
        assertEquals("Admin", resultTwo.getRoleName());
        assertEquals(organizationOne.getId(), resultTwo.getKey().getOrganization().getId());
        
        resultTwo = resultOne.getRoles().get(1);
        assertEquals("Staff", resultTwo.getRoleName());
        assertEquals(organizationOne.getId(), resultTwo.getKey().getOrganization().getId());
    }
    
    @Test
    public void updateUsersRolesRemoveAndAddRolesTest() {

        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne));
        roleOne.setDeleted(true);

        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionTwo));
        
        roleThree = new Role("Manager", organizationOne);
        roleThree.setPermissions(List.of(permissionThree));

        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.addRoles(List.of(roleOne, roleTwo));
        userAccountOne.setOrganization(organizationOne);

        userAccountTwo = new UserAccount();
        userAccountTwo.setId(1L);
        userAccountTwo.setName("Gael");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Esponjoso");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setRoles(List.of(roleTwo, roleThree));
        userAccountTwo.setOrganization(organizationOne);

        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccount(1L)).thenReturn(userAccountOne);
        when(userAccountRepository.save(userAccountOne)).thenReturn(userAccountTwo);
        
        UserAccount resultOne = securityServiceImpl
                .updateUserRoles(organizationOne.getId(), 1L, List.of(roleOne, roleTwo, roleThree));
        
        assertNotNull(resultOne);
        assertEquals(1L, resultOne.getId());
        assertEquals("Gael", resultOne.getName());
        assertEquals("Alvarez",resultOne.getPaternalLastName());
        assertEquals("Esponjoso",resultOne.getMaternalLastName());
        assertEquals("elchiqui",resultOne.getUsername());
        assertEquals("chiqui@sponjoso.com",resultOne.getEmail());
        assertEquals("password123", resultOne.getPassword());
        assertEquals(2, resultOne.getRoles().size());
        assertEquals(1L, resultOne.getOrganization().getId());
        assertEquals(false, resultOne.isActive());
        
        Role resultTwo = resultOne.getRoles().get(0);
        assertEquals("Staff", resultTwo.getRoleName());
        assertEquals(organizationOne.getId(), resultTwo.getKey().getOrganization().getId());
        
        resultTwo = resultOne.getRoles().get(1);
        assertEquals("Manager", resultTwo.getRoleName());
        assertEquals(organizationOne.getId(), resultTwo.getKey().getOrganization().getId());
    }
    
    @Test
    public void updateUsersRolesOrganizationExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));

        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));

        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of());
        userAccountOne.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenThrow(new OrganizationNotFoundException("1"));

        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            
            securityServiceImpl.updateUserRoles(organizationOne.getId(), 1L, List.of(roleOne, roleTwo));
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
    }
    
    @Test
    public void updateUsersRolesExceptionTest() {
        
        permissionOne.setId(1L);
        permissionTwo.setId(2L);
        permissionThree.setId(3L);

        roleOne = new Role("Admin", organizationOne);
        roleOne.setPermissions(List.of(permissionOne, permissionTwo));

        roleTwo = new Role("Staff", organizationOne);
        roleTwo.setPermissions(List.of(permissionThree));

        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setRoles(List.of());
        userAccountOne.setOrganization(organizationOne);
        
        when(securityValidation.validateOrganization(1L)).thenReturn(organizationOne);
        when(securityValidation.validateUserAccount(1L)).thenThrow(new UserAccountNotFoundException());
        
        Exception exception = assertThrows(UserAccountNotFoundException.class, () -> {
            
            securityServiceImpl.updateUserRoles(organizationOne.getId(), 1L, List.of(roleOne, roleTwo));
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("Usuario valido"));
        
    }
}
