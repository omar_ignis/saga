/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import com.inventory.item.security.entity.Operation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OperationRepositoryTest {
    
    @Autowired
    private OperationRepository operationRepository;
    
    private Operation operationOne;
    private Operation operationTwo;
    private Operation operationThree;
    private Operation operationFour;
    
    @BeforeEach
    void init() {
        
        operationOne = new Operation("CREATE");
        operationTwo = new Operation("READ");
        operationThree = new Operation("UPDATE");
        operationFour = new Operation("DELETE");
    }
    
    @Test
    public void saveTest() {
        
        Operation resultOne = operationRepository.save(operationOne);
        
        assertNotNull(resultOne);
        assertEquals("CREATE", resultOne.getName());
        
        Operation resultTwo = operationRepository.save(operationTwo);
        
        assertNotNull(resultTwo);
        assertEquals("READ", resultTwo.getName());
        
        Operation resultThree = operationRepository.save(operationThree);
        
        assertNotNull(resultThree);
        assertEquals("UPDATE", resultThree.getName());
        
        Operation resultFour = operationRepository.save(operationFour);
        
        assertNotNull(resultFour);
        assertEquals("DELETE", resultFour.getName());
        
    }
}
