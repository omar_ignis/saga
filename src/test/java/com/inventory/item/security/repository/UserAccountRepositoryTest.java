/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserAccountRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private OrganizationRepository organizationRepository;
    
    private Role roleOne;
    private Role roleTwo;
    private Role roleThree;
    
    private Organization organizationOne;
    private Organization organizationTwo;
    
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;

    @BeforeEach
    void init() {
        
        organizationOne = new Organization("InvesntoryStudio","sistema de inventario");
        organizationTwo = new Organization("InvestmentStudio","sistema de inversiones");
        
        userAccountOne = new UserAccount(
                "Omar Enrique","Alvarez","Zarate","ignisMX",
                "omar@alvarez.com","secreto",true);
        
        userAccountTwo = new UserAccount(
                "Carlos Gael","Alvarez","Esponjoso",
                "elchiqui","gael@sponjos.com","fifi",false);
        
        
        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);
        
        roleOne = new Role("staff", organizationOne);
        roleOne = roleRepository.save(roleOne);
        
        roleTwo = new Role("manager", organizationOne);
        roleTwo = roleRepository.save(roleTwo);
        
        userAccountOne.setOrganization(organizationOne);
    }
    
    @Test
    public void saveTest() {
        
        userAccountOne.addRole(roleOne);
        userAccountOne.addRole(roleTwo);
        UserAccount result = userAccountRepository.save(userAccountOne);
        
        assertNotNull(result);
        assertTrue(result.getId() > 0L);
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
        assertTrue(result.isActive());
        
        List<Role> roles = result.getRoles();
        assertEquals(2, result.getRoles().size());
        assertEquals("staff", roles.get(0).getKey().getName());
        assertEquals(organizationOne.getName(), roles.get(0).getKey().getOrganization().getName());
        assertEquals("manager", roles.get(1).getKey().getName());
        assertEquals(organizationOne.getName(), roles.get(1).getKey().getOrganization().getName());

        roleThree = new Role("admin", organizationTwo);
        roleThree = roleRepository.save(roleThree);
        
        userAccountTwo.setOrganization(organizationTwo);
        userAccountTwo.addRole(roleThree);
        result = userAccountRepository.save(userAccountTwo);
        
        assertNotNull(result);
        assertTrue(result.getId() > 0L);
        assertEquals(userAccountTwo.getName(), result.getName());
        assertEquals(userAccountTwo.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountTwo.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountTwo.getUsername(), result.getUsername());
        assertEquals(userAccountTwo.getEmail(), result.getEmail());
        assertEquals(userAccountTwo.getPassword(), result.getPassword());
        assertFalse(result.isActive());
        
        roles = result.getRoles();
        assertEquals(1, result.getRoles().size());
        assertEquals("admin", roles.get(0).getKey().getName());
        assertEquals(organizationTwo.getName(), roles.get(0).getKey().getOrganization().getName());
        
    }
    
    @Test
    public void findByEmailTest() {

        userAccountOne.addRole(roleOne);
        userAccountOne.addRole(roleTwo);
        userAccountOne = userAccountRepository.save(userAccountOne);
        UserAccount result = userAccountRepository.findByEmail("omar@alvarez.com");
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals(userAccountOne.getId(), result.getId());
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
    }
    
    @Test
    public void addRolesTest() {
        
        userAccountOne.addRole(roleOne);
        userAccountOne.addRole(roleTwo);
        userAccountOne = userAccountRepository.save(userAccountOne);
        UserAccount result = userAccountRepository.findById(userAccountOne.getId()).get();
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals(2, result.getRoles().size());
        assertEquals(userAccountOne.getId(), result.getId());
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
    }
    
    @Test
    public void addRolesListTest() {
        
        userAccountOne.addRoles(List.of(roleOne, roleTwo));
        userAccountOne = userAccountRepository.save(userAccountOne);
        UserAccount result = userAccountRepository.findById(userAccountOne.getId()).get();
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals(2, result.getRoles().size());
        assertEquals(userAccountOne.getId(), result.getId());
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
    }
    
    @Test
    public void removeRolesTest() {
        
        userAccountOne.addRoles(List.of(roleOne, roleTwo));
        userAccountOne = userAccountRepository.save(userAccountOne);
        UserAccount result = userAccountRepository.findById(userAccountOne.getId()).get();
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals(2, result.getRoles().size());
        assertEquals(userAccountOne.getId(), result.getId());
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
        
        roleThree = roleRepository.findById(roleOne.getKey()).get();
        userAccountOne.removeRoles(List.of(roleThree));
        userAccountOne = userAccountRepository.save(userAccountOne);
        
        result = userAccountRepository.findById(userAccountOne.getId()).get();
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals(1, result.getRoles().size());
        assertEquals(userAccountOne.getId(), result.getId());
        assertEquals(userAccountOne.getName(), result.getName());
        assertEquals(userAccountOne.getPaternalLastName(), result.getPaternalLastName());
        assertEquals(userAccountOne.getMaternalLastName(), result.getMaternalLastName());
        assertEquals(userAccountOne.getUsername(), result.getUsername());
        assertEquals(userAccountOne.getEmail(), result.getEmail());
        assertEquals(userAccountOne.getPassword(), result.getPassword());
        
    }
}
