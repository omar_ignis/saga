/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import com.inventory.item.security.entity.Organization;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OrganizationRepositoryTest {
    
    @Autowired
    private OrganizationRepository organizationRepository;
    
    private Organization organizationOne;
    private Organization organizationTwo;
    
    @BeforeEach
    void init() {
        
        organizationOne = new Organization("InventoryStudio","Basic Inventory Studio");
        organizationTwo = new Organization("InvestmentStudio","Basic Investment Studio");
        
    }
    
    @Test
    public void saveTest() {
        
        Organization resultOne = organizationRepository.save(organizationOne);
        
        assertNotNull(resultOne);
        assertTrue(resultOne.getId() > 0L);
        assertEquals(organizationOne.getName(), resultOne.getName());
        assertEquals(organizationOne.getDescription(), resultOne.getDescription());
        
        Organization resultTwo = organizationRepository.save(organizationTwo);
        
        assertNotNull(resultTwo);
        assertTrue(resultTwo.getId() > 0L);
        assertEquals(organizationTwo.getName(), resultTwo.getName());
        assertEquals(organizationTwo.getDescription(), resultTwo.getDescription());
    }
}
