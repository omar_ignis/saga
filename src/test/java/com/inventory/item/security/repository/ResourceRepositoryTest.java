/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import com.inventory.item.security.entity.Resource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ResourceRepositoryTest {
    
    @Autowired
    private ResourceRepository resourceRepository;

    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    
    @BeforeEach
    void init() {
        
        resourceOne = new Resource("Customer", "domainOne");
        resourceTwo = new Resource("IODocument", "domainOne");
        resourceThree = new Resource("Item", "domainTwo");
    }
    
    @Test
    public void saveTest() {
        
        Resource result = resourceRepository.save(resourceOne);
        
        assertNotNull(result);
        assertEquals("Customer", result.getResourceName());
        assertEquals("domainOne", result.getResourceDomain());
        
        result = resourceRepository.save(resourceTwo);
        
        assertNotNull(result);
        assertEquals("IODocument", result.getResourceName());
        assertEquals("domainOne", result.getResourceDomain());
        
        result = resourceRepository.save(resourceThree);
        
        assertNotNull(result);
        assertEquals("Item", result.getResourceName());
        assertEquals("domainTwo", result.getResourceDomain());
    }
    
}
