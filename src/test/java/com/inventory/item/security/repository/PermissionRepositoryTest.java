/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import java.util.List;

import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.Organization;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PermissionRepositoryTest {
    
    @Autowired
    private ResourceRepository resourceRepository;
    
    @Autowired
    private OrganizationRepository organizationRepository;
    
    @Autowired
    private PermissionRepository permissionRepository;
    
    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    
    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private Permission permissionFour;
    private Permission permissionFive;
    private Permission permissionSix;
    
    private Organization organizationOne;
    private Organization organizationTwo;
    
    @BeforeEach
    void init() {
        
        resourceOne = new Resource("createItem", "items");
        resourceTwo = new Resource("getAllItems", "items");
        resourceThree = new Resource("getItem", "items");
        
        organizationOne = new Organization("ROD","Republic of devs");
        organizationTwo = new Organization("Mapache Company","Compañia de mapaches");
        
    }
    
    @Test
    public void saveTest() {
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);
        
        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);
        
        permissionOne = new Permission(resourceOne, organizationOne);
        
        Permission result = permissionRepository.save(permissionOne);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("ROD", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("createItem", result.getResource().getResourceName());
        
        permissionTwo = new Permission(resourceTwo, organizationOne);
        
        result = permissionRepository.save(permissionTwo);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("ROD", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getAllItems", result.getResource().getResourceName());
        
        permissionThree = new Permission(resourceThree, organizationOne);
        
        result = permissionRepository.save(permissionThree);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("ROD", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getItem", result.getResource().getResourceName());
        
        permissionFour = new Permission(resourceOne, organizationTwo);
        
        result = permissionRepository.save(permissionFour);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("Mapache Company", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("createItem", result.getResource().getResourceName());
        
        permissionFive = new Permission(resourceTwo, organizationTwo);
        
        result = permissionRepository.save(permissionFive);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("Mapache Company", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getAllItems", result.getResource().getResourceName());
        
        permissionSix = new Permission(resourceThree, organizationTwo);
        
        result = permissionRepository.save(permissionSix);
        
        assertNotNull(result);
        assertTrue(result.isActive());
        assertEquals("Mapache Company", result.getOrganization().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getItem", result.getResource().getResourceName());
        
        List<Permission> list = permissionRepository.findAll();
        
        assertEquals(6, list.size());
    }
    
}
