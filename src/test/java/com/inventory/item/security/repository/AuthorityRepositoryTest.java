/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Operation;
import com.inventory.item.security.entity.Authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AuthorityRepositoryTest {
    
    @Autowired
    private ResourceRepository resourceRepository;
    
    @Autowired
    private OperationRepository operationRepository;
    
    @Autowired
    private AuthorityRepository authorityRepository;
    
    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    
    private Operation operationOne;
    private Operation operationTwo;
    private Operation operationThree;
    private Operation operationFour;
    
    private Authority authorityOne;
    private Authority authorityTwo;
    private Authority authorityThree;
    
    @BeforeEach
    void init() {
        
        operationOne = new Operation("CREATE");
        operationTwo = new Operation("READ");
        operationThree = new Operation("UPDATE");
        operationFour = new Operation("DELETE");
        
        resourceOne = new Resource("createItem", "items");
        resourceTwo = new Resource("getAllItems", "items");
        resourceThree = new Resource("getItem", "items");
        
        operationOne = operationRepository.save(operationOne);
        operationTwo = operationRepository.save(operationTwo);
        operationThree = operationRepository.save(operationThree);
        operationFour = operationRepository.save(operationFour);
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);
        
    }
    
    @Test
    public void saveTest() {
        
        authorityOne = new Authority();
        authorityOne.setDomain("Item");
        authorityOne.setResource(resourceOne);
        authorityOne.setOperation(operationOne);
        
        Authority result = authorityRepository.save(authorityOne);
        
        assertNotNull(result);
        assertTrue(result.getId() > 0L);
        assertEquals("Item", result.getDomain());
        assertEquals("CREATE", result.getOperation().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("createItem", result.getResource().getResourceName());
        
        authorityTwo = new Authority();
        authorityTwo.setDomain("Item");
        authorityTwo.setResource(resourceTwo);
        authorityTwo.setOperation(operationTwo);
        
        result = authorityRepository.save(authorityTwo);
        
        assertNotNull(result);
        assertTrue(result.getId() > 0L);
        assertEquals("Item", result.getDomain());
        assertEquals("READ", result.getOperation().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getAllItems", result.getResource().getResourceName());
        
        authorityThree = new Authority();
        authorityThree.setDomain("Item");
        authorityThree.setResource(resourceThree);
        authorityThree.setOperation(operationTwo);
        
        result = authorityRepository.save(authorityThree);
        
        assertNotNull(result);
        assertTrue(result.getId() > 0L);
        assertEquals("Item", result.getDomain());
        assertEquals("READ", result.getOperation().getName());
        assertEquals("items", result.getResource().getResourceDomain());
        assertEquals("getItem", result.getResource().getResourceName());
    }
}
