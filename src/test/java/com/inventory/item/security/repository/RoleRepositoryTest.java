/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.repository;

import java.util.List;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.Organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    private Role roleOne;
    private Role roleTwo;
    private Role roleThree;

    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;

    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private Permission permissionFour;
    private Permission permissionFive;

    private Organization organizationOne;
    private Organization organizationTwo;

    @BeforeEach
    void init() {

        resourceOne = new Resource("createItem", "items");
        resourceTwo = new Resource("getAllItems", "items");
        resourceThree = new Resource("getItem", "items");

        organizationOne = new Organization("InventoryStudio", "Basic Inventory Studio");
        organizationTwo = new Organization("InvestmentStudio", "Basic Investment Studio");

    }

    @Test
    public void saveTest() {

        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceOne, organizationTwo);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);

        Role result = roleRepository.save(roleOne);
        assertNotNull(result);
        assertEquals("Admin", result.getKey().getName());
        assertEquals("InventoryStudio", result.getKey().getOrganization().getName());
        assertEquals(organizationOne, result.getKey().getOrganization());
        assertEquals(2, result.getPermissions().size());
        
        List<Permission> elements = result.getPermissions();
        Permission[] permissions = elements.toArray(new Permission[(elements.size())]);
        assertEquals(resourceOne.getResourceName(), permissions[0].getResource().getResourceName());
        assertEquals(resourceOne.getResourceDomain(), permissions[0].getResource().getResourceDomain());
        assertEquals(resourceTwo.getResourceName(), permissions[1].getResource().getResourceName());
        assertEquals(resourceTwo.getResourceDomain(), permissions[1].getResource().getResourceDomain());

        roleTwo = new Role("Staff", organizationTwo);
        roleTwo.addPermission(permissionThree);

        result = roleRepository.save(roleTwo);
        assertNotNull(result);
        assertEquals("Staff", result.getKey().getName());
        assertEquals("InvestmentStudio", result.getKey().getOrganization().getName());
        assertEquals(organizationTwo, result.getKey().getOrganization());
        assertEquals(1, result.getPermissions().size());
        elements = result.getPermissions();
        permissions = elements.toArray(new Permission[(elements.size())]);
        assertEquals(resourceOne.getResourceName(), permissions[0].getResource().getResourceName());

    }
    
    @Test
    public void findByKeyOrganizationIdAndPermissionsResourceNameTest() {

        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionFour = new Permission(resourceOne, organizationTwo);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);
        permissionFour = permissionRepository.save(permissionFour);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);
        roleOne.addPermission(permissionThree);
        roleOne = roleRepository.save(roleOne);
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.addPermission(permissionOne);
        roleTwo.addPermission(permissionThree);
        roleTwo = roleRepository.save(roleTwo);
        
        roleThree = new Role("Root", organizationTwo);
        roleThree.addPermission(permissionFour);
        roleThree = roleRepository.save(roleThree);
        
        List<Role> roles = roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain());
        
        assertEquals(2, roles.size());
        
        Role result = roles.get(0);
        assertEquals("Admin", result.getRoleName());
        assertEquals(organizationOne.getId(), result.getKey().getOrganization().getId());
        assertEquals("InventoryStudio", result.getKey().getOrganization().getName());
        
        result = roles.get(1);
        assertEquals("Staff", result.getRoleName());
        assertEquals(organizationOne.getId(), result.getKey().getOrganization().getId());
        assertEquals("InventoryStudio", result.getKey().getOrganization().getName());
        
        roles = roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationOne.getId(), resourceTwo.getResourceName(), resourceTwo.getResourceDomain());
        
        assertEquals(1, roles.size());
        
        result = roles.get(0);
        assertEquals("Admin", result.getRoleName());
        assertEquals(organizationOne.getId(), result.getKey().getOrganization().getId());
        assertEquals("InventoryStudio", result.getKey().getOrganization().getName());
        
        roles = roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationTwo.getId(), resourceOne.getResourceName(), resourceOne.getResourceDomain());
        
        assertEquals(1, roles.size());
        
        result = roles.get(0);
        assertEquals("Root", result.getRoleName());
        assertEquals(organizationTwo.getId(), result.getKey().getOrganization().getId());
        assertEquals("InvestmentStudio", result.getKey().getOrganization().getName());
        
        roles = roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organizationTwo.getId(), resourceTwo.getResourceName(), resourceTwo.getResourceDomain());
        
        assertEquals(0, roles.size());
    }
    
    @Test
    public void findByKeyNameAndKeyOrganizationIdTest() {
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionFour = new Permission(resourceOne, organizationTwo);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);
        permissionFour = permissionRepository.save(permissionFour);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);
        roleOne.addPermission(permissionThree);
        roleOne = roleRepository.save(roleOne);
        
        roleTwo = new Role("Staff", organizationOne);
        roleTwo.addPermission(permissionOne);
        roleTwo.addPermission(permissionThree);
        roleTwo = roleRepository.save(roleTwo);
        
        roleThree = new Role("Root", organizationTwo);
        roleThree.addPermission(permissionFour);
        roleThree = roleRepository.save(roleThree);
        
        Role result = roleRepository
                .findByKeyNameAndKeyOrganizationId("Root", organizationTwo.getId()).get();
        
        assertNotNull(result);
        assertEquals("Root", result.getRoleName());
        assertEquals(permissionFour.getId(), result.getPermissions().get(0).getId());
        assertEquals(organizationTwo.getId(), result.getKey().getOrganization().getId());
        
        result = roleRepository
                .findByKeyNameAndKeyOrganizationId("Admin", organizationTwo.getId()).orElse(null);
        
        assertEquals(null, result);
    }
    
    @Test
    public void removePermissionOfRolesTest() {
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);
        roleOne.addPermission(permissionThree);
        roleOne = roleRepository.save(roleOne);
        
        Role resultOne = roleRepository.findById(roleOne.getKey()).get();
        
        assertEquals("Admin", resultOne.getRoleName());
        assertEquals(3, resultOne.getPermissions().size());
        assertEquals(organizationOne.getId(), resultOne.getKey().getOrganization().getId());
        
        permissionFour = permissionRepository.findById(permissionTwo.getId()).get();
        permissionFive = permissionRepository.findById(permissionThree.getId()).get();
        
        roleOne.removePermissions(List.of(permissionFour, permissionFive));
        
        resultOne = roleRepository.save(roleOne);
        
        assertEquals("Admin", resultOne.getRoleName());
        assertEquals(1, resultOne.getPermissions().size());
        assertEquals(organizationOne.getId(), resultOne.getKey().getOrganization().getId());
        
        Permission resultTwo = permissionRepository.findById(permissionTwo.getId()).get();
        
        assertNotNull(resultTwo);
        assertEquals(permissionTwo.getId(), resultTwo.getId());
        assertEquals(permissionTwo.getOrganization().getId(), resultTwo.getOrganization().getId());
        
        resultTwo = permissionRepository.findById(permissionThree.getId()).get();
        
        assertNotNull(resultTwo);
        assertEquals(permissionThree.getId(), resultTwo.getId());
        assertEquals(permissionThree.getOrganization().getId(), resultTwo.getOrganization().getId());
    }
    
    @Test
    public void addPermissionsTest() {
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionFour = new Permission(resourceOne, organizationTwo);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);
        permissionFour = permissionRepository.save(permissionFour);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermission(permissionOne);
        roleOne.addPermission(permissionTwo);
        roleOne.addPermission(permissionThree);
        roleOne = roleRepository.save(roleOne);
        
        Role resultOne = roleRepository.findById(roleOne.getKey()).get();
        
        assertEquals("Admin", resultOne.getRoleName());
        assertEquals(3, resultOne.getPermissions().size());
        assertEquals(organizationOne.getId(), resultOne.getKey().getOrganization().getId());
        
    }
    
    @Test
    public void addPermissionsListTest() {
        
        resourceOne = resourceRepository.save(resourceOne);
        resourceTwo = resourceRepository.save(resourceTwo);
        resourceThree = resourceRepository.save(resourceThree);

        organizationOne = organizationRepository.save(organizationOne);
        organizationTwo = organizationRepository.save(organizationTwo);

        permissionOne = new Permission(resourceOne,organizationOne);
        permissionTwo = new Permission(resourceTwo,organizationOne);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionFour = new Permission(resourceOne, organizationTwo);

        permissionOne = permissionRepository.save(permissionOne);
        permissionTwo = permissionRepository.save(permissionTwo);
        permissionThree = permissionRepository.save(permissionThree);
        permissionFour = permissionRepository.save(permissionFour);

        roleOne = new Role("Admin", organizationOne);
        roleOne.addPermissions(List.of(permissionOne, permissionTwo, permissionThree));
        roleOne = roleRepository.save(roleOne);
        
        Role resultOne = roleRepository.findById(roleOne.getKey()).get();
        
        assertEquals("Admin", resultOne.getRoleName());
        assertEquals(3, resultOne.getPermissions().size());
        assertEquals(organizationOne.getId(), resultOne.getKey().getOrganization().getId());
    }
}
