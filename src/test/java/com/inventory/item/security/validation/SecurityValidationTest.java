/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.validation;

import java.util.Optional;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;
import com.inventory.item.security.exception.UserAccountNotFoundException;
import com.inventory.item.security.exception.OrganizationNotFoundException;
import com.inventory.item.security.exception.UserAccountEmailDuplicatedException;

import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class SecurityValidationTest {

    @Mock
    private UserAccountRepository userAccountRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @InjectMocks
    private SecurityValidation securityValidation;
    
    private Organization organizationOne;
    
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    
    @BeforeEach
    void init(){
        
        organizationOne = new Organization("ROD","Republic of devs");
        organizationOne.setId(1L);
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setName("Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("elchiqui");
        userAccountOne.setEmail("chiqui@sponjoso.com");
        userAccountOne.setPassword("password123");
        userAccountOne.setOrganization(organizationOne);
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setId(2L);
        userAccountTwo.setName("Alexis");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Pumbin");
        userAccountTwo.setUsername("elchiqui");
        userAccountTwo.setEmail("chiqui@sponjoso.com");
        userAccountTwo.setPassword("password123");
        userAccountTwo.setOrganization(organizationOne);
        
    }
    
    @Test
    public void validateOrganizationTest() {
        
        when(organizationRepository.findById(1L)).thenReturn(Optional.of(organizationOne));
        
        Organization result = securityValidation.validateOrganization(1L);
        
        assertEquals(1L, result.getId());
        assertEquals("ROD", result.getName());
        assertEquals("Republic of devs", result.getDescription());
    }
    
    @Test
    public void validateOrganizationExceptionTest() {
        
        when(organizationRepository.findById(1L)).thenThrow(new OrganizationNotFoundException("1"));
        
        Exception exception = assertThrows(OrganizationNotFoundException.class, () -> {
            securityValidation.validateOrganization(1L);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("La organizacion con id 1 no se encuentra en el sistema"));
    }
    
    @Test
    public void validateUserAccountTest() {
        
        when(userAccountRepository.findById(1L)).thenReturn(Optional.of(userAccountOne));
        
        UserAccount result = securityValidation.validateUserAccount(1L);
        
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("Gael", result.getName());
        assertEquals("Alvarez",result.getPaternalLastName());
        assertEquals("Esponjoso",result.getMaternalLastName());
        assertEquals("elchiqui",result.getUsername());
        assertEquals("chiqui@sponjoso.com",result.getEmail());
        assertEquals("password123", result.getPassword());
        assertEquals(0, result.getRoles().size());
        assertEquals(1L, result.getOrganization().getId());
        assertEquals(false, result.isActive());
    }
    
    @Test
    public void validateUserAccountBeforeCreateTest() {

        when(userAccountRepository.findByEmail("chiqui@sponjoso.com")).thenReturn(null);
        assertDoesNotThrow(()
                -> securityValidation.validateUserAccountBeforeCreate("elchiqui", "chiqui@sponjoso.com"));
    }
    
    @Test
    public void validateUserAccountBeforeCreateEmailRepeatedExceptionTest() {
        
        when(userAccountRepository.findByEmail("chiqui@sponjoso.com")).thenReturn(userAccountOne);
        
        Exception exception = assertThrows(UserAccountEmailDuplicatedException.class, () -> {
            securityValidation.validateUserAccountBeforeCreate("elchiqui","chiqui@sponjoso.com");
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("Ya existe un usuario con el email chiqui@sponjoso.com"));
    }
    
    @Test
    public void validateUserAccountExceptionTest() {
        
        when(userAccountRepository.findById(1L)).thenThrow(new UserAccountNotFoundException());
        
        Exception exception = assertThrows(UserAccountNotFoundException.class, () -> {
            securityValidation.validateUserAccount(1L);
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("Usuario valido"));
    }
    
    @Test
    public void validateUserAccountBeforeUpdateNotFoundExceptionTest() {
        
        when(userAccountRepository.findById(1L)).thenThrow(new UserAccountNotFoundException("chiqui@sponjoso.com"));
        
        Exception exception = assertThrows(UserAccountNotFoundException.class, () -> {
            securityValidation.validateUserAccountBeforeUpdate(1L, "chiqui@sponjoso.com");
        });
        
        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("chiqui@sponjoso.com no es un usuario valido"));
        
    }
    
    @Test
    public void validateUserAccountBeforeUpdateEmailRepeatedExceptionTest() {

        when(userAccountRepository.findByEmail("chiqui@sponjoso.com")).thenReturn(userAccountTwo);

        Exception exception = assertThrows(UserAccountEmailDuplicatedException.class, () -> {
            securityValidation.validateUserAccountBeforeUpdate(1L, "chiqui@sponjoso.com");
        });

        String actualMessage = exception.getMessage();
        assertEquals(true, actualMessage.contains("Ya existe un usuario con el email chiqui@sponjoso.com"));
    }
}
