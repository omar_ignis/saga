/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.setup;

import java.util.Set;
import java.util.List;
import java.util.Arrays;
import java.lang.reflect.Method;
import java.util.stream.Collectors;

import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.Organization;

import com.inventory.item.controller.ItemController;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class PermissionSetupTest {

    @InjectMocks
    private PermissionSetup permissionSetup;

    private Organization organizationOne;

    @BeforeEach
    void init() {

        organizationOne = new Organization("rod", "republic of devs");
        organizationOne.setId(1L);
    }

    @Test
    public void createResourcesTest() {

        List<Method> methods = Arrays.asList(ItemController.class.getDeclaredMethods());
        assertEquals(8, methods.size());
        
        List<Resource> list = permissionSetup.createResources(ItemController.class, methods);
        assertEquals(8, list.size());
        assertEquals("items", list.get(0).getResourceDomain());
        assertEquals("items", list.get(1).getResourceDomain());
        assertEquals("items", list.get(2).getResourceDomain());
        assertEquals("items", list.get(3).getResourceDomain());
        assertEquals("items", list.get(4).getResourceDomain());
        assertEquals("items", list.get(5).getResourceDomain());
        assertEquals("items", list.get(6).getResourceDomain());
        assertEquals("items", list.get(7).getResourceDomain());
        
        Set<String> result = list.stream().map(Resource::getResourceName).collect(Collectors.toSet());
        
        assertEquals(true, result.contains("createItem"));
        assertEquals(true, result.contains("getItemsAsPage"));
        assertEquals(true, result.contains("getItemsAsList"));
        assertEquals(true, result.contains("getItem"));
        assertEquals(true, result.contains("getItemByname"));
        assertEquals(true, result.contains("updateItem"));
        assertEquals(true, result.contains("deleteItem"));
        assertEquals(true, result.contains("deleteAll"));
    }
    
    @Test
    public void createAllResourcesTest() {
        
        List<Resource> list = permissionSetup.createResources();
        
        assertTrue(list.size() > 0);
        
    }
    
    @Test
    public void createPermissionsTest() {
        
        List<Method> methods = Arrays.asList(ItemController.class.getDeclaredMethods());
        assertEquals(8, methods.size());
        
        List<Resource> resources = permissionSetup.createResources(ItemController.class, methods);
        assertEquals(8, resources.size());
        
        List<Permission> list = permissionSetup.createPermissions(organizationOne, resources);
        
        assertEquals(8, list.size());
        
    }
}
