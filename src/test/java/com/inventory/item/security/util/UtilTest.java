/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.util;

import java.util.List;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 *
 * @author ignis
 */
public class UtilTest {
    
    private Role roleOne;
    private Role roleTwo;
    private Role roleThree;
    private Role roleFour;
    private Role roleFive;
    
    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    private Resource resourceFour;
    private Resource resourceFive;
    private Resource resourceSix;
    
    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private Permission permissionFour;
    private Permission permissionFive;
    private Permission permissionSix;
    
    private Organization organizationOne;
    
    private List<Role> roleListOne;
    private List<Role> roleListTwo;
    private List<Permission> permissionListOne;
    private List<Permission> permissionListTwo;
    
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    
    @BeforeEach
    void init(){
        
        organizationOne = new Organization("ROD","Republic of devs");
        organizationOne.setId(1L);
        
        resourceOne = new Resource("resourceOne","domainOne");
        resourceTwo = new Resource("resourceTwo","domainOne");
        resourceThree = new Resource("resourceThree","domainOne");
        resourceFour = new Resource("resourceFour","domainOne");
        resourceFive = new Resource("resourceFive","domainOne");
        resourceSix = new Resource("resourceSix","domainOne");
        
        permissionOne = new Permission(resourceOne, organizationOne);
        permissionOne.setId(1L);
        permissionTwo = new Permission(resourceTwo, organizationOne);
        permissionTwo.setId(2L);
        permissionThree = new Permission(resourceThree, organizationOne);
        permissionThree.setId(3L);
        permissionFour = new Permission(resourceFour, organizationOne);
        permissionFour.setId(4L);
        permissionFive = new Permission(resourceFive, organizationOne);
        permissionFive.setId(5L);
        permissionSix = new Permission(resourceSix, organizationOne);
        permissionSix.setId(6L);
        
        roleOne = new Role("Admin", organizationOne);
        roleTwo = new Role("Staff",organizationOne);
        roleThree = new Role("Root",organizationOne);
        roleFour = new Role("Seller",organizationOne);
        roleFive = new Role("Manager",organizationOne);
    }
    
    @Test
    public void copyUserAccountTest() {
        
        userAccountOne = new UserAccount();
        userAccountOne.setId(1L);
        userAccountOne.setActive(true);
        userAccountOne.setName("Omar Enrique");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("ZArate");
        userAccountOne.setEmail("omar@alvarez.com");
        userAccountOne.setUsername("usernameone");
        userAccountOne.setPassword("password123");
        userAccountOne.setOrganization(organizationOne);
        userAccountOne.addRoles(List.of(roleOne, roleTwo, roleThree));
        
        userAccountTwo = new UserAccount();
        
        userAccountTwo = Util.copy(userAccountOne, userAccountTwo);
        
        assertEquals(null, userAccountTwo.getId());
        assertEquals("Omar Enrique", userAccountTwo.getName());
        assertEquals("Alvarez", userAccountTwo.getPaternalLastName());
        assertEquals("ZArate", userAccountTwo.getMaternalLastName());
        assertEquals("omar@alvarez.com", userAccountTwo.getEmail());
        assertEquals("usernameone", userAccountTwo.getUsername());
        assertEquals("password123", userAccountTwo.getPassword());
        assertEquals(null, userAccountTwo.getOrganization());
        assertEquals(0, userAccountTwo.getRoles().size());
    }
    
    @Test
    public void getDeletedPermissionsTest() {
        
        permissionFive.setDeleted(true);
        permissionSix.setDeleted(true);
        
        permissionListOne = List.of(permissionOne, permissionTwo, permissionThree, permissionFour, permissionFive, permissionSix);
        
        List<Permission> resultOne = Util.getDeletedPermissions(permissionListOne);
        
        assertEquals(2, resultOne.size());
        
        Permission resultTwo = resultOne.get(0);
        assertEquals(permissionFive.getId(), resultTwo.getId());
        assertEquals(permissionFive.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionFive.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionFive.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
        resultTwo = resultOne.get(1);
        assertEquals(permissionSix.getId(), resultTwo.getId());
        assertEquals(permissionSix.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionSix.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionSix.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
    }
    
    @Test
    public void getNotDeletedPermissionsTest() {
        
        permissionFive.setDeleted(true);
        permissionSix.setDeleted(true);
        
        permissionListOne = List.of(permissionOne, permissionTwo, permissionThree, permissionFour, permissionFive, permissionSix);
        
        List<Permission> resultOne = Util.getNotDeletedPermissions(permissionListOne);
        
        assertEquals(4, resultOne.size());
        
        Permission resultTwo = resultOne.get(0);
        assertEquals(permissionOne.getId(), resultTwo.getId());
        assertEquals(permissionOne.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionOne.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        
        resultTwo = resultOne.get(1);
        assertEquals(permissionTwo.getId(), resultTwo.getId());
        assertEquals(permissionTwo.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionTwo.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionTwo.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
        resultTwo = resultOne.get(2);
        assertEquals(permissionThree.getId(), resultTwo.getId());
        assertEquals(permissionThree.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionThree.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionThree.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
        resultTwo = resultOne.get(3);
        assertEquals(permissionFour.getId(), resultTwo.getId());
        assertEquals(permissionFour.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionFour.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionFour.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
    }
    
    @Test
    public void getNewRolePermissionsTest() {
        
        permissionListOne = List.of(permissionOne, permissionTwo, permissionThree, permissionFour, permissionFive, permissionSix);
        
        permissionListTwo = List.of(permissionOne, permissionThree, permissionFour, permissionFive);
        
        List<Permission> resultOne = Util.getNewRolePermissions(permissionListTwo, permissionListOne);
        
        assertEquals(2, resultOne.size());
        
        Permission resultTwo = resultOne.get(0);
        assertEquals(permissionTwo.getId(), resultTwo.getId());
        assertEquals(permissionTwo.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionTwo.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionTwo.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
        resultTwo = resultOne.get(1);
        assertEquals(permissionSix.getId(), resultTwo.getId());
        assertEquals(permissionSix.getOrganization().getId(), resultTwo.getOrganization().getId());
        assertEquals(permissionSix.getResource().getResourceName(), resultTwo.getResource().getResourceName());
        assertEquals(permissionSix.getResource().getResourceDomain(), resultTwo.getResource().getResourceDomain());
        
    }
    
    @Test
    public void getDeletedRolesTest() {

        roleFour.setDeleted(true);
        roleFive.setDeleted(true);

        roleListOne = List.of(roleOne, roleTwo, roleThree, roleFour, roleFive);

        List<Role> resultOne = Util.getDeletedRoles(roleListOne);

        assertEquals(2, resultOne.size());

        Role resultTwo = resultOne.get(0);
        assertEquals("Seller", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());

        resultTwo = resultOne.get(1);
        assertEquals("Manager", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());
    }
    
    @Test
    public void getNotDeletedRolesTest() {

        roleFour.setDeleted(true);
        roleFive.setDeleted(true);

        roleListOne = List.of(roleOne, roleTwo, roleThree, roleFour, roleFive);

        List<Role> resultOne = Util.getNotDeletedRoles(roleListOne);
        
        assertEquals(3, resultOne.size());
        
        Role resultTwo = resultOne.get(0);
        assertEquals("Admin", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());

        resultTwo = resultOne.get(1);
        assertEquals("Staff", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());

        resultTwo = resultOne.get(2);
        assertEquals("Root", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());
    }
    
    @Test
    public void getNewUserRolesTest() {
        
        roleListOne = List.of(roleOne, roleTwo, roleThree, roleFour, roleFive);
        roleListTwo = List.of(roleTwo, roleThree, roleFour);
        
        List<Role> resultOne = Util.getNewUserRoles(roleListTwo, roleListOne);
        
        assertEquals(2, resultOne.size());
        
        Role resultTwo = resultOne.get(0);
        assertEquals("Manager", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());

        resultTwo = resultOne.get(1);
        assertEquals("Admin", resultTwo.getRoleName());
        assertEquals(1L, resultTwo.getKey().getOrganization().getId());
        
    }
    
}
