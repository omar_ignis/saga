/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import com.inventory.item.service.impl.InputDocumentServiceImpl;
import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.util.Utils;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.document.impl.InputDocumentImpl;
import com.inventory.item.inventory.EntryInventorySummary;
import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class InputDocumentServiceImplTest {

    @Mock
    private InputDetailRepository inputDetailRepository;

    @Mock
    private EntryInventorySummary inputWarehouseSummaryService;

    @Mock
    private InputDocumentRepository inputDocumentRepository;

    @Mock
    private InputDocumentImpl inputDocumentFactoryImpl;

    @InjectMocks
    private InputDocumentServiceImpl inputDocumentService;

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;

    private Warehouse warehouseOne;

    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;
    private InputDetail inputDetailFour;
    private InputDetail inputDetailFive;
    private InputDetail inputDetailSix;
    private InputDetail inputDetailSeven;
    private InputDetail inputDetailEight;
    private InputDetail inputDetailNine;
    private InputDetail inputDetailTen;

    private InputDocument inputDocumentOne;
    private InputDocument inputDocumentTwo;
    private InputDocument inputDocumentThree;

    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        itemFour = new Item();
        itemFour.setId(4L);
        itemFour.setItemName("Item Vier");
        itemFour.setDescription("Item Vier desc");
        itemFour.setValuationType(ValuationType.AVERAGE);

        itemFive = new Item();
        itemFive.setId(3L);
        itemFive.setItemName("Item Five");
        itemFive.setDescription("Item Five desc");
        itemFive.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();
    }

    @Test
    public void saveTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000001");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> listOne = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        inputDetailFour = new InputDetail();
        inputDetailFour.setId(1L);
        inputDetailFour.setDescription("detail one");
        inputDetailFour.setItem(itemOne);
        inputDetailFour.setLineNumber(1);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailFour.setInputDocument(inputDocumentTwo);

        inputDetailFive = new InputDetail();
        inputDetailFive.setId(2L);
        inputDetailFive.setDescription("detail two");
        inputDetailFive.setItem(itemTwo);
        inputDetailFive.setLineNumber(2);
        inputDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailFive.setInputDocument(inputDocumentTwo);

        inputDetailSix = new InputDetail();
        inputDetailSix.setId(3L);
        inputDetailSix.setDescription("detail three");
        inputDetailSix.setItem(itemThree);
        inputDetailSix.setLineNumber(3);
        inputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailSix.setInputDocument(inputDocumentTwo);

        List<InputDetail> listTwo = List.of(inputDetailFour, inputDetailFive, inputDetailSix);

        when(inputDocumentFactoryImpl.createDocument(inputDocumentOne))
                .thenReturn(inputDocumentTwo);

        when(inputDocumentRepository.save(inputDocumentTwo))
                .thenReturn(inputDocumentTwo);

        when(inputDocumentFactoryImpl.generateDetails(inputDocumentTwo, listOne))
                .thenReturn(listTwo);

        when(inputDetailRepository.saveAll(listTwo)).thenReturn(listTwo);

        InputDocument result = inputDocumentService.save(inputDocumentOne, listOne);

        assertEquals("IN0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }

    @Test
    public void updateTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDocumentOne.setCounter(4);

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        inputDetailFour = new InputDetail();
        inputDetailFour.setId(4L);
        inputDetailFour.setDescription("detail four");
        inputDetailFour.setItem(itemFour);
        inputDetailFour.setLineNumber(4);
        inputDetailFour.setDeleted(true);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        inputDetailFour.setInputDocument(inputDocumentOne);

        inputDetailFive = new InputDetail();
        inputDetailFive.setDescription("detail five");
        inputDetailFive.setItem(itemFive);
        inputDetailFive.setLineNumber(5);
        inputDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));

        inputDetailSix = new InputDetail();
        inputDetailSix.setId(5L);
        inputDetailSix.setDescription("detail five");
        inputDetailSix.setItem(itemFive);
        inputDetailSix.setLineNumber(5);
        inputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        inputDetailSix.setInputDocument(inputDocumentOne);

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000001");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setCounter(5);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailSeven = new InputDetail();
        inputDetailSeven.setId(1L);
        inputDetailSeven.setDescription("detail one");
        inputDetailSeven.setItem(itemOne);
        inputDetailSeven.setLineNumber(1);
        inputDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailSeven.setInputDocument(inputDocumentTwo);

        inputDetailEight = new InputDetail();
        inputDetailEight.setId(2L);
        inputDetailEight.setDescription("detail two");
        inputDetailEight.setItem(itemTwo);
        inputDetailEight.setLineNumber(2);
        inputDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailEight.setInputDocument(inputDocumentTwo);

        inputDetailNine = new InputDetail();
        inputDetailNine.setId(3L);
        inputDetailNine.setDescription("detail three");
        inputDetailNine.setItem(itemThree);
        inputDetailNine.setLineNumber(3);
        inputDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailNine.setInputDocument(inputDocumentTwo);

        inputDetailTen = new InputDetail();
        inputDetailTen.setId(5L);
        inputDetailTen.setDescription("detail five");
        inputDetailTen.setItem(itemFive);
        inputDetailTen.setLineNumber(5);
        inputDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        inputDetailTen.setInputDocument(inputDocumentTwo);

        List<InputDetail> detailsOne = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailFour, inputDetailFive);
        List<InputDetail> detailsTwo = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailFive);
        List<InputDetail> detailsThree = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailSix);

        when(inputDocumentFactoryImpl.updateDocument(inputDocumentOne)).thenReturn(inputDocumentOne);
        when(inputDocumentRepository.save(inputDocumentOne)).thenReturn(inputDocumentOne);
        when(inputDocumentFactoryImpl.updateDetails(inputDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(inputDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        inputDetailFour.removeInputDocument(inputDocumentOne);
        doNothing().when(inputDetailRepository).deleteAll(List.of(inputDetailFour));

        InputDocument result = inputDocumentService.update(inputDocumentOne, detailsOne);

        assertEquals("IN0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());

        InputDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());

        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());

        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());

        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }

    @Test
    public void saveOrUpdateTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000001");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> listOne = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        inputDetailFour = new InputDetail();
        inputDetailFour.setId(1L);
        inputDetailFour.setDescription("detail one");
        inputDetailFour.setItem(itemOne);
        inputDetailFour.setLineNumber(1);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailFour.setInputDocument(inputDocumentTwo);

        inputDetailFive = new InputDetail();
        inputDetailFive.setId(2L);
        inputDetailFive.setDescription("detail two");
        inputDetailFive.setItem(itemTwo);
        inputDetailFive.setLineNumber(2);
        inputDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailFive.setInputDocument(inputDocumentTwo);

        inputDetailSix = new InputDetail();
        inputDetailSix.setId(3L);
        inputDetailSix.setDescription("detail three");
        inputDetailSix.setItem(itemThree);
        inputDetailSix.setLineNumber(3);
        inputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailSix.setInputDocument(inputDocumentTwo);

        List<InputDetail> listTwo = List.of(inputDetailFour, inputDetailFive, inputDetailSix);

        when(inputDocumentFactoryImpl.createDocument(inputDocumentOne))
                .thenReturn(inputDocumentTwo);

        when(inputDocumentRepository.save(inputDocumentTwo))
                .thenReturn(inputDocumentTwo);

        when(inputDocumentFactoryImpl.generateDetails(inputDocumentTwo, listOne))
                .thenReturn(listTwo);

        when(inputDetailRepository.saveAll(listTwo)).thenReturn(listTwo);

        InputDocument result = inputDocumentService.saveOrUpdate(inputDocumentOne, listOne);

        assertEquals("IN0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        //Update input document
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDocumentOne.setCounter(4);

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        inputDetailFour = new InputDetail();
        inputDetailFour.setId(4L);
        inputDetailFour.setDescription("detail four");
        inputDetailFour.setItem(itemFour);
        inputDetailFour.setLineNumber(4);
        inputDetailFour.setDeleted(true);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        inputDetailFour.setInputDocument(inputDocumentOne);

        inputDetailFive = new InputDetail();
        inputDetailFive.setDescription("detail five");
        inputDetailFive.setItem(itemFive);
        inputDetailFive.setLineNumber(5);
        inputDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));

        inputDetailSix = new InputDetail();
        inputDetailSix.setId(5L);
        inputDetailSix.setDescription("detail five");
        inputDetailSix.setItem(itemFive);
        inputDetailSix.setLineNumber(5);
        inputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        inputDetailSix.setInputDocument(inputDocumentOne);

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000001");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setCounter(5);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailSeven = new InputDetail();
        inputDetailSeven.setId(1L);
        inputDetailSeven.setDescription("detail one");
        inputDetailSeven.setItem(itemOne);
        inputDetailSeven.setLineNumber(1);
        inputDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailSeven.setInputDocument(inputDocumentTwo);

        inputDetailEight = new InputDetail();
        inputDetailEight.setId(2L);
        inputDetailEight.setDescription("detail two");
        inputDetailEight.setItem(itemTwo);
        inputDetailEight.setLineNumber(2);
        inputDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailEight.setInputDocument(inputDocumentTwo);

        inputDetailNine = new InputDetail();
        inputDetailNine.setId(3L);
        inputDetailNine.setDescription("detail three");
        inputDetailNine.setItem(itemThree);
        inputDetailNine.setLineNumber(3);
        inputDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailNine.setInputDocument(inputDocumentTwo);

        inputDetailTen = new InputDetail();
        inputDetailTen.setId(5L);
        inputDetailTen.setDescription("detail five");
        inputDetailTen.setItem(itemFive);
        inputDetailTen.setLineNumber(5);
        inputDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        inputDetailTen.setInputDocument(inputDocumentTwo);

        List<InputDetail> detailsOne = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailFour, inputDetailFive);
        List<InputDetail> detailsTwo = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailFive);
        List<InputDetail> detailsThree = List.of(inputDetailOne, inputDetailTwo, inputDetailThree, inputDetailSix);

        when(inputDocumentFactoryImpl.updateDocument(inputDocumentOne)).thenReturn(inputDocumentOne);
        when(inputDocumentRepository.save(inputDocumentOne)).thenReturn(inputDocumentOne);
        when(inputDocumentFactoryImpl.updateDetails(inputDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(inputDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        inputDetailFour.removeInputDocument(inputDocumentOne);
        doNothing().when(inputDetailRepository).deleteAll(List.of(inputDetailFour));

        result = inputDocumentService.saveOrUpdate(inputDocumentOne, detailsOne);

        assertEquals("IN0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());

        InputDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());

        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());

        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());

        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }

    @Test
    public void getDocumentByIdTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        InputDocument result = inputDocumentService.getDocumentById("IN0000000001");

        assertEquals("IN0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void getAllDocumentsTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000002");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));

        inputDocumentThree = new InputDocument();
        inputDocumentThree.setId("IN0000000003");
        inputDocumentThree.setDate(now);
        inputDocumentThree.setDescription("input document one");
        inputDocumentThree.setWarehouse(warehouseOne);
        inputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        inputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));

        Page<InputDocument> documents = new PageImpl(
                List.of(inputDocumentOne, inputDocumentTwo, inputDocumentThree));

        when(inputDocumentRepository.findByDeletedFalse(PageRequest.of(0, 10)))
                .thenReturn(documents);

        Page<InputDocument> page = inputDocumentService.getAllDocuments(PageRequest.of(0, 10));

        List<InputDocument> list = page.getContent();

        assertEquals(3, list.size());

        InputDocument result = list.get(0);
        assertEquals("IN0000000001", result.getId());

        result = list.get(1);
        assertEquals("IN0000000002", result.getId());

        result = list.get(2);
        assertEquals("IN0000000003", result.getId());

    }

    @Test
    public void getAllDocumentByIdLikeTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000002");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));

        inputDocumentThree = new InputDocument();
        inputDocumentThree.setId("IN0000000003");
        inputDocumentThree.setDate(now);
        inputDocumentThree.setDescription("input document one");
        inputDocumentThree.setWarehouse(warehouseOne);
        inputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        inputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));

        Page<InputDocument> documents = new PageImpl(
                List.of(inputDocumentOne, inputDocumentTwo, inputDocumentThree));
        when(inputDocumentRepository.findByIdLike("IN000", PageRequest.of(0, 10)))
                .thenReturn(documents);

        Page<InputDocument> page = inputDocumentService.getAllDocumentsByIdLike("IN000", PageRequest.of(0, 10));
        List<InputDocument> list = page.getContent();

        assertEquals(3, list.size());

        InputDocument result = list.get(0);
        assertEquals("IN0000000001", result.getId());

        result = list.get(1);
        assertEquals("IN0000000002", result.getId());

        result = list.get(2);
        assertEquals("IN0000000003", result.getId());
    }

    @Test
    public void getDetailsByDocumentAsListTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        List<InputDetail> deetails = List
                .of(inputDetailOne, inputDetailTwo, inputDetailThree);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        when(inputDetailRepository.findByInputDocument(inputDocumentOne))
                .thenReturn(deetails);

        List<InputDetail> list = inputDocumentService
                .getDetailsByDocument("IN0000000001");

        assertEquals(3, list.size());

        InputDetail result = list.get(0);
        assertEquals(1L, result.getId());

        result = list.get(1);
        assertEquals(2L, result.getId());

        result = list.get(2);
        assertEquals(3L, result.getId());

    }

    @Test
    public void getDetailsByDocumentAsPageTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        Page<InputDetail> details = new PageImpl(List
                .of(inputDetailOne, inputDetailTwo, inputDetailThree));

        when(inputDetailRepository.findByInputDocument(inputDocumentOne, PageRequest.of(0, 10)))
                .thenReturn(details);

        Page<InputDetail> page = inputDocumentService
                .getDetailsByDocument("IN0000000001", PageRequest.of(0, 10));

        List<InputDetail> list = page.getContent();

        assertEquals(3, list.size());

        InputDetail result = list.get(0);
        assertEquals(1L, result.getId());

        result = list.get(1);
        assertEquals(2L, result.getId());

        result = list.get(2);
        assertEquals(3L, result.getId());

    }

    @Test
    public void releaseTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDocumentOne.setCounter(4);

        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000001");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDocumentTwo.setCounter(4);
        inputDocumentTwo.setStatus(Status.RELEASED);

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentTwo);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentTwo);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentTwo);

        List<InputDetail> details = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));

        when(inputDocumentFactoryImpl.release(inputDocumentOne))
                .thenReturn(inputDocumentTwo);

        when(inputDocumentRepository.save(inputDocumentTwo)).thenReturn(inputDocumentTwo);

        when(inputDetailRepository.findByInputDocument(any(InputDocument.class)))
                .thenReturn(details);

        warehouseOne.setUsed(true);

        when(inputWarehouseSummaryService.updateInventory(inputDocumentTwo.getWarehouse().getId(), details))
                .thenReturn(warehouseOne);

        InputDocument result = inputDocumentService.release("IN0000000001");

        assertEquals("IN0000000001", result.getId());
        assertEquals(4, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("input document one", result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }

    @Test
    public void removeTest() throws Exception, Throwable {
        
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        inputDocumentOne.setCounter(4);
        inputDocumentOne.setStatus(Status.OPEN);

        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);
        
        List<InputDetail> list = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);
        
        when(inputDocumentRepository.findById("IN0000000001"))
                .thenReturn(Optional.of(inputDocumentOne));
        
        when(inputDetailRepository.findByInputDocument(inputDocumentOne))
                .thenReturn(list);
        doNothing().when(inputDetailRepository).deleteAll(list);
        doNothing().when(inputDocumentRepository).deleteById("IN0000000001");
        
        inputDocumentService.remove("IN0000000001");
        
    }

    @Test
    public void getTypeTest() {
        DocumentType result = inputDocumentService.getType();
        assertEquals(DocumentType.INPUT, result);
    }
}
