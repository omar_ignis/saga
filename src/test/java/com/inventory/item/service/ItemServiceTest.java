/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;

import com.inventory.item.util.Util;
import com.inventory.item.entity.Item;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.ItemRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;

/**
 *
 * @author ignis
 */

@ExtendWith(MockitoExtension.class)
public class ItemServiceTest {
    
    @Mock
    private ItemRepository itemRepository;
    
    @InjectMocks
    private ItemService itemService;
    
    private Item itemOne;
    private Item itemTwo;
    
    @Test
    public void saveTest() {
        
        itemOne = new Item("Item Eins", "Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        itemTwo = new Item("Item Eins", "Item Eins Desc");
        itemTwo.setId(1L);
        itemTwo.setValuationType(ValuationType.AVERAGE);
        
        when(itemRepository.save(itemOne)).thenReturn(itemTwo);
        Item result = itemService.save(itemOne);
        
        assertEquals(1L, result.getId());
        assertEquals("Item Eins", result.getItemName());
        assertEquals("Item Eins Desc", result.getDescription());
        assertEquals(ValuationType.AVERAGE, result.getValuationType());
        
    }
    
    @Test
    public void updateTest() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName("Item Updated");
        itemTwo.setDescription("Item Eins Desc");
        itemTwo.setValuationType(ValuationType.FIFO);
        
        when(itemRepository.findById(1L)).thenReturn(Optional.of(itemOne));
        when(itemRepository.save(itemTwo)).thenReturn(itemTwo);
        
        Item result = itemService.update(1L, itemTwo);
        
        assertEquals(1L, result.getId());
        assertEquals("Item Updated", result.getItemName());
        assertEquals(itemTwo.getDescription(), result.getDescription());
        assertEquals(ValuationType.FIFO, result.getValuationType());
        
    }
    
    @Test
    public void getAllItemsAsPageableTest() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei Desc");
        itemTwo.setValuationType(ValuationType.FIFO);
        
        Page<Item> items = new PageImpl(List.of(itemOne, itemTwo));

        when(itemRepository.findAllByOrderByIdAsc(PageRequest.of(0, 10))).thenReturn(items);
        
        List<Item> list = itemService.getAllItems((PageRequest.of(0, 10))).getContent();
        
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    
    @Test
    public void getAllItemsTest() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei Desc");
        itemTwo.setValuationType(ValuationType.FIFO);
        
        when(itemRepository.findAllByOrderByIdAsc()).thenReturn(List.of(itemOne, itemTwo));
        
        List<Item> list = itemService.getAllItems();
        
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    
    @Test
    public void getItemById() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        when(itemRepository.findById(1L)).thenReturn(Optional.of(itemOne));
        
        Item result = itemService.getItemById(1L);
        
        
        assertEquals(1L, result.getId());
        assertEquals("Item Eins", result.getItemName());
        assertEquals("Item Eins Desc", result.getDescription());
        assertEquals(ValuationType.AVERAGE, result.getValuationType());
    }
    
    @Test
    public void getByNameLike() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei Desc");
        itemTwo.setValuationType(ValuationType.FIFO);
        
        Page<Item> items = new PageImpl(List.of(itemOne, itemTwo));
        
        when(itemRepository.findByItemNameLike(Util.buildLike("Item"), PageRequest.of(0, 10)))
                .thenReturn(items);
        
        List<Item> list = itemService.getByNameLike("Item", PageRequest.of(0, 10)).getContent();
        
        assertNotNull(list);
        assertEquals(2, list.size());
    }
    
    @Test
    public void deleteOrDisableItemTest() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        
        when(itemRepository.findById(1L)).thenReturn(Optional.of(itemOne));
        doNothing().when(itemRepository).deleteById(1L);
        
        
        assertDoesNotThrow(() -> {
            itemService.deleteOrDisableItem(1L);
        });
    }
    
    @Test
    public void markAsUsedTest() {
        
        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne.setUsed(false);
        
        itemTwo = new Item();
        itemTwo.setId(1L);
        itemTwo.setItemName("Item Eins");
        itemTwo.setDescription("Item Eins Desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo.setUsed(true);
        
        when(itemRepository.save(itemOne)).thenReturn(itemTwo);
        
        Item result = itemService.markAsUsed(itemOne);
        
        assertEquals(1L, result.getId());
        assertEquals("Item Eins", result.getItemName());
        assertEquals("Item Eins Desc", result.getDescription());
        assertEquals(ValuationType.AVERAGE, result.getValuationType());
        
    }
}
