/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.OutputDetail;

import com.inventory.item.service.ItemService;
import com.inventory.item.inventory.impl.OutputInventoryImpl;
import com.inventory.item.validation.impl.WarehouseExitValidationImpl;

import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class OutputWarehouseSummaryServiceTest {
    
    @Mock
    private ItemService itemService;
    
    @Mock
    private WarehouseRepository warehouseRepository;
    
    @Mock
    private ItemSummaryRepository itemSummaryRepository;
    
    @Mock
    private WarehouseExitValidationImpl warehouseExitValidationImpl;
    
    @InjectMocks
    private OutputInventoryImpl outputWarehouseSummaryService;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    
    private Warehouse warehouseOne;
    
    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;
    private OutputDetail outputDetailFour;
    
    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        warehouseOne.setUsed(true);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemOne);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(30));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(600));
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemOne);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(300));
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setId(4L);
        outputDetailFour.setDescription("detail four");
        outputDetailFour.setItem(itemTwo);
        outputDetailFour.setLineNumber(4);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
    }
    
    @Test
    public void calcWeightedAverageTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        itemSummary.setQuantity(Utils.getAsBigDecimal(200));
        itemSummary.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummary.setTotalPrice(Utils.getAsBigDecimal(4000));
        
        ItemSummary result = outputWarehouseSummaryService.calcWeightedAverage(itemSummary, outputDetailOne);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(190), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3800), result.getTotalPrice());
        
        itemSummary.setQuantity(result.getQuantity());
        itemSummary.setUnitPrice(result.getUnitPrice());
        itemSummary.setTotalPrice(result.getTotalPrice());
        
        result = outputWarehouseSummaryService.calcWeightedAverage(itemSummary, outputDetailTwo);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(160), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3200), result.getTotalPrice());
        
    }
    
    @Test
    public void updateItemSummaryTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        itemSummary.setQuantity(Utils.getAsBigDecimal(100));
        itemSummary.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummary.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        ItemSummary result = outputWarehouseSummaryService.updateItemSummary(itemSummary, outputDetailThree);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(90), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(2700), result.getTotalPrice());
        
    }
    
    @Test
    public void updateInventoryTest() throws Exception{
        
        List<OutputDetail> details = List.of(outputDetailThree, outputDetailFour);
        
        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        when(warehouseRepository.findById(1L)).thenReturn(Optional.of(warehouseOne));
        when(warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemOne)).thenReturn(itemSummaryOne);
        when(warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemTwo)).thenReturn(itemSummaryTwo);
        doNothing().when(warehouseExitValidationImpl).validateInventoryBeforeUpdate(itemSummaryOne, outputDetailThree);
        doNothing().when(warehouseExitValidationImpl).validateInventoryBeforeUpdate(itemSummaryTwo, outputDetailFour);
        
        when(itemSummaryRepository.saveAll(any(List.class))).thenReturn(List.of(new ItemSummary()));
        
        when(warehouseRepository.save(warehouseOne)).thenReturn(warehouseOne);
        
        Warehouse result = outputWarehouseSummaryService.updateInventory(1L, details);
        
        assertEquals(warehouseOne.getId(), result.getId());
        assertEquals(true, result.isUsed());
        assertEquals("Warehouse one", result.getWarehouseName());
        assertEquals(2, warehouseOne.getItemSummary().size());
        
        ItemSummary itemSummary = result.getItemSummary().get(0);
        
        assertEquals(itemOne, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(90), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(2700), itemSummary.getTotalPrice());
        
        itemSummary = result.getItemSummary().get(1);
        
        assertEquals(itemTwo, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(80), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(10), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(800), itemSummary.getTotalPrice());        
        
    }
}
