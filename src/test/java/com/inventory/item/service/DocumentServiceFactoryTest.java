/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import com.inventory.item.service.factory.DocumentServiceFactory;
import com.inventory.item.service.impl.InputDocumentServiceImpl;
import com.inventory.item.service.impl.SalesReturnDocumentServiceImpl;
import com.inventory.item.service.impl.PurchaseReturnDocumentServiceImpl;
import com.inventory.item.service.impl.OutputDocumentServiceImpl;
import java.util.List;

import com.inventory.item.enums.DocumentType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class DocumentServiceFactoryTest {
    
    @Mock
    private static List<DocumentService> factories = 
            List.of(mock(InputDocumentServiceImpl.class),
            mock(SalesReturnDocumentServiceImpl.class),
            mock(OutputDocumentServiceImpl.class),
            mock(PurchaseReturnDocumentServiceImpl.class)
            );
    
    private static DocumentServiceFactory documentFactoryService;
    
    @BeforeAll
    static void setup(){
        
        when(factories.get(0).getType()).thenReturn(DocumentType.INPUT);
        when(factories.get(1).getType()).thenReturn(DocumentType.SALES_RETURN);
        when(factories.get(2).getType()).thenReturn(DocumentType.OUTPUT);
        when(factories.get(3).getType()).thenReturn(DocumentType.PURCHASE_RETURN);
        
        documentFactoryService = new DocumentServiceFactory(factories); 
    }
    
    @Test
    public void getInputDocumentServiceTest() {
        
        DocumentService result = documentFactoryService.getFactory(DocumentType.INPUT);
        
        assertEquals(result.getType(), DocumentType.INPUT);
    }
    
    @Test
    public void getSaleReturnDocumentServiceTest() {
        
        DocumentService result = documentFactoryService.getFactory(DocumentType.SALES_RETURN);
        
        assertEquals(result.getType(), DocumentType.SALES_RETURN);
    }
    
    @Test
    public void getOutputDocumentServiceTest() {
        
        DocumentService result = documentFactoryService.getFactory(DocumentType.OUTPUT);
        
        assertEquals(result.getType(), DocumentType.OUTPUT);
    }
    
    @Test
    public void getPurchaseReturnDocumentServiceTest() {
        
        DocumentService result = documentFactoryService.getFactory(DocumentType.PURCHASE_RETURN);
        
        assertEquals(result.getType(), DocumentType.PURCHASE_RETURN);
    }
}
