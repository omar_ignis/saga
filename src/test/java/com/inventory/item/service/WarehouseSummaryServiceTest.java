/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.key.ItemSummaryKey;

import com.inventory.item.util.Util;
import com.inventory.item.util.Constants;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;

/**
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class WarehouseSummaryServiceTest {
    
    @Mock
    private WarehouseRepository warehouseRepository;

    @Mock
    private ItemSummaryRepository itemSummaryRepository;

    @InjectMocks
    private WarehouseSummaryService warehouseSummaryService;

    private Warehouse warehouseOne;
    private ItemSummaryKey itemSummaryKeyOne;
    private ItemSummaryKey itemSummaryKeyTwo;
    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;

    private Item itemOne;
    private Item itemTwo;


    private final String itemNameOne = "Item One";
    private final String itemDescOne = "Item Desc One";
    private final String itemNameTwo = "Item Two";
    private final String itemDescTwo = "Item Desc Two";
    private final String warehouseNameOne = "Warehouse One";

    private BigDecimal decimal_5;
    private BigDecimal decimal_25;
    private BigDecimal decimal_5Multiply_25;
    private BigDecimal decimal_42;
    private BigDecimal decimal_42Multiply_120;
    private BigDecimal decimal_120;
    private BigDecimal decimal_10;
    private BigDecimal decimal_1000;
    private BigDecimal decimal_50;
    private BigDecimal decimal_800;
    private BigDecimal decimal_3;
    private BigDecimal decimal_12;
    private BigDecimal decimal_12Multiply_10;
    private BigDecimal decimal_7;
    private BigDecimal decimal_925;
    private BigDecimal decimal_38;
    private BigDecimal decimal_680;

    @BeforeEach
    void init() {
        decimal_5 = new BigDecimal("5.00");
        decimal_25 = new BigDecimal("25.00");
        decimal_5Multiply_25 = new BigDecimal("125.00");
        decimal_42 = new BigDecimal("42.00");
        decimal_120 = new BigDecimal("120.00");
        decimal_42Multiply_120 = new BigDecimal("5040.00");
        decimal_10 = new BigDecimal("10");
        decimal_1000 = new BigDecimal("1000");
        decimal_50 = new BigDecimal("50");
        decimal_800 = new BigDecimal("800");
        decimal_3 = new BigDecimal("3");
        decimal_12 = new BigDecimal("12");
        decimal_12Multiply_10 = new BigDecimal("120.00");
        decimal_7 = new BigDecimal("7.00");
        decimal_925 = new BigDecimal("925.00");
        decimal_38 = new BigDecimal("38.00");
        decimal_680 = new BigDecimal("680.00");

        //Crea primero un par de items que seran modificados en cantidad
        itemOne = new Item(itemNameOne, itemDescOne);
        itemOne.setId(1L);
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item(itemNameTwo, itemDescTwo);
        itemTwo.setId(2L);
        itemTwo.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse(warehouseNameOne);
        warehouseOne.setId(1L);
        
        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_5);
        itemSummaryOne.setTotalPrice(decimal_5Multiply_25);
        itemSummaryOne.setUnitPrice(decimal_25);
        itemSummaryOne.setLastUpdated(LocalDateTime.now());
        
        itemSummaryKeyTwo = new ItemSummaryKey(warehouseOne, itemTwo);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_42);
        itemSummaryTwo.setTotalPrice(decimal_42Multiply_120);
        itemSummaryTwo.setUnitPrice(decimal_120);
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());
    }
    
    @Test
    public void testGetAllItemsByWarehouse() {
        
        when(warehouseRepository.findById(1L)).thenReturn(Optional.of(warehouseOne));
        when(itemSummaryRepository.findByKeyWarehouse(warehouseOne))
                .thenReturn(List.of(itemSummaryOne, itemSummaryTwo));
        List<Item> resultOne = warehouseSummaryService.getAllItemsByWarehouse(1L);
        
        assertEquals(2, resultOne.size());
        
        Item resultTwo = resultOne.get(0);
        assertEquals(resultTwo.getId(), itemOne.getId());
        assertEquals(resultTwo.getItemName(), itemOne.getItemName());
        assertEquals(resultTwo.getDescription(), itemOne.getDescription());
        assertEquals(resultTwo.getValuationType(), itemOne.getValuationType());
        
        resultTwo = resultOne.get(1);
        assertEquals(resultTwo.getId(), itemTwo.getId());
        assertEquals(resultTwo.getItemName(), itemTwo.getItemName());
        assertEquals(resultTwo.getDescription(), itemTwo.getDescription());
        assertEquals(resultTwo.getValuationType(), itemTwo.getValuationType());
    }

//    @Test
//    public void testSaveItemSummaryWithInputDocument() {
//
//        //valida que tengan id
//
//        //Crea el documento
//        documentKeyOne = new IODocumentKey(Constants.INPUT);
//        documentOne = new IODocument(documentKeyOne);
//        documentOne.setWarehouse(warehouseOne);
//        documentOne.setStatus(Status.OPEN);
//        documentOne.setDocDate(LocalDateTime.now());
//
//        detailOne = new IODetail();
//        detailOne.setLineNbr(1);
//        detailOne.setItem(itemOne);
//        detailOne.setDescription(itemOne.getDescription());
//        detailOne.setQuantity(decimal_5);
//        detailOne.setUnitPrice(decimal_25);
//        detailOne.setTotalPrice(Util.multiply(decimal_5, decimal_25));
//
//        detailTwo = new IODetail();
//        detailTwo.setLineNbr(2);
//        detailTwo.setItem(itemTwo);
//        detailTwo.setDescription(itemTwo.getDescription());
//        detailTwo.setQuantity(decimal_42);
//        detailTwo.setUnitPrice(decimal_120);
//        detailTwo.setTotalPrice(Util.multiply(decimal_42, decimal_120));
//
//        documentOne.getDetails().add(detailOne);
//        documentOne.getDetails().add(detailTwo);
//
//        //Guarda el documento
//        documentOne = documentService.save(documentOne);
//
//        //Simula la liberacion del documento
//        warehouseOne = warehouseService.save(documentOne);
//        assertEquals(decimal_5, warehouseOne.getItemSummary().get(0).getQuantity());
//        assertEquals(decimal_25, warehouseOne.getItemSummary().get(0).getUnitPrice());
//        assertEquals(decimal_5Multiply_25, warehouseOne.getItemSummary().get(0).getTotalPrice());
//        assertEquals(decimal_42, warehouseOne.getItemSummary().get(1).getQuantity());
//        assertEquals(decimal_120, warehouseOne.getItemSummary().get(1).getUnitPrice());
//        assertEquals(decimal_42Multiply_120, warehouseOne.getItemSummary().get(1).getTotalPrice());
//    }

//    @Test
//    public void testUpdateItemSummaryWithInputDocument() {
//
//        //valida que tengan id
//        assertTrue(itemOne.getId() > 0);
//        assertTrue(itemTwo.getId() > 0);
//
//        assertTrue(warehouseOne.getId() > 0);
//        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());
//
//        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
//        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
//        itemSummaryOne.setQuantity(BigDecimal.ZERO);
//        itemSummaryOne.setUnitPrice(BigDecimal.ZERO);
//        itemSummaryOne.setTotalPrice(BigDecimal.ZERO);
//        itemSummaryOne.setLastUpdated(LocalDateTime.now());
//
//        itemSummaryKeyTwo = new ItemSummaryKey(warehouseOne, itemTwo);
//        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
//        itemSummaryTwo.setQuantity(BigDecimal.ZERO);
//        itemSummaryTwo.setUnitPrice(BigDecimal.ZERO);
//        itemSummaryTwo.setTotalPrice(BigDecimal.ZERO);
//        itemSummaryTwo.setLastUpdated(LocalDateTime.now());
//
//        warehouseOne.getItemSummary().add(itemSummaryOne);
//        warehouseOne.getItemSummary().add(itemSummaryTwo);
//
//        //Guarda el warehouse y sus itemSummary en 0
//        warehouseOne = warehouseService.save(warehouseOne);
//
//        //Crea el documento
//        documentKeyOne = new IODocumentKey(Constants.INPUT);
//        documentOne = new IODocument(documentKeyOne);
//        documentOne.setWarehouse(warehouseOne);
//        documentOne.setStatus(Status.OPEN);
//        documentOne.setDocDate(LocalDateTime.now());
//
//        detailOne = new IODetail();
//        detailOne.setLineNbr(1);
//        detailOne.setItem(itemOne);
//        detailOne.setDescription(itemOne.getDescription());
//        detailOne.setQuantity(decimal_5);
//        detailOne.setUnitPrice(decimal_25);
//        detailOne.setTotalPrice(Util.multiply(decimal_5, decimal_25));
//
//        detailTwo = new IODetail();
//        detailTwo.setLineNbr(2);
//        detailTwo.setItem(itemTwo);
//        detailTwo.setDescription(itemTwo.getDescription());
//        detailTwo.setQuantity(decimal_42);
//        detailTwo.setUnitPrice(decimal_120);
//        detailTwo.setTotalPrice(Util.multiply(decimal_42, decimal_120));
//
//        documentOne.getDetails().add(detailOne);
//        documentOne.getDetails().add(detailTwo);
//
//        //Guarda el documento
//        documentOne = documentService.save(documentOne);
//
//        //Simula la liberacion del documento
//        warehouseOne = warehouseService.save(documentOne);
//        assertEquals(decimal_5, warehouseOne.getItemSummary().get(0).getQuantity());
//        assertEquals(decimal_25, warehouseOne.getItemSummary().get(0).getUnitPrice());
//        assertEquals(decimal_5Multiply_25, warehouseOne.getItemSummary().get(0).getTotalPrice());
//        assertEquals(decimal_42, warehouseOne.getItemSummary().get(1).getQuantity());
//        assertEquals(decimal_120, warehouseOne.getItemSummary().get(1).getUnitPrice());
//        assertEquals(decimal_42Multiply_120, warehouseOne.getItemSummary().get(1).getTotalPrice());
//    }
//
//    @Test
//    public void testUpdateItemSummaryWithOutputDocument() {
//
//        //valida que tengan id
//        assertTrue(itemOne.getId() > 0);
//        assertTrue(itemTwo.getId() > 0);
//
//        assertTrue(warehouseOne.getId() > 0);
//        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());
//
//        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
//        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
//        itemSummaryOne.setQuantity(decimal_10);
//        itemSummaryOne.setUnitPrice(Util.divide(decimal_1000, decimal_10));
//        itemSummaryOne.setTotalPrice(decimal_1000);
//        itemSummaryOne.setLastUpdated(LocalDateTime.now());
//
//        itemSummaryKeyTwo = new ItemSummaryKey(warehouseOne, itemTwo);
//        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
//        itemSummaryTwo.setQuantity(decimal_50);
//        itemSummaryTwo.setUnitPrice(Util.divide(decimal_800, decimal_50));
//        itemSummaryTwo.setTotalPrice(decimal_800);
//        itemSummaryTwo.setLastUpdated(LocalDateTime.now());
//
//        warehouseOne.getItemSummary().add(itemSummaryOne);
//        warehouseOne.getItemSummary().add(itemSummaryTwo);
//
//        //Guarda el warehouse y sus itemSummary en 0
//        warehouseOne = warehouseService.save(warehouseOne);
//
//        //Crea el documento
//        documentKeyOne = new IODocumentKey(Constants.OUTPUT);
//        documentOne = new IODocument(documentKeyOne);
//        documentOne.setWarehouse(warehouseOne);
//        documentOne.setStatus(Status.OPEN);
//        documentOne.setDocDate(LocalDateTime.now());
//
//        detailOne = new IODetail();
//        detailOne.setLineNbr(1);
//        detailOne.setItem(itemOne);
//        detailOne.setDescription(itemOne.getDescription());
//        detailOne.setQuantity(decimal_3);
//        detailOne.setUnitPrice(decimal_25);
//        detailOne.setTotalPrice(Util.multiply(decimal_3, decimal_25));
//
//        detailTwo = new IODetail();
//        detailTwo.setLineNbr(2);
//        detailTwo.setItem(itemTwo);
//        detailTwo.setDescription(itemTwo.getDescription());
//        detailTwo.setQuantity(decimal_12);
//        detailTwo.setUnitPrice(decimal_10);
//        detailTwo.setTotalPrice(Util.multiply(decimal_12, decimal_10));
//
//        documentOne.getDetails().add(detailOne);
//        documentOne.getDetails().add(detailTwo);
//
//        //Guarda el documento
//        documentOne = documentService.save(documentOne);
//
//        //Simula la liberacion del documento
//        warehouseOne = warehouseService.save(documentOne);
//        assertEquals(decimal_7, warehouseOne.getItemSummary().get(0).getQuantity());
//        assertEquals(decimal_925, warehouseOne.getItemSummary().get(0).getTotalPrice());
//        assertEquals(decimal_38, warehouseOne.getItemSummary().get(1).getQuantity());
//        assertEquals(decimal_680, warehouseOne.getItemSummary().get(1).getTotalPrice());
//    }
}
