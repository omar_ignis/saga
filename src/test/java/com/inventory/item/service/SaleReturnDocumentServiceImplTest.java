/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;


import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import com.inventory.item.inventory.EntryInventorySummary;
import com.inventory.item.document.impl.SaleReturnDocumentImpl;
import com.inventory.item.repository.SalesReturnDetailRepository;
import com.inventory.item.repository.SalesReturnDocumentRepository;
import com.inventory.item.service.impl.SalesReturnDocumentServiceImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class SaleReturnDocumentServiceImplTest {
    
    @Mock
    private SalesReturnDetailRepository salesReturnDetailRepository;
    
    @Mock
    private EntryInventorySummary salesReturnWarehouseSummaryService;
    
    @Mock
    private SalesReturnDocumentRepository salesReturnDocumentRepository;
    
    @Mock
    private SaleReturnDocumentImpl salesReturnDocumentFactoryImpl;
    
    @InjectMocks
    private SalesReturnDocumentServiceImpl salesReturnDocumentService;
    
    private LocalDateTime now;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;
    
    private Warehouse warehouseOne;
    
    private SalesReturnDetail salesReturnDetailOne;
    private SalesReturnDetail salesReturnDetailTwo;
    private SalesReturnDetail salesReturnDetailThree;
    private SalesReturnDetail salesReturnDetailFour;
    private SalesReturnDetail salesReturnDetailFive;
    private SalesReturnDetail salesReturnDetailSix;
    private SalesReturnDetail salesReturnDetailSeven;
    private SalesReturnDetail salesReturnDetailEight;
    private SalesReturnDetail salesReturnDetailNine;
    private SalesReturnDetail salesReturnDetailTen;
    
    private SalesReturnDocument salesReturnDocumentOne;
    private SalesReturnDocument salesReturnDocumentTwo;
    private SalesReturnDocument salesReturnDocumentThree;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        
        itemFour = new Item();
        itemFour.setId(4L);
        itemFour.setItemName("Item Vier");
        itemFour.setDescription("Item Vier desc");
        itemFour.setValuationType(ValuationType.AVERAGE);
        
        itemFive = new Item();
        itemFive.setId(3L);
        itemFive.setItemName("Item Five");
        itemFive.setDescription("Item Five desc");
        itemFive.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");

        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000001");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<SalesReturnDetail> listOne = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree);
        
        salesReturnDetailFour = new SalesReturnDetail();
        salesReturnDetailFour.setId(1L);
        salesReturnDetailFour.setDescription("detail one");
        salesReturnDetailFour.setItem(itemOne);
        salesReturnDetailFour.setLineNumber(1);
        salesReturnDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailFour.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailFive = new SalesReturnDetail();
        salesReturnDetailFive.setId(2L);
        salesReturnDetailFive.setDescription("detail two");
        salesReturnDetailFive.setItem(itemTwo);
        salesReturnDetailFive.setLineNumber(2);
        salesReturnDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailFive.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailSix = new SalesReturnDetail();
        salesReturnDetailSix.setId(3L);
        salesReturnDetailSix.setDescription("detail three");
        salesReturnDetailSix.setItem(itemThree);
        salesReturnDetailSix.setLineNumber(3);
        salesReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailSix.setSalesReturnDocument(salesReturnDocumentTwo);
        
        List<SalesReturnDetail> listTwo = List.of(salesReturnDetailFour, salesReturnDetailFive, salesReturnDetailSix);
        
        when(salesReturnDocumentFactoryImpl.createDocument(salesReturnDocumentOne))
                .thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDocumentRepository.save(salesReturnDocumentTwo))
                .thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDocumentFactoryImpl.generateDetails(salesReturnDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(salesReturnDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        SalesReturnDocument result = salesReturnDocumentService.save(salesReturnDocumentOne, listOne);
        
        assertEquals("SR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void updateTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        salesReturnDocumentOne.setCounter(4);
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailFour = new SalesReturnDetail();
        salesReturnDetailFour.setId(4L);
        salesReturnDetailFour.setDescription("detail four");
        salesReturnDetailFour.setItem(itemFour);
        salesReturnDetailFour.setLineNumber(4);
        salesReturnDetailFour.setDeleted(true);
        salesReturnDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        salesReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        salesReturnDetailFour.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailFive = new SalesReturnDetail();
        salesReturnDetailFive.setDescription("detail five");
        salesReturnDetailFive.setItem(itemFive);
        salesReturnDetailFive.setLineNumber(5);
        salesReturnDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        salesReturnDetailSix = new SalesReturnDetail();
        salesReturnDetailSix.setId(5L);
        salesReturnDetailSix.setDescription("detail five");
        salesReturnDetailSix.setItem(itemFive);
        salesReturnDetailSix.setLineNumber(5);
        salesReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        salesReturnDetailSix.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000001");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setCounter(5);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailSeven = new SalesReturnDetail();
        salesReturnDetailSeven.setId(1L);
        salesReturnDetailSeven.setDescription("detail one");
        salesReturnDetailSeven.setItem(itemOne);
        salesReturnDetailSeven.setLineNumber(1);
        salesReturnDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailSeven.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailEight = new SalesReturnDetail();
        salesReturnDetailEight.setId(2L);
        salesReturnDetailEight.setDescription("detail two");
        salesReturnDetailEight.setItem(itemTwo);
        salesReturnDetailEight.setLineNumber(2);
        salesReturnDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailEight.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailNine = new SalesReturnDetail();
        salesReturnDetailNine.setId(3L);
        salesReturnDetailNine.setDescription("detail three");
        salesReturnDetailNine.setItem(itemThree);
        salesReturnDetailNine.setLineNumber(3);
        salesReturnDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailNine.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailTen = new SalesReturnDetail();
        salesReturnDetailTen.setId(5L);
        salesReturnDetailTen.setDescription("detail five");
        salesReturnDetailTen.setItem(itemFive);
        salesReturnDetailTen.setLineNumber(5);
        salesReturnDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        salesReturnDetailTen.setSalesReturnDocument(salesReturnDocumentTwo);
        
        List<SalesReturnDetail> detailsOne = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree, salesReturnDetailFour, salesReturnDetailFive);
        List<SalesReturnDetail> detailsTwo = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree,salesReturnDetailFive);
        List<SalesReturnDetail> detailsThree = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree, salesReturnDetailSix);
        
        when(salesReturnDocumentFactoryImpl.updateDocument(salesReturnDocumentOne)).thenReturn(salesReturnDocumentOne);
        when(salesReturnDocumentRepository.save(salesReturnDocumentOne)).thenReturn(salesReturnDocumentOne);
        when(salesReturnDocumentFactoryImpl.updateDetails(salesReturnDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(salesReturnDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        salesReturnDetailFour.removeSalesReturnDocument(salesReturnDocumentOne);
        doNothing().when(salesReturnDetailRepository).deleteAll(List.of(salesReturnDetailFour));
        
        SalesReturnDocument result = salesReturnDocumentService.update(salesReturnDocumentOne, detailsOne);
        
        assertEquals("SR0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        SalesReturnDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }    
    
    @Test
    public void saveOrUpdateTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000001");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<SalesReturnDetail> listOne = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree);
        
        salesReturnDetailFour = new SalesReturnDetail();
        salesReturnDetailFour.setId(1L);
        salesReturnDetailFour.setDescription("detail one");
        salesReturnDetailFour.setItem(itemOne);
        salesReturnDetailFour.setLineNumber(1);
        salesReturnDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailFour.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailFive = new SalesReturnDetail();
        salesReturnDetailFive.setId(2L);
        salesReturnDetailFive.setDescription("detail two");
        salesReturnDetailFive.setItem(itemTwo);
        salesReturnDetailFive.setLineNumber(2);
        salesReturnDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailFive.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailSix = new SalesReturnDetail();
        salesReturnDetailSix.setId(3L);
        salesReturnDetailSix.setDescription("detail three");
        salesReturnDetailSix.setItem(itemThree);
        salesReturnDetailSix.setLineNumber(3);
        salesReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailSix.setSalesReturnDocument(salesReturnDocumentTwo);
        
        List<SalesReturnDetail> listTwo = List.of(salesReturnDetailFour, salesReturnDetailFive, salesReturnDetailSix);
        
        when(salesReturnDocumentFactoryImpl.createDocument(salesReturnDocumentOne))
                .thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDocumentRepository.save(salesReturnDocumentTwo))
                .thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDocumentFactoryImpl.generateDetails(salesReturnDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(salesReturnDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        SalesReturnDocument result = salesReturnDocumentService.saveOrUpdate(salesReturnDocumentOne, listOne);
        
        assertEquals("SR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
        
        //Update sales return document
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        salesReturnDocumentOne.setCounter(4);
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailFour = new SalesReturnDetail();
        salesReturnDetailFour.setId(4L);
        salesReturnDetailFour.setDescription("detail four");
        salesReturnDetailFour.setItem(itemFour);
        salesReturnDetailFour.setLineNumber(4);
        salesReturnDetailFour.setDeleted(true);
        salesReturnDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        salesReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        salesReturnDetailFour.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailFive = new SalesReturnDetail();
        salesReturnDetailFive.setDescription("detail five");
        salesReturnDetailFive.setItem(itemFive);
        salesReturnDetailFive.setLineNumber(5);
        salesReturnDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        salesReturnDetailSix = new SalesReturnDetail();
        salesReturnDetailSix.setId(5L);
        salesReturnDetailSix.setDescription("detail five");
        salesReturnDetailSix.setItem(itemFive);
        salesReturnDetailSix.setLineNumber(5);
        salesReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        salesReturnDetailSix.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000001");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setCounter(5);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailSeven = new SalesReturnDetail();
        salesReturnDetailSeven.setId(1L);
        salesReturnDetailSeven.setDescription("detail one");
        salesReturnDetailSeven.setItem(itemOne);
        salesReturnDetailSeven.setLineNumber(1);
        salesReturnDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailSeven.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailEight = new SalesReturnDetail();
        salesReturnDetailEight.setId(2L);
        salesReturnDetailEight.setDescription("detail two");
        salesReturnDetailEight.setItem(itemTwo);
        salesReturnDetailEight.setLineNumber(2);
        salesReturnDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailEight.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailNine = new SalesReturnDetail();
        salesReturnDetailNine.setId(3L);
        salesReturnDetailNine.setDescription("detail three");
        salesReturnDetailNine.setItem(itemThree);
        salesReturnDetailNine.setLineNumber(3);
        salesReturnDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailNine.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailTen = new SalesReturnDetail();
        salesReturnDetailTen.setId(5L);
        salesReturnDetailTen.setDescription("detail five");
        salesReturnDetailTen.setItem(itemFive);
        salesReturnDetailTen.setLineNumber(5);
        salesReturnDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        salesReturnDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        salesReturnDetailTen.setSalesReturnDocument(salesReturnDocumentTwo);
        
        List<SalesReturnDetail> detailsOne = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree, salesReturnDetailFour, salesReturnDetailFive);
        List<SalesReturnDetail> detailsTwo = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree,salesReturnDetailFive);
        List<SalesReturnDetail> detailsThree = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree, salesReturnDetailSix);
        
        when(salesReturnDocumentFactoryImpl.updateDocument(salesReturnDocumentOne)).thenReturn(salesReturnDocumentOne);
        when(salesReturnDocumentRepository.save(salesReturnDocumentOne)).thenReturn(salesReturnDocumentOne);
        when(salesReturnDocumentFactoryImpl.updateDetails(salesReturnDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(salesReturnDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        salesReturnDetailFour.removeSalesReturnDocument(salesReturnDocumentOne);
        doNothing().when(salesReturnDetailRepository).deleteAll(List.of(salesReturnDetailFour));
        
        result = salesReturnDocumentService.saveOrUpdate(salesReturnDocumentOne, detailsOne);
        
        assertEquals("SR0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        SalesReturnDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }
    
    @Test
    public void getDocumentByIdTest() throws Exception, Throwable {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);

        when(salesReturnDocumentRepository.findById("SR0000000001"))
                .thenReturn(Optional.of(salesReturnDocumentOne));

        SalesReturnDocument result = salesReturnDocumentService.getDocumentById("SR0000000001");

        assertEquals("SR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void getAllDocumentsTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000002");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        salesReturnDocumentThree = new SalesReturnDocument();
        salesReturnDocumentThree.setId("SR0000000003");
        salesReturnDocumentThree.setDate(now);
        salesReturnDocumentThree.setDescription("sales return document one");
        salesReturnDocumentThree.setWarehouse(warehouseOne);
        salesReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        salesReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<SalesReturnDocument> documents = new PageImpl(
                List.of(salesReturnDocumentOne, salesReturnDocumentTwo, salesReturnDocumentThree));
        
        when(salesReturnDocumentRepository.findByDeletedFalse(PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<SalesReturnDocument> page = salesReturnDocumentService.getAllDocuments(PageRequest.of(0, 10));
        
        List<SalesReturnDocument> list  = page.getContent();
        
        assertEquals(3, list.size());
        
        SalesReturnDocument result = list.get(0);
        assertEquals("SR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("SR0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("SR0000000003", result.getId());
        
    }

    @Test
    public void getAllDocumentByIdLikeTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000002");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        salesReturnDocumentThree = new SalesReturnDocument();
        salesReturnDocumentThree.setId("SR0000000003");
        salesReturnDocumentThree.setDate(now);
        salesReturnDocumentThree.setDescription("sales return document one");
        salesReturnDocumentThree.setWarehouse(warehouseOne);
        salesReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        salesReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<SalesReturnDocument> documents = new PageImpl(
                 List.of(salesReturnDocumentOne, salesReturnDocumentTwo, salesReturnDocumentThree));
        when(salesReturnDocumentRepository.findByIdLike("SR000", PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<SalesReturnDocument> page = salesReturnDocumentService.getAllDocumentsByIdLike("SR000", PageRequest.of(0, 10));
        List<SalesReturnDocument> list = page.getContent();
        
        assertEquals(3, list.size());
        
        SalesReturnDocument result = list.get(0);
        assertEquals("SR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("SR0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("SR0000000003", result.getId());
    }
    
    @Test
    public void getDetailsByDocumentAsListTest() throws Exception, Throwable {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);
        
        List<SalesReturnDetail> deetails = List
                .of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree);
        
        when(salesReturnDocumentRepository.findById("SR0000000001"))
                .thenReturn(Optional.of(salesReturnDocumentOne));
        
        when(salesReturnDetailRepository.findBySalesReturnDocument(salesReturnDocumentOne))
                .thenReturn(deetails);
        
        List<SalesReturnDetail> list = salesReturnDocumentService
                .getDetailsByDocument("SR0000000001");
        
        assertEquals(3, list.size());
        
        SalesReturnDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    @Test
    public void getDetailsByDocumentAsPageTest() throws Exception, Throwable {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);
        
        when(salesReturnDocumentRepository.findById("SR0000000001"))
                .thenReturn(Optional.of(salesReturnDocumentOne));
        
        Page<SalesReturnDetail> details =  new PageImpl(List
                .of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree));
        
        when(salesReturnDetailRepository.findBySalesReturnDocument(salesReturnDocumentOne, PageRequest.of(0, 10)))
                .thenReturn(details);
        
        Page<SalesReturnDetail> page = salesReturnDocumentService
                .getDetailsByDocument("SR0000000001", PageRequest.of(0, 10));
        
        List<SalesReturnDetail> list = page.getContent();
        
        assertEquals(3, list.size());
        
        SalesReturnDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    @Test
    public void releaseTest() throws Exception, Throwable{
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        salesReturnDocumentOne.setCounter(4);
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setId("SR0000000001");
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setDescription("sales return document one");
        salesReturnDocumentTwo.setWarehouse(warehouseOne);
        salesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        salesReturnDocumentTwo.setCounter(4);
        salesReturnDocumentTwo.setStatus(Status.RELEASED);
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setId(1L);
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setId(2L);
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentTwo);
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setId(3L);
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentTwo);
        
        List<SalesReturnDetail> details = List.of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree);
        
        when(salesReturnDocumentRepository.findById("SR0000000001"))
                .thenReturn(Optional.of(salesReturnDocumentOne));
        
        when(salesReturnDocumentFactoryImpl.release(salesReturnDocumentOne))
                .thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDocumentRepository.save(salesReturnDocumentTwo)).thenReturn(salesReturnDocumentTwo);
        
        when(salesReturnDetailRepository.findBySalesReturnDocument(any(SalesReturnDocument.class)))
                .thenReturn(details);
        
        warehouseOne.setUsed(true);
        
        when(salesReturnWarehouseSummaryService.updateInventory(salesReturnDocumentTwo.getWarehouse().getId(), details))
                .thenReturn(warehouseOne);
        
        SalesReturnDocument result = salesReturnDocumentService.release("SR0000000001");
        
        assertEquals("SR0000000001", result.getId());
        assertEquals(4, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void getTypeTest() {
        DocumentType result = salesReturnDocumentService.getType();
        assertEquals(DocumentType.SALES_RETURN, result);
    }
    
}
