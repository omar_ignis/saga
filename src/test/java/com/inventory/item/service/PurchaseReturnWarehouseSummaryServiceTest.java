/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;

import com.inventory.item.util.Utils;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.PurchaseReturnDetail;

import com.inventory.item.service.ItemService;
import com.inventory.item.inventory.impl.PurchaseReturnInventoryImpl;
import com.inventory.item.validation.impl.WarehouseExitValidationImpl;

import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class PurchaseReturnWarehouseSummaryServiceTest {
    
    @Mock
    private ItemService itemService;
    
    @Mock
    private WarehouseRepository warehouseRepository;
    
    @Mock
    private ItemSummaryRepository itemSummaryRepository;
    
    @Mock
    private WarehouseExitValidationImpl warehouseExitValidationImpl;
    
    @InjectMocks
    private PurchaseReturnInventoryImpl purchaseReturnWarehouseSummaryService;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    
    private Warehouse warehouseOne;
    
    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;
    private PurchaseReturnDetail purchaseReturnDetailFour;
    
    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        warehouseOne.setUsed(true);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemOne);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(30));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(600));
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemOne);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(300));
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setId(4L);
        purchaseReturnDetailFour.setDescription("detail four");
        purchaseReturnDetailFour.setItem(itemTwo);
        purchaseReturnDetailFour.setLineNumber(4);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
    }
    
    @Test
    public void calcWeightedAverageTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        itemSummary.setQuantity(Utils.getAsBigDecimal(200));
        itemSummary.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummary.setTotalPrice(Utils.getAsBigDecimal(4000));
        
        ItemSummary result = purchaseReturnWarehouseSummaryService.calcWeightedAverage(itemSummary, purchaseReturnDetailOne);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(190), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3800), result.getTotalPrice());
        
        itemSummary.setQuantity(result.getQuantity());
        itemSummary.setUnitPrice(result.getUnitPrice());
        itemSummary.setTotalPrice(result.getTotalPrice());
        
        result = purchaseReturnWarehouseSummaryService.calcWeightedAverage(itemSummary, purchaseReturnDetailTwo);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(160), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3200), result.getTotalPrice());
        
    }
    
    @Test
    public void updateItemSummaryTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        itemSummary.setQuantity(Utils.getAsBigDecimal(100));
        itemSummary.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummary.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        ItemSummary result = purchaseReturnWarehouseSummaryService.updateItemSummary(itemSummary, purchaseReturnDetailThree);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(90), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(2700), result.getTotalPrice());
        
    }
    
    @Test
    public void updateInventoryTest() throws Exception{
        
        List<PurchaseReturnDetail> details = List.of(purchaseReturnDetailThree, purchaseReturnDetailFour);
        
        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        when(warehouseRepository.findById(1L)).thenReturn(Optional.of(warehouseOne));
        when(warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemOne)).thenReturn(itemSummaryOne);
        when(warehouseExitValidationImpl.validateItemSummary(warehouseOne, itemTwo)).thenReturn(itemSummaryTwo);
        doNothing().when(warehouseExitValidationImpl).validateInventoryBeforeUpdate(itemSummaryOne, purchaseReturnDetailThree);
        doNothing().when(warehouseExitValidationImpl).validateInventoryBeforeUpdate(itemSummaryTwo, purchaseReturnDetailFour);
        
        when(itemSummaryRepository.saveAll(any(List.class))).thenReturn(List.of(new ItemSummary()));
        
        when(warehouseRepository.save(warehouseOne)).thenReturn(warehouseOne);
        
        Warehouse result = purchaseReturnWarehouseSummaryService.updateInventory(1L, details);
        
        assertEquals(warehouseOne.getId(), result.getId());
        assertEquals(true, result.isUsed());
        assertEquals("Warehouse one", result.getWarehouseName());
        assertEquals(2, warehouseOne.getItemSummary().size());
        
        ItemSummary itemSummary = result.getItemSummary().get(0);
        
        assertEquals(itemOne, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(90), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(2700), itemSummary.getTotalPrice());
        
        itemSummary = result.getItemSummary().get(1);
        
        assertEquals(itemTwo, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(80), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(10), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(800), itemSummary.getTotalPrice());        
        
    }
    
}
