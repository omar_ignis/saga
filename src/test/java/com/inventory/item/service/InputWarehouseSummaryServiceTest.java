/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;

import com.inventory.item.util.Utils;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.ItemSummary;

import com.inventory.item.service.ItemService;
import com.inventory.item.inventory.impl.InputInventoryImpl;

import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class InputWarehouseSummaryServiceTest {
    
    @Mock
    private ItemService itemService;
    
    @Mock
    private WarehouseRepository warehouseRepository;
    
    @Mock
    private ItemSummaryRepository itemSummaryRepository;
    
    @InjectMocks
    private InputInventoryImpl inputWarehouseSummaryService;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    
    private Warehouse warehouseOne;
    
    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;
    private InputDetail inputDetailFour;
    
    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        
        inputDetailOne = new InputDetail();
        inputDetailOne.setId(1L);
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        inputDetailTwo = new InputDetail();
        inputDetailTwo.setId(2L);
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemOne);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(30));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(600));
        
        inputDetailThree = new InputDetail();
        inputDetailThree.setId(3L);
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(200));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(4000));
        
        inputDetailFour = new InputDetail();
        inputDetailFour.setId(4L);
        inputDetailFour.setDescription("detail four");
        inputDetailFour.setItem(itemTwo);
        inputDetailFour.setLineNumber(4);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(100));
    }
    
    @Test
    public void calcWeightedAverageTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        
        ItemSummary result = inputWarehouseSummaryService.calcWeightedAverage(itemSummary, inputDetailOne);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalPrice());
        
        itemSummary.setQuantity(result.getQuantity());
        itemSummary.setUnitPrice(result.getUnitPrice());
        itemSummary.setTotalPrice(result.getTotalPrice());
        
        result = inputWarehouseSummaryService.calcWeightedAverage(itemSummary, inputDetailTwo);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(30), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(26.67f), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(800), result.getTotalPrice());
        
    }
    
    @Test
    public void createItemSummaryTest() {
        
        ItemSummary result = inputWarehouseSummaryService.createItemSummary(warehouseOne, inputDetailOne);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(10), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), result.getTotalPrice());
    }
    
    @Test
    public void updateItemSummaryTest() {
        
        ItemSummary itemSummary = new ItemSummary(warehouseOne, itemOne);
        itemSummary.setQuantity(Utils.getAsBigDecimal(100));
        itemSummary.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummary.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        ItemSummary result = inputWarehouseSummaryService.updateItemSummary(itemSummary, inputDetailOne);
        
        assertEquals(itemOne, result.getKey().getItem());
        assertEquals(warehouseOne, result.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(110), result.getQuantity());
        assertEquals(Utils.getAsBigDecimal(29.09f), result.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3200), result.getTotalPrice());
        
    }
    
    @Test
    public void updateInventoryTest() throws Exception{
        
        List<InputDetail> details = List.of(inputDetailOne, inputDetailThree, inputDetailFour);
        
        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(100));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        when(warehouseRepository.findById(1L)).thenReturn(Optional.of(warehouseOne));
        when(itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemOne)).thenReturn(itemSummaryOne);
        when(itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemTwo)).thenReturn(itemSummaryTwo);
        when(itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemThree)).thenReturn(null);
        
        when(itemSummaryRepository.saveAll(any(List.class))).thenReturn(List.of(new ItemSummary()));
        
        when(warehouseRepository.save(warehouseOne)).thenReturn(warehouseOne);
        
        Warehouse result = inputWarehouseSummaryService.updateInventory(1L, details);
        
        assertEquals(warehouseOne.getId(), result.getId());
        assertEquals(true, result.isUsed());
        assertEquals("Warehouse one", result.getWarehouseName());
        assertEquals(3, warehouseOne.getItemSummary().size());
        
        ItemSummary itemSummary = result.getItemSummary().get(0);
        
        assertEquals(itemOne, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(110), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(29.09f), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(3200), itemSummary.getTotalPrice());
        
        itemSummary = result.getItemSummary().get(1);
        
        assertEquals(itemTwo, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(120), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(9.17f), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1100), itemSummary.getTotalPrice());
        
        itemSummary = result.getItemSummary().get(2);
        
        assertEquals(itemThree, itemSummary.getKey().getItem());
        assertEquals(warehouseOne, itemSummary.getKey().getWarehouse());
        assertEquals(Utils.getAsBigDecimal(200), itemSummary.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), itemSummary.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(4000), itemSummary.getTotalPrice());
        
        
    }
}
