/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import com.inventory.item.service.impl.OutputDocumentServiceImpl;
import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.util.Utils;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.OutputDocumentRepository;

import com.inventory.item.inventory.ExitInventorySummary;
import com.inventory.item.document.impl.OutputDocumentImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class OutputDocumentServiceImplTest {
    
    @Mock
    private OutputDetailRepository outputDetailRepository;
    
    @Mock
    private ExitInventorySummary outputWarehouseSummaryService;
    
    @Mock
    private OutputDocumentRepository outputDocumentRepository;
    
    @Mock
    private OutputDocumentImpl outputDocumentFactoryImpl;
    
    @InjectMocks
    private OutputDocumentServiceImpl outputDocumentService;
    
    private LocalDateTime now;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;
    
    private Warehouse warehouseOne;
    
    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;
    private OutputDetail outputDetailFour;
    private OutputDetail outputDetailFive;
    private OutputDetail outputDetailSix;
    private OutputDetail outputDetailSeven;
    private OutputDetail outputDetailEight;
    private OutputDetail outputDetailNine;
    private OutputDetail outputDetailTen;
    
    private OutputDocument outputDocumentOne;
    private OutputDocument outputDocumentTwo;
    private OutputDocument outputDocumentThree;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        
        itemFour = new Item();
        itemFour.setId(4L);
        itemFour.setItemName("Item Vier");
        itemFour.setDescription("Item Vier desc");
        itemFour.setValuationType(ValuationType.AVERAGE);
        
        itemFive = new Item();
        itemFive.setId(3L);
        itemFive.setItemName("Item Five");
        itemFive.setDescription("Item Five desc");
        itemFive.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        warehouseOne.setUsed(true);

        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000001");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<OutputDetail> listOne = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setId(1L);
        outputDetailFour.setDescription("detail one");
        outputDetailFour.setItem(itemOne);
        outputDetailFour.setLineNumber(1);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailFour.setOutputDocument(outputDocumentTwo);
        
        outputDetailFive = new OutputDetail();
        outputDetailFive.setId(2L);
        outputDetailFive.setDescription("detail two");
        outputDetailFive.setItem(itemTwo);
        outputDetailFive.setLineNumber(2);
        outputDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailFive.setOutputDocument(outputDocumentTwo);
        
        outputDetailSix = new OutputDetail();
        outputDetailSix.setId(3L);
        outputDetailSix.setDescription("detail three");
        outputDetailSix.setItem(itemThree);
        outputDetailSix.setLineNumber(3);
        outputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailSix.setOutputDocument(outputDocumentTwo);
        
        List<OutputDetail> listTwo = List.of(outputDetailFour, outputDetailFive, outputDetailSix);
        
        when(outputDocumentFactoryImpl.createDocument(outputDocumentOne))
                .thenReturn(outputDocumentTwo);
        
        when(outputDocumentRepository.save(outputDocumentTwo))
                .thenReturn(outputDocumentTwo);
        
        when(outputDocumentFactoryImpl.generateDetails(outputDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(outputDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        OutputDocument result = outputDocumentService.save(outputDocumentOne, listOne);
        
        assertEquals("OU0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void updateTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        outputDocumentOne.setCounter(4);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setId(4L);
        outputDetailFour.setDescription("detail four");
        outputDetailFour.setItem(itemFour);
        outputDetailFour.setLineNumber(4);
        outputDetailFour.setDeleted(true);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        outputDetailFour.setOutputDocument(outputDocumentOne);
        
        outputDetailFive = new OutputDetail();
        outputDetailFive.setDescription("detail five");
        outputDetailFive.setItem(itemFive);
        outputDetailFive.setLineNumber(5);
        outputDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        outputDetailSix = new OutputDetail();
        outputDetailSix.setId(5L);
        outputDetailSix.setDescription("detail five");
        outputDetailSix.setItem(itemFive);
        outputDetailSix.setLineNumber(5);
        outputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        outputDetailSix.setOutputDocument(outputDocumentOne);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000001");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setCounter(5);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailSeven = new OutputDetail();
        outputDetailSeven.setId(1L);
        outputDetailSeven.setDescription("detail one");
        outputDetailSeven.setItem(itemOne);
        outputDetailSeven.setLineNumber(1);
        outputDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailSeven.setOutputDocument(outputDocumentTwo);
        
        outputDetailEight = new OutputDetail();
        outputDetailEight.setId(2L);
        outputDetailEight.setDescription("detail two");
        outputDetailEight.setItem(itemTwo);
        outputDetailEight.setLineNumber(2);
        outputDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailEight.setOutputDocument(outputDocumentTwo);
        
        outputDetailNine = new OutputDetail();
        outputDetailNine.setId(3L);
        outputDetailNine.setDescription("detail three");
        outputDetailNine.setItem(itemThree);
        outputDetailNine.setLineNumber(3);
        outputDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailNine.setOutputDocument(outputDocumentTwo);
        
        outputDetailTen = new OutputDetail();
        outputDetailTen.setId(5L);
        outputDetailTen.setDescription("detail five");
        outputDetailTen.setItem(itemFive);
        outputDetailTen.setLineNumber(5);
        outputDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        outputDetailTen.setOutputDocument(outputDocumentTwo);
        
        List<OutputDetail> detailsOne = List.of(outputDetailOne, outputDetailTwo, outputDetailThree, outputDetailFour, outputDetailFive);
        List<OutputDetail> detailsTwo = List.of(outputDetailOne, outputDetailTwo, outputDetailThree,outputDetailFive);
        List<OutputDetail> detailsThree = List.of(outputDetailOne, outputDetailTwo, outputDetailThree, outputDetailSix);
        
        when(outputDocumentFactoryImpl.updateDocument(outputDocumentOne)).thenReturn(outputDocumentOne);
        when(outputDocumentRepository.save(outputDocumentOne)).thenReturn(outputDocumentOne);
        when(outputDocumentFactoryImpl.updateDetails(outputDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(outputDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        outputDetailFour.removeOutputDocument(outputDocumentOne);
        doNothing().when(outputDetailRepository).deleteAll(List.of(outputDetailFour));
        
        OutputDocument result = outputDocumentService.update(outputDocumentOne, detailsOne);
        
        assertEquals("OU0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        OutputDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }    
    
    @Test
    public void saveOrUpdateTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000001");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<OutputDetail> listOne = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setId(1L);
        outputDetailFour.setDescription("detail one");
        outputDetailFour.setItem(itemOne);
        outputDetailFour.setLineNumber(1);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailFour.setOutputDocument(outputDocumentTwo);
        
        outputDetailFive = new OutputDetail();
        outputDetailFive.setId(2L);
        outputDetailFive.setDescription("detail two");
        outputDetailFive.setItem(itemTwo);
        outputDetailFive.setLineNumber(2);
        outputDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailFive.setOutputDocument(outputDocumentTwo);
        
        outputDetailSix = new OutputDetail();
        outputDetailSix.setId(3L);
        outputDetailSix.setDescription("detail three");
        outputDetailSix.setItem(itemThree);
        outputDetailSix.setLineNumber(3);
        outputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailSix.setOutputDocument(outputDocumentTwo);
        
        List<OutputDetail> listTwo = List.of(outputDetailFour, outputDetailFive, outputDetailSix);
        
        when(outputDocumentFactoryImpl.createDocument(outputDocumentOne))
                .thenReturn(outputDocumentTwo);
        
        when(outputDocumentRepository.save(outputDocumentTwo))
                .thenReturn(outputDocumentTwo);
        
        when(outputDocumentFactoryImpl.generateDetails(outputDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(outputDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        OutputDocument result = outputDocumentService.saveOrUpdate(outputDocumentOne, listOne);
        
        assertEquals("OU0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
        
        //Update output document
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        outputDocumentOne.setCounter(4);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setId(4L);
        outputDetailFour.setDescription("detail four");
        outputDetailFour.setItem(itemFour);
        outputDetailFour.setLineNumber(4);
        outputDetailFour.setDeleted(true);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        outputDetailFour.setOutputDocument(outputDocumentOne);
        
        outputDetailFive = new OutputDetail();
        outputDetailFive.setDescription("detail five");
        outputDetailFive.setItem(itemFive);
        outputDetailFive.setLineNumber(5);
        outputDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        outputDetailSix = new OutputDetail();
        outputDetailSix.setId(5L);
        outputDetailSix.setDescription("detail five");
        outputDetailSix.setItem(itemFive);
        outputDetailSix.setLineNumber(5);
        outputDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        outputDetailSix.setOutputDocument(outputDocumentOne);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000001");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setCounter(5);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailSeven = new OutputDetail();
        outputDetailSeven.setId(1L);
        outputDetailSeven.setDescription("detail one");
        outputDetailSeven.setItem(itemOne);
        outputDetailSeven.setLineNumber(1);
        outputDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailSeven.setOutputDocument(outputDocumentTwo);
        
        outputDetailEight = new OutputDetail();
        outputDetailEight.setId(2L);
        outputDetailEight.setDescription("detail two");
        outputDetailEight.setItem(itemTwo);
        outputDetailEight.setLineNumber(2);
        outputDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailEight.setOutputDocument(outputDocumentTwo);
        
        outputDetailNine = new OutputDetail();
        outputDetailNine.setId(3L);
        outputDetailNine.setDescription("detail three");
        outputDetailNine.setItem(itemThree);
        outputDetailNine.setLineNumber(3);
        outputDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailNine.setOutputDocument(outputDocumentTwo);
        
        outputDetailTen = new OutputDetail();
        outputDetailTen.setId(5L);
        outputDetailTen.setDescription("detail five");
        outputDetailTen.setItem(itemFive);
        outputDetailTen.setLineNumber(5);
        outputDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        outputDetailTen.setOutputDocument(outputDocumentTwo);
        
        List<OutputDetail> detailsOne = List.of(outputDetailOne, outputDetailTwo, outputDetailThree, outputDetailFour, outputDetailFive);
        List<OutputDetail> detailsTwo = List.of(outputDetailOne, outputDetailTwo, outputDetailThree,outputDetailFive);
        List<OutputDetail> detailsThree = List.of(outputDetailOne, outputDetailTwo, outputDetailThree, outputDetailSix);
        
        when(outputDocumentFactoryImpl.updateDocument(outputDocumentOne)).thenReturn(outputDocumentOne);
        when(outputDocumentRepository.save(outputDocumentOne)).thenReturn(outputDocumentOne);
        when(outputDocumentFactoryImpl.updateDetails(outputDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(outputDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        outputDetailFour.removeOutputDocument(outputDocumentOne);
        doNothing().when(outputDetailRepository).deleteAll(List.of(outputDetailFour));
        
        result = outputDocumentService.saveOrUpdate(outputDocumentOne, detailsOne);
        
        assertEquals("OU0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        OutputDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }
    
    @Test
    public void getDocumentByIdTest() throws Exception, Throwable {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);

        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);

        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);

        when(outputDocumentRepository.findById("OU0000000001"))
                .thenReturn(Optional.of(outputDocumentOne));

        OutputDocument result = outputDocumentService.getDocumentById("OU0000000001");

        assertEquals("OU0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void getAllDocumentsTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000002");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        outputDocumentThree = new OutputDocument();
        outputDocumentThree.setId("OU0000000003");
        outputDocumentThree.setDate(now);
        outputDocumentThree.setDescription("output document one");
        outputDocumentThree.setWarehouse(warehouseOne);
        outputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        outputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<OutputDocument> documents = new PageImpl(
                List.of(outputDocumentOne, outputDocumentTwo, outputDocumentThree));
        
        when(outputDocumentRepository.findByDeletedFalse(PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<OutputDocument> page = outputDocumentService.getAllDocuments(PageRequest.of(0, 10));
        
        List<OutputDocument> list  = page.getContent();
        
        assertEquals(3, list.size());
        
        OutputDocument result = list.get(0);
        assertEquals("OU0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("OU0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("OU0000000003", result.getId());
        
    }

    @Test
    public void getAllDocumentByIdLikeTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000002");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        outputDocumentThree = new OutputDocument();
        outputDocumentThree.setId("OU0000000003");
        outputDocumentThree.setDate(now);
        outputDocumentThree.setDescription("output document one");
        outputDocumentThree.setWarehouse(warehouseOne);
        outputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        outputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<OutputDocument> documents = new PageImpl(
                 List.of(outputDocumentOne, outputDocumentTwo, outputDocumentThree));
        when(outputDocumentRepository.findByIdLike("OU000", PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<OutputDocument> page = outputDocumentService.getAllDocumentsByIdLike("OU000", PageRequest.of(0, 10));
        List<OutputDocument> list = page.getContent();
        
        assertEquals(3, list.size());
        
        OutputDocument result = list.get(0);
        assertEquals("OU0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("OU0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("OU0000000003", result.getId());
    }
    
    @Test
    public void getDetailsByDocumentAsListTest() throws Exception, Throwable {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        List<OutputDetail> deetails = List
                .of(outputDetailOne, outputDetailTwo, outputDetailThree);
        
        when(outputDocumentRepository.findById("OU0000000001"))
                .thenReturn(Optional.of(outputDocumentOne));
        
        when(outputDetailRepository.findByOutputDocument(outputDocumentOne))
                .thenReturn(deetails);
        
        List<OutputDetail> list = outputDocumentService
                .getDetailsByDocument("OU0000000001");
        
        assertEquals(3, list.size());
        
        OutputDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    @Test
    public void getDetailsByDocumentAsPageTest() throws Exception, Throwable {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        when(outputDocumentRepository.findById("OU0000000001"))
                .thenReturn(Optional.of(outputDocumentOne));
        
        Page<OutputDetail> details =  new PageImpl(List
                .of(outputDetailOne, outputDetailTwo, outputDetailThree));
        
        when(outputDetailRepository.findByOutputDocument(outputDocumentOne, PageRequest.of(0, 10)))
                .thenReturn(details);
        
        Page<OutputDetail> page = outputDocumentService
                .getDetailsByDocument("OU0000000001", PageRequest.of(0, 10));
        
        List<OutputDetail> list = page.getContent();
        
        assertEquals(3, list.size());
        
        OutputDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    
    @Test
    public void releaseTest() throws Exception, Throwable{
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        outputDocumentOne.setCounter(4);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000001");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        outputDocumentTwo.setCounter(4);
        outputDocumentTwo.setStatus(Status.RELEASED);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setId(1L);
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentTwo);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setId(2L);
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentTwo);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setId(3L);
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentTwo);
        
        List<OutputDetail> details = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);
        
        when(outputDocumentRepository.findById("OU0000000001"))
                .thenReturn(Optional.of(outputDocumentOne));
        
        when(outputDocumentFactoryImpl.release(outputDocumentOne))
                .thenReturn(outputDocumentTwo);
        
        when(outputDocumentRepository.save(outputDocumentTwo)).thenReturn(outputDocumentTwo);
        
        when(outputDetailRepository.findByOutputDocument(any(OutputDocument.class)))
                .thenReturn(details);
        
        when(outputWarehouseSummaryService.updateInventory(outputDocumentTwo.getWarehouse().getId(), details))
                .thenReturn(warehouseOne);
        
        OutputDocument result = outputDocumentService.release("OU0000000001");
        
        assertEquals("OU0000000001", result.getId());
        assertEquals(4, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals("output document one", result.getDescription());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void getTypeTest() {
        DocumentType result = outputDocumentService.getType();
        assertEquals(DocumentType.OUTPUT, result);
    }
}
