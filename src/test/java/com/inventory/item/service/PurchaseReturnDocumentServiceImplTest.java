/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service;

import com.inventory.item.service.impl.PurchaseReturnDocumentServiceImpl;
import java.util.List;
import java.util.Optional;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import com.inventory.item.inventory.ExitInventorySummary;
import com.inventory.item.document.impl.PurchaseReturnDocumentImpl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.ArgumentMatchers.any;
/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class PurchaseReturnDocumentServiceImplTest {
    
    @Mock
    private PurchaseReturnDetailRepository purchaseReturnDetailRepository;
    
    @Mock
    private ExitInventorySummary purchaseReturnWarehouseSummaryService;
    
    @Mock
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;
    
    @Mock
    private PurchaseReturnDocumentImpl purchaseReturnDocumentFactoryImpl;
    
    @InjectMocks
    private PurchaseReturnDocumentServiceImpl purchaseReturnDocumentService;
    
    private LocalDateTime now;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;
    
    private Warehouse warehouseOne;
    
    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;
    private PurchaseReturnDetail purchaseReturnDetailFour;
    private PurchaseReturnDetail purchaseReturnDetailFive;
    private PurchaseReturnDetail purchaseReturnDetailSix;
    private PurchaseReturnDetail purchaseReturnDetailSeven;
    private PurchaseReturnDetail purchaseReturnDetailEight;
    private PurchaseReturnDetail purchaseReturnDetailNine;
    private PurchaseReturnDetail purchaseReturnDetailTen;
    
    private PurchaseReturnDocument purchaseReturnDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentTwo;
    private PurchaseReturnDocument purchaseReturnDocumentThree;
    
    @BeforeEach
    void init() {

        itemOne = new Item();
        itemOne.setId(1L);
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);

        itemTwo = new Item();
        itemTwo.setId(2L);
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);

        itemThree = new Item();
        itemThree.setId(3L);
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        
        itemFour = new Item();
        itemFour.setId(4L);
        itemFour.setItemName("Item Vier");
        itemFour.setDescription("Item Vier desc");
        itemFour.setValuationType(ValuationType.AVERAGE);
        
        itemFive = new Item();
        itemFive.setId(3L);
        itemFive.setItemName("Item Five");
        itemFive.setDescription("Item Five desc");
        itemFive.setValuationType(ValuationType.AVERAGE);

        warehouseOne = new Warehouse();
        warehouseOne.setId(1L);
        warehouseOne.setWarehouseName("Warehouse one");
        warehouseOne.setUsed(true);

        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000001");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<PurchaseReturnDetail> listOne = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setId(1L);
        purchaseReturnDetailFour.setDescription("detail one");
        purchaseReturnDetailFour.setItem(itemOne);
        purchaseReturnDetailFour.setLineNumber(1);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailFour.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailFive = new PurchaseReturnDetail();
        purchaseReturnDetailFive.setId(2L);
        purchaseReturnDetailFive.setDescription("detail two");
        purchaseReturnDetailFive.setItem(itemTwo);
        purchaseReturnDetailFive.setLineNumber(2);
        purchaseReturnDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFive.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailSix = new PurchaseReturnDetail();
        purchaseReturnDetailSix.setId(3L);
        purchaseReturnDetailSix.setDescription("detail three");
        purchaseReturnDetailSix.setItem(itemThree);
        purchaseReturnDetailSix.setLineNumber(3);
        purchaseReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailSix.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        List<PurchaseReturnDetail> listTwo = List.of(purchaseReturnDetailFour, purchaseReturnDetailFive, purchaseReturnDetailSix);
        
        when(purchaseReturnDocumentFactoryImpl.createDocument(purchaseReturnDocumentOne))
                .thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDocumentRepository.save(purchaseReturnDocumentTwo))
                .thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDocumentFactoryImpl.generateDetails(purchaseReturnDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(purchaseReturnDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        PurchaseReturnDocument result = purchaseReturnDocumentService.save(purchaseReturnDocumentOne, listOne);
        
        assertEquals("PR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.PURCHASE_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void updateTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        purchaseReturnDocumentOne.setCounter(4);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setId(4L);
        purchaseReturnDetailFour.setDescription("detail four");
        purchaseReturnDetailFour.setItem(itemFour);
        purchaseReturnDetailFour.setLineNumber(4);
        purchaseReturnDetailFour.setDeleted(true);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        purchaseReturnDetailFour.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailFive = new PurchaseReturnDetail();
        purchaseReturnDetailFive.setDescription("detail five");
        purchaseReturnDetailFive.setItem(itemFive);
        purchaseReturnDetailFive.setLineNumber(5);
        purchaseReturnDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        purchaseReturnDetailSix = new PurchaseReturnDetail();
        purchaseReturnDetailSix.setId(5L);
        purchaseReturnDetailSix.setDescription("detail five");
        purchaseReturnDetailSix.setItem(itemFive);
        purchaseReturnDetailSix.setLineNumber(5);
        purchaseReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        purchaseReturnDetailSix.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000001");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setCounter(5);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailSeven = new PurchaseReturnDetail();
        purchaseReturnDetailSeven.setId(1L);
        purchaseReturnDetailSeven.setDescription("detail one");
        purchaseReturnDetailSeven.setItem(itemOne);
        purchaseReturnDetailSeven.setLineNumber(1);
        purchaseReturnDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailSeven.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailEight = new PurchaseReturnDetail();
        purchaseReturnDetailEight.setId(2L);
        purchaseReturnDetailEight.setDescription("detail two");
        purchaseReturnDetailEight.setItem(itemTwo);
        purchaseReturnDetailEight.setLineNumber(2);
        purchaseReturnDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailEight.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailNine = new PurchaseReturnDetail();
        purchaseReturnDetailNine.setId(3L);
        purchaseReturnDetailNine.setDescription("detail three");
        purchaseReturnDetailNine.setItem(itemThree);
        purchaseReturnDetailNine.setLineNumber(3);
        purchaseReturnDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailNine.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailTen = new PurchaseReturnDetail();
        purchaseReturnDetailTen.setId(5L);
        purchaseReturnDetailTen.setDescription("detail five");
        purchaseReturnDetailTen.setItem(itemFive);
        purchaseReturnDetailTen.setLineNumber(5);
        purchaseReturnDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        purchaseReturnDetailTen.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        List<PurchaseReturnDetail> detailsOne = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree, purchaseReturnDetailFour, purchaseReturnDetailFive);
        List<PurchaseReturnDetail> detailsTwo = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree,purchaseReturnDetailFive);
        List<PurchaseReturnDetail> detailsThree = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree, purchaseReturnDetailSix);
        
        when(purchaseReturnDocumentFactoryImpl.updateDocument(purchaseReturnDocumentOne)).thenReturn(purchaseReturnDocumentOne);
        when(purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne)).thenReturn(purchaseReturnDocumentOne);
        when(purchaseReturnDocumentFactoryImpl.updateDetails(purchaseReturnDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(purchaseReturnDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        purchaseReturnDetailFour.removePurchaseReturnDocument(purchaseReturnDocumentOne);
        doNothing().when(purchaseReturnDetailRepository).deleteAll(List.of(purchaseReturnDetailFour));
        
        PurchaseReturnDocument result = purchaseReturnDocumentService.update(purchaseReturnDocumentOne, detailsOne);
        
        assertEquals("PR0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        PurchaseReturnDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }    
    
    @Test
    public void saveOrUpdateTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000001");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        
        List<PurchaseReturnDetail> listOne = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setId(1L);
        purchaseReturnDetailFour.setDescription("detail one");
        purchaseReturnDetailFour.setItem(itemOne);
        purchaseReturnDetailFour.setLineNumber(1);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailFour.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailFive = new PurchaseReturnDetail();
        purchaseReturnDetailFive.setId(2L);
        purchaseReturnDetailFive.setDescription("detail two");
        purchaseReturnDetailFive.setItem(itemTwo);
        purchaseReturnDetailFive.setLineNumber(2);
        purchaseReturnDetailFive.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFive.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailSix = new PurchaseReturnDetail();
        purchaseReturnDetailSix.setId(3L);
        purchaseReturnDetailSix.setDescription("detail three");
        purchaseReturnDetailSix.setItem(itemThree);
        purchaseReturnDetailSix.setLineNumber(3);
        purchaseReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailSix.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        List<PurchaseReturnDetail> listTwo = List.of(purchaseReturnDetailFour, purchaseReturnDetailFive, purchaseReturnDetailSix);
        
        when(purchaseReturnDocumentFactoryImpl.createDocument(purchaseReturnDocumentOne))
                .thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDocumentRepository.save(purchaseReturnDocumentTwo))
                .thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDocumentFactoryImpl.generateDetails(purchaseReturnDocumentTwo, listOne))
                .thenReturn(listTwo);
        
        when(purchaseReturnDetailRepository.saveAll(listTwo)).thenReturn(listTwo);
        
        PurchaseReturnDocument result = purchaseReturnDocumentService.saveOrUpdate(purchaseReturnDocumentOne, listOne);
        
        assertEquals("PR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.PURCHASE_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
        
        //Update purchase return document
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        purchaseReturnDocumentOne.setCounter(4);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setId(4L);
        purchaseReturnDetailFour.setDescription("detail four");
        purchaseReturnDetailFour.setItem(itemFour);
        purchaseReturnDetailFour.setLineNumber(4);
        purchaseReturnDetailFour.setDeleted(true);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(11));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(1100));
        purchaseReturnDetailFour.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailFive = new PurchaseReturnDetail();
        purchaseReturnDetailFive.setDescription("detail five");
        purchaseReturnDetailFive.setItem(itemFive);
        purchaseReturnDetailFive.setLineNumber(5);
        purchaseReturnDetailFive.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailFive.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailFive.setTotalPrice(Utils.getAsBigDecimal(3000));
        
        purchaseReturnDetailSix = new PurchaseReturnDetail();
        purchaseReturnDetailSix.setId(5L);
        purchaseReturnDetailSix.setDescription("detail five");
        purchaseReturnDetailSix.setItem(itemFive);
        purchaseReturnDetailSix.setLineNumber(5);
        purchaseReturnDetailSix.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailSix.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailSix.setTotalPrice(Utils.getAsBigDecimal(3000));
        purchaseReturnDetailSix.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000001");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setCounter(5);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailSeven = new PurchaseReturnDetail();
        purchaseReturnDetailSeven.setId(1L);
        purchaseReturnDetailSeven.setDescription("detail one");
        purchaseReturnDetailSeven.setItem(itemOne);
        purchaseReturnDetailSeven.setLineNumber(1);
        purchaseReturnDetailSeven.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailSeven.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailSeven.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailSeven.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailEight = new PurchaseReturnDetail();
        purchaseReturnDetailEight.setId(2L);
        purchaseReturnDetailEight.setDescription("detail two");
        purchaseReturnDetailEight.setItem(itemTwo);
        purchaseReturnDetailEight.setLineNumber(2);
        purchaseReturnDetailEight.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailEight.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailEight.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailEight.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailNine = new PurchaseReturnDetail();
        purchaseReturnDetailNine.setId(3L);
        purchaseReturnDetailNine.setDescription("detail three");
        purchaseReturnDetailNine.setItem(itemThree);
        purchaseReturnDetailNine.setLineNumber(3);
        purchaseReturnDetailNine.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailNine.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailNine.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailNine.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailTen = new PurchaseReturnDetail();
        purchaseReturnDetailTen.setId(5L);
        purchaseReturnDetailTen.setDescription("detail five");
        purchaseReturnDetailTen.setItem(itemFive);
        purchaseReturnDetailTen.setLineNumber(5);
        purchaseReturnDetailTen.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTen.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailTen.setTotalPrice(Utils.getAsBigDecimal(3000));
        purchaseReturnDetailTen.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        List<PurchaseReturnDetail> detailsOne = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree, purchaseReturnDetailFour, purchaseReturnDetailFive);
        List<PurchaseReturnDetail> detailsTwo = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree,purchaseReturnDetailFive);
        List<PurchaseReturnDetail> detailsThree = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree, purchaseReturnDetailSix);
        
        when(purchaseReturnDocumentFactoryImpl.updateDocument(purchaseReturnDocumentOne)).thenReturn(purchaseReturnDocumentOne);
        when(purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne)).thenReturn(purchaseReturnDocumentOne);
        when(purchaseReturnDocumentFactoryImpl.updateDetails(purchaseReturnDocumentOne, detailsTwo)).thenReturn(detailsThree);
        when(purchaseReturnDetailRepository.saveAll(detailsThree)).thenReturn(detailsThree);
        purchaseReturnDetailFour.removePurchaseReturnDocument(purchaseReturnDocumentOne);
        doNothing().when(purchaseReturnDetailRepository).deleteAll(List.of(purchaseReturnDetailFour));
        
        result = purchaseReturnDocumentService.saveOrUpdate(purchaseReturnDocumentOne, detailsOne);
        
        assertEquals("PR0000000001", result.getId());
        assertEquals(5, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(4, result.getDetails().size());
        
        PurchaseReturnDetail detail = result.getDetails().get(0);
        assertEquals(1L, detail.getId());
        
        detail = result.getDetails().get(1);
        assertEquals(2L, detail.getId());
        
        detail = result.getDetails().get(2);
        assertEquals(3L, detail.getId());
        
        detail = result.getDetails().get(3);
        assertEquals(5L, detail.getId());
    }
    
    @Test
    public void getDocumentByIdTest() throws Exception, Throwable {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);

        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);

        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);

        when(purchaseReturnDocumentRepository.findById("PR0000000001"))
                .thenReturn(Optional.of(purchaseReturnDocumentOne));

        PurchaseReturnDocument result = purchaseReturnDocumentService.getDocumentById("PR0000000001");

        assertEquals("PR0000000001", result.getId());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(DocumentType.PURCHASE_RETURN, result.getType());
        assertEquals(now, result.getDate());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void getAllDocumentsTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000002");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        purchaseReturnDocumentThree = new PurchaseReturnDocument();
        purchaseReturnDocumentThree.setId("PR0000000003");
        purchaseReturnDocumentThree.setDate(now);
        purchaseReturnDocumentThree.setDescription("purchase return document one");
        purchaseReturnDocumentThree.setWarehouse(warehouseOne);
        purchaseReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        purchaseReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<PurchaseReturnDocument> documents = new PageImpl(
                List.of(purchaseReturnDocumentOne, purchaseReturnDocumentTwo, purchaseReturnDocumentThree));
        
        when(purchaseReturnDocumentRepository.findByDeletedFalse(PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<PurchaseReturnDocument> page = purchaseReturnDocumentService.getAllDocuments(PageRequest.of(0, 10));
        
        List<PurchaseReturnDocument> list  = page.getContent();
        
        assertEquals(3, list.size());
        
        PurchaseReturnDocument result = list.get(0);
        assertEquals("PR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("PR0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("PR0000000003", result.getId());
        
    }

    @Test
    public void getAllDocumentByIdLikeTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000002");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        
        purchaseReturnDocumentThree = new PurchaseReturnDocument();
        purchaseReturnDocumentThree.setId("PR0000000003");
        purchaseReturnDocumentThree.setDate(now);
        purchaseReturnDocumentThree.setDescription("purchase return document one");
        purchaseReturnDocumentThree.setWarehouse(warehouseOne);
        purchaseReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        purchaseReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        Page<PurchaseReturnDocument> documents = new PageImpl(
                 List.of(purchaseReturnDocumentOne, purchaseReturnDocumentTwo, purchaseReturnDocumentThree));
        when(purchaseReturnDocumentRepository.findByIdLike("PR000", PageRequest.of(0, 10)))
                .thenReturn(documents);
        
        Page<PurchaseReturnDocument> page = purchaseReturnDocumentService.getAllDocumentsByIdLike("PR000", PageRequest.of(0, 10));
        List<PurchaseReturnDocument> list = page.getContent();
        
        assertEquals(3, list.size());
        
        PurchaseReturnDocument result = list.get(0);
        assertEquals("PR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("PR0000000002", result.getId());
        
        result = list.get(2);
        assertEquals("PR0000000003", result.getId());
    }
    
    @Test
    public void getDetailsByDocumentAsListTest() throws Exception, Throwable {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        List<PurchaseReturnDetail> deetails = List
                .of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);
        
        when(purchaseReturnDocumentRepository.findById("PR0000000001"))
                .thenReturn(Optional.of(purchaseReturnDocumentOne));
        
        when(purchaseReturnDetailRepository.findByPurchaseReturnDocument(purchaseReturnDocumentOne))
                .thenReturn(deetails);
        
        List<PurchaseReturnDetail> list = purchaseReturnDocumentService
                .getDetailsByDocument("PR0000000001");
        
        assertEquals(3, list.size());
        
        PurchaseReturnDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    @Test
    public void getDetailsByDocumentAsPageTest() throws Exception, Throwable {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        when(purchaseReturnDocumentRepository.findById("PR0000000001"))
                .thenReturn(Optional.of(purchaseReturnDocumentOne));
        
        Page<PurchaseReturnDetail> details =  new PageImpl(List
                .of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree));
        
        when(purchaseReturnDetailRepository.findByPurchaseReturnDocument(purchaseReturnDocumentOne, PageRequest.of(0, 10)))
                .thenReturn(details);
        
        Page<PurchaseReturnDetail> page = purchaseReturnDocumentService
                .getDetailsByDocument("PR0000000001", PageRequest.of(0, 10));
        
        List<PurchaseReturnDetail> list = page.getContent();
        
        assertEquals(3, list.size());
        
        PurchaseReturnDetail result = list.get(0);
        assertEquals(1L, result.getId());
        
        result = list.get(1);
        assertEquals(2L, result.getId());
        
        result = list.get(2);
        assertEquals(3L, result.getId());
        
    }
    
    @Test
    public void releaseTest() throws Exception, Throwable{
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        purchaseReturnDocumentOne.setCounter(4);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000001");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(130));
        purchaseReturnDocumentTwo.setCounter(4);
        purchaseReturnDocumentTwo.setStatus(Status.RELEASED);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setId(1L);
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setId(2L);
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setId(3L);
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentTwo);
        
        List<PurchaseReturnDetail> details = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);
        
        when(purchaseReturnDocumentRepository.findById("PR0000000001"))
                .thenReturn(Optional.of(purchaseReturnDocumentOne));
        
        when(purchaseReturnDocumentFactoryImpl.release(purchaseReturnDocumentOne))
                .thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDocumentRepository.save(purchaseReturnDocumentTwo)).thenReturn(purchaseReturnDocumentTwo);
        
        when(purchaseReturnDetailRepository.findByPurchaseReturnDocument(any(PurchaseReturnDocument.class)))
                .thenReturn(details);
        
        when(purchaseReturnWarehouseSummaryService.updateInventory(purchaseReturnDocumentTwo.getWarehouse().getId(), details))
                .thenReturn(warehouseOne);
        
        PurchaseReturnDocument result = purchaseReturnDocumentService.release("PR0000000001");
        
        assertEquals("PR0000000001", result.getId());
        assertEquals(4, result.getCounter());
        assertEquals(now, result.getDate());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());
    }
    
    @Test
    public void getTypeTest() {
        DocumentType result = purchaseReturnDocumentService.getType();
        assertEquals(DocumentType.PURCHASE_RETURN, result);
    }
}
