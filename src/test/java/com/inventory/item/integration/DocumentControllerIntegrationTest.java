/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.integration;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import java.io.IOException;
import java.text.MessageFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.json.JSONArray;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.security.setup.PermissionSetup;
import com.inventory.item.security.service.SecurityService;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;

import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.ResourceRepository;
import com.inventory.item.security.repository.PermissionRepository;
import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;

import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;
import com.inventory.item.repository.OutputDocumentRepository;
import com.inventory.item.repository.SalesReturnDetailRepository;
import com.inventory.item.repository.SalesReturnDocumentRepository;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import com.inventory.item.util.Utils;
import com.inventory.item.util.Messages;
import com.inventory.item.util.RestPageImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

/**
 *
 * @author ignis
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DocumentControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private PermissionSetup permissionSetup;
    
    @Autowired
    private SecurityService securityServiceImpl;
    
    // security repositories
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private ResourceRepository resourceRepository;
    
    @Autowired
    private PermissionRepository permissionRepository;
    
    @Autowired
    private UserAccountRepository userAccountRepository;
    
    @Autowired
    private OrganizationRepository organizationRepository;
    
    // document repositories

    @Autowired
    private ItemSummaryRepository itemSummaryRepository;

    @Autowired
    private InputDetailRepository inputDetailRepository;

    @Autowired
    private OutputDetailRepository outputDetailRepository;

    @Autowired
    private InputDocumentRepository inputDocumentRepository;

    @Autowired
    private OutputDocumentRepository outputDocumentRepository;

    @Autowired
    private SalesReturnDetailRepository salesReturnDetailRepository;

    @Autowired
    private SalesReturnDocumentRepository salesReturnDocumentRepository;

    @Autowired
    private PurchaseReturnDetailRepository purchaseReturnDetailRepository;

    @Autowired
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    private HttpHeaders headers = new HttpHeaders();

    private ObjectMapper mapper;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS");
    private LocalDateTime now = LocalDateTime.now();
    String formatDateTime = now.format(formatter);

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private HttpEntity<Item> itemEntity;
    private final String itemPath = "/inventory-item/items";
    private final String itemNameOne = "Item One";
    private final String itemNameTwo = "Item Two";
    private final String itemNameThree = "Item Three";
    private final String itemNameFour = "Item Four";

    private Warehouse warehouseOne;
    private HttpEntity<Warehouse> warehouseEntity;
    private final String warehousePath = "/inventory-item/warehouses/";
    private final String warehouseNameOne = "Warehouse One";

    private HttpEntity<String> entity;
    private HttpEntity<InputDocument> inputDocumentEntity;
    private HttpEntity<SalesReturnDocument> saleReturnDocumentEntity;
    private final String receptionPath = "/inventory-item/receptions";

    private InputDocument inputDocumentOne;
    private SalesReturnDocument salesReturnDocumentOne;

    private HttpEntity<OutputDocument> outputDocumentEntity;
    private HttpEntity<PurchaseReturnDocument> purchaseReturnDocumentEntity;
    private final String dispatchPath = "/inventory-item/dispatches";

    private OutputDocument outputDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentOne;

    private final String reportPath = "/inventory-item/reports";
    
    //security
    
    private String tokenOne;
    private String tokenTwo;
    private Role roleOne;
    private Role roleTwo;
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    private Organization organizationOne;
    private final String passwordOne = "password123";
    private final String passwordTwo = "password456";
    

    JSONObject jsonWarehouse;
    JSONObject jsonItemOne;
    JSONObject jsonItemTwo;
    JSONObject jsonItemThree;
    JSONObject jsonItemFour;

    JSONObject jsonDocument;
    JSONObject jsonDetailOne;
    JSONObject jsonDetailTwo;
    JSONObject jsonDetailThree;
    JSONObject jsonDetailFour;

    JSONArray jsonDetails;
    JSONArray jsonPositions;
    
    @BeforeEach
    void init() throws Exception {
        
        //<editor-fold defaultstate="collapsed" desc="Security">
        
        organizationOne = securityServiceImpl.createOrganization("ROD", "Republic of devs");
       
        List<Resource> resources = permissionSetup.createResources();
        
        resources = securityServiceImpl.createResources(resources);
        
        List<Permission> permissions = permissionSetup.createPermissions(organizationOne, resources);
        
        permissions = securityServiceImpl.createPermissions(permissions);
        
        var permissionOne = Utils.getPermissionByResource("getItemsAsList", permissions);
        var permissionTwo = Utils.getPermissionByResource("getItem", permissions);
        var permissionThree = Utils.getPermissionByResource("createItem", permissions);
        var permissionFour = Utils.getPermissionByResource("deleteItems", permissions);
        var permissionFive = Utils.getPermissionByResource("getWarehousesAsList", permissions);
        var permissionSix = Utils.getPermissionByResource("getWarehouse", permissions);
        var permissionSeven = Utils.getPermissionByResource("deleteWarehouses", permissions);
        var permissionEight = Utils.getPermissionByResource("createWarehouse", permissions);
        
        roleOne = securityServiceImpl.createRole("Admin",organizationOne.getId());
        roleOne = securityServiceImpl.updateRole("Admin", organizationOne.getId(), permissions);
        
        roleTwo = securityServiceImpl.createRole("Staff", organizationOne.getId());
        roleTwo = securityServiceImpl
                .updateRole("Staff", organizationOne.getId(), List.of(permissionTwo, permissionSix));
        
        userAccountOne = new UserAccount();
        userAccountOne.setName("carlos Gael");
        userAccountOne.setPaternalLastName("Alvarez");
        userAccountOne.setMaternalLastName("Esponjoso");
        userAccountOne.setUsername("Elchiqui");
        userAccountOne.setPassword(passwordEncoder.encode(passwordOne));
        userAccountOne.setEmail("gael@chiqui.com");
        
        userAccountOne = securityServiceImpl.createUserAccount(organizationOne.getId(), userAccountOne);
        userAccountOne = securityServiceImpl
                .updateUserRoles(organizationOne.getId(), userAccountOne.getId(), List.of(roleOne));
        
        userAccountTwo = new UserAccount();
        userAccountTwo.setName("carlos Alexis");
        userAccountTwo.setPaternalLastName("Alvarez");
        userAccountTwo.setMaternalLastName("Pumboso");
        userAccountTwo.setUsername("Elpumbin");
        userAccountTwo.setPassword(passwordEncoder.encode(passwordTwo));
        userAccountTwo.setEmail("alexis@chiqui.com");
        
        userAccountTwo = securityServiceImpl.createUserAccount(organizationOne.getId(), userAccountTwo);
        userAccountTwo = securityServiceImpl
                .updateUserRoles(organizationOne.getId(), userAccountTwo.getId(), List.of(roleTwo));
        
        tokenOne = Utils.getToken(restTemplate, port, userAccountOne.getEmail(), passwordOne);
        
        assertEquals(false, tokenOne.isBlank());
        
        tokenTwo = Utils.getToken(restTemplate, port, userAccountTwo.getEmail(), passwordTwo);
        
        assertEquals(false, tokenTwo.isBlank());
        
        // </editor-fold>
        
        headers = Utils.getHeaders(headers, tokenOne);
        
        itemOne = new Item();
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription("Items Eins Description");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne.setUsed(false);

        itemTwo = new Item();
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription("Items Zwei Description");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo.setUsed(false);

        itemThree = new Item();
        itemThree.setItemName(itemNameThree);
        itemThree.setDescription("Items Drei Description");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree.setUsed(false);

        itemFour = new Item();
        itemFour.setItemName(itemNameFour);
        itemFour.setDescription("Items Vier Description");
        itemFour.setValuationType(ValuationType.AVERAGE);
        itemFour.setUsed(false);

        warehouseOne = new Warehouse(warehouseNameOne);

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        //<editor-fold defaultstate="collapsed" desc="Crea Items">
        
        itemEntity = new HttpEntity<Item>(itemOne, headers);
        ResponseEntity<String> response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath),
                        HttpMethod.POST, itemEntity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        try {
            itemOne = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        itemEntity = new HttpEntity<Item>(headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/" + itemOne.getId()),
                        HttpMethod.GET, itemEntity, String.class);

        try {
            itemOne = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        assertTrue(itemOne.getId() > 0);
        assertEquals(itemNameOne, itemOne.getItemName());

        itemEntity = new HttpEntity<Item>(itemTwo, headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath),
                        HttpMethod.POST, itemEntity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        try {
            itemTwo = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        itemEntity = new HttpEntity<Item>(headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/" + itemTwo.getId()),
                        HttpMethod.GET, itemEntity, String.class);

        try {
            itemTwo = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        assertTrue(itemTwo.getId() > 0);
        assertEquals(itemNameTwo, itemTwo.getItemName());

        itemEntity = new HttpEntity<Item>(itemThree, headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath),
                        HttpMethod.POST, itemEntity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        try {
            itemThree = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        itemEntity = new HttpEntity<Item>(headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/" + itemThree.getId()),
                        HttpMethod.GET, itemEntity, String.class);

        try {
            itemThree = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        assertTrue(itemThree.getId() > 0);
        assertEquals(itemNameThree, itemThree.getItemName());

        itemEntity = new HttpEntity<Item>(itemFour, headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath),
                        HttpMethod.POST, itemEntity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        try {
            itemFour = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        itemEntity = new HttpEntity<Item>(headers);
        response = restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/" + itemFour.getId()),
                        HttpMethod.GET, itemEntity, String.class);

        try {
            itemFour = mapper.readValue(response.getBody(), Item.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        assertTrue(itemFour.getId() > 0);
        assertEquals(itemNameFour, itemFour.getItemName());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Crea el jsonWarehouse">
        
        warehouseEntity = new HttpEntity<Warehouse>(warehouseOne, headers);

        response = restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath),
                        HttpMethod.POST, warehouseEntity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        try {
            warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        warehouseEntity = new HttpEntity<Warehouse>(headers);

        response = restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath + warehouseOne.getId()),
                        HttpMethod.GET, warehouseEntity, String.class);

        try {
            warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);
        } catch (IOException error) {
            error.printStackTrace();
        }

        assertNotNull(warehouseOne.getId());
        assertTrue(warehouseOne.getId() > 0);
        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());

        // </editor-fold>
        
        jsonWarehouse = new JSONObject();
        jsonWarehouse.put("id", warehouseOne.getId());
        jsonWarehouse.put("warehouseName", warehouseOne.getWarehouseName());
        jsonWarehouse.put("used", warehouseOne.isUsed());
        jsonWarehouse.put("deleted", warehouseOne.isDeleted());

        jsonItemOne = new JSONObject();
        jsonItemOne.put("id", itemOne.getId());
        jsonItemOne.put("itemName", itemOne.getItemName());
        jsonItemOne.put("description", itemOne.getDescription());
        jsonItemOne.put("used", itemOne.isUsed());
        jsonItemOne.put("valuationType", itemOne.getValuationType());
        jsonItemOne.put("deleted", itemOne.isDeleted());

        jsonItemTwo = new JSONObject();
        jsonItemTwo.put("id", itemTwo.getId());
        jsonItemTwo.put("itemName", itemTwo.getItemName());
        jsonItemTwo.put("description", itemTwo.getDescription());
        jsonItemTwo.put("used", itemTwo.isUsed());
        jsonItemTwo.put("valuationType", itemTwo.getValuationType());
        jsonItemTwo.put("deleted", itemTwo.isDeleted());

        jsonItemThree = new JSONObject();
        jsonItemThree.put("id", itemThree.getId());
        jsonItemThree.put("itemName", itemThree.getItemName());
        jsonItemThree.put("description", itemThree.getDescription());
        jsonItemThree.put("used", itemThree.isUsed());
        jsonItemThree.put("valuationType", itemThree.getValuationType());
        jsonItemThree.put("deleted", itemThree.isDeleted());

        jsonItemFour = new JSONObject();
        jsonItemFour.put("id", itemFour.getId());
        jsonItemFour.put("itemName", itemFour.getItemName());
        jsonItemFour.put("description", itemFour.getDescription());
        jsonItemFour.put("used", itemFour.isUsed());
        jsonItemFour.put("valuationType", itemFour.getValuationType());
        jsonItemFour.put("deleted", itemFour.isDeleted());
        
    }
    
    @Test
    public void itemTest() throws Exception {
        
        headers = Utils.getHeaders(headers, tokenOne);
        
        entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/options"),
                        HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        
        List<Item> list = Arrays.asList(mapper.readValue(response.getBody(), Item[].class));
        
        assertEquals(4, list.size());
        
        headers = Utils.getHeaders(headers, tokenTwo);
        
        entity = new HttpEntity<>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, itemPath + "/options"),
                        HttpMethod.GET, entity, String.class);
        
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        
    }

    @Test
    public void inputDocumentTest() throws Exception {

        // <editor-fold defaultstate="collapsed" desc="Crea el documento">
        
        headers = Utils.getHeaders(headers, tokenOne);
        
        jsonDocument = new JSONObject();
        jsonDocument.put("id", JSONObject.NULL);
        jsonDocument.put("type", "INPUT");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "input document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(130));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1300));
        jsonDocument.put("counter", 0);
        jsonDocument.put("warehouse", jsonWarehouse);

        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", JSONObject.NULL);
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(10));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(200));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", JSONObject.NULL);
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", false);
        jsonDetailTwo.put("item", jsonItemTwo);

        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", JSONObject.NULL);
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);

        jsonDocument.put("details", jsonDetails);

        String body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, receptionPath + "/type/INPUT"),
                        HttpMethod.POST, entity, String.class);

        inputDocumentOne = mapper.readValue(response.getBody(), InputDocument.class);

        inputDocumentEntity = new HttpEntity<InputDocument>(headers);

        String pathGetById = receptionPath + "/type/INPUT/id/" + inputDocumentOne.getId();

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, inputDocumentEntity, String.class);

        InputDocument result = mapper.readValue(response.getBody(), InputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(inputDocumentOne.getId(), result.getId());
        assertEquals("input document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(3, inputDocumentOne.getDetails().size());

        ResponseEntity<RestPageImpl<InputDetail>> responseEntity;
        responseEntity = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/details"),
                        HttpMethod.GET, entity,
                        new ParameterizedTypeReference<RestPageImpl<InputDetail>>() {
                });

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<InputDetail> list = new ArrayList(responseEntity.getBody().getContent());
        assertNotNull(list);
        assertEquals(3, list.size());
        
        InputDetail inputDetailThree = list.get(2);

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Actualizar el documento">
        
        jsonDocument = new JSONObject();
        jsonDocument.put("id", inputDocumentOne.getId());
        jsonDocument.put("type", "INPUT");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "input document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(165));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1800));
        jsonDocument.put("counter", 4);
        jsonDocument.put("warehouse", jsonWarehouse);

        InputDetail detailOne = list.get(0);
        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", detailOne.getId());
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(15));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(300));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        InputDetail detailTwo = list.get(1);
        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", detailTwo.getId());
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", true);
        jsonDetailTwo.put("item", jsonItemTwo);

        InputDetail detailThree = list.get(2);
        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", detailThree.getId());
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetailFour = new JSONObject();
        jsonDetailFour.put("id", JSONObject.NULL);
        jsonDetailFour.put("lineNumber", 4);
        jsonDetailFour.put("description", "detail four");
        jsonDetailFour.put("quantity", Utils.getAsBigDecimal(50));
        jsonDetailFour.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailFour.put("totalPrice", Utils.getAsBigDecimal(500));
        jsonDetailFour.put("deleted", false);
        jsonDetailFour.put("item", jsonItemFour);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);
        jsonDetails.put(jsonDetailFour);

        jsonDocument.put("details", jsonDetails);

        body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.PUT, entity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, entity, String.class);

        result = mapper.readValue(response.getBody(), InputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(inputDocumentOne.getId(), result.getId());
        assertEquals("input document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165f), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Libera el documento">
        
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/release"),
                        HttpMethod.PUT, inputDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, entity, String.class);

        result = mapper.readValue(response.getBody(), InputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.INPUT, result.getType());
        assertEquals(inputDocumentOne.getId(), result.getId());
        assertEquals("input document one", result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Generar etiquetas">
        
        jsonPositions = new JSONArray();
        jsonPositions.put(1);
        jsonPositions.put(8);
        jsonPositions.put(9);
        jsonPositions.put(15);
        jsonPositions.put(16);
        jsonPositions.put(24);
        jsonPositions.put(25);
        jsonPositions.put(26);
        jsonPositions.put(30);
        jsonPositions.put(59);

        body = jsonPositions.toString();
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<String>(body, headers);
        String barcodePath = reportPath + "/type/INPUT/id/" + inputDocumentOne.getId();
        ResponseEntity<byte[]> pdfReponse = this.restTemplate
                .exchange(Utils.createURLWithPort(port, barcodePath + "/format/CODE128/sheet/OD5160/barcode"),
                        HttpMethod.POST, entity, byte[].class);

        byte[] pdf = pdfReponse.getBody();

        assertEquals(HttpStatus.OK, pdfReponse.getStatusCode());
        assertNotNull(pdf);

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Comprueba el inventario">
        headers = Utils.getHeaders(headers, tokenOne);
        warehouseEntity = new HttpEntity<Warehouse>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath + warehouseOne.getId()),
                        HttpMethod.GET, warehouseEntity, String.class);

        warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);

        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Obtiene el articulo por codigo de barras">
        
        String barcode = Utils.buildBarcode(DocumentType.INPUT, inputDocumentOne.getId(), 3, itemThree.getId());
        
        String barcodeReceptionPath = receptionPath + "/warehouse/" + warehouseOne.getId() + "/barcode/" + barcode + "/format/CODE128";
        
        entity = new HttpEntity<String>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, barcodeReceptionPath),
                        HttpMethod.GET, entity, String.class);
        
        InputDetail detail = mapper.readValue(response.getBody(), InputDetail.class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(inputDetailThree.getId(), detail.getId());
        assertEquals(inputDetailThree.getLineNumber(), detail.getLineNumber());
        assertEquals(inputDetailThree.getItem().getId(), detail.getItem().getId());
        assertEquals(inputDetailThree.getQuantity(), detail.getQuantity());
        assertEquals(inputDetailThree.getUnitPrice(), detail.getUnitPrice());
        assertEquals(inputDetailThree.getTotalPrice(), detail.getTotalPrice());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Elimina el documento">
        
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.DELETE, inputDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, inputDocumentEntity, String.class);

        boolean message = response.getBody().contains(
                MessageFormat.format(
                        Messages.DOCUMENT_NOT_FOUND, inputDocumentOne.getId(), DocumentType.INPUT));

        assertTrue(message);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        // </editor-fold>
    }

    @Test
    public void saleReturnDocumentTest() throws Exception {

        // <editor-fold defaultstate="collapsed" desc="Crea el documento">
        jsonDocument = new JSONObject();
        jsonDocument.put("id", JSONObject.NULL);
        jsonDocument.put("type", "SALES_RETURN");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "sale return document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(130));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1300));
        jsonDocument.put("counter", 0);
        jsonDocument.put("warehouse", jsonWarehouse);

        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", JSONObject.NULL);
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(10));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(200));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", JSONObject.NULL);
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", false);
        jsonDetailTwo.put("item", jsonItemTwo);

        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", JSONObject.NULL);
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);

        jsonDocument.put("details", jsonDetails);

        String body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, receptionPath + "/type/SALES_RETURN"),
                        HttpMethod.POST, entity, String.class);

        salesReturnDocumentOne = mapper.readValue(response.getBody(), SalesReturnDocument.class);

        saleReturnDocumentEntity = new HttpEntity<SalesReturnDocument>(headers);

        String pathGetById = receptionPath + "/type/SALES_RETURN/id/" + salesReturnDocumentOne.getId();

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, saleReturnDocumentEntity, String.class);

        SalesReturnDocument result = mapper.readValue(response.getBody(), SalesReturnDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(salesReturnDocumentOne.getId(), result.getId());
        assertEquals("sale return document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        ResponseEntity<RestPageImpl<SalesReturnDetail>> responseEntity;
        responseEntity = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/details"),
                        HttpMethod.GET, entity,
                        new ParameterizedTypeReference<RestPageImpl<SalesReturnDetail>>() {
                });

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<SalesReturnDetail> list = new ArrayList(responseEntity.getBody().getContent());
        assertNotNull(list);
        assertEquals(3, list.size());
        
        SalesReturnDetail salesReturnDetailThree = list.get(2);

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Actualizar el documento">
        jsonDocument = new JSONObject();
        jsonDocument.put("id", salesReturnDocumentOne.getId());
        jsonDocument.put("type", "SALES_RETURN");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "sale return document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(165));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1800));
        jsonDocument.put("counter", 4);
        jsonDocument.put("warehouse", jsonWarehouse);

        SalesReturnDetail detailOne = list.get(0);
        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", detailOne.getId());
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(15));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(300));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        SalesReturnDetail detailTwo = list.get(1);
        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", detailTwo.getId());
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", true);
        jsonDetailTwo.put("item", jsonItemTwo);

        SalesReturnDetail detailThree = list.get(2);
        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", detailThree.getId());
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetailFour = new JSONObject();
        jsonDetailFour.put("id", JSONObject.NULL);
        jsonDetailFour.put("lineNumber", 4);
        jsonDetailFour.put("description", "detail four");
        jsonDetailFour.put("quantity", Utils.getAsBigDecimal(50));
        jsonDetailFour.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailFour.put("totalPrice", Utils.getAsBigDecimal(500));
        jsonDetailFour.put("deleted", false);
        jsonDetailFour.put("item", jsonItemFour);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);
        jsonDetails.put(jsonDetailFour);

        jsonDocument.put("details", jsonDetails);

        body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.PUT, entity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, entity, String.class);

        result = mapper.readValue(response.getBody(), SalesReturnDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(salesReturnDocumentOne.getId(), result.getId());
        assertEquals("sale return document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165f), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Libera el documento">
        saleReturnDocumentEntity = new HttpEntity<SalesReturnDocument>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/release"),
                        HttpMethod.PUT, saleReturnDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, saleReturnDocumentEntity, String.class);

        result = mapper.readValue(response.getBody(), SalesReturnDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.SALES_RETURN, result.getType());
        assertEquals(salesReturnDocumentOne.getId(), result.getId());
        assertEquals("sale return document one", result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Generat etiquetas">
        jsonPositions = new JSONArray();
        jsonPositions.put(1);
        jsonPositions.put(8);
        jsonPositions.put(9);
        jsonPositions.put(15);
        jsonPositions.put(16);
        jsonPositions.put(24);
        jsonPositions.put(25);
        jsonPositions.put(26);
        jsonPositions.put(30);
        jsonPositions.put(59);

        body = jsonPositions.toString();
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        entity = new HttpEntity<String>(body, headers);
        String barcodePath = reportPath + "/type/SALES_RETURN/id/" + salesReturnDocumentOne.getId();
        ResponseEntity<byte[]> pdfReponse = this.restTemplate
                .exchange(Utils.createURLWithPort(port, barcodePath + "/format/CODE128/sheet/OD5160/barcode"),
                        HttpMethod.POST, entity, byte[].class);

        byte[] pdf = pdfReponse.getBody();

        assertEquals(HttpStatus.OK, pdfReponse.getStatusCode());
        assertNotNull(pdf);

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Comprueba el inventario">
        
        headers = Utils.getHeaders(headers, tokenOne);
        warehouseEntity = new HttpEntity<Warehouse>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath + warehouseOne.getId()),
                        HttpMethod.GET, warehouseEntity, String.class);

        warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);

        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Obtiene el articulo por codigo de barras">
        
        String barcode = Utils
                .buildBarcode(DocumentType.SALES_RETURN, salesReturnDocumentOne.getId(), 3, itemThree.getId());

        String barcodeReceptionPath = receptionPath + "/warehouse/" + warehouseOne.getId() + "/barcode/" + barcode + "/format/CODE128";
        entity = new HttpEntity<String>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, barcodeReceptionPath ),
                        HttpMethod.GET, entity, String.class);

        SalesReturnDetail detail = mapper.readValue(response.getBody(), SalesReturnDetail.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(salesReturnDetailThree.getId(), detail.getId());
        assertEquals(salesReturnDetailThree.getLineNumber(), detail.getLineNumber());
        assertEquals(salesReturnDetailThree.getItem().getId(), detail.getItem().getId());
        assertEquals(salesReturnDetailThree.getQuantity(), detail.getQuantity());
        assertEquals(salesReturnDetailThree.getUnitPrice(), detail.getUnitPrice());
        assertEquals(salesReturnDetailThree.getTotalPrice(), detail.getTotalPrice());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Elimina el documento">
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.DELETE, saleReturnDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, saleReturnDocumentEntity, String.class);

        boolean message = response.getBody().contains(
                MessageFormat.format(Messages.DOCUMENT_NOT_FOUND, salesReturnDocumentOne.getId(), DocumentType.SALES_RETURN));

        assertTrue(message);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        // </editor-fold>
    }

    @Test
    public void outputDocumentTest() throws Exception {

        // <editor-fold defaultstate="collapsed" desc="Crea el documento">
        jsonDocument = new JSONObject();
        jsonDocument.put("id", JSONObject.NULL);
        jsonDocument.put("type", "OUTPUT");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "output document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(130));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1300));
        jsonDocument.put("counter", 0);
        jsonDocument.put("warehouse", jsonWarehouse);

        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", JSONObject.NULL);
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(10));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(200));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", JSONObject.NULL);
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", false);
        jsonDetailTwo.put("item", jsonItemTwo);

        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", JSONObject.NULL);
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);

        jsonDocument.put("details", jsonDetails);

        ItemSummary itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(30));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(600));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryOne);

        ItemSummary itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(40));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(200));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryTwo);

        ItemSummary itemSummaryThree = new ItemSummary(warehouseOne, itemThree);
        itemSummaryThree.setQuantity(Utils.getAsBigDecimal(400));
        itemSummaryThree.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryThree.setTotalPrice(Utils.getAsBigDecimal(4000));
        itemSummaryThree.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryThree);

        ItemSummary itemSummaryFour = new ItemSummary(warehouseOne, itemFour);
        itemSummaryFour.setQuantity(Utils.getAsBigDecimal(200));
        itemSummaryFour.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryFour.setTotalPrice(Utils.getAsBigDecimal(2000));
        itemSummaryFour.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryFour);

        String body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        ResponseEntity<String> response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, dispatchPath + "/type/OUTPUT"),
                        HttpMethod.POST, entity, String.class);

        outputDocumentOne = mapper.readValue(response.getBody(), OutputDocument.class);

        outputDocumentEntity = new HttpEntity<OutputDocument>(headers);

        String pathGetById = dispatchPath + "/type/OUTPUT/id/" + outputDocumentOne.getId();

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, outputDocumentEntity, String.class);

        OutputDocument result = mapper.readValue(response.getBody(), OutputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(outputDocumentOne.getId(), result.getId());
        assertEquals("output document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        ResponseEntity<RestPageImpl<OutputDetail>> responseEntity;
        responseEntity = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/details"),
                        HttpMethod.GET, entity,
                        new ParameterizedTypeReference<RestPageImpl<OutputDetail>>() {
                });

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<OutputDetail> list = new ArrayList(responseEntity.getBody().getContent());
        assertNotNull(list);
        assertEquals(3, list.size());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Actualizar el documento">
        jsonDocument = new JSONObject();
        jsonDocument.put("id", outputDocumentOne.getId());
        jsonDocument.put("type", "OUTPUT");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "output document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(165));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1800));
        jsonDocument.put("counter", 4);
        jsonDocument.put("warehouse", jsonWarehouse);

        OutputDetail detailOne = list.get(0);
        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", detailOne.getId());
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(15));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(300));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        OutputDetail detailTwo = list.get(1);
        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", detailTwo.getId());
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", true);
        jsonDetailTwo.put("item", jsonItemTwo);

        OutputDetail detailThree = list.get(2);
        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", detailThree.getId());
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetailFour = new JSONObject();
        jsonDetailFour.put("id", JSONObject.NULL);
        jsonDetailFour.put("lineNumber", 4);
        jsonDetailFour.put("description", "detail four");
        jsonDetailFour.put("quantity", Utils.getAsBigDecimal(50));
        jsonDetailFour.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailFour.put("totalPrice", Utils.getAsBigDecimal(500));
        jsonDetailFour.put("deleted", false);
        jsonDetailFour.put("item", jsonItemFour);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);
        jsonDetails.put(jsonDetailFour);

        jsonDocument.put("details", jsonDetails);

        body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.PUT, entity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, entity, String.class);

        result = mapper.readValue(response.getBody(), OutputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(outputDocumentOne.getId(), result.getId());
        assertEquals("output document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Libera el documento">
        outputDocumentEntity = new HttpEntity<OutputDocument>(outputDocumentOne, headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/release"),
                        HttpMethod.PUT, outputDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, outputDocumentEntity, String.class);

        result = mapper.readValue(response.getBody(), OutputDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.OUTPUT, result.getType());
        assertEquals(outputDocumentOne.getId(), result.getId());
        assertEquals("output document one", result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(165), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1800), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Comprueba el inventario">
        
        headers = Utils.getHeaders(headers, tokenOne);
        warehouseEntity = new HttpEntity<Warehouse>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath + warehouseOne.getId()),
                        HttpMethod.GET, warehouseEntity, String.class);

        warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);

        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());
        //</editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Elimina el documento">
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.DELETE, outputDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, outputDocumentEntity, String.class);

        boolean message = response.getBody().contains(
                MessageFormat.format(
                        Messages.DOCUMENT_NOT_FOUND, outputDocumentOne.getId(), DocumentType.OUTPUT));

        assertTrue(message);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        // </editor-fold>
    }

    @Test
    public void purchaseReturnDocumentTest() throws Exception {

        // <editor-fold defaultstate="collapsed" desc="Crea el documento">
        
        jsonDocument = new JSONObject();
        jsonDocument.put("id", JSONObject.NULL);
        jsonDocument.put("type", "PURCHASE_RETURN");
        jsonDocument.put("date", formatDateTime);
        jsonDocument.put("status", "OPEN");
        jsonDocument.put("description", "purchase return document one");
        jsonDocument.put("totalQuantity", Utils.getAsBigDecimal(130));
        jsonDocument.put("totalAmount", Utils.getAsBigDecimal(1300));
        jsonDocument.put("counter", 0);
        jsonDocument.put("warehouse", jsonWarehouse);

        jsonDetailOne = new JSONObject();
        jsonDetailOne.put("id", JSONObject.NULL);
        jsonDetailOne.put("lineNumber", 1);
        jsonDetailOne.put("description", "detail one");
        jsonDetailOne.put("quantity", Utils.getAsBigDecimal(10));
        jsonDetailOne.put("unitPrice", Utils.getAsBigDecimal(20));
        jsonDetailOne.put("totalPrice", Utils.getAsBigDecimal(200));
        jsonDetailOne.put("deleted", false);
        jsonDetailOne.put("item", jsonItemOne);

        jsonDetailTwo = new JSONObject();
        jsonDetailTwo.put("id", JSONObject.NULL);
        jsonDetailTwo.put("lineNumber", 2);
        jsonDetailTwo.put("description", "detail two");
        jsonDetailTwo.put("quantity", Utils.getAsBigDecimal(20));
        jsonDetailTwo.put("unitPrice", Utils.getAsBigDecimal(5));
        jsonDetailTwo.put("totalPrice", Utils.getAsBigDecimal(100));
        jsonDetailTwo.put("deleted", false);
        jsonDetailTwo.put("item", jsonItemTwo);

        jsonDetailThree = new JSONObject();
        jsonDetailThree.put("id", JSONObject.NULL);
        jsonDetailThree.put("lineNumber", 3);
        jsonDetailThree.put("description", "detail three");
        jsonDetailThree.put("quantity", Utils.getAsBigDecimal(100));
        jsonDetailThree.put("unitPrice", Utils.getAsBigDecimal(10));
        jsonDetailThree.put("totalPrice", Utils.getAsBigDecimal(1000));
        jsonDetailThree.put("deleted", false);
        jsonDetailThree.put("item", jsonItemThree);

        jsonDetails = new JSONArray();
        jsonDetails.put(jsonDetailOne);
        jsonDetails.put(jsonDetailTwo);
        jsonDetails.put(jsonDetailThree);

        jsonDocument.put("details", jsonDetails);

        ItemSummary itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(30));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(600));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryOne);

        ItemSummary itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(40));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(200));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryTwo);

        ItemSummary itemSummaryThree = new ItemSummary(warehouseOne, itemThree);
        itemSummaryThree.setQuantity(Utils.getAsBigDecimal(400));
        itemSummaryThree.setUnitPrice(Utils.getAsBigDecimal(10));
        itemSummaryThree.setTotalPrice(Utils.getAsBigDecimal(4000));
        itemSummaryThree.setLastUpdated(LocalDateTime.now());
        itemSummaryRepository.save(itemSummaryThree);

        String body = jsonDocument.toString();
        entity = new HttpEntity<String>(body, headers);

        ResponseEntity<String> response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, dispatchPath + "/type/PURCHASE_RETURN"),
                        HttpMethod.POST, entity, String.class);

        purchaseReturnDocumentOne = mapper.readValue(response.getBody(), PurchaseReturnDocument.class);

        purchaseReturnDocumentEntity = new HttpEntity<PurchaseReturnDocument>(headers);

        String pathGetById = dispatchPath + "/type/PURCHASE_RETURN/id/" + purchaseReturnDocumentOne.getId();

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, purchaseReturnDocumentEntity, String.class);

        PurchaseReturnDocument result = mapper.readValue(response.getBody(), PurchaseReturnDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.PURCHASE_RETURN, result.getType());
        assertEquals(purchaseReturnDocumentOne.getId(), result.getId());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(Status.OPEN, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        ResponseEntity<RestPageImpl<PurchaseReturnDetail>> responseEntity;
        responseEntity = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/details"),
                        HttpMethod.GET, entity,
                        new ParameterizedTypeReference<RestPageImpl<PurchaseReturnDetail>>() {
                });

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<InputDetail> list = new ArrayList(responseEntity.getBody().getContent());
        assertNotNull(list);
        assertEquals(3, list.size());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Libera el documento">
        purchaseReturnDocumentEntity = new HttpEntity<PurchaseReturnDocument>(purchaseReturnDocumentOne, headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById + "/release"),
                        HttpMethod.PUT, purchaseReturnDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, purchaseReturnDocumentEntity, String.class);

        result = mapper.readValue(response.getBody(), PurchaseReturnDocument.class);

        assertNotNull(result);
        assertEquals(DocumentType.PURCHASE_RETURN, result.getType());
        assertEquals(purchaseReturnDocumentOne.getId(), result.getId());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(3, result.getDetails().size());

        //</editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Comprueba el inventario">
        
        headers = Utils.getHeaders(headers, tokenOne);
        warehouseEntity = new HttpEntity<Warehouse>(headers);
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, warehousePath + warehouseOne.getId()),
                        HttpMethod.GET, warehouseEntity, String.class);

        warehouseOne = mapper.readValue(response.getBody(), Warehouse.class);

        assertEquals(warehouseNameOne, warehouseOne.getWarehouseName());
//        assertEquals(3, warehouseOne.getItemSummary().size());
//        
//        itemSummaryOne = warehouseOne.getItemSummary().get(0);
//        assertEquals(itemOne.getId(), itemSummaryOne.getKey().getItem().getId());
//        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getQuantity());
//        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getUnitPrice());
//        assertEquals(Utils.getAsBigDecimal(400), itemSummaryOne.getTotalPrice());
//        
//        itemSummaryTwo = warehouseOne.getItemSummary().get(1);
//        assertEquals(itemTwo.getId(), itemSummaryTwo.getKey().getItem().getId());
//        assertEquals(Utils.getAsBigDecimal(20), itemSummaryTwo.getQuantity());
//        assertEquals(Utils.getAsBigDecimal(5), itemSummaryTwo.getUnitPrice());
//        assertEquals(Utils.getAsBigDecimal(100), itemSummaryTwo.getTotalPrice());
//        
//        itemSummaryThree = warehouseOne.getItemSummary().get(2);
//        assertEquals(itemThree.getId(), itemSummaryThree.getKey().getItem().getId());
//        assertEquals(Utils.getAsBigDecimal(300), itemSummaryThree.getQuantity());
//        assertEquals(Utils.getAsBigDecimal(10), itemSummaryThree.getUnitPrice());
//        assertEquals(Utils.getAsBigDecimal(3000), itemSummaryThree.getTotalPrice());

        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Elimina el documento">
        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.DELETE, purchaseReturnDocumentEntity, String.class);

        response = this.restTemplate
                .exchange(Utils.createURLWithPort(port, pathGetById),
                        HttpMethod.GET, purchaseReturnDocumentEntity, String.class);

        boolean message = response.getBody().contains(
                MessageFormat.format(
                        Messages.DOCUMENT_NOT_FOUND,
                        purchaseReturnDocumentOne.getId(),
                        DocumentType.PURCHASE_RETURN));

        assertTrue(message);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        // </editor-fold>
    }

    @AfterEach
    void cleanUp() {
        
        purchaseReturnDetailRepository.deleteAll();
        purchaseReturnDocumentRepository.deleteAll();
        outputDetailRepository.deleteAll();
        outputDocumentRepository.deleteAll();
        salesReturnDetailRepository.deleteAll();
        salesReturnDocumentRepository.deleteAll();
        inputDetailRepository.deleteAll();
        inputDocumentRepository.deleteAll();
        itemSummaryRepository.deleteAll();

        headers = Utils.getHeaders(headers, tokenOne);
        entity = new HttpEntity<>(headers);

        this.restTemplate.exchange(Utils.createURLWithPort(port, warehousePath),
                        HttpMethod.DELETE, entity, String.class);

        this.restTemplate.exchange(Utils.createURLWithPort(port, itemPath),
                        HttpMethod.DELETE, entity, String.class);

        userAccountRepository.deleteAll();
        roleRepository.deleteAll();
        permissionRepository.deleteAll();
        resourceRepository.deleteAll();
        organizationRepository.deleteAll();
    }
}
