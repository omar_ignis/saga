/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.integration;

import java.util.List;
import java.text.MessageFormat;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.util.Messages;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.service.ItemService;
import com.inventory.item.service.impl.InputDocumentServiceImpl;
import com.inventory.item.service.impl.OutputDocumentServiceImpl;
import com.inventory.item.service.impl.SalesReturnDocumentServiceImpl;
import com.inventory.item.service.impl.PurchaseReturnDocumentServiceImpl;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;
import com.inventory.item.repository.OutputDocumentRepository;
import com.inventory.item.repository.SalesReturnDetailRepository;
import com.inventory.item.repository.SalesReturnDocumentRepository;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 *
 * @author ignis
 */
@SpringBootTest
public class DocumentServiceIntegrationTest {

    @Autowired
    private ItemService itemService;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private InputDetailRepository inputDetailRepository;

    @Autowired
    private ItemSummaryRepository itemSummaryRepository;

    @Autowired
    private OutputDetailRepository outputDetailRepository;

    @Autowired
    private InputDocumentServiceImpl inputDocumentService;

    @Autowired
    private InputDocumentRepository inputDocumentRepository;

    @Autowired
    private OutputDocumentServiceImpl outputDocumentService;

    @Autowired
    private OutputDocumentRepository outputDocumentRepository;

    @Autowired
    private SalesReturnDetailRepository salesReturnDetailRepository;

    @Autowired
    private SalesReturnDocumentServiceImpl salesReturnDocumentService;

    @Autowired
    private SalesReturnDocumentRepository salesReturnDocumentRepository;

    @Autowired
    private PurchaseReturnDetailRepository purchaseReturnDetailRepository;

    @Autowired
    private PurchaseReturnDocumentServiceImpl purchaseReturnDocumentService;

    @Autowired
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;

    private Warehouse warehouseOne;

    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;
    private InputDetail inputDetailFour;
    private InputDetail inputDetailFive;

    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;
    private OutputDetail outputDetailFour;
    private OutputDetail outputDetailFive;

    private SalesReturnDetail saleReturnDetailOne;
    private SalesReturnDetail saleReturnDetailTwo;
    private SalesReturnDetail saleReturnDetailThree;
    private SalesReturnDetail saleReturnDetailFour;
    private SalesReturnDetail saleReturnDetailFive;

    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;
    private PurchaseReturnDetail purchaseReturnDetailFour;
    private PurchaseReturnDetail purchaseReturnDetailFive;

    private InputDocument inputDocumentOne;
    private InputDocument inputDocumentTwo;

    private OutputDocument outputDocumentOne;
    private OutputDocument outputDocumentTwo;

    private SalesReturnDocument saleReturnDocumentOne;
    private SalesReturnDocument saleReturnDocumentTwo;

    private PurchaseReturnDocument purchaseReturnDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentTwo;

    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;
    private ItemSummary itemSummaryThree;

    private LocalDateTime now;
    private final String itemNameOne = "Item One";
    private final String itemNameTwo = "Item Two";
    private final String itemNameThree = "Item Three";
    private final String itemNameFour = "Item Four";
    private final String itemNameFive = "Item Five";
    private final String warehouseNameOne = "Warehouse One";

    @BeforeEach
    @DisplayName("Crea los items y los warehouse")
    void init() {

        inputDetailRepository.deleteAll();
        inputDocumentRepository.deleteAll();
        salesReturnDetailRepository.deleteAll();
        salesReturnDocumentRepository.deleteAll();
        outputDetailRepository.deleteAll();
        outputDocumentRepository.deleteAll();
        purchaseReturnDetailRepository.deleteAll();
        purchaseReturnDocumentRepository.deleteAll();
        itemSummaryRepository.deleteAll();
        warehouseRepository.deleteAll();
        itemService.deleteAll();

        itemOne = new Item();
        itemOne.setItemName(itemNameOne);
        itemOne.setDescription("Items Eins Description");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne.setUsed(false);
        itemOne = itemService.save(itemOne);

        itemTwo = new Item();
        itemTwo.setItemName(itemNameTwo);
        itemTwo.setDescription("Items Zwei Description");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo.setUsed(false);
        itemTwo = itemService.save(itemTwo);

        itemThree = new Item();
        itemThree.setItemName(itemNameThree);
        itemThree.setDescription("Items Drei Description");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree.setUsed(false);
        itemThree = itemService.save(itemThree);

        itemFour = new Item();
        itemFour.setItemName(itemNameFour);
        itemFour.setDescription("Items Vier Description");
        itemFour.setValuationType(ValuationType.AVERAGE);
        itemFour.setUsed(false);
        itemFour = itemService.save(itemFour);

        itemFive = new Item();
        itemFive.setItemName(itemNameFive);
        itemFive.setDescription("Items Funf Description");
        itemFive.setValuationType(ValuationType.AVERAGE);
        itemFive.setUsed(false);
        itemFive = itemService.save(itemFive);

        warehouseOne = new Warehouse(warehouseNameOne);
        warehouseOne = warehouseRepository.save(warehouseOne);

        now = LocalDateTime.now();
    }

    @Test
    public void saveInputDocumentTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setCounter(3);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> details = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        InputDocument result = inputDocumentService.saveOrUpdate(inputDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("IN0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<InputDetail> list = result.getDetails();

        InputDetail detail = list.get(0);

        assertEquals(1, detail.getLineNumber());

        detail = list.get(1);

        assertEquals(2, detail.getLineNumber());

        detail = list.get(2);

        assertEquals(3, detail.getLineNumber());
    }

    @Test
    public void updateInputDocumentTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setCounter(3);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> details = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        InputDocument result = inputDocumentService.saveOrUpdate(inputDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("IN0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<InputDetail> list = result.getDetails();

        InputDetail detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(1, detail.getLineNumber());
        assertEquals(itemOne.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        inputDocumentTwo = inputDocumentService.getDocumentById(result.getId());

        //It returns details order by line number desc
        // position 0 detail three
        // position 1 detail two
        // position 2 details one
        inputDocumentTwo.setDescription("input document updated");

        inputDetailFour = new InputDetail();
        inputDetailFour.setDescription("detail four");
        inputDetailFour.setItem(itemFour);
        inputDetailFour.setLineNumber(4);
        inputDetailFour.setQuantity(Utils.getAsBigDecimal(30));
        inputDetailFour.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailFour.setTotalPrice(Utils.getAsBigDecimal(150));

        inputDetailFive = inputDocumentTwo.getDetails().get(2);
        inputDetailFive.setDeleted(true);

        details = List.of(
                inputDocumentTwo.getDetails().get(0),
                inputDocumentTwo.getDetails().get(1),
                inputDetailFour,
                inputDetailFive);

        //it removes the detail one
        result = inputDocumentService.update(inputDocumentTwo, details);
        result = inputDocumentService.getDocumentById(result.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("IN0000"));
        assertEquals(now, result.getDate());
        assertEquals(4, result.getCounter());
        assertEquals("input document updated", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        list = result.getDetails();

        detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(4, detail.getLineNumber());
        assertEquals(itemFour.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());
    }

    @Test
    public void releaseInputDocumentTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setCounter(3);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> details = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        inputDocumentOne = inputDocumentService.saveOrUpdate(inputDocumentOne, details);

        InputDocument result = inputDocumentService.release(inputDocumentOne.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("IN0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("input document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(3, result.getDetails().size());

        List<ItemSummary> list = itemSummaryRepository.findAll();

        assertEquals(3, list.size());

        itemSummaryOne = list.get(0);
        assertEquals(itemOne.getId(), itemSummaryOne.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryOne.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(10), itemSummaryOne.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), itemSummaryOne.getTotalPrice());

        itemSummaryTwo = list.get(1);
        assertEquals(itemTwo.getId(), itemSummaryTwo.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryTwo.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryTwo.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), itemSummaryTwo.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(100), itemSummaryTwo.getTotalPrice());

        itemSummaryThree = list.get(2);
        assertEquals(itemThree.getId(), itemSummaryThree.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryThree.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(100), itemSummaryThree.getQuantity());
        assertEquals(Utils.getAsBigDecimal(10), itemSummaryThree.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1000), itemSummaryThree.getTotalPrice());

    }

    @Test
    public void deleteInputDocumentTest() throws Exception, Throwable {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setCounter(3);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<InputDetail> details = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        inputDocumentOne = inputDocumentService.saveOrUpdate(inputDocumentOne, details);

        inputDocumentService.remove(inputDocumentOne.getId());

        Exception exception = assertThrows(Exception.class, () -> {
            inputDocumentService.getDocumentById(inputDocumentOne.getId());
        });

        boolean message = exception.getMessage().contains(
                MessageFormat.format(
                        Messages.DOCUMENT_NOT_FOUND, inputDocumentOne.getId(), DocumentType.INPUT));

        assertTrue(message);

    }

    @Test
    public void saveSaleReturnDocumentTest() {

        saleReturnDocumentOne = new SalesReturnDocument();
        saleReturnDocumentOne.setDate(now);
        saleReturnDocumentOne.setCounter(3);
        saleReturnDocumentOne.setDescription("sales return document one");
        saleReturnDocumentOne.setWarehouse(warehouseOne);
        saleReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        saleReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        saleReturnDetailOne = new SalesReturnDetail();
        saleReturnDetailOne.setDescription("detail one");
        saleReturnDetailOne.setItem(itemOne);
        saleReturnDetailOne.setLineNumber(1);
        saleReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        saleReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        saleReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        saleReturnDetailTwo = new SalesReturnDetail();
        saleReturnDetailTwo.setDescription("detail two");
        saleReturnDetailTwo.setItem(itemTwo);
        saleReturnDetailTwo.setLineNumber(2);
        saleReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        saleReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        saleReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        saleReturnDetailThree = new SalesReturnDetail();
        saleReturnDetailThree.setDescription("detail three");
        saleReturnDetailThree.setItem(itemThree);
        saleReturnDetailThree.setLineNumber(3);
        saleReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        saleReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        saleReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<SalesReturnDetail> details = List.of(saleReturnDetailOne, saleReturnDetailTwo, saleReturnDetailThree);

        SalesReturnDocument result = salesReturnDocumentService.saveOrUpdate(saleReturnDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("SR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<SalesReturnDetail> list = result.getDetails();

        SalesReturnDetail detail = list.get(0);

        assertEquals(1, detail.getLineNumber());

        detail = list.get(1);

        assertEquals(2, detail.getLineNumber());

        detail = list.get(2);

        assertEquals(3, detail.getLineNumber());
    }

    @Test
    public void updateSaleReturnDocumentTest() throws Exception, Throwable {

        saleReturnDocumentOne = new SalesReturnDocument();
        saleReturnDocumentOne.setDate(now);
        saleReturnDocumentOne.setCounter(3);
        saleReturnDocumentOne.setDescription("sales return document one");
        saleReturnDocumentOne.setWarehouse(warehouseOne);
        saleReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        saleReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        saleReturnDetailOne = new SalesReturnDetail();
        saleReturnDetailOne.setDescription("detail one");
        saleReturnDetailOne.setItem(itemOne);
        saleReturnDetailOne.setLineNumber(1);
        saleReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        saleReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        saleReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        saleReturnDetailTwo = new SalesReturnDetail();
        saleReturnDetailTwo.setDescription("detail two");
        saleReturnDetailTwo.setItem(itemTwo);
        saleReturnDetailTwo.setLineNumber(2);
        saleReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        saleReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        saleReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        saleReturnDetailThree = new SalesReturnDetail();
        saleReturnDetailThree.setDescription("detail three");
        saleReturnDetailThree.setItem(itemThree);
        saleReturnDetailThree.setLineNumber(3);
        saleReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        saleReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        saleReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<SalesReturnDetail> details = List
                .of(saleReturnDetailOne, saleReturnDetailTwo, saleReturnDetailThree);

        SalesReturnDocument result = salesReturnDocumentService
                .saveOrUpdate(saleReturnDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("SR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<SalesReturnDetail> list = result.getDetails();

        SalesReturnDetail detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(1, detail.getLineNumber());
        assertEquals(itemOne.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        saleReturnDocumentTwo = salesReturnDocumentService.getDocumentById(result.getId());

        saleReturnDocumentTwo.setDescription("sales return document updated");

        saleReturnDetailFour = new SalesReturnDetail();
        saleReturnDetailFour.setDescription("detail four");
        saleReturnDetailFour.setItem(itemFour);
        saleReturnDetailFour.setLineNumber(4);
        saleReturnDetailFour.setQuantity(Utils.getAsBigDecimal(30));
        saleReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(5));
        saleReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(150));

        saleReturnDetailFive = saleReturnDocumentTwo.getDetails().get(2);
        saleReturnDetailFive.setDeleted(true);

        details = List.of(
                saleReturnDocumentTwo.getDetails().get(0),
                saleReturnDocumentTwo.getDetails().get(1),
                saleReturnDetailFour,
                saleReturnDetailFive);

        result = salesReturnDocumentService.update(saleReturnDocumentTwo, details);
        result = salesReturnDocumentService.getDocumentById(result.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("SR0000"));
        assertEquals(now, result.getDate());
        assertEquals(4, result.getCounter());
        assertEquals("sales return document updated", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        list = result.getDetails();

        detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(4, detail.getLineNumber());
        assertEquals(itemFour.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());
    }

    @Test
    public void releaseSaleReturnDocumentTest() throws Exception, Throwable {

        saleReturnDocumentOne = new SalesReturnDocument();
        saleReturnDocumentOne.setDate(now);
        saleReturnDocumentOne.setCounter(3);
        saleReturnDocumentOne.setDescription("sales return document one");
        saleReturnDocumentOne.setWarehouse(warehouseOne);
        saleReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        saleReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        saleReturnDetailOne = new SalesReturnDetail();
        saleReturnDetailOne.setDescription("detail one");
        saleReturnDetailOne.setItem(itemOne);
        saleReturnDetailOne.setLineNumber(1);
        saleReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        saleReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        saleReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        saleReturnDetailTwo = new SalesReturnDetail();
        saleReturnDetailTwo.setDescription("detail two");
        saleReturnDetailTwo.setItem(itemTwo);
        saleReturnDetailTwo.setLineNumber(2);
        saleReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        saleReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        saleReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        saleReturnDetailThree = new SalesReturnDetail();
        saleReturnDetailThree.setDescription("detail three");
        saleReturnDetailThree.setItem(itemThree);
        saleReturnDetailThree.setLineNumber(3);
        saleReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        saleReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        saleReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<SalesReturnDetail> details = List.of(saleReturnDetailOne, saleReturnDetailTwo, saleReturnDetailThree);

        saleReturnDocumentOne = salesReturnDocumentService.saveOrUpdate(saleReturnDocumentOne, details);

        SalesReturnDocument result = salesReturnDocumentService.release(saleReturnDocumentOne.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("SR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("sales return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(3, result.getDetails().size());

        List<ItemSummary> list = itemSummaryRepository.findAll();

        assertEquals(3, list.size());

        itemSummaryOne = list.get(0);
        assertEquals(itemOne.getId(), itemSummaryOne.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryOne.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(10), itemSummaryOne.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(200), itemSummaryOne.getTotalPrice());

        itemSummaryTwo = list.get(1);
        assertEquals(itemTwo.getId(), itemSummaryTwo.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryTwo.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryTwo.getQuantity());
        assertEquals(Utils.getAsBigDecimal(5), itemSummaryTwo.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(100), itemSummaryTwo.getTotalPrice());

        itemSummaryThree = list.get(2);
        assertEquals(itemThree.getId(), itemSummaryThree.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryThree.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(100), itemSummaryThree.getQuantity());
        assertEquals(Utils.getAsBigDecimal(10), itemSummaryThree.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1000), itemSummaryThree.getTotalPrice());
    }

    @Test
    public void saveOutputDocumentTest() {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setCounter(3);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<OutputDetail> details = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);

        OutputDocument result = outputDocumentService.saveOrUpdate(outputDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("OU0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<OutputDetail> list = result.getDetails();

        OutputDetail detail = list.get(0);

        assertEquals(1, detail.getLineNumber());

        detail = list.get(1);

        assertEquals(2, detail.getLineNumber());

        detail = list.get(2);

        assertEquals(3, detail.getLineNumber());
    }

    @Test
    public void updateOutputDocumentTest() throws Exception, Throwable {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setCounter(3);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<OutputDetail> details = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);

        OutputDocument result = outputDocumentService.saveOrUpdate(outputDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("OU0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<OutputDetail> list = result.getDetails();

        OutputDetail detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(1, detail.getLineNumber());
        assertEquals(itemOne.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        outputDocumentTwo = outputDocumentService.getDocumentById(result.getId());

        //It returns details order by line number desc
        // position 0 detail three
        // position 1 detail two
        // position 2 details one
        outputDocumentTwo.setDescription("output document updated");

        outputDetailFour = new OutputDetail();
        outputDetailFour.setDescription("detail four");
        outputDetailFour.setItem(itemFour);
        outputDetailFour.setLineNumber(4);
        outputDetailFour.setQuantity(Utils.getAsBigDecimal(30));
        outputDetailFour.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailFour.setTotalPrice(Utils.getAsBigDecimal(150));

        outputDetailFive = outputDocumentTwo.getDetails().get(2);
        outputDetailFive.setDeleted(true);

        details = List.of(
                outputDocumentTwo.getDetails().get(0),
                outputDocumentTwo.getDetails().get(1),
                outputDetailFour,
                outputDetailFive);

        result = outputDocumentService.update(outputDocumentTwo, details);
        result = outputDocumentService.getDocumentById(result.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("OU0000"));
        assertEquals(now, result.getDate());
        assertEquals(4, result.getCounter());
        assertEquals("output document updated", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        list = result.getDetails();

        detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(4, detail.getLineNumber());
        assertEquals(itemFour.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());
    }

    @Test
    public void releaseOutputDocumentTest() throws Exception, Throwable {

        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setCounter(3);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(30));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(600));

        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(45));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(4500));

        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(500));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(10000));
        itemSummaryOne.setLastUpdated(now);
        itemSummaryOne = itemSummaryRepository.save(itemSummaryOne);

        itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(200));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(6000));
        itemSummaryTwo.setLastUpdated(now);
        itemSummaryTwo = itemSummaryRepository.save(itemSummaryTwo);

        itemSummaryThree = new ItemSummary(warehouseOne, itemThree);
        itemSummaryThree.setQuantity(Utils.getAsBigDecimal(135));
        itemSummaryThree.setUnitPrice(Utils.getAsBigDecimal(45));
        itemSummaryThree.setTotalPrice(Utils.getAsBigDecimal(5850));
        itemSummaryThree.setLastUpdated(now);
        itemSummaryThree = itemSummaryRepository.save(itemSummaryThree);

        List<OutputDetail> details = List.of(outputDetailOne, outputDetailTwo, outputDetailThree);

        outputDocumentOne = outputDocumentService.saveOrUpdate(outputDocumentOne, details);

        OutputDocument result = outputDocumentService.release(outputDocumentOne.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("OU0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("output document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(3, result.getDetails().size());

        List<ItemSummary> list = itemSummaryRepository.findAll();

        assertEquals(3, list.size());

        itemSummaryOne = list.get(0);
        assertEquals(itemOne.getId(), itemSummaryOne.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryOne.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(490), itemSummaryOne.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(9800), itemSummaryOne.getTotalPrice());

        itemSummaryTwo = list.get(1);
        assertEquals(itemTwo.getId(), itemSummaryTwo.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryTwo.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(180), itemSummaryTwo.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), itemSummaryTwo.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(5400), itemSummaryTwo.getTotalPrice());

        itemSummaryThree = list.get(2);
        assertEquals(itemThree.getId(), itemSummaryThree.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryThree.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(35), itemSummaryThree.getQuantity());
        assertEquals(Utils.getAsBigDecimal(45), itemSummaryThree.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1350), itemSummaryThree.getTotalPrice());
    }

    @Test
    public void savePurchaseReturnDocumentTest() {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setCounter(3);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<PurchaseReturnDetail> details = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);

        PurchaseReturnDocument result = purchaseReturnDocumentService.saveOrUpdate(purchaseReturnDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("PR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<PurchaseReturnDetail> list = result.getDetails();

        PurchaseReturnDetail detail = list.get(0);

        assertEquals(1, detail.getLineNumber());

        detail = list.get(1);

        assertEquals(2, detail.getLineNumber());

        detail = list.get(2);

        assertEquals(3, detail.getLineNumber());
    }

    @Test
    public void updatePurchaseReturnDocumentTest() throws Exception, Throwable {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setCounter(3);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));

        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));

        List<PurchaseReturnDetail> details = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);

        PurchaseReturnDocument result = purchaseReturnDocumentService.saveOrUpdate(purchaseReturnDocumentOne, details);

        assertNotNull(result);
        assertTrue(result.getId().contains("PR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        List<PurchaseReturnDetail> list = result.getDetails();

        PurchaseReturnDetail detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(1, detail.getLineNumber());
        assertEquals(itemOne.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        purchaseReturnDocumentTwo = purchaseReturnDocumentService.getDocumentById(result.getId());

        //It returns details order by line number desc
        // position 0 detail three
        // position 1 detail two
        // position 2 details one
        purchaseReturnDocumentTwo.setDescription("purchase return document updated");

        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setDescription("detail four");
        purchaseReturnDetailFour.setItem(itemFour);
        purchaseReturnDetailFour.setLineNumber(4);
        purchaseReturnDetailFour.setQuantity(Utils.getAsBigDecimal(30));
        purchaseReturnDetailFour.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailFour.setTotalPrice(Utils.getAsBigDecimal(150));

        purchaseReturnDetailFive = purchaseReturnDocumentTwo.getDetails().get(2);
        purchaseReturnDetailFive.setDeleted(true);

        details = List.of(
                purchaseReturnDocumentTwo.getDetails().get(0),
                purchaseReturnDocumentTwo.getDetails().get(1),
                purchaseReturnDetailFour,
                purchaseReturnDetailFive);

        result = purchaseReturnDocumentService.update(purchaseReturnDocumentTwo, details);
        result = purchaseReturnDocumentService.getDocumentById(result.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("PR0000"));
        assertEquals(now, result.getDate());
        assertEquals(4, result.getCounter());
        assertEquals("purchase return document updated", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(3, result.getDetails().size());

        list = result.getDetails();

        detail = list.get(0);

        assertTrue(detail.getId() > 0L);
        assertEquals(4, detail.getLineNumber());
        assertEquals(itemFour.getId(), detail.getItem().getId());

        detail = list.get(1);

        assertTrue(detail.getId() > 0L);
        assertEquals(3, detail.getLineNumber());
        assertEquals(itemThree.getId(), detail.getItem().getId());

        detail = list.get(2);

        assertTrue(detail.getId() > 0L);
        assertEquals(2, detail.getLineNumber());
        assertEquals(itemTwo.getId(), detail.getItem().getId());
    }

    @Test
    public void releasePurchaseReturnDocumentTest() throws Exception, Throwable {

        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setCounter(3);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));

        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(30));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(600));

        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(45));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(4500));

        itemSummaryOne = new ItemSummary(warehouseOne, itemOne);
        itemSummaryOne.setQuantity(Utils.getAsBigDecimal(500));
        itemSummaryOne.setUnitPrice(Utils.getAsBigDecimal(20));
        itemSummaryOne.setTotalPrice(Utils.getAsBigDecimal(10000));
        itemSummaryOne.setLastUpdated(now);
        itemSummaryOne = itemSummaryRepository.save(itemSummaryOne);

        itemSummaryTwo = new ItemSummary(warehouseOne, itemTwo);
        itemSummaryTwo.setQuantity(Utils.getAsBigDecimal(200));
        itemSummaryTwo.setUnitPrice(Utils.getAsBigDecimal(30));
        itemSummaryTwo.setTotalPrice(Utils.getAsBigDecimal(6000));
        itemSummaryTwo.setLastUpdated(now);
        itemSummaryTwo = itemSummaryRepository.save(itemSummaryTwo);

        itemSummaryThree = new ItemSummary(warehouseOne, itemThree);
        itemSummaryThree.setQuantity(Utils.getAsBigDecimal(135));
        itemSummaryThree.setUnitPrice(Utils.getAsBigDecimal(45));
        itemSummaryThree.setTotalPrice(Utils.getAsBigDecimal(5850));
        itemSummaryThree.setLastUpdated(now);
        itemSummaryThree = itemSummaryRepository.save(itemSummaryThree);

        List<PurchaseReturnDetail> details = List.of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);

        purchaseReturnDocumentOne = purchaseReturnDocumentService.saveOrUpdate(purchaseReturnDocumentOne, details);

        PurchaseReturnDocument result = purchaseReturnDocumentService.release(purchaseReturnDocumentOne.getId());

        assertNotNull(result);
        assertTrue(result.getId().contains("PR0000"));
        assertEquals(now, result.getDate());
        assertEquals(3, result.getCounter());
        assertEquals("purchase return document one", result.getDescription());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(1300), result.getTotalAmount());
        assertEquals(Utils.getAsBigDecimal(130), result.getTotalQuantity());
        assertEquals(Status.RELEASED, result.getStatus());
        assertEquals(3, result.getDetails().size());

        List<ItemSummary> list = itemSummaryRepository.findAll();

        assertEquals(3, list.size());

        itemSummaryOne = list.get(0);
        assertEquals(itemOne.getId(), itemSummaryOne.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryOne.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(490), itemSummaryOne.getQuantity());
        assertEquals(Utils.getAsBigDecimal(20), itemSummaryOne.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(9800), itemSummaryOne.getTotalPrice());

        itemSummaryTwo = list.get(1);
        assertEquals(itemTwo.getId(), itemSummaryTwo.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryTwo.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(180), itemSummaryTwo.getQuantity());
        assertEquals(Utils.getAsBigDecimal(30), itemSummaryTwo.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(5400), itemSummaryTwo.getTotalPrice());

        itemSummaryThree = list.get(2);
        assertEquals(itemThree.getId(), itemSummaryThree.getKey().getItem().getId());
        assertEquals(warehouseOne.getId(), itemSummaryThree.getKey().getWarehouse().getId());
        assertEquals(Utils.getAsBigDecimal(35), itemSummaryThree.getQuantity());
        assertEquals(Utils.getAsBigDecimal(45), itemSummaryThree.getUnitPrice());
        assertEquals(Utils.getAsBigDecimal(1350), itemSummaryThree.getTotalPrice());
    }

}
