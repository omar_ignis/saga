/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.report;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.BarcodeLabel;

import com.inventory.item.report.ReportGenerator;
import com.inventory.item.barcode.sheet.impl.OD5160SheetImpl;

import org.mockito.InjectMocks;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 *
 * @author ignis
 */
@ExtendWith(MockitoExtension.class)
public class ReportGeneratorTest {

    @InjectMocks
    private ReportGenerator labelSheet;

    @InjectMocks
    private OD5160SheetImpl oD5160SheetImpl;

    @Test
    public void generateReportTest() throws Exception {

        BarcodeLabel barcodeLabelOne = new BarcodeLabel();
        barcodeLabelOne.setBarcode("00000000001000100001");
        barcodeLabelOne.setLabel("itemOne");
        barcodeLabelOne.setQuantity(1);

        BarcodeLabel barcodeLabelTwo = new BarcodeLabel();
        barcodeLabelTwo.setBarcode("00000000001000100002");
        barcodeLabelTwo.setLabel("itemTwo");
        barcodeLabelTwo.setQuantity(2);

        BarcodeLabel barcodeLabelThree = new BarcodeLabel();
        barcodeLabelThree.setBarcode("00000000001000100003");
        barcodeLabelThree.setLabel("itemThree");
        barcodeLabelThree.setQuantity(2);

        BarcodeLabel barcodeLabelFour = new BarcodeLabel();
        barcodeLabelFour.setBarcode("00000000001000100004");
        barcodeLabelFour.setLabel("itemFour");
        barcodeLabelFour.setQuantity(5);

        BarcodeLabel barcodeLabelFive = new BarcodeLabel();
        barcodeLabelFive.setBarcode("00000000001000100005");
        barcodeLabelFive.setLabel("itemFour");
        barcodeLabelFive.setQuantity(10);

        List<BarcodeLabel> barcodeLabelList = new ArrayList();
        barcodeLabelList.add(barcodeLabelOne);
        barcodeLabelList.add(barcodeLabelTwo);
        barcodeLabelList.add(barcodeLabelThree);
        barcodeLabelList.add(barcodeLabelFour);
        barcodeLabelList.add(barcodeLabelFive);

        List<Integer> list = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);

        List<RowSheet> result = oD5160SheetImpl.createCustomSheet(barcodeLabelList, list);

        byte[] data = labelSheet.getReportAsByArray(result);
        assertNotNull(data);

    }

    @Test
    public void generatePDFReportTest() throws Exception {

        BarcodeLabel barcodeLabelOne = new BarcodeLabel();
        barcodeLabelOne.setBarcode("00000000001000100001");
        barcodeLabelOne.setLabel("itemOne");
        barcodeLabelOne.setQuantity(1);

        BarcodeLabel barcodeLabelTwo = new BarcodeLabel();
        barcodeLabelTwo.setBarcode("00000000001000100002");
        barcodeLabelTwo.setLabel("itemTwo");
        barcodeLabelTwo.setQuantity(2);

        BarcodeLabel barcodeLabelThree = new BarcodeLabel();
        barcodeLabelThree.setBarcode("00000000001000100003");
        barcodeLabelThree.setLabel("itemThree");
        barcodeLabelThree.setQuantity(2);

        BarcodeLabel barcodeLabelFour = new BarcodeLabel();
        barcodeLabelFour.setBarcode("00000000001000100004");
        barcodeLabelFour.setLabel("itemFour");
        barcodeLabelFour.setQuantity(5);

        BarcodeLabel barcodeLabelFive = new BarcodeLabel();
        barcodeLabelFive.setBarcode("00000000001000100005");
        barcodeLabelFive.setLabel("itemFour");
        barcodeLabelFive.setQuantity(10);

        List<BarcodeLabel> barcodeLabelList = new ArrayList();
        barcodeLabelList.add(barcodeLabelOne);
        barcodeLabelList.add(barcodeLabelTwo);
        barcodeLabelList.add(barcodeLabelThree);
        barcodeLabelList.add(barcodeLabelFour);
        barcodeLabelList.add(barcodeLabelFive);

        List<Integer> list = List.of(1, 8, 9, 15, 16, 24, 25, 26, 30, 59);

        List<RowSheet> result = oD5160SheetImpl.createCustomSheet(barcodeLabelList, list);

        labelSheet.generateReport(result);
    }
}
