/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.enums.ValuationType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class InputDetailRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private InputDetailRepository inputDetailRepository;

    @Autowired
    private InputDocumentRepository inputDocumentRepository;

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;

    private InputDocument inputDocumentOne;

    @BeforeEach
    void init() {
        warehouseRepository.deleteAll();
        itemRepository.deleteAll();
        inputDocumentRepository.deleteAll();

        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);

        itemOne = new Item();
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne = itemRepository.save(itemOne);

        itemTwo = new Item();
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo = itemRepository.save(itemTwo);

        itemThree = new Item();
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree = itemRepository.save(itemThree);

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN000000001");
        inputDocumentOne.setDate(LocalDateTime.now());
        inputDocumentOne.setDescription("input document description one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalQuantity(BigDecimal.ONE);
        inputDocumentOne.setTotalAmount(BigDecimal.TEN);

        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);

        now = LocalDateTime.now();
    }

    @Test
    public void saveTest() {

        inputDetailOne = new InputDetail();
        inputDetailOne.setInputDocument(inputDocumentOne);
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(BigDecimal.TEN);
        inputDetailOne.setUnitPrice(BigDecimal.TEN);
        inputDetailOne.setTotalPrice(BigDecimal.TEN);

        InputDetail result = inputDetailRepository.save(inputDetailOne);

        assertTrue(result.getId() > 1);
        assertEquals(itemOne, result.getItem());
        assertEquals(itemOne.getId(), result.getItem().getId());
        assertEquals(1, result.getLineNumber());
        assertEquals(BigDecimal.TEN, result.getQuantity());
        assertEquals(BigDecimal.TEN, result.getUnitPrice());
        assertEquals(BigDecimal.TEN, result.getTotalPrice());

        inputDocumentOne = inputDocumentRepository.findById("IN000000001").get();
        assertEquals(1, inputDocumentOne.getDetails().size());
    }

    @Test
    public void updateTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailTwo);

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailThree);

        InputDocument result = inputDocumentRepository.findById("IN0000000001").get();

        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void findByInputDocumentTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailTwo);

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailThree);

        List<InputDetail> list = inputDetailRepository.findByInputDocument(inputDocumentOne);

        assertEquals(3, list.size());
    }

    @Test
    public void findByInputDocumentAndLineNumberAndItemIdTest() {

        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        inputDetailOne.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailOne);

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        inputDetailTwo.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailTwo);

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        inputDetailThree.setInputDocument(inputDocumentOne);

        inputDetailRepository.save(inputDetailThree);

        var result = inputDetailRepository
                .findByInputDocumentAndLineNumberAndItemId(inputDocumentOne, 2, itemTwo.getId());

        assertEquals(inputDocumentOne.getId(), result.getInputDocument().getId());
        assertEquals(itemTwo.getId(), result.getItem().getId());
        assertEquals(2, result.getLineNumber());
    }
}
