/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.PurchaseReturnDocument;

import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PurchaseReturnDocumentRepositoryTest {
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;
    
    private LocalDateTime now;
    
    private Warehouse warehouseOne;
    private PurchaseReturnDocument purchaseReturnDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentTwo;
    private PurchaseReturnDocument purchaseReturnDocumentThree;
    
    @BeforeEach
    void init() {
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR000000001");
        purchaseReturnDocumentOne.setDate(LocalDateTime.now());
        purchaseReturnDocumentOne.setDescription("purchase return document description one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalQuantity(BigDecimal.ONE);
        purchaseReturnDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        PurchaseReturnDocument result = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        assertEquals("PR000000001", result.getId());
        assertEquals(purchaseReturnDocumentOne.getDate(), result.getDate());
        assertEquals("purchase return document description one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(BigDecimal.ONE, result.getTotalQuantity());
        assertEquals(BigDecimal.TEN, result.getTotalAmount());
    }
    
    @Test
    public void getNextIdTest(){
        
        Long id = purchaseReturnDocumentRepository.getNextId();
        
        assertTrue(id > 0L);
    }
    
    @Test
    public void getPurchaseReturnDocumentByDeletedFalseTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000002");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        purchaseReturnDocumentTwo.setDeleted(true);
        
        purchaseReturnDocumentTwo = purchaseReturnDocumentRepository.save(purchaseReturnDocumentTwo);
        
        purchaseReturnDocumentThree = new PurchaseReturnDocument();
        purchaseReturnDocumentThree.setId("PR0000000003");
        purchaseReturnDocumentThree.setDate(now);
        purchaseReturnDocumentThree.setDescription("purchase return document one");
        purchaseReturnDocumentThree.setWarehouse(warehouseOne);
        purchaseReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        purchaseReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        purchaseReturnDocumentThree = purchaseReturnDocumentRepository.save(purchaseReturnDocumentThree);
        
        List<PurchaseReturnDocument> list = purchaseReturnDocumentRepository
                .findByDeletedFalse(PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        PurchaseReturnDocument result = list.get(0);
        assertEquals("PR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("PR0000000003", result.getId());
        
    }
    
    @Test
    public void getByIdLikeTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setId("PR0000000002");
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setDescription("purchase return document one");
        purchaseReturnDocumentTwo.setWarehouse(warehouseOne);
        purchaseReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        purchaseReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        purchaseReturnDocumentTwo.setDeleted(true);
        
        purchaseReturnDocumentTwo = purchaseReturnDocumentRepository.save(purchaseReturnDocumentTwo);
        
        purchaseReturnDocumentThree = new PurchaseReturnDocument();
        purchaseReturnDocumentThree.setId("PR0000000003");
        purchaseReturnDocumentThree.setDate(now);
        purchaseReturnDocumentThree.setDescription("purchase return document one");
        purchaseReturnDocumentThree.setWarehouse(warehouseOne);
        purchaseReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        purchaseReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        purchaseReturnDocumentThree = purchaseReturnDocumentRepository.save(purchaseReturnDocumentThree);
        
        List<PurchaseReturnDocument> list = purchaseReturnDocumentRepository
                .findByIdLike("PR000",PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        PurchaseReturnDocument result = list.get(0);
        assertEquals("PR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("PR0000000003", result.getId());
    }
}
