/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.enums.ValuationType;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SalesReturnDetailRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private SalesReturnDetailRepository salesReturnDetailRepository;

    @Autowired
    private SalesReturnDocumentRepository salesReturnDocumentRepository;

    private LocalDateTime now;

    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;

    private Warehouse warehouseOne;

    private SalesReturnDetail salesReturnDetailOne;
    private SalesReturnDetail salesReturnDetailTwo;
    private SalesReturnDetail salesReturnDetailThree;

    private SalesReturnDocument salesReturnDocumentOne;

    @BeforeEach
    void init() {
        warehouseRepository.deleteAll();
        itemRepository.deleteAll();
        salesReturnDocumentRepository.deleteAll();

        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);

        itemOne = new Item();
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne = itemRepository.save(itemOne);

        itemTwo = new Item();
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo = itemRepository.save(itemTwo);

        itemThree = new Item();
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree = itemRepository.save(itemThree);

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR000000001");
        salesReturnDocumentOne.setDate(LocalDateTime.now());
        salesReturnDocumentOne.setDescription("input document description one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalQuantity(BigDecimal.ONE);
        salesReturnDocumentOne.setTotalAmount(BigDecimal.TEN);

        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);

        now = LocalDateTime.now();
    }

    @Test
    public void saveTest() {

        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(BigDecimal.TEN);
        salesReturnDetailOne.setUnitPrice(BigDecimal.TEN);
        salesReturnDetailOne.setTotalPrice(BigDecimal.TEN);

        SalesReturnDetail result = salesReturnDetailRepository.save(salesReturnDetailOne);

        assertTrue(result.getId() > 1);
        assertEquals(itemOne, result.getItem());
        assertEquals(itemOne.getId(), result.getItem().getId());
        assertEquals(1, result.getLineNumber());
        assertEquals(BigDecimal.TEN, result.getQuantity());
        assertEquals(BigDecimal.TEN, result.getUnitPrice());
        assertEquals(BigDecimal.TEN, result.getTotalPrice());

        salesReturnDocumentOne = salesReturnDocumentRepository.findById("SR000000001").get();
        assertEquals(1, salesReturnDocumentOne.getDetails().size());
    }

    @Test
    public void updateTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("input document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);

        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailOne);

        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailTwo);

        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailThree);

        SalesReturnDocument result = salesReturnDocumentRepository.findById("SR0000000001").get();

        assertEquals(3, result.getDetails().size());

    }

    @Test
    public void findBySalesReturnDocumentTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("input document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);

        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailOne);

        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailTwo);

        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailThree);

        List<SalesReturnDetail> list = salesReturnDetailRepository.findBySalesReturnDocument(salesReturnDocumentOne);

        assertEquals(3, list.size());
    }

    @Test
    public void findBySalesReturnDocumentAndLineNumberAndItemIdTest() {

        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("input document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));

        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);

        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemOne);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        salesReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        salesReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        salesReturnDetailOne.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailOne);

        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemTwo);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        salesReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        salesReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        salesReturnDetailTwo.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailTwo);

        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemThree);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        salesReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        salesReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        salesReturnDetailThree.setSalesReturnDocument(salesReturnDocumentOne);

        salesReturnDetailRepository.save(salesReturnDetailThree);

        var result = salesReturnDetailRepository
                .findBySalesReturnDocumentAndLineNumberAndItemId(salesReturnDocumentOne, 2, itemTwo.getId());

        assertEquals(salesReturnDocumentOne.getId(), result.getSalesReturnDocument().getId());
        assertEquals(itemTwo.getId(), result.getItem().getId());
        assertEquals(2, result.getLineNumber());
    }

}
