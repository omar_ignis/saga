/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import com.inventory.item.util.Util;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.key.ItemSummaryKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

/**
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ItemSummaryRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private ItemSummaryRepository itemSummaryRepository;

    private Warehouse warehouseOne;
    private Warehouse warehouseTwo;

    private Item itemOne;
    private Item itemTwo;

    List<ItemSummary> listOne;
    List<ItemSummary> listTwo;

    private ItemSummaryKey itemSummaryKeyOne;
    private ItemSummaryKey itemSummaryKeyTwo;
    private ItemSummary itemSummaryOne;
    private ItemSummary itemSummaryTwo;

    private final String nameOne = "WarehouseOne";
    private final String nameTwo = "WarehouseTwo";

    private final String itemNameOne = "Item Eins";
    private final String itemDescOne = "Desc Eins";
    private final String itemNameTwo = "Item Zwei";
    private final String itemDescTwo = "Desc Zwei";

    private BigDecimal decimal_15;
    private BigDecimal decimal_200;
    private BigDecimal decimal_200Divide_15;
    private BigDecimal decimal_30;
    private BigDecimal decimal_600;
    private BigDecimal decimal_600Divide_30;
    private BigDecimal decimal_120;
    private BigDecimal decimal_1600;
    private BigDecimal decimal_1600Divide_120;
    private BigDecimal decimal_25;
    private BigDecimal decimal_400;
    private BigDecimal decimal_400Divide_25;

    @BeforeEach
    void init() {
        decimal_15 = new BigDecimal("15.00");
        decimal_200 = new BigDecimal("200.00");
        decimal_200Divide_15 = new BigDecimal("13.33");
        decimal_30 = new BigDecimal("30.00");
        decimal_600 = new BigDecimal("600.00");
        decimal_600Divide_30 = new BigDecimal("20.00");
        decimal_120 = new BigDecimal("120.00");
        decimal_1600 = new BigDecimal("1600.00");
        decimal_1600Divide_120 = new BigDecimal("13.33");
        decimal_25 = new BigDecimal("25.00");
        decimal_400 = new BigDecimal("400.00");
        decimal_400Divide_25 = new BigDecimal("16.00");

        itemOne = new Item(itemNameOne, itemDescOne);
        itemOne = itemRepository.save(itemOne);
        
        itemTwo = new Item(itemNameTwo, itemDescTwo);
        itemTwo = itemRepository.save(itemTwo);
        
        warehouseOne = new Warehouse(nameOne);
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        warehouseTwo = new Warehouse(nameTwo);
        warehouseTwo = warehouseRepository.save(warehouseTwo);
    }

    @Test
    public void testSaveItemSummary() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        ItemSummary result = itemSummaryRepository.save(itemSummaryOne);

        assertNotNull(result);
        assertNotNull(result.getLastUpdated());
        assertEquals(decimal_15, result.getQuantity());
        assertEquals(decimal_200, result.getTotalPrice());
        assertEquals(decimal_200Divide_15, result.getUnitPrice());
        assertEquals(warehouseOne.getId(), result.getKey().getWarehouse().getId());
        
    }

    @Test
    public void testGetItemSummaryById() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        ItemSummary result = itemSummaryRepository.save(itemSummaryOne);

        assertNotNull(result);
        assertNotNull(result.getLastUpdated());
        assertEquals(decimal_15, result.getQuantity());
        assertEquals(decimal_200, result.getTotalPrice());
        assertEquals(decimal_200Divide_15, result.getUnitPrice());

        result = itemSummaryRepository.findById(result.getKey()).get();
        assertEquals(decimal_15, result.getQuantity());
        assertEquals(decimal_200, result.getTotalPrice());
        assertEquals(decimal_200Divide_15, result.getUnitPrice());
        assertEquals(warehouseOne.getId(), result.getKey().getWarehouse().getId());


    }

    @Test
    public void testGetItemSummaryByItemId() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        ItemSummary result = itemSummaryRepository.save(itemSummaryOne);

        assertNotNull(result);
        assertNotNull(result.getLastUpdated());
        assertEquals(decimal_15, result.getQuantity());
        assertEquals(decimal_200, result.getTotalPrice());
        assertEquals(decimal_200Divide_15, result.getUnitPrice());

        result = itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouseOne, itemOne);

        assertNotNull(result);
        assertEquals(decimal_15, result.getQuantity());
        assertEquals(decimal_200, result.getTotalPrice());
        assertEquals(decimal_200Divide_15, result.getUnitPrice());
        assertEquals(warehouseOne.getId(), result.getKey().getWarehouse().getId());

    }

    @Test
    public void testGetItemSummaryByItemIdAndWarehouseId() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        itemSummaryKeyTwo = new ItemSummaryKey(warehouseTwo, itemOne);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_30);
        itemSummaryTwo.setTotalPrice(decimal_600);
        itemSummaryTwo.setUnitPrice(Util.divide(decimal_600, decimal_30));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());

        ItemSummary resultOne = itemSummaryRepository.save(itemSummaryOne);
        ItemSummary resultTwo = itemSummaryRepository.save(itemSummaryTwo);

        assertNotNull(resultOne);
        assertNotNull(resultOne.getLastUpdated());
        assertEquals(decimal_15, resultOne.getQuantity());
        assertEquals(decimal_200, resultOne.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultOne.getUnitPrice());

        assertNotNull(resultTwo);
        assertNotNull(resultTwo.getLastUpdated());
        assertEquals(decimal_30, resultTwo.getQuantity());
        assertEquals(decimal_600, resultTwo.getTotalPrice());
        assertEquals(decimal_600Divide_30, resultTwo.getUnitPrice());

        ItemSummary resultThree = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseOne, itemOne.getId());

        assertNotNull(resultThree);
        assertNotNull(resultThree.getLastUpdated());
        assertEquals(decimal_15, resultThree.getQuantity());
        assertEquals(decimal_200, resultThree.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultThree.getUnitPrice());
    }
    
    @Test
    public void testGetItemListItemSummaryByWarehouse() {
        
        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        itemSummaryKeyTwo = new ItemSummaryKey(warehouseOne, itemTwo);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_30);
        itemSummaryTwo.setTotalPrice(decimal_600);
        itemSummaryTwo.setUnitPrice(Util.divide(decimal_600, decimal_30));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());

        itemSummaryRepository.save(itemSummaryOne);
        itemSummaryRepository.save(itemSummaryTwo);
        
        List<ItemSummary> resultOne = itemSummaryRepository.findByKeyWarehouse(warehouseOne);
        
        assertEquals(2, resultOne.size());
        
        ItemSummary resultTwo = resultOne.get(0);
        
        assertEquals(warehouseOne.getId(), resultTwo.getKey().getWarehouse().getId());
        assertEquals(itemOne.getId(), resultTwo.getItem().getId());
        
        resultTwo = resultOne.get(1);
        
        assertEquals(warehouseOne.getId(), resultTwo.getKey().getWarehouse().getId());
        assertEquals(itemTwo.getId(), resultTwo.getItem().getId());
        
    }

    @Test
    public void testUpdateItemSummary() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        itemSummaryKeyTwo = new ItemSummaryKey(warehouseTwo, itemOne);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_30);
        itemSummaryTwo.setTotalPrice(decimal_600);
        itemSummaryTwo.setUnitPrice(Util.divide(decimal_600, decimal_30));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());

        ItemSummary resultOne = itemSummaryRepository.save(itemSummaryOne);
        
        assertNotNull(resultOne);
        assertNotNull(resultOne.getLastUpdated());
        assertEquals(decimal_15, resultOne.getQuantity());
        assertEquals(decimal_200, resultOne.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultOne.getUnitPrice());

        ItemSummary resultTwo = itemSummaryRepository.save(itemSummaryTwo);

        assertNotNull(resultTwo);
        assertNotNull(resultTwo.getLastUpdated());
        assertEquals(decimal_30, resultTwo.getQuantity());
        assertEquals(decimal_600, resultTwo.getTotalPrice());
        assertEquals(decimal_600Divide_30, resultTwo.getUnitPrice());

        ItemSummary resultThree = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseOne, itemOne.getId());

        assertNotNull(resultThree);
        assertNotNull(resultThree.getLastUpdated());
        assertEquals(decimal_15, resultThree.getQuantity());
        assertEquals(decimal_200, resultThree.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultThree.getUnitPrice());

        warehouseOne.setUsed(true);
        itemSummaryOne.setQuantity(decimal_120);
        itemSummaryOne.setTotalPrice(decimal_1600);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_1600, decimal_120));

        warehouseRepository.save(warehouseOne);
        itemSummaryRepository.save(itemSummaryOne);
        ItemSummary resultFour = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseOne, itemOne.getId());
        assertNotNull(resultThree);
        assertNotNull(resultThree.getLastUpdated());
        assertEquals(true, resultFour.getKey().getWarehouse().isUsed());
        assertEquals(decimal_120, resultThree.getQuantity());
        assertEquals(decimal_1600, resultThree.getTotalPrice());
        assertEquals(decimal_1600Divide_120, resultThree.getUnitPrice());


        ItemSummary resultSeven = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseTwo, itemOne.getId());

        assertNotNull(resultSeven);
        assertNotNull(resultSeven.getLastUpdated());
        assertEquals(decimal_30, resultSeven.getQuantity());
        assertEquals(decimal_600, resultSeven.getTotalPrice());
        assertEquals(decimal_600Divide_30, resultSeven.getUnitPrice());
    }

    @Test
    public void testDeleteItemSummary() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        itemSummaryKeyTwo = new ItemSummaryKey(warehouseTwo, itemOne);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_30);
        itemSummaryTwo.setTotalPrice(decimal_600);
        itemSummaryTwo.setUnitPrice(Util.divide(decimal_600, decimal_30));
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());

        ItemSummary resultOne = itemSummaryRepository.save(itemSummaryOne);
        assertNotNull(resultOne);
        assertNotNull(resultOne.getLastUpdated());
        assertEquals(decimal_15, resultOne.getQuantity());
        assertEquals(decimal_200, resultOne.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultOne.getUnitPrice());

        ItemSummary resultTwo = itemSummaryRepository.save(itemSummaryTwo);
        
        assertNotNull(resultTwo);
        assertNotNull(resultTwo.getLastUpdated());
        assertEquals(decimal_30, resultTwo.getQuantity());
        assertEquals(decimal_600, resultTwo.getTotalPrice());
        assertEquals(decimal_600Divide_30, resultTwo.getUnitPrice());
        
        itemSummaryRepository.deleteById(resultOne.getKey());
        ItemSummary resultThree = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseOne, itemOne.getId());
        assertNull(resultThree);

        ItemSummary resultFour = itemSummaryRepository.findItemSummaryByWarehouseAndItemId(warehouseTwo, itemOne.getId());

        assertNotNull(resultFour);
        assertNotNull(resultFour.getLastUpdated());
        assertEquals(decimal_30, resultFour.getQuantity());
        assertEquals(decimal_600, resultFour.getTotalPrice());
        assertEquals(decimal_600Divide_30, resultFour.getUnitPrice());
    }

    @Test
    public void testDeleteAllItemSummary() {

        itemSummaryKeyOne = new ItemSummaryKey(warehouseOne, itemOne);
        itemSummaryOne = new ItemSummary(itemSummaryKeyOne);
        itemSummaryOne.setQuantity(decimal_15);
        itemSummaryOne.setTotalPrice(decimal_200);
        itemSummaryOne.setUnitPrice(Util.divide(decimal_200, decimal_15));
        itemSummaryOne.setLastUpdated(LocalDateTime.now());

        ItemSummary resultOne = itemSummaryRepository.save(itemSummaryOne);

        assertNotNull(resultOne);
        assertNotNull(resultOne.getLastUpdated());
        assertEquals(decimal_15, resultOne.getQuantity());
        assertEquals(decimal_200, resultOne.getTotalPrice());
        assertEquals(decimal_200Divide_15, resultOne.getUnitPrice());

        itemSummaryKeyTwo = new ItemSummaryKey(warehouseTwo, itemOne);
        itemSummaryTwo = new ItemSummary(itemSummaryKeyTwo);
        itemSummaryTwo.setQuantity(decimal_25);
        itemSummaryTwo.setTotalPrice(decimal_400);
        itemSummaryTwo.setUnitPrice(decimal_400Divide_25);
        itemSummaryTwo.setLastUpdated(LocalDateTime.now());

        ItemSummary resultTwo = itemSummaryRepository.save(itemSummaryTwo);

        assertNotNull(resultTwo);
        assertNotNull(resultTwo.getLastUpdated());
        assertEquals(decimal_25, resultTwo.getQuantity());
        assertEquals(decimal_400, resultTwo.getTotalPrice());
        assertEquals(decimal_400Divide_25, resultTwo.getUnitPrice());
        
        itemSummaryRepository.deleteAll();

        List<ItemSummary> list = itemSummaryRepository.findAll();
        assertTrue(list.size() == 0);

    }
}
