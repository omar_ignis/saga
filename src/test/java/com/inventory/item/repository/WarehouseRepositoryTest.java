/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import com.inventory.item.entity.Warehouse;
import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

/**
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class WarehouseRepositoryTest {

    @Autowired
    private WarehouseRepository warehouseRepository;

    private Warehouse warehouseOne;
    private String WarehouseName = "Main";

    @BeforeEach
    void init() {
        warehouseOne = new Warehouse(WarehouseName);
    }
    
    @Test
    public void testSaveWarehouse() {
        
        warehouseRepository.save(warehouseOne);
        
        assertTrue(warehouseOne.getId() > 0);
        assertEquals(WarehouseName, warehouseOne.getWarehouseName());
    }

    @Test
    public void testGetWarehouseById() {

        warehouseRepository.save(warehouseOne);
        assertTrue(warehouseOne.getId() > 0);

        Optional<Warehouse> result = warehouseRepository.findById(warehouseOne.getId());

        assertEquals(WarehouseName, result.get().getWarehouseName());
        assertEquals(false, result.get().isUsed());
    }

    @Test
    public void testGetWarehouseByName() {

        warehouseRepository.save(warehouseOne);
        assertTrue(warehouseOne.getId() > 0);

        List<Warehouse> list = warehouseRepository.findByDeletedFalseAndWarehouseNameLike(warehouseOne.getWarehouseName(), PageRequest.of(0, 10)).getContent();
        Warehouse result = list.get(0);
        assertEquals(warehouseOne.getId(), result.getId());
        assertEquals(warehouseOne.getWarehouseName(), result.getWarehouseName());
        assertEquals(warehouseOne.isUsed(), result.isUsed());
    }

    @Test
    public void testUpdateWarehouse() {

        warehouseRepository.save(warehouseOne);
        assertTrue(warehouseOne.getId() > 0);

        String newName = "Main Zwei";
        warehouseOne.setWarehouseName(newName);
        warehouseOne.setUsed(true);
        warehouseRepository.save(warehouseOne);

        Optional<Warehouse> result = warehouseRepository.findById(warehouseOne.getId());
        assertEquals(newName, result.get().getWarehouseName());
        assertEquals(true, result.get().isUsed());
        assertEquals(warehouseOne.getId(), result.get().getId());
    }

    @Test
    public void testDisableWarehouse() {

        warehouseRepository.save(warehouseOne);
        assertTrue(warehouseOne.getId() > 0);


        warehouseRepository.disableWarehouse(warehouseOne.getId());

        List<Warehouse> list = warehouseRepository.findByDeletedFalseAndWarehouseNameLike(warehouseOne.getWarehouseName(), PageRequest.of(0, 10)).getContent();
        assertTrue(list.isEmpty());

    }

    @Test
    public void testDeleteWarehouse() {

        warehouseRepository.save(warehouseOne);
        assertTrue(warehouseOne.getId() > 0);

        List<Warehouse> list = warehouseRepository.findByDeletedFalseAndWarehouseNameLike(warehouseOne.getWarehouseName(), PageRequest.of(0, 10)).getContent();
        Warehouse result = list.get(0);
        assertEquals(warehouseOne.getWarehouseName(), result.getWarehouseName());
        assertEquals(false, result.isUsed());
        assertEquals(warehouseOne.getId(), result.getId());

        warehouseRepository.delete(warehouseOne);

        list = warehouseRepository.findByDeletedFalseAndWarehouseNameLike(warehouseOne.getWarehouseName(), PageRequest.of(0, 10)).getContent();
        assertTrue(list.isEmpty());
    }
}
