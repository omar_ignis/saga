/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.SalesReturnDocument;

import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SalesReturnDocumentRepositoryTest {
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private SalesReturnDocumentRepository salesReturnDocumentRepository;
    
    private LocalDateTime now;
    
    private Warehouse warehouseOne;
    private SalesReturnDocument salesReturnDocumentOne;
    private SalesReturnDocument SalesReturnDocumentTwo;
    private SalesReturnDocument salesReturnDocumentThree;
    
    @BeforeEach
    void init() {
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR000000001");
        salesReturnDocumentOne.setDate(LocalDateTime.now());
        salesReturnDocumentOne.setDescription("sales return document description one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalQuantity(BigDecimal.ONE);
        salesReturnDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        SalesReturnDocument result = salesReturnDocumentRepository.save(salesReturnDocumentOne);
        
        assertEquals("SR000000001", result.getId());
        assertEquals(salesReturnDocumentOne.getDate(), result.getDate());
        assertEquals("sales return document description one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(BigDecimal.ONE, result.getTotalQuantity());
        assertEquals(BigDecimal.TEN, result.getTotalAmount());
    }
    
    @Test
    public void getNextIdTest(){
        
        Long id = salesReturnDocumentRepository.getNextId();
        
        assertTrue(id > 0L);
    }
    
    @Test
    public void getInputDocumentByDeletedFalseTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);
        
        SalesReturnDocumentTwo = new SalesReturnDocument();
        SalesReturnDocumentTwo.setId("SR0000000002");
        SalesReturnDocumentTwo.setDate(now);
        SalesReturnDocumentTwo.setDescription("sales return document one");
        SalesReturnDocumentTwo.setWarehouse(warehouseOne);
        SalesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        SalesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        SalesReturnDocumentTwo.setDeleted(true);
        
        SalesReturnDocumentTwo = salesReturnDocumentRepository.save(SalesReturnDocumentTwo);
        
        salesReturnDocumentThree = new SalesReturnDocument();
        salesReturnDocumentThree.setId("SR0000000003");
        salesReturnDocumentThree.setDate(now);
        salesReturnDocumentThree.setDescription("sales return document one");
        salesReturnDocumentThree.setWarehouse(warehouseOne);
        salesReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        salesReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        salesReturnDocumentThree = salesReturnDocumentRepository.save(salesReturnDocumentThree);
        
        List<SalesReturnDocument> list = salesReturnDocumentRepository
                .findByDeletedFalse(PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        SalesReturnDocument result = list.get(0);
        assertEquals("SR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("SR0000000003", result.getId());
        
    }
    
    @Test
    public void getByIdLikeTest() {
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setId("SR0000000001");
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseOne);
        salesReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        salesReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        salesReturnDocumentOne = salesReturnDocumentRepository.save(salesReturnDocumentOne);
        
        SalesReturnDocumentTwo = new SalesReturnDocument();
        SalesReturnDocumentTwo.setId("SR0000000002");
        SalesReturnDocumentTwo.setDate(now);
        SalesReturnDocumentTwo.setDescription("sales return document one");
        SalesReturnDocumentTwo.setWarehouse(warehouseOne);
        SalesReturnDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        SalesReturnDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        SalesReturnDocumentTwo.setDeleted(true);
        
        SalesReturnDocumentTwo = salesReturnDocumentRepository.save(SalesReturnDocumentTwo);
        
        salesReturnDocumentThree = new SalesReturnDocument();
        salesReturnDocumentThree.setId("SR0000000003");
        salesReturnDocumentThree.setDate(now);
        salesReturnDocumentThree.setDescription("sales return document one");
        salesReturnDocumentThree.setWarehouse(warehouseOne);
        salesReturnDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        salesReturnDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        salesReturnDocumentThree = salesReturnDocumentRepository.save(salesReturnDocumentThree);
        
        List<SalesReturnDocument> list = salesReturnDocumentRepository
                .findByIdLike("SR000",PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        SalesReturnDocument result = list.get(0);
        assertEquals("SR0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("SR0000000003", result.getId());
    }
}
