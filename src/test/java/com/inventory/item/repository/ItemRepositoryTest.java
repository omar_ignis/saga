/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import java.util.Optional;

import com.inventory.item.entity.Item;
import com.inventory.item.enums.ValuationType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

/**
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ItemRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    private String itemName = "Item One";

    private Item itemOne;
    
    @BeforeEach
    void init() {
        
        itemOne = new Item();
        itemOne.setItemName(itemName);
        itemOne.setDescription("Items Eins Description");
        itemOne.setUsed(false);
        itemOne.setValuationType(ValuationType.AVERAGE);
    }
    
    @Test
    public void testSaveItemTest() {
        Item result = itemRepository.save(itemOne);
        assertTrue(result.getId() > 0);
        assertEquals(ValuationType.AVERAGE, result.getValuationType());

    }

    @Test
    public void testGetItemById() {
        
        itemRepository.save(itemOne);
        assertTrue(itemOne.getId() > 0);
        Optional<Item> result = itemRepository.findById(itemOne.getId());
        assertEquals(itemOne.getId(), result.get().getId());
        assertEquals(itemOne.getItemName(), result.get().getItemName());
        assertEquals(itemOne.getDescription(), result.get().getDescription());
        assertEquals(itemOne.isUsed(), result.get().isUsed());
    }

    @Test
    public void testGetItemByName() {

        itemRepository.save(itemOne);
        assertTrue(itemOne.getId() > 0);
        List<Item> list = itemRepository.findByItemNameLike(itemName, PageRequest.of(0, 10)).getContent();
        Item result = list.get(0);
        assertEquals(itemOne.getId(), result.getId());
        assertEquals(itemOne.getDescription(), result.getDescription());
        assertEquals(itemOne.isUsed(), result.isUsed());
    }

    @Test
    public void testUpdateItemByName() {
        
        itemRepository.save(itemOne);
        assertTrue(itemOne.getId() > 0);
        List<Item> list = itemRepository.findByItemNameLike(itemName, PageRequest.of(0, 10)).getContent();
        Item result = list.get(0);
        assertEquals(itemOne.getId(), result.getId());
        assertEquals(itemOne.getDescription(), result.getDescription());
        assertEquals(itemOne.isUsed(), result.isUsed());

        String updatedDesc = "Items Eins Updated";
        itemOne.setDescription(updatedDesc);
        itemOne.setUsed(true);
        itemRepository.save(itemOne);
        list = itemRepository.findByItemNameLike(itemName, PageRequest.of(0, 10)).getContent();
        result = list.get(0);
        assertEquals(itemOne.getId(), result.getId());
        assertEquals(updatedDesc, result.getDescription());
        assertEquals(true, result.isUsed());
    }

    @Test
    public void testDeleteItem() {
        
        itemRepository.save(itemOne);
        assertTrue(itemOne.getId() > 0);

        itemRepository.delete(itemOne);

        List<Item> list = itemRepository.findByItemNameLike(itemName, PageRequest.of(0, 10)).getContent();
        assertTrue(list.isEmpty());
    }
}
