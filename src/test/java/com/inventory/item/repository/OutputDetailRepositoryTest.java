/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.enums.ValuationType;
import com.inventory.item.entity.OutputDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OutputDetailRepositoryTest {
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private OutputDetailRepository outputDetailRepository;
    
    @Autowired
    private OutputDocumentRepository outputDocumentRepository;
    
    private LocalDateTime now;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    
    private Warehouse warehouseOne;
    
    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;
    
    private OutputDocument outputDocumentOne;
    
    @BeforeEach
    void init() {
        warehouseRepository.deleteAll();
        itemRepository.deleteAll();
        outputDocumentRepository.deleteAll();
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        itemOne = new Item("item One","Item desc one");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne = itemRepository.save(itemOne);
        
        itemTwo = new Item();
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo = itemRepository.save(itemTwo);
        
        itemThree = new Item();
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree = itemRepository.save(itemThree);
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU000000001");
        outputDocumentOne.setDate(LocalDateTime.now());
        outputDocumentOne.setDescription("output document description one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalQuantity(BigDecimal.ONE);
        outputDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        outputDocumentOne = outputDocumentRepository.save(outputDocumentOne);
        
        now = LocalDateTime.now();
    }

    @Test
    public void saveTest() {
        
        outputDetailOne =  new OutputDetail();
        outputDetailOne.setOutputDocument(outputDocumentOne);
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(BigDecimal.TEN);
        outputDetailOne.setUnitPrice(BigDecimal.TEN);
        outputDetailOne.setTotalPrice(BigDecimal.TEN);
        
        OutputDetail result = outputDetailRepository.save(outputDetailOne);
    
        assertTrue(result.getId() > 1);
        assertEquals(itemOne, result.getItem());
        assertEquals(itemOne.getId(), result.getItem().getId());
        assertEquals(1, result.getLineNumber());
        assertEquals(BigDecimal.TEN, result.getQuantity());
        assertEquals(BigDecimal.TEN, result.getUnitPrice());
        assertEquals(BigDecimal.TEN, result.getTotalPrice());
        
        outputDocumentOne = outputDocumentRepository.findById("OU000000001").get();
        assertEquals(1, outputDocumentOne.getDetails().size());
    }
    
    @Test
    public void updateTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentOne = outputDocumentRepository.save(outputDocumentOne);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailTwo);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailThree);
        
        OutputDocument result = outputDocumentRepository.findById("OU0000000001").get();
        
        assertEquals(3, result.getDetails().size());
        
    }
    
    @Test
    public void findByOutputDocumentTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentOne = outputDocumentRepository.save(outputDocumentOne);
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemOne);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        outputDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        outputDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        outputDetailOne.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailOne);
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemTwo);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        outputDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        outputDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        outputDetailTwo.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailTwo);
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemThree);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        outputDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        outputDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        outputDetailThree.setOutputDocument(outputDocumentOne);
        
        outputDetailRepository.save(outputDetailThree);
        
        List<OutputDetail> list = outputDetailRepository.findByOutputDocument(outputDocumentOne);
        
        assertEquals(3, list.size());
    }
}
