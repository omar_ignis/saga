/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.enums.ValuationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PurchaseReturnDetailRepositoryTest {
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private PurchaseReturnDetailRepository purchaseReturnDetailRepository;
    
    @Autowired
    private PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;
    
    private LocalDateTime now;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    
    private Warehouse warehouseOne;
    
    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;
    
    private PurchaseReturnDocument purchaseReturnDocumentOne;
    
    @BeforeEach
    void init() {
        
        warehouseRepository.deleteAll();
        itemRepository.deleteAll();
        purchaseReturnDocumentRepository.deleteAll();
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        itemOne = new Item("item One","Item desc one");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne = itemRepository.save(itemOne);
        
        itemTwo = new Item();
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo = itemRepository.save(itemTwo);
        
        itemThree = new Item();
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree = itemRepository.save(itemThree);
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR000000001");
        purchaseReturnDocumentOne.setDate(LocalDateTime.now());
        purchaseReturnDocumentOne.setDescription("purchase return document description one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalQuantity(BigDecimal.ONE);
        purchaseReturnDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        now = LocalDateTime.now();
    }

    @Test
    public void saveTest() {
        
        purchaseReturnDetailOne =  new PurchaseReturnDetail();
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(BigDecimal.TEN);
        purchaseReturnDetailOne.setUnitPrice(BigDecimal.TEN);
        purchaseReturnDetailOne.setTotalPrice(BigDecimal.TEN);
        
        PurchaseReturnDetail result = purchaseReturnDetailRepository.save(purchaseReturnDetailOne);
    
        assertTrue(result.getId() > 1);
        assertEquals(itemOne, result.getItem());
        assertEquals(itemOne.getId(), result.getItem().getId());
        assertEquals(1, result.getLineNumber());
        assertEquals(BigDecimal.TEN, result.getQuantity());
        assertEquals(BigDecimal.TEN, result.getUnitPrice());
        assertEquals(BigDecimal.TEN, result.getTotalPrice());
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.findById("PR000000001").get();
        assertEquals(1, purchaseReturnDocumentOne.getDetails().size());
    }
    
    @Test
    public void updateTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailTwo);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailThree);
        
        PurchaseReturnDocument result = purchaseReturnDocumentRepository.findById("PR0000000001").get();
        
        assertEquals(3, result.getDetails().size());
        
    }
    
    @Test
    public void findByPurchaseReturnDocumentTest() {
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setId("PR0000000001");
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        purchaseReturnDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        purchaseReturnDocumentOne = purchaseReturnDocumentRepository.save(purchaseReturnDocumentOne);
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(Utils.getAsBigDecimal(10));
        purchaseReturnDetailOne.setUnitPrice(Utils.getAsBigDecimal(20));
        purchaseReturnDetailOne.setTotalPrice(Utils.getAsBigDecimal(200));
        purchaseReturnDetailOne.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailOne);
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemTwo);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(Utils.getAsBigDecimal(20));
        purchaseReturnDetailTwo.setUnitPrice(Utils.getAsBigDecimal(5));
        purchaseReturnDetailTwo.setTotalPrice(Utils.getAsBigDecimal(100));
        purchaseReturnDetailTwo.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailTwo);
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemThree);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(Utils.getAsBigDecimal(100));
        purchaseReturnDetailThree.setUnitPrice(Utils.getAsBigDecimal(10));
        purchaseReturnDetailThree.setTotalPrice(Utils.getAsBigDecimal(1000));
        purchaseReturnDetailThree.setPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        purchaseReturnDetailRepository.save(purchaseReturnDetailThree);
        
        List<PurchaseReturnDetail> list = purchaseReturnDetailRepository.findByPurchaseReturnDocument(purchaseReturnDocumentOne);
        
        assertEquals(3, list.size());
    }
}
