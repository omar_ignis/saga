/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.OutputDocument;

import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OutputDocumentRepositoryTest {
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private OutputDocumentRepository outputDocumentRepository;
    
    private LocalDateTime now;
    
    private Warehouse warehouseOne;
    private OutputDocument outputDocumentOne;
    private OutputDocument outputDocumentTwo;
    private OutputDocument outputDocumentThree;
    
    @BeforeEach
    void init() {
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU000000001");
        outputDocumentOne.setDate(LocalDateTime.now());
        outputDocumentOne.setDescription("output document description one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalQuantity(BigDecimal.ONE);
        outputDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        OutputDocument result = outputDocumentRepository.save(outputDocumentOne);
        
        assertEquals("OU000000001", result.getId());
        assertEquals(outputDocumentOne.getDate(), result.getDate());
        assertEquals("output document description one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(BigDecimal.ONE, result.getTotalQuantity());
        assertEquals(BigDecimal.TEN, result.getTotalAmount());
    }
    
    @Test
    public void getNextIdTest(){
        
        Long id = outputDocumentRepository.getNextId();
        
        assertTrue(id > 0L);
    }
    
    @Test
    public void getOutputDocumentByDeletedFalseTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentOne = outputDocumentRepository.save(outputDocumentOne);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000002");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        outputDocumentTwo.setDeleted(true);
        
        outputDocumentTwo = outputDocumentRepository.save(outputDocumentTwo);
        
        outputDocumentThree = new OutputDocument();
        outputDocumentThree.setId("OU0000000003");
        outputDocumentThree.setDate(now);
        outputDocumentThree.setDescription("output document one");
        outputDocumentThree.setWarehouse(warehouseOne);
        outputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        outputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        outputDocumentThree = outputDocumentRepository.save(outputDocumentThree);
        
        List<OutputDocument> list = outputDocumentRepository
                .findByDeletedFalse(PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        OutputDocument result = list.get(0);
        assertEquals("OU0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("OU0000000003", result.getId());
        
    }
    
    @Test
    public void getByIdLikeTest() {
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setId("OU0000000001");
        outputDocumentOne.setDate(now);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        outputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        outputDocumentOne = outputDocumentRepository.save(outputDocumentOne);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setId("OU0000000002");
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setDescription("output document one");
        outputDocumentTwo.setWarehouse(warehouseOne);
        outputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        outputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        outputDocumentTwo.setDeleted(true);
        
        outputDocumentTwo = outputDocumentRepository.save(outputDocumentTwo);
        
        outputDocumentThree = new OutputDocument();
        outputDocumentThree.setId("OU0000000003");
        outputDocumentThree.setDate(now);
        outputDocumentThree.setDescription("output document one");
        outputDocumentThree.setWarehouse(warehouseOne);
        outputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        outputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        outputDocumentThree = outputDocumentRepository.save(outputDocumentThree);
        
        List<OutputDocument> list = outputDocumentRepository
                .findByIdLike("OU000",PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        OutputDocument result = list.get(0);
        assertEquals("OU0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("OU0000000003", result.getId());
    }
}
