/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.util.Utils;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDocument;

import org.springframework.data.domain.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author ignis
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class InputDocumentRepositoryTest {
    
    @Autowired
    private WarehouseRepository warehouseRepository;
    
    @Autowired
    private InputDocumentRepository inputDocumentRepository;
    
    private LocalDateTime now;
    
    private Warehouse warehouseOne;
    private InputDocument inputDocumentOne;
    private InputDocument inputDocumentTwo;
    private InputDocument inputDocumentThree;
    
    @BeforeEach
    void init() {
        
        warehouseOne = new Warehouse("warehouse one");
        warehouseOne = warehouseRepository.save(warehouseOne);
        
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN000000001");
        inputDocumentOne.setDate(LocalDateTime.now());
        inputDocumentOne.setDescription("input document description one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalQuantity(BigDecimal.ONE);
        inputDocumentOne.setTotalAmount(BigDecimal.TEN);
        
        now = LocalDateTime.now();
    }
    
    @Test
    public void saveTest() {
        
        InputDocument result = inputDocumentRepository.save(inputDocumentOne);
        
        assertEquals("IN000000001", result.getId());
        assertEquals(inputDocumentOne.getDate(), result.getDate());
        assertEquals("input document description one", result.getDescription());
        assertEquals(warehouseOne, result.getWarehouse());
        assertEquals(warehouseOne.getId(), result.getWarehouse().getId());
        assertEquals(BigDecimal.ONE, result.getTotalQuantity());
        assertEquals(BigDecimal.TEN, result.getTotalAmount());
    }
    
    @Test
    public void getNextIdTest(){
        
        Long id = inputDocumentRepository.getNextId();
        
        assertTrue(id > 0L);
    }
    
    @Test
    public void getInputDocumentByDeletedFalseTest() {
        
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);
        
        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000002");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        inputDocumentTwo.setDeleted(true);
        
        inputDocumentTwo = inputDocumentRepository.save(inputDocumentTwo);
        
        inputDocumentThree = new InputDocument();
        inputDocumentThree.setId("IN0000000003");
        inputDocumentThree.setDate(now);
        inputDocumentThree.setDescription("input document one");
        inputDocumentThree.setWarehouse(warehouseOne);
        inputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        inputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        inputDocumentThree = inputDocumentRepository.save(inputDocumentThree);
        
        List<InputDocument> list = inputDocumentRepository
                .findByDeletedFalse(PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        InputDocument result = list.get(0);
        assertEquals("IN0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("IN0000000003", result.getId());
        
    }
    
    @Test
    public void getByIdLikeTest() {
        
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setId("IN0000000001");
        inputDocumentOne.setDate(now);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(Utils.getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(Utils.getAsBigDecimal(130));
        
        inputDocumentOne = inputDocumentRepository.save(inputDocumentOne);
        
        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setId("IN0000000002");
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setDescription("input document one");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(Utils.getAsBigDecimal(1500));
        inputDocumentTwo.setTotalQuantity(Utils.getAsBigDecimal(150));
        inputDocumentTwo.setDeleted(true);
        
        inputDocumentTwo = inputDocumentRepository.save(inputDocumentTwo);
        
        inputDocumentThree = new InputDocument();
        inputDocumentThree.setId("IN0000000003");
        inputDocumentThree.setDate(now);
        inputDocumentThree.setDescription("input document one");
        inputDocumentThree.setWarehouse(warehouseOne);
        inputDocumentThree.setTotalAmount(Utils.getAsBigDecimal(2000));
        inputDocumentThree.setTotalQuantity(Utils.getAsBigDecimal(200));
        
        inputDocumentThree = inputDocumentRepository.save(inputDocumentThree);
        
        List<InputDocument> list = inputDocumentRepository
                .findByIdLike("IN000",PageRequest.of(0, 10)).getContent();
        
        assertEquals(2, list.size());
        
        InputDocument result = list.get(0);
        assertEquals("IN0000000001", result.getId());
        
        result = list.get(1);
        assertEquals("IN0000000003", result.getId());
    }

}
