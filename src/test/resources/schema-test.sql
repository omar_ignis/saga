/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ignis
 * Created: Oct 9, 2020
 */

CREATE SEQUENCE IF NOT EXISTS input_doc_seq START 1;
CREATE SEQUENCE IF NOT EXISTS output_doc_seq START 1;
CREATE SEQUENCE IF NOT EXISTS purchase_return_doc_seq START 1;
CREATE SEQUENCE IF NOT EXISTS sale_return_doc_seq START 1;