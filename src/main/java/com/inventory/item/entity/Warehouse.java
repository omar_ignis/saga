/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.entity;


import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author ignis
 */
@Entity
public class Warehouse implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "warehouse_name", nullable = false, unique = true)
    private String warehouseName;

    @Column(name = "used", nullable = false)
    private boolean used;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "key.warehouse")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<ItemSummary> itemSummary;

    public Warehouse() {
        this.itemSummary = new ArrayList<>();
    }

    public Warehouse(String name) {
        this.warehouseName = name.trim();
        this.used = false;
        this.deleted = false;
        this.itemSummary = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName.trim();
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<ItemSummary> getItemSummary() {
        return itemSummary;
    }

    public void setItemSummary(List<ItemSummary> itemSummary) {
        this.itemSummary = itemSummary;
    }
    
    public void addItemSummary(ItemSummary itemSummary){
        this.itemSummary.add(itemSummary);
    }

    @Override
    public String toString() {
        return "Warehouse{" + "id=" + id + ", warehouseName=" + warehouseName +
                ", used=" + used + ", deleted=" + deleted + ", itemSummary=" + itemSummary + '}';
    }
}
