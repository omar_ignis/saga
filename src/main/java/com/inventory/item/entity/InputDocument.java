/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;


import java.util.List;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *
 * @author ignis
 */
@Entity
public class InputDocument extends Document{
    
    @JsonProperty(access = JsonProperty.Access.AUTO)
    @OneToMany(mappedBy = "inputDocument", fetch = FetchType.EAGER)
    @OrderBy("lineNumber DESC")
    private List<InputDetail> details;

    public InputDocument() {
        super();
        super.setType(DocumentType.INPUT);
        super.setStatus(Status.OPEN);
        super.setCounter(0);
        details = new ArrayList<>();
    }
    
    public InputDocument(String id) {
        super();
        super.setId(id);
        super.setType(DocumentType.INPUT);
        super.setStatus(Status.OPEN);
        super.setCounter(0);
        details = new ArrayList<>();
    }

    public List<InputDetail> getDetails() {
        return details;
    }

    public void setDetails(List<InputDetail> details) {
        this.details = details;
    }
    
    public void addDetail(InputDetail detail){
        this.details.add(detail);
    }
    
}
