/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ignis
 */
@Entity
@Table(uniqueConstraints={
   @UniqueConstraint(columnNames={"purchase_return_document_id", "lineNumber"})
})
public class PurchaseReturnDetail extends Detail {
    
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private PurchaseReturnDocument purchaseReturnDocument;

    public PurchaseReturnDetail() {
    }

    public PurchaseReturnDocument getPurchaseReturnDocument() {
        return purchaseReturnDocument;
    }

    public void setPurchaseReturnDocument(PurchaseReturnDocument purchaseReturnDocument) {
        this.purchaseReturnDocument = purchaseReturnDocument;
        this.purchaseReturnDocument.addDetail(this);
    }
    
    public void removePurchaseReturnDocument(PurchaseReturnDocument purchaseReturnDocument){
        
        this.purchaseReturnDocument.getDetails().remove(this);
        this.purchaseReturnDocument = null;
        
    }
}
