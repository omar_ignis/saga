/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import java.util.List;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ignis
 */
@Entity
public class SalesReturnDocument extends Document{
    
    @JsonProperty(access = JsonProperty.Access.AUTO)
    @OneToMany(mappedBy = "salesReturnDocument", fetch = FetchType.EAGER)
    @OrderBy("lineNumber DESC")
    private List<SalesReturnDetail> details;

    public SalesReturnDocument() {
        
        super();
        super.setType(DocumentType.SALES_RETURN);
        super.setStatus(Status.OPEN);
        super.setCounter(0);
        details = new ArrayList<>();
    }

    public SalesReturnDocument(String id) {
        
        super();
        super.setId(id);
        super.setType(DocumentType.SALES_RETURN);
        super.setStatus(Status.OPEN);
        super.setCounter(0);
        details = new ArrayList<>();
    }

    public List<SalesReturnDetail> getDetails() {
        return details;
    }

    public void setDetails(List<SalesReturnDetail> details) {
        this.details = details;
    }
    
    public void addDetail(SalesReturnDetail detail){
        this.details.add(detail);
    } 
    
}
