/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ignis
 */
@Entity
@Table(uniqueConstraints={
   @UniqueConstraint(columnNames={"input_document_id", "lineNumber"})
})
public class InputDetail extends Detail{
    
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private InputDocument inputDocument;

    public InputDetail() {
    }

    public InputDocument getInputDocument() {
        return inputDocument;
    }

    public void setInputDocument(InputDocument inputDocument) {
        this.inputDocument = inputDocument;
        this.inputDocument.addDetail(this);
    }
    
    public void removeInputDocument(InputDocument inputDocument){
        this.inputDocument.getDetails().remove(this);
        this.inputDocument = null;
        
    }

}
