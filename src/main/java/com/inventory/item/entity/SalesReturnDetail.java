/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author ignis
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"sales_return_document_id", "lineNumber"})
})
public class SalesReturnDetail extends Detail {

    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private SalesReturnDocument salesReturnDocument;

    public SalesReturnDetail() {
    }

    public SalesReturnDocument getSalesReturnDocument() {
        return salesReturnDocument;
    }

    public void setSalesReturnDocument(SalesReturnDocument salesReturnDocument) {
        this.salesReturnDocument = salesReturnDocument;
        this.salesReturnDocument.addDetail(this);
    }

    public void removeSalesReturnDocument(SalesReturnDocument salesReturnDocument) {
        this.salesReturnDocument.getDetails().remove(this);
        this.salesReturnDocument = null;

    }
}
