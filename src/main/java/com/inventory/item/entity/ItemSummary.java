/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.entity;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.inventory.item.entity.key.ItemSummaryKey;

/**
 * @author ignis
 */
@Entity
@Table(name = "item_summary")
public class ItemSummary {

    @EmbeddedId
    private ItemSummaryKey key;

    @Column(name = "quantity", nullable = false, scale = 2)
    private BigDecimal quantity;

    @Column(name = "unitPrice", nullable = false, scale = 2)
    private BigDecimal unitPrice;

    @Column(name = "total_price", nullable = false, scale = 2)
    private BigDecimal totalPrice;

    @Column(name = "last_updated", nullable = false)
    @JsonFormat(pattern="dd-MM-yyyy HH:mm:ss.SSS")
    private LocalDateTime lastUpdated;

    public ItemSummary() {
    }

    public ItemSummary(ItemSummaryKey key) {
        this.key = key;
        this.quantity = BigDecimal.ZERO;
        this.unitPrice = BigDecimal.ZERO;
        this.totalPrice = BigDecimal.ZERO;
    }
    
    public ItemSummary(Warehouse warehouse, Item item) {
        
        this.key = new ItemSummaryKey(warehouse, item);
        this.quantity = BigDecimal.ZERO;
        this.unitPrice = BigDecimal.ZERO;
        this.totalPrice = BigDecimal.ZERO;
        this.key.getWarehouse().addItemSummary(this);
    }

    public ItemSummaryKey getKey() {
        return key;
    }

    public void setKey(ItemSummaryKey key) {
        this.key = key;
    }

    public BigDecimal getQuantity() {
        return quantity.setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice.setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice.setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    public Item getItem(){
        return this.key.getItem();
    }

    @Override
    public String toString() {
        return "ItemSummary{" + "key=" + key + ", quantity=" + quantity + ", unitPrice=" + unitPrice + ", totalPrice=" + totalPrice + ", lastUpdated=" + lastUpdated + '}';
    }
}
