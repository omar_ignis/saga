/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.entity.key;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;

import java.io.Serializable;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author ignis
 */
@Embeddable
public class ItemSummaryKey implements Serializable {

    @ManyToOne
    @JoinColumn
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Warehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "itemId")
    private Item item;

    public ItemSummaryKey() {
    }

    public ItemSummaryKey(Warehouse warehouse, Item item) {
        this.warehouse = warehouse;
        this.item = item;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
