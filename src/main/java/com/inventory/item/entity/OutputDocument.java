/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author ignis
 */
@Entity
public class OutputDocument extends Document{
    
    @JsonProperty(access = JsonProperty.Access.AUTO)
    @OneToMany(mappedBy = "outputDocument", fetch = FetchType.EAGER)
    @OrderBy("lineNumber DESC")
    private List<OutputDetail> details;

    public OutputDocument() {
        super();
        super.setStatus(Status.OPEN);
        super.setType(DocumentType.OUTPUT);
        super.setCounter(0);
        details = new ArrayList<>();
    }
    
    public OutputDocument(String id) {
        super();
        super.setId(id);
        super.setStatus(Status.OPEN);
        super.setType(DocumentType.OUTPUT);
        super.setCounter(0);
        details = new ArrayList<>();
    }

    public List<OutputDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OutputDetail> details) {
        this.details = details;
    }

    public void addDetail(OutputDetail detail){
        this.details.add(detail);
    }
    
}
