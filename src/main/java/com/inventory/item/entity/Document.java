/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;


import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *
 * @author ignis
 */
@MappedSuperclass
public class Document {
    
    @Id
    private String id;
    
    @Column
    private DocumentType type;
    
    @Column(nullable = false)
    @JsonFormat(pattern="dd-MM-yyyy HH:mm:ss.SSS")
    private LocalDateTime date;

    @Column(nullable = false)
    private Status status;
    
    @ManyToOne
    @JoinColumn
    private Warehouse warehouse;

    @Column
    private String description;

    @Column(nullable = false, scale = 2)
    private BigDecimal totalQuantity;

    @Column(nullable = false, scale = 2)
    private BigDecimal totalAmount;
    
    @Column
    private int counter;

    @JsonIgnore
    @Column( nullable = false)
    private boolean deleted;
    
    public Document() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(BigDecimal totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Document{" + "id=" + id + ", type=" + type + ", date=" + date
                + ", status=" + status + ", warehouse=" + warehouse
                + ", description=" + description + ", totalQuantity=" + totalQuantity
                + ", totalAmount=" + totalAmount + ", counter=" + counter
                + ", deleted=" + deleted + '}';
    }
    
}
