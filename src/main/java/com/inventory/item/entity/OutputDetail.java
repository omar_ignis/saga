/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.entity;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.UniqueConstraint;
import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 *
 * @author ignis
 */
@Entity
@Table(uniqueConstraints={
   @UniqueConstraint(columnNames={"output_document_id", "lineNumber"})
})
public class OutputDetail extends Detail{
    
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private OutputDocument outputDocument;

    public OutputDetail() {
    }

    public OutputDocument getOutputDocument() {
        return outputDocument;
    }

    public void setOutputDocument(OutputDocument outputDocument) {
        this.outputDocument = outputDocument;
        this.outputDocument.addDetail(this);
    }
    
    public void removeOutputDocument(OutputDocument outputDocument){
        this.outputDocument.getDetails().remove(this);
        this.outputDocument = null;
    }
    
}
