/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.inventory;

import java.util.List;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;

/**
 *
 * @author ignis
 */
public interface InventorySummary {
    
    public DocumentType getType();
    
    public <S extends Detail> ItemSummary updateItemSummary(ItemSummary itemSummary, S detail);
    
    public <S extends Detail> ItemSummary calcWeightedAverage(ItemSummary itemSummary, S detail);
    
    public <S extends Detail> Warehouse updateInventory(Long warehouseId, List<S> details) throws Exception;
}
