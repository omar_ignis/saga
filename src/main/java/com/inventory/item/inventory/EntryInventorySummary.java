/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.inventory;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;

/**
 *
 * @author ignis
 */
public interface EntryInventorySummary extends InventorySummary{
    
    public <S extends Detail> ItemSummary createItemSummary(Warehouse warehouse, S detail);
    
}
