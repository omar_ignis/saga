/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.inventory.impl;

import org.springframework.stereotype.Service;

import com.inventory.item.enums.DocumentType;
import com.inventory.item.inventory.impl.EntryInventoryImpl;

import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Service
public class SaleReturnInventoryImpl extends EntryInventoryImpl{
    
    private final ItemService itemService;
    private final WarehouseRepository warehouseRepository;
    private final ItemSummaryRepository itemSummaryRepository;

    @Autowired
    public SaleReturnInventoryImpl(
            ItemService itemService, 
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository) {
        
        super(itemService, warehouseRepository, itemSummaryRepository);
        this.itemService = itemService;
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
    }

    @Override
    public DocumentType getType() {
        return DocumentType.SALES_RETURN;
    }
    
    
}
