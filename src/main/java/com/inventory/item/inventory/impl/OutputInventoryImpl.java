/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.inventory.impl;

import com.inventory.item.enums.DocumentType;
import com.inventory.item.validation.WarehouseValidation;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.inventory.impl.ExitInventoryImpl;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
/**
 *
 * @author ignis
 */
@Component
public class OutputInventoryImpl extends ExitInventoryImpl{
    
    private final WarehouseRepository warehouseRepository;
    private final ItemSummaryRepository itemSummaryRepository;
    
    @Autowired
    public OutputInventoryImpl(
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository,
            WarehouseValidation warehouseExitValidationImpl) {
        
        super(warehouseExitValidationImpl, warehouseRepository, itemSummaryRepository);
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
    }

    @Override
    public DocumentType getType() {
        return DocumentType.OUTPUT;
    }
}
