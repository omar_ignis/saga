/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.inventory.impl;

import java.util.List;
import java.util.ArrayList;
import java.time.LocalDateTime;

import com.inventory.item.util.Util;
import com.inventory.item.service.ItemService;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import org.springframework.transaction.annotation.Transactional;
import com.inventory.item.inventory.EntryInventorySummary;
/**
 *
 * @author ignis
 */
@Transactional
public abstract class EntryInventoryImpl implements EntryInventorySummary{

    private final ItemService itemService;
    private final WarehouseRepository warehouseRepository;
    private final ItemSummaryRepository itemSummaryRepository;

    public EntryInventoryImpl(
            ItemService itemService,
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository) {
        
        this.itemService = itemService;
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
    }
    
    @Override
    public <S extends Detail> ItemSummary createItemSummary(Warehouse warehouse, S detail) {

        ItemSummary itemSummary = new ItemSummary(warehouse, detail.getItem());
        itemSummary = calcWeightedAverage(itemSummary, detail);
        itemService.markAsUsed(detail.getItem());
        return itemSummary;
    }
    
    @Override
    public <S extends Detail> ItemSummary updateItemSummary(ItemSummary itemSummary, S detail) {
        
        itemSummary = calcWeightedAverage(itemSummary, detail);
        itemService.markAsUsed(detail.getItem());
        return itemSummary;
    }

    @Override
    public <S extends Detail> ItemSummary calcWeightedAverage(ItemSummary itemSummary, S detail) {
        
        itemSummary.setQuantity(itemSummary.getQuantity().add(detail.getQuantity()));
        itemSummary.setTotalPrice(itemSummary.getTotalPrice().add(detail.getTotalPrice()));
        itemSummary.setUnitPrice(Util.divide(itemSummary.getTotalPrice(), itemSummary.getQuantity()));
        
        return itemSummary;
    }
    
    @Override
    public <S extends Detail> Warehouse updateInventory(Long warehouseId, List<S> details) throws Exception{
        
        Warehouse warehouse = warehouseRepository.findById(warehouseId).orElseThrow(Exception::new);
        List<ItemSummary> itemSummaries = new ArrayList<>();
        
        for(Detail detail : details){
            
            ItemSummary itemSummary = itemSummaryRepository.findByKeyWarehouseAndKeyItem(warehouse, detail.getItem());
            if(itemSummary != null){
                itemSummary = updateItemSummary(itemSummary, detail);
            }
            else{
                itemSummary= createItemSummary(warehouse, detail);
            }
            itemSummary.setLastUpdated(LocalDateTime.now());
            itemSummaries.add(itemSummary);
        }
        
        itemSummaryRepository.saveAll(itemSummaries);
        
        if(!warehouse.isUsed()){
            warehouse.setUsed(true);
        }
        
        return warehouseRepository.save(warehouse);
    }
    
}
