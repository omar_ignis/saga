/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.inventory.impl;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;

import com.inventory.item.util.Util;
import com.inventory.item.validation.WarehouseValidation;

import com.inventory.item.inventory.ExitInventorySummary;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ignis
 */
@Transactional
public abstract class ExitInventoryImpl implements ExitInventorySummary {

    private final WarehouseValidation warehouseValidation;
    private final WarehouseRepository warehouseRepository;
    private final ItemSummaryRepository itemSummaryRepository;

    public ExitInventoryImpl(
            WarehouseValidation warehouseValidation,
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository) {
        
        this.warehouseValidation = warehouseValidation;
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
    }
    
    @Override
    public <S extends Detail> ItemSummary calcWeightedAverage(ItemSummary itemSummary, S detail){
        
        itemSummary.setQuantity(itemSummary.getQuantity().subtract(detail.getQuantity()));
        BigDecimal totalPrice = Util.multiply(detail.getQuantity(), itemSummary.getUnitPrice());
        itemSummary.setTotalPrice(itemSummary.getTotalPrice().subtract(totalPrice));
        
        return itemSummary;
    }
    
    @Override
    public <S extends Detail> ItemSummary updateItemSummary(ItemSummary itemSummary, S detail) {
        
        itemSummary = calcWeightedAverage(itemSummary, detail);
        return itemSummary;
    }
    
    @Override
    public <S extends Detail> Warehouse updateInventory(Long warehouseId, List<S> details) throws Exception{
        
        Warehouse warehouse = warehouseRepository.findById(warehouseId).orElseThrow(Exception::new);
        List<ItemSummary> itemSummaries = new ArrayList<>();

        for (Detail detail : details) {

            ItemSummary itemSummary = warehouseValidation
                    .validateItemSummary(warehouse, detail.getItem());

            warehouseValidation.validateInventoryBeforeUpdate(itemSummary, detail);

            itemSummary = updateItemSummary(itemSummary, detail);
            itemSummary.setLastUpdated(LocalDateTime.now());
            itemSummaries.add(itemSummary);
        }
        
        itemSummaryRepository.saveAll(itemSummaries);
        
        return warehouseRepository.save(warehouse);
    }
}
