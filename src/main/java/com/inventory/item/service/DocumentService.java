/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.service;

import java.util.List;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.enums.DocumentType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author ignis
 */
public interface DocumentService {

    public DocumentType getType();
    
    public void remove(String id) throws Throwable;

    public <T extends Document> T release(String id) throws Throwable;

    public <T extends Document> Page<T> getAllDocuments(Pageable pageable);
    
    public <T extends Document> T saveOrUpdate(String document) throws Exception;
    
    public <T extends Document, S extends Detail> T save(T document, List<S> details);

    public <T extends Document, S extends Detail> T update(T document, List<S> details);
    
    public <T extends Document> T getDocumentById(String id) throws Exception, Throwable;

    public <T extends Document, S extends Detail> T saveOrUpdate(T document, List<S> details);
    
    public <T extends Document> Page<T> getAllDocumentsByIdLike(String id, Pageable pageable);
    
    public <S extends Detail> List<S> getDetailsByDocument(String id) throws Exception, Throwable;
    
    public <S extends Detail> Page<S> getDetailsByDocument(String id, Pageable pageable) throws Exception, Throwable;
}
