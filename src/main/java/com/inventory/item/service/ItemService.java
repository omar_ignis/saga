/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;
import java.text.MessageFormat;

import com.inventory.item.util.Util;
import com.inventory.item.entity.Item;
import com.inventory.item.util.Messages;
import com.inventory.item.repository.ItemRepository;

import org.springframework.http.HttpStatus;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author ignis
 */
@Service
@Transactional
public class ItemService {

    private final ItemRepository itemRepository;
    
    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }
    
    public Item save(Item item){
        
        return itemRepository.save(item);
    }
    
    public Item update(Long id, Item item){
        getItemById(id);
        item.setId(id);
        return itemRepository.save(item);
    }
    
    public Page<Item> getAllItems(Pageable pageable){
        return itemRepository.findAllByOrderByIdAsc(pageable);
    }
    
    public List<Item> getAllItems(){
        return itemRepository.findAllByOrderByIdAsc();
    }
    
    public Item getItemById(Long id){
         Optional<Item> optional = itemRepository.findById(id);
         Item result = optional.orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
         
         return result;
    }
    
    public Page<Item> getByNameLike(String name, Pageable pageable){
        return itemRepository.findByItemNameLike(Util.buildLike(name), pageable);
    }
    
    public void deleteAll(){
        
        itemRepository.deleteAll();
    }

    public void deleteOrDisableItem(Long id) {

        //Validamos que exista antes de intentar actualizar
        Optional<Item> itemOptional = itemRepository.findById(id);
        if (itemOptional.isPresent()) {
            Item item = itemOptional.get();
            if (item.isUsed()) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                        MessageFormat.format(Messages.ITEM_NOT_DELETED, item.getItemName()));
            }
        }

        try {
            itemRepository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            //If there is detail with reference to this item
            //then it will be disabled
            itemRepository.disableItem(id);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public Item markAsUsed(Item item){
        if(!item.isUsed()){
            item.setUsed(true);
            item = itemRepository.save(item);
        }
        
        return item;
    }
}
