/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service.factory;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import com.inventory.item.enums.DocumentType;
import com.inventory.item.service.DocumentService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
/**
 *
 * @author ignis
 */
@Service
public class DocumentServiceFactory {
    
    private final Map<DocumentType, DocumentService> factorySerivceCache = new HashMap<>();
    
    @Autowired
    public DocumentServiceFactory(List<DocumentService> services) {
        
        for(DocumentService service : services){
            factorySerivceCache.put(service.getType(), service);
        }
    }
    
    public DocumentService getFactory(DocumentType type){
        DocumentService factory = factorySerivceCache.get(type);
        if(factory == null) throw new RuntimeException("Unknown factory type: " + type);
        return factory;
    }
}
