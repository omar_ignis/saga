/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import java.text.MessageFormat;

import com.inventory.item.util.Util;
import com.inventory.item.util.Messages;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.exception.WarehouseNotFoundException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ignis
 */
@Service
public class WarehouseSummaryService {

    private final WarehouseRepository warehouseRepository;
    
    private final ItemSummaryRepository itemSummaryRepository;
    
    @Autowired
    public WarehouseSummaryService(
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository) {
        
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
    }
    
    public Warehouse save(Warehouse warehouse) {
        
        return warehouseRepository.save(warehouse);
    }
    
    public Warehouse update(Long id, Warehouse warehouse) {
        getWarehouseById(id);
        warehouse.setId(id);
        return warehouseRepository.save(warehouse);
    }

    public Warehouse getWarehouseById(Long warehouseId) {

        Optional<Warehouse> optional = warehouseRepository.findById(warehouseId);
        Warehouse result = optional.orElseThrow(()-> new WarehouseNotFoundException( warehouseId ));
        return result;
    }
    
    public Page<Warehouse> getAllWarehouses(Pageable pageable){
        return warehouseRepository.findAll(pageable);
    }
    
    public List<Warehouse> getAllWarehouses(){
        return warehouseRepository.findAllByOrderByIdAsc();
    }
    
    public Page<Warehouse> getWarehouseByName(String name, Pageable pageable){
        return warehouseRepository.findByDeletedFalseAndWarehouseNameLike(Util.buildLike(name), pageable);
    }
    
    public Page<ItemSummary> getItemSummaryByWarehouse(Long warehouseId, Pageable pageable){
        
        Warehouse warehouse = getWarehouseById(warehouseId);
        return itemSummaryRepository.findByKeyWarehouse(warehouse, pageable);    
    }
    
    public List<Item> getAllItemsByWarehouse(Long warehouseId) {

        Warehouse warehouse = getWarehouseById(warehouseId);
        List<ItemSummary> itemSummaryList = itemSummaryRepository.findByKeyWarehouse(warehouse);
        List<Item> items = itemSummaryList.stream().map(ItemSummary::getItem).collect(Collectors.toList());

        return items;
    }
    
    public void deleteWarehouse(Long id) {
        Warehouse warehouse = getWarehouseById(id);

        if (warehouse.isUsed()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    MessageFormat.format(Messages.WAREHOUSE_NOT_DELETED,
                            warehouse.getWarehouseName()));
        }

        warehouseRepository.deleteById(id);
    }

    public void deleteAllWarehouses(){
        warehouseRepository.deleteAll();
    }
    
}
