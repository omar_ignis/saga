/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service.impl;

import java.util.List;
import javax.transaction.Transactional;

import com.inventory.item.util.Util;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;

import com.inventory.item.document.InventoryDocument;
import com.inventory.item.inventory.EntryInventorySummary;
import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.inventory.item.exception.DocumentNotFoundException;
import com.inventory.item.service.DocumentService;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
@Transactional
public class InputDocumentServiceImpl implements DocumentService {

    private final ObjectMapper mapper;

    private final InventoryDocument inputDocumentImpl;

    private final EntryInventorySummary inputInventoryImpl;

    private final InputDetailRepository inputDetailRepository;

    private final InputDocumentRepository inputDocumentRepository;

    @Autowired
    public InputDocumentServiceImpl(
            InventoryDocument inputDocumentImpl,
            EntryInventorySummary inputInventoryImpl,
            InputDetailRepository inputDetailRepository,
            InputDocumentRepository inputDocumentRepository) {

        this.inputDocumentImpl = inputDocumentImpl;
        this.inputInventoryImpl = inputInventoryImpl;
        this.inputDetailRepository = inputDetailRepository;
        this.inputDocumentRepository = inputDocumentRepository;

        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public DocumentType getType() {
        return DocumentType.INPUT;
    }

    @Override
    public void remove(String id) throws Throwable {
        
        InputDocument document = getDocumentById(id);
        List<InputDetail> details = inputDetailRepository.findByInputDocument(document);
        inputDetailRepository.deleteAll(details);
        inputDocumentRepository.deleteById(id);
    }

    @Override
    public <T extends Document> T release(String id) throws Throwable {

        InputDocument inputDocument = getDocumentById(id);
        inputDocument = inputDocumentImpl.release(inputDocument);
        inputDocument = inputDocumentRepository.save(inputDocument);
        List<InputDetail> details = getDetailsByDocument(inputDocument.getId());
        inputInventoryImpl.updateInventory(inputDocument.getWarehouse().getId(), details);

        return (T) inputDocument;
    }

    @Override
    public <T extends Document> Page<T> getAllDocuments(Pageable pageable) {

        return (Page<T>) inputDocumentRepository.findByDeletedFalse(pageable);
    }

    @Override
    public <T extends Document, S extends Detail> T save(T document, List<S> details) {

        InputDocument inputDocument = (InputDocument) document;
        List<InputDetail> inputDetails = (List<InputDetail>) details;
        inputDocument = inputDocumentImpl.createDocument(inputDocument);
        inputDocument = inputDocumentRepository.save(inputDocument);
        List<InputDetail> list = inputDocumentImpl.generateDetails(inputDocument, inputDetails);
        inputDetailRepository.saveAll(list);
        inputDocument.setCounter(document.getCounter());

        return (T) inputDocumentRepository.save(inputDocument);
    }

    @Override
    public <T extends Document, S extends Detail> T update(T document, List<S> details) {

        InputDocument inputDocument = (InputDocument) document;
        List<InputDetail> inputDetails = (List<InputDetail>) details;

        inputDocument = inputDocumentImpl.updateDocument(inputDocument);
        inputDocument = inputDocumentRepository.save(inputDocument);

        //Obtiene todos los detalles que no han sido marcados para ser eliminados
        //Actualiza los existentes y agregar los nuevos en caso de haberlos
        List<InputDetail> list = Util.getNotDeletedDetails(inputDetails);
        list = inputDocumentImpl.updateDetails(inputDocument, list);
        inputDetailRepository.saveAll(list);

        //Elimina todo los detalles que fueron marcados
        inputDetailRepository.deleteAll(Util.getDeletedDetails(inputDetails));

        //Obtiene el max line number y lo actualiza
        int maxLineNumber = Util.getMaxLineNumber(list);
        inputDocument.setCounter(maxLineNumber);

        return (T) inputDocumentRepository.save(inputDocument);

    }

    @Override
    public <T extends Document> T getDocumentById(String id) throws Exception, Throwable {

        InputDocument document = inputDocumentRepository.findById(id)
                .orElseThrow(() -> new DocumentNotFoundException(id, DocumentType.INPUT.toString()));

        return (T) document;
    }

    @Override
    public <T extends Document> T saveOrUpdate(String document) throws Exception {

        InputDocument inputDocument = convert(document);
        List<InputDetail> inputDetails = (List<InputDetail>) inputDocument.getDetails();

        return (T) saveOrUpdate(inputDocument, inputDetails);
    }

    @Override
    public <T extends Document, S extends Detail> T saveOrUpdate(T document, List<S> details) {

        return document.getId() == null
                ? save(document, details) : update(document, details);
    }

    @Override
    public <T extends Document> Page<T> getAllDocumentsByIdLike(String id, Pageable pageable) {

        return (Page<T>) inputDocumentRepository.findByIdLike(id, pageable);
    }

    @Override
    public <S extends Detail> List<S> getDetailsByDocument(String id) throws Exception, Throwable {

        InputDocument document = getDocumentById(id);
        List<InputDetail> list = inputDetailRepository.findByInputDocument(document);

        return (List<S>) list;
    }

    @Override
    public <S extends Detail> Page<S> getDetailsByDocument(String id, Pageable pageable) throws Exception, Throwable {

        InputDocument document = getDocumentById(id);

        return (Page<S>) inputDetailRepository.findByInputDocument(document, pageable);
    }

    private InputDocument convert(String body) throws Exception {
        InputDocument document = mapper.readValue(body, InputDocument.class);

        return document;
    }

}
