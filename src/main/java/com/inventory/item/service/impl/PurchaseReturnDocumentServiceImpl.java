/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service.impl;

import java.util.List;
import javax.transaction.Transactional;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.util.Util;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.service.DocumentService;
import com.inventory.item.document.InventoryDocument;
import com.inventory.item.inventory.ExitInventorySummary;
import com.inventory.item.exception.DocumentNotFoundException;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
@Transactional
public class PurchaseReturnDocumentServiceImpl implements DocumentService {

    private final ObjectMapper mapper;

    private final InventoryDocument purchaseReturnDocumentImpl;

    private final ExitInventorySummary purchaseReturnInventoryImpl;

    private final PurchaseReturnDetailRepository purchaseReturnDetailRepository;

    private final PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    @Autowired
    public PurchaseReturnDocumentServiceImpl(
            InventoryDocument purchaseReturnDocumentImpl,
            ExitInventorySummary purchaseReturnInventoryImpl,
            PurchaseReturnDetailRepository purchaseReturnDetailRepository,
            PurchaseReturnDocumentRepository purchaseReturnDocumentRepository) {

        this.purchaseReturnDocumentImpl = purchaseReturnDocumentImpl;
        this.purchaseReturnInventoryImpl = purchaseReturnInventoryImpl;
        this.purchaseReturnDetailRepository = purchaseReturnDetailRepository;
        this.purchaseReturnDocumentRepository = purchaseReturnDocumentRepository;

        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public DocumentType getType() {
        return DocumentType.PURCHASE_RETURN;
    }

    @Override
    public void remove(String id) throws Throwable {
        
        PurchaseReturnDocument document = getDocumentById(id);
        List<PurchaseReturnDetail> details = purchaseReturnDetailRepository
                .findByPurchaseReturnDocument(document);
        
        purchaseReturnDetailRepository.deleteAll(details);
        purchaseReturnDocumentRepository.deleteById(id);
        
    }
    
    @Override
    public <T extends Document> T release(String id) throws Throwable {

        PurchaseReturnDocument purchaseReturnDocument = getDocumentById(id);
        purchaseReturnDocument = purchaseReturnDocumentImpl.release(purchaseReturnDocument);
        purchaseReturnDocument = purchaseReturnDocumentRepository.save(purchaseReturnDocument);
        List<PurchaseReturnDetail> details = getDetailsByDocument(purchaseReturnDocument.getId());
        purchaseReturnInventoryImpl.updateInventory(purchaseReturnDocument.getWarehouse().getId(), details);

        return (T) purchaseReturnDocument;
    }

    @Override
    public <T extends Document> Page<T> getAllDocuments(Pageable pageable) {

        return (Page<T>) purchaseReturnDocumentRepository.findByDeletedFalse(pageable);
    }

    @Override
    public <T extends Document, S extends Detail> T save(T document, List<S> details) {

        PurchaseReturnDocument purchaseReturnDocument = (PurchaseReturnDocument) document;
        List<PurchaseReturnDetail> purchaseReturnDetails = (List<PurchaseReturnDetail>) details;

        purchaseReturnDocument = purchaseReturnDocumentImpl.createDocument(purchaseReturnDocument);
        purchaseReturnDocument = purchaseReturnDocumentRepository.save(purchaseReturnDocument);
        List<PurchaseReturnDetail> list = purchaseReturnDocumentImpl
                .generateDetails(purchaseReturnDocument, purchaseReturnDetails);
        purchaseReturnDetailRepository.saveAll(list);
        purchaseReturnDocument.setCounter(document.getCounter());

        return (T) purchaseReturnDocumentRepository.save(purchaseReturnDocument);
    }

    @Override
    public <T extends Document, S extends Detail> T update(T document, List<S> details) {

        PurchaseReturnDocument purchaseReturnDocument = (PurchaseReturnDocument) document;
        List<PurchaseReturnDetail> purchaseReturnDetails = (List<PurchaseReturnDetail>) details;

        purchaseReturnDocument = purchaseReturnDocumentImpl.updateDocument(purchaseReturnDocument);
        purchaseReturnDocument = purchaseReturnDocumentRepository.save(purchaseReturnDocument);

        //Obtiene todos los detalles que no han sido marcados para ser eliminados
        //Actualiza los existentes y agregar los nuevos en caso de haberlos
        List<PurchaseReturnDetail> list = Util.getNotDeletedDetails(purchaseReturnDetails);
        list = purchaseReturnDocumentImpl.updateDetails(purchaseReturnDocument, list);
        purchaseReturnDetailRepository.saveAll(list);

        //Elimina todo los detalles que fueron marcados
        purchaseReturnDetailRepository.deleteAll(Util.getDeletedDetails(purchaseReturnDetails));

        //Obtiene el max line number y lo actualiza
        int maxLineNumber = Util.getMaxLineNumber(list);
        purchaseReturnDocument.setCounter(maxLineNumber);

        return (T) purchaseReturnDocumentRepository.save(purchaseReturnDocument);

    }

    @Override
    public <T extends Document> T getDocumentById(String id) throws Exception, Throwable {

        PurchaseReturnDocument document = purchaseReturnDocumentRepository.findById(id)
                .orElseThrow(() -> new DocumentNotFoundException(id, DocumentType.PURCHASE_RETURN.toString()));
        
        return (T) document;
    }

    @Override
    public <T extends Document> T saveOrUpdate(String document) throws Exception {

        PurchaseReturnDocument purchaseReturnDocument = convert(document);
        List<PurchaseReturnDetail> purchaseReturnDetails = (List<PurchaseReturnDetail>) purchaseReturnDocument.getDetails();

        return (T) saveOrUpdate(purchaseReturnDocument, purchaseReturnDetails);
    }

    @Override
    public <T extends Document, S extends Detail> T saveOrUpdate(T document, List<S> details) {

        return document.getId() == null
                ? save(document, details) : update(document, details);
    }

    @Override
    public <T extends Document> Page<T> getAllDocumentsByIdLike(String id, Pageable pageable) {

        return (Page<T>) purchaseReturnDocumentRepository.findByIdLike(id, pageable);
    }

    @Override
    public <S extends Detail> List<S> getDetailsByDocument(String id) throws Exception, Throwable {

        PurchaseReturnDocument document = getDocumentById(id);
        List<PurchaseReturnDetail> list = purchaseReturnDetailRepository.findByPurchaseReturnDocument(document);

        return (List<S>) list;
    }

    @Override
    public <S extends Detail> Page<S> getDetailsByDocument(String id, Pageable pageable) throws Exception, Throwable {

        PurchaseReturnDocument document = getDocumentById(id);
        return (Page<S>) purchaseReturnDetailRepository.findByPurchaseReturnDocument(document, pageable);
    }

    private PurchaseReturnDocument convert(String body) throws Exception {

        PurchaseReturnDocument document = mapper.readValue(body, PurchaseReturnDocument.class);

        return document;
    }
}
