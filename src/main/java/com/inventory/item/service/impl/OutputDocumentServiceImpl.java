/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service.impl;

import java.util.List;
import javax.transaction.Transactional;

import com.inventory.item.util.Util;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.service.DocumentService;
import com.inventory.item.document.InventoryDocument;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.inventory.ExitInventorySummary;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.OutputDocumentRepository;
import com.inventory.item.exception.DocumentNotFoundException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
@Transactional
public class OutputDocumentServiceImpl implements DocumentService {

    private final ObjectMapper mapper;

    private final InventoryDocument outputDocumentImpl;

    private final ExitInventorySummary outputInventoryImpl;

    private final OutputDetailRepository outputDetailRepository;

    private final OutputDocumentRepository outputDocumentRepository;

    @Autowired
    public OutputDocumentServiceImpl(
            InventoryDocument outputDocumentImpl,
            ExitInventorySummary outputInventoryImpl,
            OutputDetailRepository outputDetailRepository,
            OutputDocumentRepository outputDocumentRepository) {
        
        this.outputDocumentImpl = outputDocumentImpl;
        this.outputInventoryImpl = outputInventoryImpl;
        this.outputDetailRepository = outputDetailRepository;
        this.outputDocumentRepository = outputDocumentRepository;

        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public DocumentType getType() {
        return DocumentType.OUTPUT;
    }
    
    @Override
    public void remove(String id) throws Throwable {
        
        OutputDocument document = getDocumentById(id);
        List<OutputDetail> details = outputDetailRepository.findByOutputDocument(document);
        outputDetailRepository.deleteAll(details);
        outputDocumentRepository.deleteById(id);
    }

    @Override
    public <T extends Document> T release(String id) throws Throwable {

        OutputDocument outputDocument = getDocumentById(id);
        outputDocument = outputDocumentImpl.release(outputDocument);
        outputDocument = outputDocumentRepository.save(outputDocument);
        List<OutputDetail> details = getDetailsByDocument(outputDocument.getId());
        outputInventoryImpl.updateInventory(outputDocument.getWarehouse().getId(), details);

        return (T) outputDocument;
    }

    @Override
    public <T extends Document> Page<T> getAllDocuments(Pageable pageable) {

        return (Page<T>) outputDocumentRepository.findByDeletedFalse(pageable);
    }

    @Override
    public <T extends Document, S extends Detail> T save(T document, List<S> details) {

        OutputDocument outputDocument = (OutputDocument) document;
        List<OutputDetail> outputDetails = (List<OutputDetail>) details;

        outputDocument = outputDocumentImpl.createDocument(outputDocument);
        outputDocument = outputDocumentRepository.save(outputDocument);
        List<OutputDetail> list = outputDocumentImpl.generateDetails(outputDocument, outputDetails);
        outputDetailRepository.saveAll(list);
        outputDocument.setCounter(document.getCounter());

        return (T) outputDocumentRepository.save(outputDocument);
    }

    @Override
    public <T extends Document, S extends Detail> T update(T document, List<S> details) {

        OutputDocument outputDocument = (OutputDocument) document;
        List<OutputDetail> outputDetails = (List<OutputDetail>) details;

        outputDocument = outputDocumentImpl.updateDocument(outputDocument);
        outputDocument = outputDocumentRepository.save(outputDocument);

        //Obtiene todos los detalles que no han sido marcados para ser eliminados
        //Actualiza los existentes y agregar los nuevos en caso de haberlos
        List<OutputDetail> list = Util.getNotDeletedDetails(outputDetails);
        list = outputDocumentImpl.updateDetails(outputDocument, list);
        outputDetailRepository.saveAll(list);

        //Elimina todo los detalles que fueron marcados
        outputDetailRepository.deleteAll(Util.getDeletedDetails(outputDetails));

        //Obtiene el max line number y lo actualiza
        int maxLineNumber = Util.getMaxLineNumber(list);
        outputDocument.setCounter(maxLineNumber);

        return (T) outputDocumentRepository.save(outputDocument);
    }

    @Override
    public <T extends Document> T getDocumentById(String id) throws Exception, Throwable {

        OutputDocument document = outputDocumentRepository.findById(id)
                .orElseThrow(() -> new DocumentNotFoundException(id, DocumentType.OUTPUT.toString()));
        return (T) document;
    }

    @Override
    public <T extends Document> T saveOrUpdate(String document) throws Exception {

        OutputDocument outputDocument = convert(document);
        List<OutputDetail> outputDetails = (List<OutputDetail>) outputDocument.getDetails();

        return (T) saveOrUpdate(outputDocument, outputDetails);
    }

    @Override
    public <T extends Document, S extends Detail> T saveOrUpdate(T document, List<S> details) {

        return document.getId() == null
                ? save(document, details) : update(document, details);
    }

    @Override
    public <T extends Document> Page<T> getAllDocumentsByIdLike(String id, Pageable pageable) {

        return (Page<T>) outputDocumentRepository.findByIdLike(id, pageable);
    }

    @Override
    public <S extends Detail> List<S> getDetailsByDocument(String id) throws Exception, Throwable {

        OutputDocument document = getDocumentById(id);
        List<OutputDetail> list = outputDetailRepository.findByOutputDocument(document);

        return (List<S>) list;
    }

    @Override
    public <S extends Detail> Page<S> getDetailsByDocument(String id, Pageable pageable) throws Exception, Throwable {

        OutputDocument document = getDocumentById(id);
        return (Page<S>) outputDetailRepository.findByOutputDocument(document, pageable);
    }

    private OutputDocument convert(String body) throws Exception {
        OutputDocument document = mapper.readValue(body, OutputDocument.class);

        return document;
    }

}
