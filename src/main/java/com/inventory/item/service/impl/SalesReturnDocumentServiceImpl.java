/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.service.impl;

import java.util.List;
import javax.transaction.Transactional;

import com.inventory.item.util.Util;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import com.inventory.item.service.DocumentService;
import com.inventory.item.document.InventoryDocument;
import com.inventory.item.inventory.EntryInventorySummary;
import com.inventory.item.exception.DocumentNotFoundException;
import com.inventory.item.repository.SalesReturnDetailRepository;
import com.inventory.item.repository.SalesReturnDocumentRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
@Transactional
public class SalesReturnDocumentServiceImpl implements DocumentService {

    private final ObjectMapper mapper;

    private final InventoryDocument saleReturnDocumentImpl;

    private final EntryInventorySummary saleReturnInventoryImpl;

    private final SalesReturnDetailRepository saleReturnDetailRepository;

    private final SalesReturnDocumentRepository saleReturnDocumentRepository;

    @Autowired
    public SalesReturnDocumentServiceImpl(
            InventoryDocument saleReturnDocumentImpl,
            EntryInventorySummary saleReturnInventoryImpl,
            SalesReturnDetailRepository saleReturnDetailRepository,
            SalesReturnDocumentRepository saleReturnDocumentRepository) {

        this.saleReturnDocumentImpl = saleReturnDocumentImpl;
        this.saleReturnInventoryImpl = saleReturnInventoryImpl;
        this.saleReturnDetailRepository = saleReturnDetailRepository;
        this.saleReturnDocumentRepository = saleReturnDocumentRepository;

        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
    }

    @Override
    public DocumentType getType() {
        return DocumentType.SALES_RETURN;
    }
    
    @Override
    public void remove(String id) throws Throwable {
        
        SalesReturnDocument document = getDocumentById(id);
        List<SalesReturnDetail> details = saleReturnDetailRepository.findBySalesReturnDocument(document);
        saleReturnDetailRepository.deleteAll(details);
        saleReturnDocumentRepository.deleteById(id);
        
    }

    @Override
    public <T extends Document> T release(String id) throws Throwable {

        SalesReturnDocument saleReturnDocument = getDocumentById(id);
        saleReturnDocument = saleReturnDocumentImpl.release(saleReturnDocument);
        saleReturnDocument = saleReturnDocumentRepository.save(saleReturnDocument);
        List<SalesReturnDetail> details = getDetailsByDocument(saleReturnDocument.getId());
        saleReturnInventoryImpl.updateInventory(saleReturnDocument.getWarehouse().getId(), details);

        return (T) saleReturnDocument;
    }

    @Override
    public <T extends Document> Page<T> getAllDocuments(Pageable pageable) {

        return (Page<T>) saleReturnDocumentRepository.findByDeletedFalse(pageable);
    }

    @Override
    public <T extends Document, S extends Detail> T save(T document, List<S> details) {

        SalesReturnDocument saleReturnDocument = (SalesReturnDocument) document;
        List<SalesReturnDetail> saleReturnDetails = (List<SalesReturnDetail>) details;

        saleReturnDocument = saleReturnDocumentImpl.createDocument(saleReturnDocument);
        saleReturnDocument = saleReturnDocumentRepository.save(saleReturnDocument);
        List<SalesReturnDetail> list = saleReturnDocumentImpl.generateDetails(saleReturnDocument, saleReturnDetails);
        saleReturnDetailRepository.saveAll(list);
        saleReturnDocument.setCounter(document.getCounter());

        return (T) saleReturnDocumentRepository.save(saleReturnDocument);
    }

    @Override
    public <T extends Document, S extends Detail> T update(T document, List<S> details) {

        SalesReturnDocument saleReturnDocument = (SalesReturnDocument) document;
        List<SalesReturnDetail> saleReturnDetails = (List<SalesReturnDetail>) details;

        saleReturnDocument = saleReturnDocumentImpl.updateDocument(saleReturnDocument);
        saleReturnDocument = saleReturnDocumentRepository.save(saleReturnDocument);

        //Obtiene todos los detalles que no han sido marcados para ser eliminados
        //Actualiza los existentes y agregar los nuevos en caso de haberlos
        List<SalesReturnDetail> list = Util.getNotDeletedDetails(saleReturnDetails);
        list = saleReturnDocumentImpl.updateDetails(saleReturnDocument, list);
        saleReturnDetailRepository.saveAll(list);

        //Elimina todo los detalles que fueron marcados
        saleReturnDetailRepository.deleteAll(Util.getDeletedDetails(saleReturnDetails));

        //Obtiene el max line number y lo actualiza
        int maxLineNumber = Util.getMaxLineNumber(list);
        saleReturnDocument.setCounter(maxLineNumber);

        return (T) saleReturnDocumentRepository.save(saleReturnDocument);
    }

    @Override
    public <T extends Document> T getDocumentById(String id) throws Exception, Throwable {

        SalesReturnDocument document = saleReturnDocumentRepository.findById(id)
                .orElseThrow(() -> new DocumentNotFoundException(id, DocumentType.SALES_RETURN.toString()));
        
        return (T) document;
    }

    @Override
    public <T extends Document> T saveOrUpdate(String document) throws Exception {

        SalesReturnDocument saleReturnDocument = convert(document);
        List<SalesReturnDetail> saleReturnDetails = (List<SalesReturnDetail>) saleReturnDocument.getDetails();

        return (T) saveOrUpdate(saleReturnDocument, saleReturnDetails);
    }

    @Override
    public <T extends Document, S extends Detail> T saveOrUpdate(T document, List<S> details) {

        return document.getId() == null
                ? save(document, details) : update(document, details);
    }

    @Override
    public <T extends Document> Page<T> getAllDocumentsByIdLike(String id, Pageable pageable) {

        return (Page<T>) saleReturnDocumentRepository.findByIdLike(id, pageable);
    }

    @Override
    public <S extends Detail> List<S> getDetailsByDocument(String id) throws Exception, Throwable {

        SalesReturnDocument document = getDocumentById(id);
        List<SalesReturnDetail> list = saleReturnDetailRepository.findBySalesReturnDocument(document);

        return (List<S>) list;
    }

    @Override
    public <S extends Detail> Page<S> getDetailsByDocument(String id, Pageable pageable) throws Exception, Throwable {

        SalesReturnDocument document = getDocumentById(id);

        return (Page<S>) saleReturnDetailRepository.findBySalesReturnDocument(document, pageable);
    }

    private SalesReturnDocument convert(String body) throws Exception {

        SalesReturnDocument document = mapper.readValue(body, SalesReturnDocument.class);

        return document;
    }

}
