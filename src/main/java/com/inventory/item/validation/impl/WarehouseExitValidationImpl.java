/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.validation.impl;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.validation.WarehouseValidation;
import com.inventory.item.repository.WarehouseRepository;
import com.inventory.item.repository.ItemSummaryRepository;
import com.inventory.item.exception.NotEnoughInventoryException;
import com.inventory.item.exception.ItemSummaryNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Service
public class WarehouseExitValidationImpl implements WarehouseValidation {
    
    private final WarehouseRepository warehouseRepository;
    private final ItemSummaryRepository itemSummaryRepository;

    @Autowired
    public WarehouseExitValidationImpl(
            WarehouseRepository warehouseRepository, 
            ItemSummaryRepository itemSummaryRepository) {
        
        this.warehouseRepository = warehouseRepository;
        this.itemSummaryRepository = itemSummaryRepository;
        
    }
    
    @Override
    public ItemSummary validateItemSummary(Warehouse warehouse, Item item) throws Exception {
        
        ItemSummary itemSummary = itemSummaryRepository
                    .findByKeyWarehouseAndKeyItem(warehouse, item);
        
        if(itemSummary == null){
            throw new ItemSummaryNotFoundException(item.getId(), item.getItemName(), warehouse.getWarehouseName());
        }
        
        return itemSummary;
    }

    @Override
    public <S extends Detail> void validateInventoryBeforeUpdate(ItemSummary itemSummary, S detail) throws Exception {
        
        if(itemSummary.getQuantity().compareTo(detail.getQuantity()) < 0 ){
            throw new NotEnoughInventoryException(detail.getItem().getId(), detail.getItem().getItemName());
        }
    }
    
}
