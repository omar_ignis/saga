/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.validation;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;

/**
 *
 * @author ignis
 */
public interface WarehouseValidation {
    
    public ItemSummary validateItemSummary(Warehouse warehouse, Item item) throws Exception;
    
    public <S extends Detail> void validateInventoryBeforeUpdate(ItemSummary itemSummary, S detail) throws Exception;
}
