/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.Item;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author ignis
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findAllByOrderByIdAsc();
    Page<Item> findByItemNameLike(String name, Pageable pageable);
    Page<Item> findAllByOrderByIdAsc(Pageable pageable);

    @Modifying
    @Query("UPDATE Item i SET i.deleted = true WHERE i.id = ?1")
    void disableItem(Long id);
}
