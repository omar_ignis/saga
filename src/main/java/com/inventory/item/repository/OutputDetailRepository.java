/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author ignis
 */
public interface OutputDetailRepository extends DetailRepository<OutputDetail> {
    
    List<OutputDetail> findByOutputDocument(OutputDocument document);
    
    Page<OutputDetail> findByOutputDocument(OutputDocument document, Pageable pageable);
    
    OutputDetail findByOutputDocumentAndLineNumberAndItemId(OutputDocument document, int lineNumber, long itemId);
}
