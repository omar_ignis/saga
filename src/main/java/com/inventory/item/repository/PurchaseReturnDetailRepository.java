/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
/**
 *
 * @author ignis
 */
public interface PurchaseReturnDetailRepository extends DetailRepository<PurchaseReturnDetail> {
    
    List<PurchaseReturnDetail> findByPurchaseReturnDocument(PurchaseReturnDocument document);
    
    Page<PurchaseReturnDetail> findByPurchaseReturnDocument(PurchaseReturnDocument document, Pageable pageable);
    
    PurchaseReturnDetail findByPurchaseReturnDocumentAndLineNumberAndItemId(PurchaseReturnDocument document, int lineNumber, long itemId);
    
}
