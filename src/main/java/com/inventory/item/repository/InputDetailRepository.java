/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author ignis
 */
public interface InputDetailRepository extends DetailRepository<InputDetail> {
    
    List<InputDetail> findByInputDocument(InputDocument document);
    
    Page<InputDetail> findByInputDocument(InputDocument document, Pageable pageable);
    
    InputDetail findByInputDocumentAndLineNumberAndItemId(InputDocument document, int lineNumber, long itemId);
}
