/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author ignis
 */
public interface SalesReturnDetailRepository extends DetailRepository<SalesReturnDetail> {

    List<SalesReturnDetail> findBySalesReturnDocument(SalesReturnDocument document);

    Page<SalesReturnDetail> findBySalesReturnDocument(SalesReturnDocument document, Pageable pageable);

    SalesReturnDetail findBySalesReturnDocumentAndLineNumberAndItemId(SalesReturnDocument document, int lineNumber, long itemId);
}
