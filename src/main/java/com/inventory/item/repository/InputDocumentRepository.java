/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.InputDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ignis
 */
public interface InputDocumentRepository extends DocumentRepository <InputDocument> {
    
    @Query(value = "SELECT nextval('input_doc_seq')", nativeQuery = true)
    Long getNextId();
    
    Page<InputDocument> findByDeletedFalse(Pageable pageable);
    
    @Query("SELECT d FROM InputDocument d WHERE d.id like %?1% AND d.deleted = false")
    Page<InputDocument> findByIdLike(String id, Pageable pageable);
    
}
