/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.entity.key.ItemSummaryKey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ignis
 */
@Repository
public interface ItemSummaryRepository extends JpaRepository<ItemSummary, ItemSummaryKey> {

    @Query("SELECT i FROM ItemSummary i WHERE i.key.warehouse = ?1 AND i.key.item.id = ?2")
    ItemSummary findItemSummaryByWarehouseAndItemId(Warehouse warehouse, Long itemId);

    ItemSummary findByKeyWarehouseAndKeyItem(Warehouse warehouse, Item item);
    
    Page<ItemSummary> findByKeyWarehouse(Warehouse warehouse, Pageable pageable);
    
    List<ItemSummary> findByKeyWarehouse(Warehouse warehouse);

}
