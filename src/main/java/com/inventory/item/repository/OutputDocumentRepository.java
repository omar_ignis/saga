/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.OutputDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author ignis
 */
public interface OutputDocumentRepository extends DocumentRepository<OutputDocument> {
    
    @Query(value = "SELECT nextval('output_doc_seq')", nativeQuery = true)
    Long getNextId();
    
    Page<OutputDocument> findByDeletedFalse(Pageable pageable);
    
    @Query("SELECT d FROM OutputDocument d WHERE d.id like %?1% AND d.deleted = false")
    Page<OutputDocument> findByIdLike(String id, Pageable pageable);
}
