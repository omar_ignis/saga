/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import java.util.List;
import java.util.Optional;
import com.inventory.item.entity.Warehouse;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ignis
 */
@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {

    @EntityGraph(attributePaths = {"itemSummary"})
    Optional<Warehouse> findById(Long Id);
    
    Page<Warehouse> findByDeletedFalseAndWarehouseNameLike(String name, Pageable pageable);
    
    List<Warehouse> findAllByOrderByIdAsc();

    @Modifying
    @Query("UPDATE Warehouse w SET w.deleted = true WHERE w.id = ?1")
    void disableWarehouse(Long id);

}
