/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.repository;

import org.springframework.stereotype.Repository;

/**
 * @author ignis
 */
@Repository
public interface CoreRepository {
    String generateRefNbr(Object object);

    void CreateSequence(String sequenceName);
}
