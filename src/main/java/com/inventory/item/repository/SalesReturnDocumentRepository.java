/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.SalesReturnDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author ignis
 */
public interface SalesReturnDocumentRepository extends DocumentRepository<SalesReturnDocument> {
    
    @Query(value = "SELECT nextval('sale_return_doc_seq')", nativeQuery = true)
    Long getNextId();
    
    Page<SalesReturnDocument> findByDeletedFalse(Pageable pageable);
    
    @Query("SELECT d FROM SalesReturnDocument d WHERE d.id like %?1% AND d.deleted = false")
    Page<SalesReturnDocument> findByIdLike(String id, Pageable pageable);
}
