/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.Detail;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ignis
 */
@Repository
public interface DetailRepository<S extends Detail> extends JpaRepository<S, Long> {
    
}
