/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.PurchaseReturnDocument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author ignis
 */
public interface PurchaseReturnDocumentRepository extends DocumentRepository<PurchaseReturnDocument> {
    
    @Query(value = "SELECT nextval('purchase_return_doc_seq')", nativeQuery = true)
    Long getNextId();
    
    Page<PurchaseReturnDocument> findByDeletedFalse(Pageable pageable);
    
    @Query("SELECT d FROM PurchaseReturnDocument d WHERE d.id like %?1% AND d.deleted = false")
    Page<PurchaseReturnDocument> findByIdLike(String id, Pageable pageable);
    
}
