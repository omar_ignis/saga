/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.repository;

import com.inventory.item.entity.Document;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ignis
 */
@Repository
public interface DocumentRepository<T extends Document> extends JpaRepository<T, String> {

}
