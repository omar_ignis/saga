/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;

import com.inventory.item.util.Constants;
import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.BarcodeLabel;
import com.inventory.item.barcode.sheet.Sheet;
import com.inventory.item.barcode.BarcodeWriter;
import com.inventory.item.barcode.BarcodeReader;
import com.inventory.item.barcode.sheet.factory.SheetFactory;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class Barcode128AutoImpl implements BarcodeWriter, BarcodeReader {

    private final SheetFactory sheetFactoryService;

    @Autowired
    public Barcode128AutoImpl(SheetFactory sheetFactoryService) {

        this.sheetFactoryService = sheetFactoryService;
    }

    @Override
    public String buildBarcode(DocumentType type, String documentId, int lineNumber, long itemId) {

        String lineNbrFormatted = String.format("%04d", lineNumber);
        String itemIdFormatted = String.format("%05d", itemId);
        String refNbrFormatted = documentId.substring(Constants.DOCUMENT_TYPE_LENGTH);
        return type.ordinal() + refNbrFormatted + lineNbrFormatted + itemIdFormatted;

    }

    @Override
    public <S extends Detail> List<BarcodeLabel> generateBarcodeLabel(Document document, List<S> details) {

        int quantity;
        String barcode;
        List<BarcodeLabel> list = new ArrayList<>();

        for (S detail : details) {
            //Construye codigo de barras
            barcode = buildBarcode(document.getType(), document.getId(),
                    detail.getLineNumber(), detail.getItem().getId());
            quantity = detail.getQuantity().intValue();
            //Crea un nuevo objeto BarcodeLabel y lo agrega a la lista.
            list.add(new BarcodeLabel(detail.getItem().getItemName(), barcode, quantity));
        }

        return list;
    }

    @Override
    public List<RowSheet> generateFullSheetLabel(List<BarcodeLabel> barcodeLabelList, SheetType type) {

        Sheet service = sheetFactoryService.getSheetImpl(type);

        List<RowSheet> result = service.createFullSheet(barcodeLabelList);

        return result;
    }

    @Override
    public List<RowSheet> generatePartialSheetLabel(List<BarcodeLabel> barcodeLabelList, List<Integer> positionList, SheetType type) {

        Sheet service = sheetFactoryService.getSheetImpl(type);

        List<RowSheet> result = service.createCustomSheet(barcodeLabelList, positionList);

        return result;
    }

    @Override
    public BarcodeType getType() {
        return BarcodeType.CODE128;
    }

    @Override
    public DocumentType readDocumenType(String barcode) {

        int docTypePosition = Character.getNumericValue(barcode.charAt(0));

        return DocumentType.values()[docTypePosition];
    }

    @Override
    public String readDocumentId(String barcode) {

        return barcode.substring(1, Constants.DOCUMENT_ID_LENGTH + 1);
    }

    @Override
    public int readLineNumber(String barcode) {

        int startIndex = Constants.DOCUMENT_ID_LENGTH + 1;
        int endIndex = startIndex + Constants.LINE_NUMBER_LENGTH;
        String lineNumber = barcode.substring(startIndex, endIndex);

        return Integer.parseInt(lineNumber);
    }

    @Override
    public long readItemId(String barcode) {

        int startIndex = Constants.DOCUMENT_ID_LENGTH + Constants.LINE_NUMBER_LENGTH + 1;
        String itemId = barcode.substring(startIndex);

        return Long.parseLong(itemId);
    }

}
