/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.impl;

import com.inventory.item.entity.Detail;
import com.inventory.item.util.Constants;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.barcode.BarcodeReader;
import com.inventory.item.barcode.DocumentBarcodeReader;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.exception.WarehouseNotValidException;
import com.inventory.item.repository.PurchaseReturnDetailRepository;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class PurchaseReturnDocumentBarcodeImpl implements DocumentBarcodeReader {

    private final BarcodeFactory barcodeFactoryService;
    private final PurchaseReturnDetailRepository purchaseReturnDetailRepository;
    private final PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    @Autowired
    public PurchaseReturnDocumentBarcodeImpl(
            BarcodeFactory barcodeFactoryService,
            PurchaseReturnDetailRepository purchaseReturnDetailRepository,
            PurchaseReturnDocumentRepository purchaseReturnDocumentRepository) {

        this.barcodeFactoryService = barcodeFactoryService;
        this.purchaseReturnDetailRepository = purchaseReturnDetailRepository;
        this.purchaseReturnDocumentRepository = purchaseReturnDocumentRepository;
    }

    @Override
    public DocumentType getType() {
        return DocumentType.PURCHASE_RETURN;
    }

    @Override
    public <S extends Detail> S readLabel(Long warehouseId, String barcode, BarcodeType type) throws Exception {

        BarcodeReader reader = barcodeFactoryService.getReaderFactory(type);

        long itemId = reader.readItemId(barcode);
        int lineNumber = reader.readLineNumber(barcode);
        String id = Constants.RETURN_FOR_PURCHASE_PREFIX + reader.readDocumentId(barcode);
        var document = purchaseReturnDocumentRepository.findById(id).get();

        if (!document.getStatus().equals(Status.OPEN)) {
            throw new Exception("Status not valid for this action");
        }

        if (!document.getWarehouse().getId().equals(warehouseId)) {
            throw new WarehouseNotValidException(document.getWarehouse().getWarehouseName());
        }

        return (S) purchaseReturnDetailRepository.findByPurchaseReturnDocumentAndLineNumberAndItemId(document, lineNumber, itemId);
    }

}
