/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.impl;

import com.inventory.item.util.Constants;

import com.inventory.item.entity.Detail;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.barcode.BarcodeReader;
import com.inventory.item.barcode.DocumentBarcodeReader;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.repository.OutputDetailRepository;
import com.inventory.item.repository.OutputDocumentRepository;
import com.inventory.item.exception.WarehouseNotValidException;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class OutputDocumentBarcodeImpl implements DocumentBarcodeReader {

    private final BarcodeFactory barcodeFactoryService;
    private final OutputDetailRepository outputDetailRepository;
    private final OutputDocumentRepository outputDocumentRepository;

    @Autowired
    public OutputDocumentBarcodeImpl(
            BarcodeFactory barcodeFactoryService,
            OutputDetailRepository outputDetailRepository,
            OutputDocumentRepository outputDocumentRepository) {

        this.barcodeFactoryService = barcodeFactoryService;
        this.outputDetailRepository = outputDetailRepository;
        this.outputDocumentRepository = outputDocumentRepository;
    }

    @Override
    public DocumentType getType() {

        return DocumentType.OUTPUT;
    }

    @Override
    public <S extends Detail> S readLabel(Long warehouseId, String barcode, BarcodeType type) throws Exception {

        BarcodeReader reader = barcodeFactoryService.getReaderFactory(type);

        long itemId = reader.readItemId(barcode);
        int lineNumber = reader.readLineNumber(barcode);
        String id = Constants.OUTPUT_PREFIX + reader.readDocumentId(barcode);
        var document = outputDocumentRepository.findById(id).get();

        if (!document.getStatus().equals(Status.OPEN)) {
            throw new Exception("Status not valid for this action");
        }

        if (!document.getWarehouse().getId().equals(warehouseId)) {
            throw new WarehouseNotValidException(document.getWarehouse().getWarehouseName());
        }

        return (S) outputDetailRepository.findByOutputDocumentAndLineNumberAndItemId(document, lineNumber, itemId);
    }

}
