/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.util.Constants;
import com.inventory.item.model.RowSheet;
import com.inventory.item.model.BarcodeLabel;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.barcode.BarcodeWriter;
import com.inventory.item.barcode.BarcodeReader;
import com.inventory.item.barcode.DocumentBarcodeReader;
import com.inventory.item.barcode.DocumentBarcodeWriter;
import com.inventory.item.barcode.factory.BarcodeFactory;
import com.inventory.item.repository.InputDetailRepository;
import com.inventory.item.repository.InputDocumentRepository;
import com.inventory.item.exception.DocumentNotFoundException;
import com.inventory.item.exception.WarehouseNotValidException;
import com.inventory.item.exception.DocumentNotValidStatusException;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class InputDocumentBarcodeImpl implements DocumentBarcodeWriter, DocumentBarcodeReader {

    private final BarcodeFactory barcodeFactoryService;
    private final InputDetailRepository inputDetailRepository;
    private final InputDocumentRepository inputDocumentRepository;

    @Autowired
    public InputDocumentBarcodeImpl(
            BarcodeFactory barcodeFactoryService,
            InputDetailRepository inputDetailRepository,
            InputDocumentRepository inputDocumentRepository) {

        this.barcodeFactoryService = barcodeFactoryService;
        this.inputDetailRepository = inputDetailRepository;
        this.inputDocumentRepository = inputDocumentRepository;

    }

    @Override
    public DocumentType getType() {
        return DocumentType.INPUT;
    }

    @Override
    public List<RowSheet> generateLabels(List<Integer> positionList, String id, BarcodeType barcodeType, SheetType sheetType) throws Exception {

        List<RowSheet> result = new ArrayList<>();
        InputDocument document = inputDocumentRepository.findById(id).get();
        List<InputDetail> details = inputDetailRepository.findByInputDocument(document);

        if (!document.getStatus().equals(Status.RELEASED)) {

            throw new DocumentNotValidStatusException("imprimir etiquetas");
        }

        BarcodeWriter writer = barcodeFactoryService.getWriterFactory(barcodeType);

        List<BarcodeLabel> barcodeLabelList = writer.generateBarcodeLabel(document, details);

        if (!positionList.isEmpty()) {
            result.addAll(writer.generatePartialSheetLabel(barcodeLabelList, positionList, sheetType));
        }

        result.addAll(writer.generateFullSheetLabel(barcodeLabelList, sheetType));

        return result;
    }

    @Override
    public <S extends Detail> S readLabel(Long warehouseId, String barcode, BarcodeType type) throws Exception {
        
        BarcodeReader reader = barcodeFactoryService.getReaderFactory(type);
        long itemId = reader.readItemId(barcode);
        int lineNumber = reader.readLineNumber(barcode);
        String id = Constants.INPUT_PREFIX + reader.readDocumentId(barcode);
        var document = inputDocumentRepository.findById(id).orElseThrow(() -> {
            throw new DocumentNotFoundException(id, DocumentType.INPUT.toString());
        });
        
        if (!document.getStatus().equals(Status.RELEASED)) {
            throw new DocumentNotValidStatusException("obtener los articulos");
        }
        
        if(!document.getWarehouse().getId().equals(warehouseId)){
            throw new WarehouseNotValidException(document.getWarehouse().getWarehouseName());
        }
        
        return (S) inputDetailRepository.findByInputDocumentAndLineNumberAndItemId(document, lineNumber, itemId);
    }

}
