/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.factory;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import com.inventory.item.enums.BarcodeType;
import com.inventory.item.barcode.BarcodeWriter;
import com.inventory.item.barcode.BarcodeReader;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class BarcodeFactory {

    private final Map<BarcodeType, BarcodeWriter> writerFactoryCache = new HashMap<>();
    private final Map<BarcodeType, BarcodeReader> readerFactoryCache = new HashMap<>();

    @Autowired
    public BarcodeFactory(List<BarcodeWriter> writters, List<BarcodeReader> readers) {

        for (BarcodeWriter service : writters) {
            writerFactoryCache.put(service.getType(), service);
        }

        for (BarcodeReader service : readers) {
            readerFactoryCache.put(service.getType(), service);
        }
    }

    public BarcodeWriter getWriterFactory(BarcodeType type) {
        BarcodeWriter factory = writerFactoryCache.get(type);
        if (factory == null) {
            throw new RuntimeException("Unknown factory type: " + type);
        }
        return factory;
    }

    public BarcodeReader getReaderFactory(BarcodeType type) {
        BarcodeReader factory = readerFactoryCache.get(type);
        if (factory == null) {
            throw new RuntimeException("Unknown factory type: " + type);
        }
        return factory;
    }
}
