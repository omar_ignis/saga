/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.factory;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import com.inventory.item.enums.DocumentType;
import com.inventory.item.barcode.DocumentBarcodeWriter;
import com.inventory.item.barcode.DocumentBarcodeReader;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class DocumentBarcodeFactory {
    
    private final Map<DocumentType, DocumentBarcodeWriter> factoryWriterServiceCache = new HashMap<>();
    
    private final Map<DocumentType, DocumentBarcodeReader> factoryReaderServiceCache = new HashMap<>();

    @Autowired
    public DocumentBarcodeFactory(List<DocumentBarcodeWriter> writers, List<DocumentBarcodeReader> readers) {
        
        for(DocumentBarcodeWriter service : writers){
            factoryWriterServiceCache.put(service.getType(), service);
        }
        
        for(DocumentBarcodeReader service : readers){
            factoryReaderServiceCache.put(service.getType(), service);
        }
        
    }

    public DocumentBarcodeWriter getWriterFactory(DocumentType type){
        
        DocumentBarcodeWriter factory = factoryWriterServiceCache.get(type);
        if(factory == null) throw new RuntimeException("Unknown factory type: " + type);
        
        return factory;
    }
    
    public DocumentBarcodeReader getReaderFactory(DocumentType type){
        
        DocumentBarcodeReader factory = factoryReaderServiceCache.get(type);
        if(factory == null) throw new RuntimeException("Unknown factory type: " + type);
        
        return factory;
    }
}
