/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;

import com.inventory.item.model.RowSheet;
import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

/**
 *
 * @author ignis
 */
public interface DocumentBarcodeWriter {

    public DocumentType getType();

    public List<RowSheet> generateLabels(List<Integer> positionList, String id, BarcodeType barcodeType, SheetType sheetType) throws Exception;
}
