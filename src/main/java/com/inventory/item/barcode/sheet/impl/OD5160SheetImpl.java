/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.sheet.impl;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.RowOD5160;
import com.inventory.item.model.BarcodeLabel;

import com.inventory.item.enums.SheetType;
import com.inventory.item.barcode.sheet.Sheet;
import org.springframework.stereotype.Component;

/**
 *
 * @author ignis
 */
@Component
public class OD5160SheetImpl implements Sheet {

    @Override
    public SheetType getType() {
        return SheetType.OD5160;
    }

    @Override
    public List<RowSheet> createFullSheet(List<BarcodeLabel> list) {
        List<RowSheet> accumulator = new ArrayList<>();
        RowSheet rowSheet = null;
        for (BarcodeLabel barcodeLabel : list) {
            rowSheet = fill(rowSheet, barcodeLabel, accumulator);
        }

        if (rowSheet != null) {
            accumulator.add(rowSheet);
        }

        return accumulator;
    }

    @Override
    public RowSheet fill(RowSheet rowSheet, BarcodeLabel barcodeLabel, List<RowSheet> accumulator) {

        String barcode = barcodeLabel.getBarcode(), label = barcodeLabel.getLabel();
        int quantity = barcodeLabel.getQuantity();
        if (rowSheet != null) {
            quantity -= rowSheet.fillPartialRow(quantity > 1, barcode, label);
            if (rowSheet.isCompleted()) {
                accumulator.add(rowSheet);
                rowSheet = null;
            }
        }

        int pending = quantity % 3;
        int rows = (quantity - pending) / 3;
        fillRows(rows, barcode, label, accumulator);
        if (pending > 0) {
            rowSheet = fillPendingRow(pending, barcode, label);
        }

        return rowSheet;
    }

    @Override
    public RowSheet fillPendingRow(int pending, String barcode, String label) {

        RowSheet rowSheet = new RowOD5160();
        rowSheet.fillPartialRow(pending, barcode, label);

        return rowSheet;
    }

    @Override
    public void fillRows(int rows, String barcode, String label, List<RowSheet> accumulator) {
        RowSheet rowSheet;
        while (rows > 0) {
            rowSheet = new RowOD5160();
            rowSheet.fillRow(barcode, label);
            accumulator.add(rowSheet);
            rows--;
        }
    }

    @Override
    public List<RowSheet> createCustomSheet(List<BarcodeLabel> barcodeLabelList, List<Integer> positions) {

        RowSheet rowSheet;
        List<RowSheet> rowSheetList = new ArrayList<>();
        int rows = getTotalRows(positions), counter = 1;
        Map<Integer, List<Integer>> map = getRowPositionMap(positions);
        while (counter <= rows) {
            
            rowSheet = map.containsKey(counter)
                    ? fillCustomRow(barcodeLabelList, map.get(counter))
                    : new RowOD5160();

            rowSheetList.add(rowSheet);
            counter++;
        }

        return rowSheetList;
    }

    @Override
    public RowSheet fillCustomRow(List<BarcodeLabel> barcodeLabelList, List<Integer> positions) {

        BarcodeLabel barcodeLabel = barcodeLabelList.get(0);
        int quantity = barcodeLabel.getQuantity();
        RowSheet rowSheet = new RowOD5160();
        for (int position : positions) {

            if (quantity == 0) {

                barcodeLabelList.remove(0);
                barcodeLabel = barcodeLabelList.get(0);
                quantity = barcodeLabel.getQuantity();
            }

            rowSheet.setBarcode(position, barcodeLabel.getBarcode());
            rowSheet.setLabel(position, barcodeLabel.getLabel());
            quantity--;
        }

        barcodeLabel.setQuantity(quantity);
        barcodeLabelList.set(0, barcodeLabel);

        if (quantity == 0) {
            barcodeLabelList.remove(0);
        }

        return rowSheet;
    }

    @Override
    public Map<Integer, List<Integer>> getRowPositionMap(List<Integer> positions) {

        List<Integer> labelList;
        Map<Integer, List<Integer>> map = new HashMap<>();

        for (int position : positions) {
            int mod = position % 3;
            int row = (position / 3) + 1;
            labelList = map.containsKey(row) ? map.get(row) : new ArrayList<>();
            labelList.add(mod);
            map.put(row, labelList);
        }

        return map;
    }

    @Override
    public int getTotalRows(List<Integer> positions) {

        int lastIndex = positions.get(positions.size() - 1);
        int pages = lastIndex / 30;
        int remaining = lastIndex % 30 > 0 ? 1 : 0;
        int totalPages = pages + remaining;
        int rows = totalPages * 10;

        return rows;
    }

}
