/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.barcode.sheet.factory;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import com.inventory.item.enums.SheetType;
import com.inventory.item.barcode.sheet.Sheet;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Component
public class SheetFactory {

    private final Map<SheetType, Sheet> sheetFactoryCache = new HashMap<>();

    @Autowired
    public SheetFactory(List<Sheet> sheets) {

        for (Sheet service : sheets) {
            sheetFactoryCache.put(service.getType(), service);
        }
    }

    public Sheet getSheetImpl(SheetType type) {

        Sheet implementation = sheetFactoryCache.get(type);

        if (implementation == null) {
            throw new RuntimeException("Unknown factory type: " + type);
        }

        return implementation;
    }
}
