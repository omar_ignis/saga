/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.barcode.sheet;

import java.util.Map;
import java.util.List;
import com.inventory.item.enums.SheetType;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.BarcodeLabel;

/**
 *
 * @author ignis
 */
public interface Sheet {
    
    public SheetType getType();
    
    public int getTotalRows(List<Integer> positions);
    
    public List<RowSheet> createFullSheet(List<BarcodeLabel> list);
    
    public RowSheet fillPendingRow(int pending, String barcode, String label);
    
    public Map<Integer, List<Integer>> getRowPositionMap(List<Integer> positions);
    
    public void fillRows(int rows, String barcode, String label, List<RowSheet> accumulator);
    
    public List<RowSheet> createCustomSheet(List<BarcodeLabel> list, List<Integer> positions);
    
    public RowSheet fillCustomRow(List<BarcodeLabel> barcodeLabelList, List<Integer> positions);
    
    public RowSheet fill(RowSheet rowSheet, BarcodeLabel barcodeLabel, List<RowSheet> accumulator);
    
}
