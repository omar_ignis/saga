/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.barcode;

import java.util.List;
import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;

import com.inventory.item.model.RowSheet;
import com.inventory.item.model.BarcodeLabel;

import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

/**
 *
 * @author ignis
 */
public interface BarcodeWriter {

    public BarcodeType getType();

    public String buildBarcode(DocumentType type, String documentId, int lineNumber, long itemId);

    public List<RowSheet> generateFullSheetLabel(List<BarcodeLabel> barcodeLabelList, SheetType type);

    public <S extends Detail> List<BarcodeLabel> generateBarcodeLabel(Document document, List<S> details);

    public List<RowSheet> generatePartialSheetLabel(List<BarcodeLabel> barcodeLabelList, List<Integer> positionList, SheetType type);

}
