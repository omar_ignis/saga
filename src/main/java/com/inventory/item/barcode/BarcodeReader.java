/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.barcode;

import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

/**
 *
 * @author ignis
 */
public interface BarcodeReader {
    
    public BarcodeType getType();
    
    public long readItemId(String barcode);
    
    public int readLineNumber(String barcode);
    
    public String readDocumentId(String barcode);
    
    public DocumentType readDocumenType(String barcode);
}
