/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.barcode;

import com.inventory.item.entity.Detail;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

/**
 *
 * @author ignis
 */
public interface DocumentBarcodeReader {

    public DocumentType getType();

    public <S extends Detail> S readLabel(Long warehouseId, String barcode, BarcodeType type) throws Exception;
}
