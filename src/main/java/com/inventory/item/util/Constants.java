/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

/**
 * @author ignis
 */
public class Constants {

    /**
     * Tipos de documentos
     */
    public static final String INPUT = "RCP";
    public static final String OUTPUT = "OUT";
    public static final String RETURN_FOR_PURCHASE = "RFP";
    public static final String RETURN_FOR_SALE = "RFS";

    public static final String INVOICE = "INV";
    public static final String CREDIT_MEMO = "CRM";
    public static final String PAYMENT = "PMT";

    /**
     * Folios o numeros de referencia
     */
    public static final int DOCUMENT_ID_LENGTH = 10;
    public static final int DOCUMENT_TYPE_LENGTH = 2;
    public static final int LINE_NUMBER_LENGTH = 4;
    public static final int REF_NBR_LENGTH = DOCUMENT_TYPE_LENGTH + DOCUMENT_ID_LENGTH;
    
    public static final String INPUT_PREFIX = "IN";
    public static final String OUTPUT_PREFIX = "OU";
    public static final String RETURN_FOR_PURCHASE_PREFIX = "PR";
    public static final String RETURN_FOR_SELLING_PREFIX = "SR";
    public static final String INPUT_SEQUENCE_NAME = "input_doc_seq";
    public static final String OUTPUT_SEQUENCE_NAME = "output_doc_seq";
    public static final String RETURN_FOR_PURCHASE_SEQUENCE_NAME = "return_for_purchase_doc_seq";
    public static final String RETURN_FOR_SALE_SEQUENCE_NAME = "return_for_sale_doc_seq";
    public static final String NEW_REFNBR = "NUEVO";

    /**
     * Filters
     */
    public static final String WAREHOUSE_FILTER = "WarehouseFilter";
    public static final String IGNORE_ITEM_SUMMARY = "itemSummary";
}
