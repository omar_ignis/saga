/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.enums.DocumentType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * @author ignis
 */
public class Util {

    private final static String BARCODE_FORMAT = "%04d";

    public static boolean isNullOrEmpty(String value) {
        if (value == null) return true;
        return value.trim().isEmpty();
    }

    /**
     * Recibe como parametro el prefijo del consecutivo, el ultimo numero y
     * el tamaño de la cadena.
     * Devuelve el siguiente consecutivo en cadena de texto
     */
    public static String buildRefNbr(String prefix, Long sequenceNbr, int lengthOfRefNbr) {
        //String sequence = Long.toString(sequenceNbr);
        //obtiene la cantidad de 0 que debe tener a la izquierda
        int lengthOfSequence = lengthOfRefNbr - prefix.length();
        //Agrega 0 a la izquierda del consecutivo
        //%0 significa que va a agregar 0 la longitud de la sequencia indica
        //cuantos serán agregados y la d indica que seran enteros
        String sequence = String.format("%0" + lengthOfSequence + "d", sequenceNbr);
        return prefix + sequence;
    }

    public static BigDecimal divide(BigDecimal dividendo, BigDecimal divisor) {
        return dividendo.divide(divisor, 2, RoundingMode.HALF_UP);
    }

    public static BigDecimal multiply(BigDecimal numberOne, BigDecimal numberTwo) {
        return numberOne.multiply(numberTwo).setScale(2, RoundingMode.HALF_UP);
    }
    
    public static DocumentType getDocTypeFromBarcode(String barcode) {

        int docTypePosition = Character.getNumericValue(barcode.charAt(0));

        return DocumentType.values()[docTypePosition];
    }

    public static String buildBarcode(String docType, String refNbr, int lineNbr, Long itemId) {
        String lineNbrFormatted = String.format(BARCODE_FORMAT, lineNbr);
        String itemIdFormatted = String.format(BARCODE_FORMAT, itemId);
        String refNbrFormatted = refNbr.substring(2);

        return docType + refNbrFormatted + lineNbrFormatted + itemIdFormatted;
    }

    public static String buildLike(String param) {
        return MessageFormat.format("%{0}%", param);
    }
    
    public static <T extends Document> T copy(T document, T result) {

        result.setDate(document.getDate());
        result.setCounter(document.getCounter());
        result.setDescription(document.getDescription());
        result.setTotalAmount(document.getTotalAmount());
        result.setTotalQuantity(document.getTotalQuantity());
        result.setWarehouse(document.getWarehouse());
        return result;
    }

    public static <S extends Detail> S copy(S detail, S result) {
        
        result.setItem(detail.getItem());
        result.setQuantity(detail.getQuantity());
        result.setUnitPrice(detail.getUnitPrice());
        result.setTotalPrice(detail.getTotalPrice());
        result.setLineNumber(detail.getLineNumber());
        result.setDescription(detail.getDescription());
        return result;
    }
    
    public static <S extends Detail> int getMaxLineNumber(List<S> list){
        if(list == null || list.isEmpty()) return 0;
        S detail = list
                .stream()
                .max(Comparator.comparing(S::getLineNumber))
                .orElseThrow(NoSuchElementException::new);
        
        return detail.getLineNumber();
    }
    
    public static <S extends Detail> List<S> getDeletedDetails(List<S> details) {
        
        List<S> list = details
                .stream()
                .filter(detail -> detail.isDeleted() == true)
                .collect(Collectors.toList());

        return list;
    }
    
    public static <S extends Detail> List<S> getNotDeletedDetails(List<S> details) {
        
        List<S> list = details
                .stream()
                .filter(detail -> detail.isDeleted() == false)
                .collect(Collectors.toList());

        return list;
    }
    
}
