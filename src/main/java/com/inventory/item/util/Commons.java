/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

/**
 * @author ignis
 */
public class Commons {
    public static final String HQL_WHERE = " D WHERE ";
    public static final String HQL_AND = " AND ";
    public static final String NEXT_SEQUENCE_VALUE = "SELECT nextval(''{0}'')";
    public static final String CREATE_SEQUENCE = "CREATE SEQUENCE IF NOT EXISTS {0} START 1";
}
