/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.util;

/**
 * @author ignis
 */
public class Messages {
    public final static String ITEM_OUTPUT_FAILURE = "No es posible retirar {0} del articulo {1} ";
    public final static String ITEM_NOT_FOUND = "El articulo de la linea {0} no fue encontrado";
    public final static String ITEM_NOT_DELETED = "El articulo {0} ya fue usado en alguna transacción y no es posible eliminarlo";
    public final static String WAREHOUSE_NOT_DELETED = "El almacen {0} ya fue usado en alguna transacción y no es posible eliminarlo";
    public final static String DOCUMENT_NOT_FOUND = "El documento {0} de tipo {1} no fue encontrado en el sistema";
    public final static String IODOCUMENT_NOT_DELETED = "El documento {0} ya fue liberado y no es posible eliminarlo ";
    public final static String IODOCUMENT_NOT_UPDATED = "El documento {0} ya fue liberado y no es posible actualizarlo ";
    public final static String BARCODE_NOT_GENERATED = "No es posible generar codigos de barra para el documento {0}.";
    public final static String BARCODE_NOT_FOUND = "No fue posible encontrar el articulo para el codigo de barra {0}";
    public final static String BARCODE_NOT_AVAILABLE = "El codigo de barras {0} no cumple con el patron";
    public final static String NOT_ENOUGH_INVENTORY = "No hay suficiente inventario para el articulo {0} {1}";
    public final static String ITEM_SUMMARY_NOT_FOUND = "El articulo {0} {1} no se encuentra en el almacen {2}";
    public final static String WAREHOUSE_NOT_FOUND = "El almacen con id {0} no se encuentra en el sistema";
    public final static String DOCUMENT_NOT_VALID_STATUS = "El estado del documento no es valido para {0}";
    public final static String WAREHOUSE_NOT_VALID = "El articulo no puede ser retirado de este almacen, pertence al almacen {0}";
}
