/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.enums;

/**
 * @author ignis
 */
public enum Status {
    OPEN, RELEASED, HOLD
}
