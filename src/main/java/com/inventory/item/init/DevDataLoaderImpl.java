/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.init;

import java.util.List;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.InputDocument;
import com.inventory.item.entity.OutputDocument;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Operation;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;

import com.inventory.item.security.setup.PermissionSetup;
import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.ResourceRepository;
import com.inventory.item.security.repository.OperationRepository;
import com.inventory.item.security.repository.PermissionRepository;
import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;

import com.inventory.item.enums.ValuationType;
import com.inventory.item.repository.ItemRepository;
import com.inventory.item.repository.WarehouseRepository;

import com.inventory.item.service.impl.InputDocumentServiceImpl;
import com.inventory.item.service.impl.OutputDocumentServiceImpl;
import com.inventory.item.service.impl.SalesReturnDocumentServiceImpl;
import com.inventory.item.service.impl.PurchaseReturnDocumentServiceImpl;

import org.springframework.stereotype.Component;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author ignis
 */
@Component
@Profile("dev")
public class DevDataLoaderImpl implements CommandLineRunner {
    
    private final ItemRepository itemRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final ResourceRepository resourceRepository;
    private final WarehouseRepository warehouseRepository;
    private final OperationRepository operationRepository;
    private final PermissionRepository permissionRepository;
    private final UserAccountRepository userAccountRepository;
    private final OrganizationRepository organizationRepository;
    
    private final InputDocumentServiceImpl inputDocumentServiceImpl;
    private final OutputDocumentServiceImpl outputDocumentServiceImpl;
    private final SalesReturnDocumentServiceImpl salesReturnDocumentServiceImpl;
    private final PurchaseReturnDocumentServiceImpl purchaseReturnDocumentServiceImpl;
    
    private final PermissionSetup permissionSetup;
    
    private Role roleOne;
    private Role roleTwo;
    private Resource resourceOne;
    private Resource resourceTwo;
    private Resource resourceThree;
    private Operation operationOne;
    private Operation operationTwo;
    private Operation operationThree;
    private Permission permissionOne;
    private Permission permissionTwo;
    private Permission permissionThree;
    private UserAccount userAccountOne;
    private UserAccount userAccountTwo;
    private Organization organizationOne;
    
    private Item itemOne;
    private Item itemTwo;
    private Item itemThree;
    private Item itemFour;
    private Item itemFive;
    private Item itemSix;
    private Item itemSeven;
    private Item itemEight;
    private Item itemNine;
    private Item itemTen;
    private Item itemEleven;
    private Item itemTwelve;
    private Item itemThirtteen;
    private Item itemFourteen;
    private Item itemFifteen;
    private Item itemSixteen;
    private Item itemSeventeen;
    private Item itemEightteen;
    private Item itemNineteen;
    private Item itemTwenty;
    
    private Warehouse warehouseOne;
    private Warehouse warehouseTwo;
    private Warehouse warehouseThree;
    private Warehouse warehouseFour;
    
    private LocalDateTime now;
    private InputDocument inputDocumentOne;
    private InputDocument inputDocumentTwo;
    private InputDocument inputDocumentThree;
    
    private InputDetail inputDetailOne;
    private InputDetail inputDetailTwo;
    private InputDetail inputDetailThree;
    private InputDetail inputDetailFour;
    private InputDetail inputDetailFive;
    private InputDetail inputDetailSix;
    private InputDetail inputDetailSeven;
    private InputDetail inputDetailEight;
    private InputDetail inputDetailNine;
    
    private SalesReturnDocument salesReturnDocumentOne;
    private SalesReturnDocument salesReturnDocumentTwo;
    private SalesReturnDocument salesReturnDocumentThree;
    
    private SalesReturnDetail salesReturnDetailOne;
    private SalesReturnDetail salesReturnDetailTwo;
    private SalesReturnDetail salesReturnDetailThree;
    private SalesReturnDetail salesReturnDetailFour;
    private SalesReturnDetail salesReturnDetailFive;
    private SalesReturnDetail salesReturnDetailSix;
    private SalesReturnDetail salesReturnDetailSeven;
    
    private OutputDocument outputDocumentOne;
    private OutputDocument outputDocumentTwo;
    private OutputDocument outputDocumentThree;
    
    private OutputDetail outputDetailOne;
    private OutputDetail outputDetailTwo;
    private OutputDetail outputDetailThree;
    private OutputDetail outputDetailFour;
    private OutputDetail outputDetailFive;
    private OutputDetail outputDetailSix;
    private OutputDetail outputDetailSeven;
    private OutputDetail outputDetailEight;
    private OutputDetail outputDetailNine;
    private OutputDetail outputDetailTen;
    private OutputDetail outputDetailEleven;
    
    private PurchaseReturnDocument purchaseReturnDocumentOne;
    private PurchaseReturnDocument purchaseReturnDocumentTwo;
    private PurchaseReturnDocument purchaseReturnDocumentThree;
    
    private PurchaseReturnDetail purchaseReturnDetailOne;
    private PurchaseReturnDetail purchaseReturnDetailTwo;
    private PurchaseReturnDetail purchaseReturnDetailThree;
    private PurchaseReturnDetail purchaseReturnDetailFour;
    private PurchaseReturnDetail purchaseReturnDetailFive;
    private PurchaseReturnDetail purchaseReturnDetailSix;
    private PurchaseReturnDetail purchaseReturnDetailSeven;

    @Autowired
    public DevDataLoaderImpl(
            RoleRepository roleRepository,
            ItemRepository itemRepository, 
            PermissionSetup permissionSetup,
            PasswordEncoder passwordEncoder,
            ResourceRepository resourceRepository,
            WarehouseRepository warehouseRepository,
            OperationRepository operationRepository, 
            PermissionRepository permissionRepository,
            UserAccountRepository userAccountRepository, 
            OrganizationRepository organizationRepository, 
            InputDocumentServiceImpl inputDocumentServiceImpl, 
            OutputDocumentServiceImpl outputDocumentServiceImpl, 
            SalesReturnDocumentServiceImpl salesReturnDocumentServiceImpl, 
            PurchaseReturnDocumentServiceImpl purchaseReturnDocumentServiceImpl) {

        now = LocalDateTime.now();

        this.itemRepository = itemRepository;
        this.roleRepository = roleRepository;
        this.permissionSetup = permissionSetup;
        this.passwordEncoder = passwordEncoder;
        this.resourceRepository = resourceRepository;
        this.warehouseRepository = warehouseRepository;
        this.operationRepository = operationRepository;
        this.permissionRepository = permissionRepository;
        this.userAccountRepository = userAccountRepository;
        this.organizationRepository = organizationRepository;
        this.inputDocumentServiceImpl = inputDocumentServiceImpl;
        this.outputDocumentServiceImpl = outputDocumentServiceImpl;
        this.salesReturnDocumentServiceImpl = salesReturnDocumentServiceImpl;
        this.purchaseReturnDocumentServiceImpl = purchaseReturnDocumentServiceImpl;
    }

    @Override
    public void run(String... args) throws Exception {
        
        organizationOne = new Organization("InventoryStudio","Organization by Default");
        organizationOne = organizationRepository.save(organizationOne);
        
        List<Resource> resources = permissionSetup.createResources();
        
        resources = resourceRepository.saveAll(resources);
        
//        resourceOne = new Resource(Item.class.getSimpleName());
//        resourceTwo = new Resource(Warehouse.class.getSimpleName());
//        resourceThree = new Resource(InputDocument.class.getSimpleName());
//        
//        resourceOne = resourceRepository.save(resourceOne);
//        resourceTwo = resourceRepository.save(resourceTwo);
//        resourceThree = resourceRepository.save(resourceThree);
        
        operationOne = new Operation("CREATE");
        operationTwo = new Operation("READ");
        operationThree = new Operation("UPDATE");
        
        operationOne = operationRepository.save(operationOne);
        operationTwo = operationRepository.save(operationTwo);
        operationThree = operationRepository.save(operationThree);
        
//        permissionOne = new Permission(operationOne, resourceOne);
//        permissionTwo = new Permission(operationOne, resourceTwo);
//        permissionThree = new Permission(operationOne, resourceThree);
        
        List<Permission> permissions = permissionSetup.createPermissions(organizationOne, resources);
        permissions = permissionRepository.saveAll(permissions);
//        
//        permissionOne = permissionRepository.save(permissionOne);
//        permissionTwo = permissionRepository.save(permissionTwo);
//        permissionThree = permissionRepository.save(permissionThree);
//        
        roleOne = new Role("ROLE_ADMIN",organizationOne);
        roleOne.addPermissions(permissions);
        roleOne = roleRepository.save(roleOne);
        
//        roleTwo = new Role("ROLE_STAFF",organizationOne);
//        roleTwo.addPermission(permissionTwo);
//        roleTwo.addPermission(permissionThree);
//        roleTwo = roleRepository.save(roleTwo);
//        
        userAccountOne = new UserAccount("Omar Enrique", "Alvarez", "Zarate",
                "ignisMX","omar@alvarez.pro",passwordEncoder.encode("password123"), true);
        userAccountOne.setOrganization(organizationOne);
        userAccountOne.addRole(roleOne);
        userAccountOne = userAccountRepository.save(userAccountOne);
//        
//        userAccountTwo = new UserAccount("Gael", "Alvarez", "Esponjoso","sponjosoMX",
//                "gael@alvarez.pro",passwordEncoder.encode("password456"), true);
//        userAccountTwo.addRole(roleTwo);
//        userAccountTwo = userAccountRepository.save(userAccountTwo);
//        
        // <editor-fold defaultstate="collapsed" desc="Items">
        
        itemOne = new Item();
        itemOne.setItemName("Item Eins");
        itemOne.setDescription("Item Eins Desc");
        itemOne.setValuationType(ValuationType.AVERAGE);
        itemOne = itemRepository.save(itemOne);
        
        itemTwo = new Item();
        itemTwo.setItemName("Item Zwei");
        itemTwo.setDescription("Item Zwei Desc");
        itemTwo.setValuationType(ValuationType.AVERAGE);
        itemTwo.setUsed(true);
        itemTwo = itemRepository.save(itemTwo);
        
        itemThree = new Item();
        itemThree.setItemName("Item Drei");
        itemThree.setDescription("Item Drei Desc");
        itemThree.setValuationType(ValuationType.AVERAGE);
        itemThree = itemRepository.save(itemThree);
        
        itemFour = new Item();
        itemFour.setItemName("Item Vier");
        itemFour.setDescription("Item Vier Desc");
        itemFour.setValuationType(ValuationType.AVERAGE);
        itemFour = itemRepository.save(itemFour);
        
        itemFive = new Item();
        itemFive.setItemName("Item Funf");
        itemFive.setDescription("Item Funf Desc");
        itemFive.setValuationType(ValuationType.AVERAGE);
        itemFive = itemRepository.save(itemFive);
        
        itemSix = new Item();
        itemSix.setItemName("Item Sechs");
        itemSix.setDescription("Item Sechs Desc");
        itemSix.setValuationType(ValuationType.AVERAGE);
        itemSix = itemRepository.save(itemSix);
        
        itemSeven = new Item();
        itemSeven.setItemName("Item Sieben");
        itemSeven.setDescription("Item Sieben Desc");
        itemSeven.setValuationType(ValuationType.AVERAGE);
        itemSeven = itemRepository.save(itemSeven);
        
        itemEight = new Item();
        itemEight.setItemName("Item Acht");
        itemEight.setDescription("Item Acht Desc");
        itemEight.setValuationType(ValuationType.AVERAGE);
        itemEight = itemRepository.save(itemEight);
        
        itemNine = new Item();
        itemNine.setItemName("Item Neun");
        itemNine.setDescription("Item Neun Desc");
        itemNine.setValuationType(ValuationType.AVERAGE);
        itemNine = itemRepository.save(itemNine);
        
        itemTen = new Item();
        itemTen.setItemName("Item Zehn");
        itemTen.setDescription("Item Zehn Desc");
        itemTen.setValuationType(ValuationType.AVERAGE);
        itemTen = itemRepository.save(itemTen);
        
        itemEleven = new Item();
        itemEleven.setItemName("Item Elf");
        itemEleven.setDescription("Item Elf Desc");
        itemEleven.setValuationType(ValuationType.AVERAGE);
        itemEleven = itemRepository.save(itemEleven);
        
        itemTwelve = new Item();
        itemTwelve.setItemName("Item Zwolf");
        itemTwelve.setDescription("Item Twelve Desc");
        itemTwelve.setValuationType(ValuationType.AVERAGE);
        itemTwelve = itemRepository.save(itemTwelve);
        
        itemThirtteen = new Item();
        itemThirtteen.setItemName("Item Dreizehn");
        itemThirtteen.setDescription("Item Dreihen Desc");
        itemThirtteen.setValuationType(ValuationType.AVERAGE);
        itemThirtteen = itemRepository.save(itemThirtteen);
        
        itemFourteen = new Item();
        itemFourteen.setItemName("Item Vierzehn");
        itemFourteen.setDescription("Item Vierzehn Desc");
        itemFourteen.setValuationType(ValuationType.AVERAGE);
        itemFourteen = itemRepository.save(itemFourteen);
        
        itemFifteen = new Item();
        itemFifteen.setItemName("Item Funfzehn");
        itemFifteen.setDescription("Item Funfzehn Desc");
        itemFifteen.setValuationType(ValuationType.AVERAGE);
        itemFifteen = itemRepository.save(itemFifteen);
        
        itemSixteen = new Item();
        itemSixteen.setItemName("Item Sechzehn");
        itemSixteen.setDescription("Item Sechzehn Desc");
        itemSixteen.setValuationType(ValuationType.AVERAGE);
        itemSixteen = itemRepository.save(itemSixteen);
        
        itemSeventeen = new Item();
        itemSeventeen.setItemName("Item Siebenzehn");
        itemSeventeen.setDescription("Item Siebenzehn Desc");
        itemSeventeen.setValuationType(ValuationType.AVERAGE);
        itemSeventeen = itemRepository.save(itemSeventeen);
        
        itemEightteen = new Item();
        itemEightteen.setItemName("Item Achtzehn");
        itemEightteen.setDescription("Item Achtzehn Desc");
        itemEightteen.setValuationType(ValuationType.AVERAGE);
        itemEightteen = itemRepository.save(itemEightteen);
        
        itemNineteen = new Item();
        itemNineteen.setItemName("Item Neunzehn");
        itemNineteen.setDescription("Item Neunzehn Desc");
        itemNineteen.setValuationType(ValuationType.AVERAGE);
        itemNineteen = itemRepository.save(itemNineteen);
        
        itemTwenty = new Item();
        itemTwenty.setItemName("Item Zwanzig");
        itemTwenty.setDescription("Item Zwanzig Desc");
        itemTwenty.setValuationType(ValuationType.AVERAGE);
        itemTwenty = itemRepository.save(itemTwenty);
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Warehouse">
        
        warehouseOne = new Warehouse("Warehouse One");
        warehouseRepository.save(warehouseOne);
        warehouseTwo = new Warehouse("Warehouse Two");
        warehouseRepository.save(warehouseTwo);
        warehouseThree = new Warehouse("Warehouse Three");
        warehouseThree.setUsed(true);
        warehouseRepository.save(warehouseThree);
        warehouseFour = new Warehouse("Warehouse Four");
        warehouseRepository.save(warehouseFour);
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Input Documents">
        
        inputDocumentOne = new InputDocument();
        inputDocumentOne.setDate(now);
        inputDocumentOne.setCounter(3);
        inputDocumentOne.setDescription("input document one");
        inputDocumentOne.setWarehouse(warehouseOne);
        inputDocumentOne.setTotalAmount(getAsBigDecimal(1300));
        inputDocumentOne.setTotalQuantity(getAsBigDecimal(130));

        inputDetailOne = new InputDetail();
        inputDetailOne.setDescription("detail one");
        inputDetailOne.setItem(itemOne);
        inputDetailOne.setLineNumber(1);
        inputDetailOne.setQuantity(getAsBigDecimal(10));
        inputDetailOne.setUnitPrice(getAsBigDecimal(20));
        inputDetailOne.setTotalPrice(getAsBigDecimal(200));

        inputDetailTwo = new InputDetail();
        inputDetailTwo.setDescription("detail two");
        inputDetailTwo.setItem(itemTwo);
        inputDetailTwo.setLineNumber(2);
        inputDetailTwo.setQuantity(getAsBigDecimal(20));
        inputDetailTwo.setUnitPrice(getAsBigDecimal(5));
        inputDetailTwo.setTotalPrice(getAsBigDecimal(100));

        inputDetailThree = new InputDetail();
        inputDetailThree.setDescription("detail three");
        inputDetailThree.setItem(itemThree);
        inputDetailThree.setLineNumber(3);
        inputDetailThree.setQuantity(getAsBigDecimal(100));
        inputDetailThree.setUnitPrice(getAsBigDecimal(10));
        inputDetailThree.setTotalPrice(getAsBigDecimal(1000));

        List<InputDetail> detailsOne = List.of(inputDetailOne, inputDetailTwo, inputDetailThree);

        inputDocumentServiceImpl.saveOrUpdate(inputDocumentOne, detailsOne);
        
        inputDocumentTwo = new InputDocument();
        inputDocumentTwo.setDate(now);
        inputDocumentTwo.setCounter(4);
        inputDocumentTwo.setDescription("input document two");
        inputDocumentTwo.setWarehouse(warehouseOne);
        inputDocumentTwo.setTotalAmount(getAsBigDecimal(6500));
        inputDocumentTwo.setTotalQuantity(getAsBigDecimal(220));

        inputDetailFour = new InputDetail();
        inputDetailFour.setDescription("detail four");
        inputDetailFour.setItem(itemFour);
        inputDetailFour.setLineNumber(1);
        inputDetailFour.setQuantity(getAsBigDecimal(20));
        inputDetailFour.setUnitPrice(getAsBigDecimal(20));
        inputDetailFour.setTotalPrice(getAsBigDecimal(400));

        inputDetailFive = new InputDetail();
        inputDetailFive = new InputDetail();
        inputDetailFive.setDescription("detail five");
        inputDetailFive.setItem(itemFive);
        inputDetailFive.setLineNumber(2);
        inputDetailFive.setQuantity(getAsBigDecimal(20));
        inputDetailFive.setUnitPrice(getAsBigDecimal(15));
        inputDetailFive.setTotalPrice(getAsBigDecimal(300));

        inputDetailSix = new InputDetail();
        inputDetailSix.setDescription("detail six");
        inputDetailSix.setItem(itemSix);
        inputDetailSix.setLineNumber(3);
        inputDetailSix.setQuantity(getAsBigDecimal(100));
        inputDetailSix.setUnitPrice(getAsBigDecimal(50));
        inputDetailSix.setTotalPrice(getAsBigDecimal(5000));
        
        inputDetailSeven = new InputDetail();
        inputDetailSeven.setDescription("detail seven");
        inputDetailSeven.setItem(itemSeven);
        inputDetailSeven.setLineNumber(4);
        inputDetailSeven.setQuantity(getAsBigDecimal(80));
        inputDetailSeven.setUnitPrice(getAsBigDecimal(10));
        inputDetailSeven.setTotalPrice(getAsBigDecimal(800));

        detailsOne = List.of(inputDetailFour, inputDetailFive, inputDetailSix, inputDetailSeven);

        inputDocumentServiceImpl.saveOrUpdate(inputDocumentTwo, detailsOne);
        
        inputDocumentThree = new InputDocument();
        inputDocumentThree.setDate(now);
        inputDocumentThree.setCounter(2);
        inputDocumentThree.setDescription("input document three");
        inputDocumentThree.setWarehouse(warehouseTwo);
        inputDocumentThree.setTotalAmount(getAsBigDecimal(9600));
        inputDocumentThree.setTotalQuantity(getAsBigDecimal(90));
        
        inputDetailEight = new InputDetail();
        inputDetailEight.setDescription("detail ten");
        inputDetailEight.setItem(itemTen);
        inputDetailEight.setLineNumber(1);
        inputDetailEight.setQuantity(getAsBigDecimal(30));
        inputDetailEight.setUnitPrice(getAsBigDecimal(80));
        inputDetailEight.setTotalPrice(getAsBigDecimal(2400));
        
        inputDetailNine = new InputDetail();
        inputDetailNine.setDescription("detail eleven");
        inputDetailNine.setItem(itemEleven);
        inputDetailNine.setLineNumber(2);
        inputDetailNine.setQuantity(getAsBigDecimal(60));
        inputDetailNine.setUnitPrice(getAsBigDecimal(120));
        inputDetailNine.setTotalPrice(getAsBigDecimal(7200));
        
        detailsOne = List.of(inputDetailEight, inputDetailNine);

        inputDocumentServiceImpl.saveOrUpdate(inputDocumentThree, detailsOne);
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Sales Return Documents">
        
        salesReturnDocumentOne = new SalesReturnDocument();
        salesReturnDocumentOne.setDate(now);
        salesReturnDocumentOne.setCounter(3);
        salesReturnDocumentOne.setDescription("sales return document one");
        salesReturnDocumentOne.setWarehouse(warehouseTwo);
        salesReturnDocumentOne.setTotalAmount(getAsBigDecimal(2035));
        salesReturnDocumentOne.setTotalQuantity(getAsBigDecimal(72));
        
        salesReturnDetailOne = new SalesReturnDetail();
        salesReturnDetailOne.setDescription("detail one");
        salesReturnDetailOne.setItem(itemTen);
        salesReturnDetailOne.setLineNumber(1);
        salesReturnDetailOne.setQuantity(getAsBigDecimal(18));
        salesReturnDetailOne.setUnitPrice(getAsBigDecimal(56));
        salesReturnDetailOne.setTotalPrice(getAsBigDecimal(1008));
        
        salesReturnDetailTwo = new SalesReturnDetail();
        salesReturnDetailTwo.setDescription("detail two");
        salesReturnDetailTwo.setItem(itemEleven);
        salesReturnDetailTwo.setLineNumber(2);
        salesReturnDetailTwo.setQuantity(getAsBigDecimal(23));
        salesReturnDetailTwo.setUnitPrice(getAsBigDecimal(15));
        salesReturnDetailTwo.setTotalPrice(getAsBigDecimal(345));
        
        salesReturnDetailThree = new SalesReturnDetail();
        salesReturnDetailThree.setDescription("detail three");
        salesReturnDetailThree.setItem(itemTwelve);
        salesReturnDetailThree.setLineNumber(3);
        salesReturnDetailThree.setQuantity(getAsBigDecimal(31));
        salesReturnDetailThree.setUnitPrice(getAsBigDecimal(22));
        salesReturnDetailThree.setTotalPrice(getAsBigDecimal(682));
        
        List<SalesReturnDetail> detailsTwo = List
                .of(salesReturnDetailOne, salesReturnDetailTwo, salesReturnDetailThree);

        salesReturnDocumentServiceImpl.saveOrUpdate(salesReturnDocumentOne, detailsTwo);
        
        salesReturnDocumentTwo = new SalesReturnDocument();
        salesReturnDocumentTwo.setDate(now);
        salesReturnDocumentTwo.setCounter(3);
        salesReturnDocumentTwo.setDescription("sales return document two");
        salesReturnDocumentTwo.setWarehouse(warehouseTwo);
        salesReturnDocumentTwo.setTotalAmount(getAsBigDecimal(1093));
        salesReturnDocumentTwo.setTotalQuantity(getAsBigDecimal(45));
        
        salesReturnDetailFour = new SalesReturnDetail();
        salesReturnDetailFour.setDescription("detail one");
        salesReturnDetailFour.setItem(itemThirtteen);
        salesReturnDetailFour.setLineNumber(1);
        salesReturnDetailFour.setQuantity(getAsBigDecimal(25));
        salesReturnDetailFour.setUnitPrice(getAsBigDecimal(13));
        salesReturnDetailFour.setTotalPrice(getAsBigDecimal(325));
        
        salesReturnDetailFive = new SalesReturnDetail();
        salesReturnDetailFive.setDescription("detail two");
        salesReturnDetailFive.setItem(itemFourteen);
        salesReturnDetailFive.setLineNumber(2);
        salesReturnDetailFive.setQuantity(getAsBigDecimal(9));
        salesReturnDetailFive.setUnitPrice(getAsBigDecimal(45));
        salesReturnDetailFive.setTotalPrice(getAsBigDecimal(405));
        
        salesReturnDetailSix = new SalesReturnDetail();
        salesReturnDetailSix.setDescription("detail three");
        salesReturnDetailSix.setItem(itemFifteen);
        salesReturnDetailSix.setLineNumber(3);
        salesReturnDetailSix.setQuantity(getAsBigDecimal(11));
        salesReturnDetailSix.setUnitPrice(getAsBigDecimal(33));
        salesReturnDetailSix.setTotalPrice(getAsBigDecimal(363));
        
        detailsTwo = List
                .of(salesReturnDetailFour, salesReturnDetailFive, salesReturnDetailSix);

        salesReturnDocumentServiceImpl.saveOrUpdate(salesReturnDocumentTwo, detailsTwo);
        
        salesReturnDocumentThree = new SalesReturnDocument();
        salesReturnDocumentThree.setDate(now);
        salesReturnDocumentThree.setCounter(1);
        salesReturnDocumentThree.setDescription("sales return document three");
        salesReturnDocumentThree.setWarehouse(warehouseTwo);
        salesReturnDocumentThree.setTotalAmount(getAsBigDecimal(176));
        salesReturnDocumentThree.setTotalQuantity(getAsBigDecimal(22));
        
        salesReturnDetailSeven = new SalesReturnDetail();
        salesReturnDetailSeven.setDescription("detail one");
        salesReturnDetailSeven.setItem(itemSixteen);
        salesReturnDetailSeven.setLineNumber(1);
        salesReturnDetailSeven.setQuantity(getAsBigDecimal(22));
        salesReturnDetailSeven.setUnitPrice(getAsBigDecimal(8));
        salesReturnDetailSeven.setTotalPrice(getAsBigDecimal(176));
        
        detailsTwo = List
                .of(salesReturnDetailSeven);

        salesReturnDocumentServiceImpl.saveOrUpdate(salesReturnDocumentThree, detailsTwo);
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Output Documents">
        
        outputDocumentOne = new OutputDocument();
        outputDocumentOne.setDate(now);
        outputDocumentOne.setCounter(4);
        outputDocumentOne.setDescription("output document one");
        outputDocumentOne.setWarehouse(warehouseOne);
        outputDocumentOne.setTotalAmount(getAsBigDecimal(2090));
        outputDocumentOne.setTotalQuantity(getAsBigDecimal(75));
        
        outputDetailOne = new OutputDetail();
        outputDetailOne.setDescription("detail one");
        outputDetailOne.setItem(itemSix);
        outputDetailOne.setLineNumber(1);
        outputDetailOne.setQuantity(getAsBigDecimal(4));
        outputDetailOne.setUnitPrice(getAsBigDecimal(8));
        outputDetailOne.setTotalPrice(getAsBigDecimal(32));
        
        outputDetailTwo = new OutputDetail();
        outputDetailTwo.setDescription("detail two");
        outputDetailTwo.setItem(itemSeven);
        outputDetailTwo.setLineNumber(2);
        outputDetailTwo.setQuantity(getAsBigDecimal(12));
        outputDetailTwo.setUnitPrice(getAsBigDecimal(9));
        outputDetailTwo.setTotalPrice(getAsBigDecimal(108));
        
        outputDetailThree = new OutputDetail();
        outputDetailThree.setDescription("detail three");
        outputDetailThree.setItem(itemEight);
        outputDetailThree.setLineNumber(3);
        outputDetailThree.setQuantity(getAsBigDecimal(15));
        outputDetailThree.setUnitPrice(getAsBigDecimal(42));
        outputDetailThree.setTotalPrice(getAsBigDecimal(630));
        
        outputDetailFour = new OutputDetail();
        outputDetailFour.setDescription("detail four");
        outputDetailFour.setItem(itemNine);
        outputDetailFour.setLineNumber(4);
        outputDetailFour.setQuantity(getAsBigDecimal(44));
        outputDetailFour.setUnitPrice(getAsBigDecimal(30));
        outputDetailFour.setTotalPrice(getAsBigDecimal(1320));
        
        List<OutputDetail> detailsThree = List
                .of(outputDetailOne, outputDetailTwo, outputDetailThree, outputDetailFour);

        outputDocumentServiceImpl.saveOrUpdate(outputDocumentOne, detailsThree);
        
        outputDocumentTwo = new OutputDocument();
        outputDocumentTwo.setDate(now);
        outputDocumentTwo.setCounter(5);
        outputDocumentTwo.setDescription("ouput document two");
        outputDocumentTwo.setWarehouse(warehouseTwo);
        outputDocumentTwo.setTotalAmount(getAsBigDecimal(3089));
        outputDocumentTwo.setTotalQuantity(getAsBigDecimal(86));
        
        outputDetailFive = new OutputDetail();
        outputDetailFive.setDescription("detail one");
        outputDetailFive.setItem(itemThirtteen);
        outputDetailFive.setLineNumber(1);
        outputDetailFive.setQuantity(getAsBigDecimal(12));
        outputDetailFive.setUnitPrice(getAsBigDecimal(25));
        outputDetailFive.setTotalPrice(getAsBigDecimal(300));
        
        outputDetailSix = new OutputDetail();
        outputDetailSix.setDescription("detail two");
        outputDetailSix.setItem(itemFourteen);
        outputDetailSix.setLineNumber(2);
        outputDetailSix.setQuantity(getAsBigDecimal(14));
        outputDetailSix.setUnitPrice(getAsBigDecimal(14));
        outputDetailSix.setTotalPrice(getAsBigDecimal(196));
        
        outputDetailSeven = new OutputDetail();
        outputDetailSeven.setDescription("detail three");
        outputDetailSeven.setItem(itemFifteen);
        outputDetailSeven.setLineNumber(3);
        outputDetailSeven.setQuantity(getAsBigDecimal(13));
        outputDetailSeven.setUnitPrice(getAsBigDecimal(31));
        outputDetailSeven.setTotalPrice(getAsBigDecimal(403));
        
        outputDetailEight = new OutputDetail();
        outputDetailEight.setDescription("detail four");
        outputDetailEight.setItem(itemSixteen);
        outputDetailEight.setLineNumber(4);
        outputDetailEight.setQuantity(getAsBigDecimal(15));
        outputDetailEight.setUnitPrice(getAsBigDecimal(59));
        outputDetailEight.setTotalPrice(getAsBigDecimal(750));
        
        outputDetailNine = new OutputDetail();
        outputDetailNine.setDescription("detail five");
        outputDetailNine.setItem(itemSeventeen);
        outputDetailNine.setLineNumber(5);
        outputDetailNine.setQuantity(getAsBigDecimal(32));
        outputDetailNine.setUnitPrice(getAsBigDecimal(45));
        outputDetailNine.setTotalPrice(getAsBigDecimal(1440));
        
        detailsThree = List
                .of(outputDetailFive, outputDetailSix, outputDetailSeven, outputDetailEight, outputDetailNine);

        outputDocumentServiceImpl.saveOrUpdate(outputDocumentTwo, detailsThree);
        
        outputDocumentThree = new OutputDocument();
        outputDocumentThree.setDate(now);
        outputDocumentThree.setCounter(2);
        outputDocumentThree.setDescription("output document three");
        outputDocumentThree.setWarehouse(warehouseFour);
        outputDocumentThree.setTotalAmount(getAsBigDecimal(711));
        outputDocumentThree.setTotalQuantity(getAsBigDecimal(27));
        
        outputDetailTen = new OutputDetail();
        outputDetailTen.setDescription("detail one");
        outputDetailTen.setItem(itemNineteen);
        outputDetailTen.setLineNumber(1);
        outputDetailTen.setQuantity(getAsBigDecimal(18));
        outputDetailTen.setUnitPrice(getAsBigDecimal(17));
        outputDetailTen.setTotalPrice(getAsBigDecimal(306));
        
        outputDetailEleven = new OutputDetail();
        outputDetailEleven.setDescription("detail two");
        outputDetailEleven.setItem(itemTwenty);
        outputDetailEleven.setLineNumber(2);
        outputDetailEleven.setQuantity(getAsBigDecimal(9));
        outputDetailEleven.setUnitPrice(getAsBigDecimal(45));
        outputDetailEleven.setTotalPrice(getAsBigDecimal(405));
        
        detailsThree = List
                .of(outputDetailTen, outputDetailEleven);

        outputDocumentServiceImpl.saveOrUpdate(outputDocumentThree, detailsThree);
        
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="Purchase Return Documents">
        
        purchaseReturnDocumentOne = new PurchaseReturnDocument();
        purchaseReturnDocumentOne.setDate(now);
        purchaseReturnDocumentOne.setCounter(4);
        purchaseReturnDocumentOne.setDescription("purchase return document one");
        purchaseReturnDocumentOne.setWarehouse(warehouseOne);
        purchaseReturnDocumentOne.setTotalAmount(getAsBigDecimal(770));
        purchaseReturnDocumentOne.setTotalQuantity(getAsBigDecimal(31));
        
        purchaseReturnDetailOne = new PurchaseReturnDetail();
        purchaseReturnDetailOne.setDescription("detail one");
        purchaseReturnDetailOne.setItem(itemOne);
        purchaseReturnDetailOne.setLineNumber(1);
        purchaseReturnDetailOne.setQuantity(getAsBigDecimal(4));
        purchaseReturnDetailOne.setUnitPrice(getAsBigDecimal(8));
        purchaseReturnDetailOne.setTotalPrice(getAsBigDecimal(32));
        
        purchaseReturnDetailTwo = new PurchaseReturnDetail();
        purchaseReturnDetailTwo.setDescription("detail two");
        purchaseReturnDetailTwo.setItem(itemThree);
        purchaseReturnDetailTwo.setLineNumber(2);
        purchaseReturnDetailTwo.setQuantity(getAsBigDecimal(12));
        purchaseReturnDetailTwo.setUnitPrice(getAsBigDecimal(9));
        purchaseReturnDetailTwo.setTotalPrice(getAsBigDecimal(108));
        
        purchaseReturnDetailThree = new PurchaseReturnDetail();
        purchaseReturnDetailThree.setDescription("detail three");
        purchaseReturnDetailThree.setItem(itemFour);
        purchaseReturnDetailThree.setLineNumber(3);
        purchaseReturnDetailThree.setQuantity(getAsBigDecimal(15));
        purchaseReturnDetailThree.setUnitPrice(getAsBigDecimal(42));
        purchaseReturnDetailThree.setTotalPrice(getAsBigDecimal(630));
        
        List<PurchaseReturnDetail> detailsFour = List
                .of(purchaseReturnDetailOne, purchaseReturnDetailTwo, purchaseReturnDetailThree);

        purchaseReturnDocumentServiceImpl.saveOrUpdate(purchaseReturnDocumentOne, detailsFour);
        
        purchaseReturnDocumentTwo = new PurchaseReturnDocument();
        purchaseReturnDocumentTwo.setDate(now);
        purchaseReturnDocumentTwo.setCounter(5);
        purchaseReturnDocumentTwo.setDescription("purchase return document two");
        purchaseReturnDocumentTwo.setWarehouse(warehouseTwo);
        purchaseReturnDocumentTwo.setTotalAmount(getAsBigDecimal(496));
        purchaseReturnDocumentTwo.setTotalQuantity(getAsBigDecimal(26));
        
        purchaseReturnDetailFour = new PurchaseReturnDetail();
        purchaseReturnDetailFour.setDescription("detail one");
        purchaseReturnDetailFour.setItem(itemThirtteen);
        purchaseReturnDetailFour.setLineNumber(1);
        purchaseReturnDetailFour.setQuantity(getAsBigDecimal(12));
        purchaseReturnDetailFour.setUnitPrice(getAsBigDecimal(25));
        purchaseReturnDetailFour.setTotalPrice(getAsBigDecimal(300));
        
        purchaseReturnDetailFive = new PurchaseReturnDetail();
        purchaseReturnDetailFive.setDescription("detail two");
        purchaseReturnDetailFive.setItem(itemFourteen);
        purchaseReturnDetailFive.setLineNumber(2);
        purchaseReturnDetailFive.setQuantity(getAsBigDecimal(14));
        purchaseReturnDetailFive.setUnitPrice(getAsBigDecimal(14));
        purchaseReturnDetailFive.setTotalPrice(getAsBigDecimal(196));
        
        detailsFour = List.of(purchaseReturnDetailFour, purchaseReturnDetailFive);

        purchaseReturnDocumentServiceImpl.saveOrUpdate(purchaseReturnDocumentTwo, detailsFour);
        
        purchaseReturnDocumentThree = new PurchaseReturnDocument();
        purchaseReturnDocumentThree.setDate(now);
        purchaseReturnDocumentThree.setCounter(2);
        purchaseReturnDocumentThree.setDescription("purchase return document three");
        purchaseReturnDocumentThree.setWarehouse(warehouseOne);
        purchaseReturnDocumentThree.setTotalAmount(getAsBigDecimal(711));
        purchaseReturnDocumentThree.setTotalQuantity(getAsBigDecimal(27));
        
        purchaseReturnDetailSix = new PurchaseReturnDetail();
        purchaseReturnDetailSix.setDescription("detail one");
        purchaseReturnDetailSix.setItem(itemNineteen);
        purchaseReturnDetailSix.setLineNumber(1);
        purchaseReturnDetailSix.setQuantity(getAsBigDecimal(18));
        purchaseReturnDetailSix.setUnitPrice(getAsBigDecimal(17));
        purchaseReturnDetailSix.setTotalPrice(getAsBigDecimal(306));
        
        purchaseReturnDetailSeven = new PurchaseReturnDetail();
        purchaseReturnDetailSeven.setDescription("detail two");
        purchaseReturnDetailSeven.setItem(itemTwenty);
        purchaseReturnDetailSeven.setLineNumber(2);
        purchaseReturnDetailSeven.setQuantity(getAsBigDecimal(9));
        purchaseReturnDetailSeven.setUnitPrice(getAsBigDecimal(45));
        purchaseReturnDetailSeven.setTotalPrice(getAsBigDecimal(405));
        
        detailsFour = List.of(purchaseReturnDetailSix, purchaseReturnDetailSeven);

        purchaseReturnDocumentServiceImpl.saveOrUpdate(purchaseReturnDocumentThree, detailsFour);
        
        // </editor-fold>
    }
    
    public static BigDecimal getAsBigDecimal(float value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_DOWN);
    }

    public static BigDecimal getAsBigDecimal(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_DOWN);
    }
    
}
