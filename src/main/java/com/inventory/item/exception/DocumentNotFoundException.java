/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.exception;

import java.text.MessageFormat;
import com.inventory.item.util.Messages;
/**
 *
 * @author ignis
 */
public class DocumentNotFoundException extends RuntimeException {

    public DocumentNotFoundException( String id, String type) {
        super(MessageFormat.format(Messages.DOCUMENT_NOT_FOUND, id, type));
    }
}
