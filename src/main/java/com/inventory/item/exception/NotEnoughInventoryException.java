/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.exception;

import java.text.MessageFormat;
import com.inventory.item.util.Messages;

/**
 *
 * @author ignis
 */
public class NotEnoughInventoryException extends RuntimeException {

    public NotEnoughInventoryException(Long id, String name) {
     
        super(MessageFormat.format(Messages.NOT_ENOUGH_INVENTORY, id, name));
    }
    
}
