/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.exception;

import java.text.MessageFormat;
import com.inventory.item.util.Messages;
/**
 *
 * @author ignis
 */
public class ItemSummaryNotFoundException extends RuntimeException {

    public ItemSummaryNotFoundException(Long id,String itemName, String warehouseName) {
        super(MessageFormat.format(Messages.ITEM_SUMMARY_NOT_FOUND, id, itemName, warehouseName));
    }
    
}
