/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.controller;

import java.util.List;
import javax.validation.Valid;

import com.inventory.item.enums.SheetType;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;

import com.inventory.item.model.RowSheet;
import com.inventory.item.report.ReportGenerator;
import com.inventory.item.barcode.DocumentBarcodeWriter;
import com.inventory.item.barcode.factory.DocumentBarcodeFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ignis
 */
@CrossOrigin
@RestController
@RequestMapping("/reports")
public class ReportController {

    private final ReportGenerator reportGenerator;
    private final DocumentBarcodeFactory documentBarcodeFactoryService;

    @Autowired
    public ReportController(
            ReportGenerator reportGenerator,
            DocumentBarcodeFactory documentBarcodeFactoryService) {

        this.reportGenerator = reportGenerator;
        this.documentBarcodeFactoryService = documentBarcodeFactoryService;
    }

    @PostMapping(
            produces = MediaType.APPLICATION_PDF_VALUE,
            value = "/type/{type}/id/{id}/format/{barcodeType}/sheet/{sheetType}/barcode")
    public ResponseEntity<byte[]> generateBarcode(
            @PathVariable String id,
            @PathVariable DocumentType type,
            @PathVariable SheetType sheetType,
            @PathVariable BarcodeType barcodeType,
            @Valid @RequestBody List<Integer> positions) throws Throwable {

        if (positions == null) {
            positions = List.of();
        }

        var writer = documentBarcodeFactoryService.getWriterFactory(type);
        List<RowSheet> rowSheetList = writer.generateLabels(positions, id, barcodeType, sheetType);
        byte data[] = reportGenerator.getReportAsByArray(rowSheetList);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=barcodeLabels.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(data);
    }
}
