/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.controller;

import javax.validation.Valid;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.enums.BarcodeType;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.barcode.DocumentBarcodeReader;
import com.inventory.item.barcode.factory.DocumentBarcodeFactory;

import com.inventory.item.util.Util;
import com.inventory.item.service.DocumentService;
import com.inventory.item.service.factory.DocumentServiceFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author ignis
 */
@CrossOrigin
@RestController
@RequestMapping("/receptions")
public class ReceptionController {

    private final DocumentServiceFactory documentFactoryService;
    private final DocumentBarcodeFactory documentBarcodeFactoryService;

    @Autowired
    public ReceptionController(
            DocumentServiceFactory documentFactoryService,
            DocumentBarcodeFactory documentBarcodeFactoryService) {

        this.documentFactoryService = documentFactoryService;
        this.documentBarcodeFactoryService = documentBarcodeFactoryService;
    }

    @PostMapping("/type/{type}")
    @PreAuthorize("hasPermission('receptions','createDocument')")
    public <T extends Document> ResponseEntity<Object> createDocument(@PathVariable DocumentType type, @Valid @RequestBody String document) throws Exception {

        DocumentService service = documentFactoryService.getFactory(type);

        T result = service.saveOrUpdate(document);

        return ResponseEntity.ok(result);
    }

    @GetMapping("/type/{type}/id/{id}")
    @PreAuthorize("hasPermission('receptions','getDocumentById')")
    public <T extends Document> T getDocumentById(@PathVariable DocumentType type, @PathVariable String id) throws Throwable {

        DocumentService service = documentFactoryService.getFactory(type);

        return service.getDocumentById(id);
    }

    @GetMapping("/type/{type}/id/{id}/filter")
    @PreAuthorize("hasPermission('receptions','getDocumentsByIdAsPage')")
    public <T extends Document> Page<T> getDocumentsByIdAsPage(@PathVariable DocumentType type, @PathVariable String id, Pageable pageable) {

        DocumentService service = documentFactoryService.getFactory(type);

        return service.getAllDocumentsByIdLike(id, pageable);
    }

    @GetMapping("/type/{type}")
    @PreAuthorize("hasPermission('receptions','getDocuments')")
    public <T extends Document> Page<T> getDocuments(@PathVariable DocumentType type, Pageable pageable) {

        DocumentService service = documentFactoryService.getFactory(type);

        return service.getAllDocuments(pageable);
    }

    @GetMapping("/type/{type}/id/{id}/details")
    @PreAuthorize("hasPermission('receptions','getDetails')")
    public <S extends Detail> Page<S> getDetails(@PathVariable DocumentType type, @PathVariable String id, Pageable pageable) throws Throwable {

        DocumentService service = documentFactoryService.getFactory(type);

        return service.getDetailsByDocument(id, pageable);
    }

    @GetMapping("warehouse/{id}/barcode/{barcode}/format/{format}")
    @PreAuthorize("hasPermission('receptions','getDetailFromBarcode')")
    public <S extends Detail> S getDetailFromBarcode(@PathVariable Long id, @PathVariable String barcode, @PathVariable BarcodeType format) throws Exception {

        DocumentType documentType = Util.getDocTypeFromBarcode(barcode);
        DocumentBarcodeReader documentBarcodeFactory = documentBarcodeFactoryService.getReaderFactory(documentType);
        return documentBarcodeFactory.readLabel(id, barcode, format);
    }

    @PutMapping("type/{type}/id/{id}")
    @PreAuthorize("hasPermission('receptions','updateDocument')")
    public <T extends Document> ResponseEntity<Object> updateDocument(@PathVariable DocumentType type, @PathVariable String id, @Valid @RequestBody String document) throws Throwable {
        DocumentService service = documentFactoryService.getFactory(type);

        T result = service.saveOrUpdate(document);

        return ResponseEntity.ok(result);
    }

    @PutMapping("type/{type}/id/{id}/release")
    @PreAuthorize("hasPermission('receptions','releaseDocument')")
    public <T extends Document> T releaseDocument(@PathVariable DocumentType type, @PathVariable String id) throws Throwable {

        DocumentService service = documentFactoryService.getFactory(type);
        return service.release(id);
    }

    @DeleteMapping("type/{type}/id/{id}")
    @PreAuthorize("hasPermission('receptions','removeDocument')")
    public void removeDocument(@PathVariable DocumentType type, @PathVariable String id) throws Throwable {

        DocumentService service = documentFactoryService.getFactory(type);
        service.remove(id);
    }
}
