/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.controller;

import java.util.List;
import javax.validation.Valid;

import com.inventory.item.entity.Item;
import com.inventory.item.service.ItemService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author ignis
 */

@CrossOrigin
@RestController
@RequestMapping("/items")
public class ItemController {

    private final ItemService itemService;
    
    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }
    
    @PostMapping
    @PreAuthorize("hasPermission('items','createItem')")
    public ResponseEntity<Object> createItem(@Valid @RequestBody Item item) {
        Item result = itemService.save(item);

        return ResponseEntity.ok(result);
    }
    
    @GetMapping
    @PreAuthorize("hasPermission('items','getItemsAsPage')")
    public Page<Item> getItemsAsPage(Pageable pageable) {

        return itemService.getAllItems(pageable);
    }

    @GetMapping("/options")
    @PreAuthorize("hasPermission('items','getItemsAsList')")
    public List<Item> getItemsAsList() {
        
        return itemService.getAllItems();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('items','getItem')")
    public Item getItem(@PathVariable Long id) {
        return itemService.getItemById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasPermission('items','getItemByname')")
    public Page<Item> getItemByname(@PathVariable String name, Pageable pageable) {
        return itemService.getByNameLike(name, pageable);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasPermission('items','updateItem')")
    public ResponseEntity<Object> updateItem(@RequestBody Item item, @PathVariable Long id) {

        Item result = itemService.update(id, item);

        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('items','deleteItem')")
    public void deleteItem(@PathVariable Long id) {
        itemService.deleteOrDisableItem(id);
    }

    @DeleteMapping
    @PreAuthorize("hasPermission('items','deleteAll')")
    public void deleteAll() {
        itemService.deleteAll();
    }
}