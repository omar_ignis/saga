/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.controller;

import java.util.List;
import javax.validation.Valid;

import com.inventory.item.entity.Item;
import com.inventory.item.entity.Warehouse;
import com.inventory.item.entity.ItemSummary;
import com.inventory.item.service.WarehouseSummaryService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author ignis
 */
@CrossOrigin
@RestController
@RequestMapping("/warehouses")
public class WarehouseController {

    private final WarehouseSummaryService warehouseService;

    @Autowired
    public WarehouseController(WarehouseSummaryService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @PostMapping
    @PreAuthorize("hasPermission('warehouses','createWarehouse')")
    public ResponseEntity<Object> createWarehouse(@Valid @RequestBody Warehouse warehouse) {

        Warehouse result = warehouseService.save(warehouse);

        return ResponseEntity.ok(result);
    }

    @GetMapping
    @PreAuthorize("hasPermission('warehouses','getWarehousesAsPage')")
    public Page<Warehouse> getWarehousesAsPage(Pageable pageable) {
        return warehouseService.getAllWarehouses(pageable);
    }

    @GetMapping("/options")
    @PreAuthorize("hasPermission('warehouses','getWarehousesAsList')")
    public List<Warehouse> getWarehousesAsList() {
        return warehouseService.getAllWarehouses();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('warehouses','getWarehouse')")
    public Warehouse getWarehouse(@PathVariable Long id) {

        return warehouseService.getWarehouseById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasPermission('warehouses','getWarehousesByName')")
    public Page<Warehouse> getWarehousesByName(@PathVariable String name, Pageable pageable) {
        return warehouseService.getWarehouseByName(name, pageable);
    }
    
    @GetMapping("/{id}/item-summary")
    @PreAuthorize("hasPermission('warehouses','getItemSummary')")
    public Page<ItemSummary> getItemSummary(@PathVariable Long id, Pageable pageable){
        return warehouseService.getItemSummaryByWarehouse(id, pageable);
    }
    
    @GetMapping("/{id}/items")
    @PreAuthorize("hasPermission('warehouses','getAllItemsByWarehouse')")
    public List<Item> getAllItemsByWarehouse(@PathVariable Long id){
        return warehouseService.getAllItemsByWarehouse(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasPermission('warehouses','updateItem')")
    public ResponseEntity<Object> updateItem(@RequestBody Warehouse warehouse, @PathVariable Long id) {

        //Validamos que exista antes de intentar actualizar
        Warehouse result = warehouseService.update(id, warehouse);

        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('warehouses','deleteWarehouse')")
    public void deleteWarehouse(@PathVariable Long id) {
        warehouseService.deleteWarehouse(id);
    }

    @DeleteMapping
    @PreAuthorize("hasPermission('warehouses','deleteAll')")
    public void deleteAll() {
        warehouseService.deleteAllWarehouses();
    }
}
