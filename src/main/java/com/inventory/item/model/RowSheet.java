/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.model;

/**
 *
 * @author ignis
 */
public abstract class RowSheet {

    public abstract boolean isCompleted();

    public abstract String getLabel(int position);

    public abstract String getBarcode(int position);
    
    public abstract void setLabel(int position, String label);
    
    public abstract int fillRow(String barcode, String label);
    
    public abstract void setBarcode(int position, String barcode);

    public abstract int fillPartialRow(boolean full, String barcode, String label);

    public abstract void fillPartialRow(int pending, String barcode, String label);
}
