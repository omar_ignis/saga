/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inventory.item.model;

/**
 * @author ignis
 */
public class BarcodeLabel {

    private String label;
    private String barcode;
    private int quantity;

    public BarcodeLabel() {
    }

    public BarcodeLabel(String label, String barcode, int quantity) {
        this.label = label;
        this.barcode = barcode;
        this.quantity = quantity;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
