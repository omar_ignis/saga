/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.model;

/**
 *
 * @author ignis
 */
public class RowOD5160 extends RowSheet {

    private String barcodeOne;
    private String labelOne;
    private String barcodeTwo;
    private String labelTwo;
    private String barcodeThree;
    private String labelThree;

    public String getBarcodeOne() {
        return barcodeOne;
    }

    public void setBarcodeOne(String barcodeOne) {
        this.barcodeOne = barcodeOne;
    }

    public String getLabelOne() {
        return labelOne;
    }

    public void setLabelOne(String labelOne) {
        this.labelOne = labelOne;
    }

    public String getBarcodeTwo() {
        return barcodeTwo;
    }

    public void setBarcodeTwo(String barcodeTwo) {
        this.barcodeTwo = barcodeTwo;
    }

    public String getLabelTwo() {
        return labelTwo;
    }

    public void setLabelTwo(String labelTwo) {
        this.labelTwo = labelTwo;
    }

    public String getBarcodeThree() {
        return barcodeThree;
    }

    public void setBarcodeThree(String barcodeThree) {
        this.barcodeThree = barcodeThree;
    }

    public String getLabelThree() {
        return labelThree;
    }

    public void setLabelThree(String labelThree) {
        this.labelThree = labelThree;
    }

    @Override
    public boolean isCompleted() {

        return this.barcodeOne != null
                && this.barcodeTwo != null && this.barcodeThree != null;
    }

    @Override
    public int fillRow(String barcode, String label) {

        this.barcodeOne = barcode;
        this.barcodeTwo = barcode;
        this.barcodeThree = barcode;

        this.labelOne = label;
        this.labelTwo = label;
        this.labelThree = label;

        return 3;
    }

    @Override
    public void fillPartialRow(int pending, String barcode, String label) {

        boolean completed = pending == 2;
        this.barcodeOne = barcode;
        this.labelOne = label;

        if (completed) {

            this.barcodeTwo = barcode;
            this.labelTwo = label;
        }

    }

    @Override
    public int fillPartialRow(boolean full, String barcode, String label) {

        boolean filled = this.barcodeTwo == null;
        if (filled) {

            this.barcodeTwo = barcode;
            this.labelTwo = label;
        }

        if (full || !filled) {

            this.barcodeThree = barcode;
            this.labelThree = label;

            return filled ? 2 : 1;
        }

        //cuantas posiciones fueron llenadas
        return 1;
    }

    @Override
    public String getLabel(int position) {

        String label = "";
        switch (position) {
            case 0:
                label = this.labelOne;
                break;
            case 1:
                label = this.labelTwo;
                break;
            case 2:
                label = this.labelThree;
                break;
            default:
                break;
        }

        return label;
    }

    @Override
    public String getBarcode(int position) {

        String barcode = "";
        switch (position) {
            case 0:
                barcode = this.barcodeOne;
                break;
            case 1:
                barcode = this.barcodeTwo;
                break;
            case 2:
                barcode = this.barcodeThree;
                break;
            default:
                break;
        }

        return barcode;
    }

    @Override
    public void setLabel(int position, String label) {

        switch (position) {
            case 0:
                this.labelOne = label;
                break;
            case 1:
                this.labelTwo = label;
                break;
            case 2:
                this.labelThree = label;
                break;
            default:
                break;
        }
    }

    @Override
    public void setBarcode(int position, String barcode) {

        switch (position) {
            case 0:
                this.barcodeOne = barcode;
                break;
            case 1:
                this.barcodeTwo = barcode;
                break;
            case 2:
                this.barcodeThree = barcode;
                break;
            default:
                break;
        }
    }

}
