/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.validation;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;
import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;

import com.inventory.item.security.exception.RoleNotFoundException;
import com.inventory.item.security.exception.RoleDuplicatedException;
import com.inventory.item.security.exception.UserAccountNotFoundException;
import com.inventory.item.security.exception.OrganizationNotFoundException;
import com.inventory.item.security.exception.UserAccountEmailDuplicatedException;

/**
 *
 * @author ignis
 */
@Service
public class SecurityValidation {

    private final RoleRepository roleRepository;

    private final UserAccountRepository userAccountRepository;

    private final OrganizationRepository organizationRepository;

    @Autowired
    public SecurityValidation(
            RoleRepository roleRepository,
            UserAccountRepository userAccountRepository,
            OrganizationRepository organizationRepository) {

        this.roleRepository = roleRepository;
        this.userAccountRepository = userAccountRepository;
        this.organizationRepository = organizationRepository;
    }

    public Organization validateOrganization(Long id) {

        var organization = organizationRepository.findById(id).orElseThrow(()
                -> new OrganizationNotFoundException(id.toString()));

        return organization;
    }

    public UserAccount validateUserAccount(Long id) {

        var userAccount = userAccountRepository.findById(id)
                .orElseThrow(UserAccountNotFoundException::new);

        return userAccount;
    }

    public void validateUserAccountBeforeCreate(String username, String email) {

        var userAccount = userAccountRepository.findByEmail(email);

        if (userAccount != null) {

            throw new UserAccountEmailDuplicatedException(email);
        }

    }

    public UserAccount validateUserAccountBeforeUpdate(Long id, String email) {

        var userAccount = userAccountRepository.findByEmail(email);

        if (userAccount != null && !userAccount.getId().equals(id)) {

            throw new UserAccountEmailDuplicatedException(email);
        }

        userAccount = userAccountRepository.findById(id)
                .orElseThrow(() -> new UserAccountNotFoundException(email));

        return userAccount;
    }

    public void validateRoleBeforeCreate(String name, Long organizationId) {

        var role = roleRepository.findByKeyNameAndKeyOrganizationId(name, organizationId);

        if (role.isPresent()) {
            throw new RoleDuplicatedException(name);
        }
    }

    public Role validateRoleBeforeUpdate(String name, Long organizationId) {

        var role = roleRepository.findByKeyNameAndKeyOrganizationId(name, organizationId)
                .orElseThrow(() -> new RoleNotFoundException(name));

        return role;
    }
}
