/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.exception;

import java.text.MessageFormat;
import com.inventory.item.security.util.Messages;

/**
 *
 * @author ignis
 */
public class UserAccountNotFoundException extends RuntimeException {

    public UserAccountNotFoundException() {
        
        super(Messages.USERACCOUNT_ID_NOT_FOUND);
    }

    public UserAccountNotFoundException(String email) {
        super(MessageFormat.format(Messages.USERACCOUNT_EMAIL_NOT_FOUND, email));
    }
    
}
