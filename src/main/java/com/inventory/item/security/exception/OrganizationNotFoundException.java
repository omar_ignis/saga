/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.exception;

import java.text.MessageFormat;
import com.inventory.item.security.util.Messages;
/**
 *
 * @author ignis
 */
public class OrganizationNotFoundException extends RuntimeException {
    

    public OrganizationNotFoundException(String id) {
        super(MessageFormat.format(Messages.ORGANIZATION_NOT_FOUND, id));
    }
    
}
