/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.exception;

import com.inventory.item.security.util.Messages;
/**
 *
 * @author ignis
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super(Messages.UNAUTHORIZED);
    }
    
}
