/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.exception;

import java.text.MessageFormat;
import com.inventory.item.security.util.Messages;
/**
 *
 * @author ignis
 */
public class RoleDuplicatedException extends RuntimeException {

    public RoleDuplicatedException(String name) {
        super(MessageFormat.format(Messages.ROLE_DUPLICATED, name));
    }
    
}
