/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.exception;

import java.text.MessageFormat;
import com.inventory.item.security.util.Messages;
/**
 *
 * @author ignis
 */
public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException(String name) {
        super(MessageFormat.format(Messages.ROLE_NOT_FOUND, name));
    }
    
}
