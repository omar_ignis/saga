/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.setup;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.lang.reflect.Method;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.Organization;

import com.inventory.item.controller.ItemController;
import com.inventory.item.controller.ReportController;
import com.inventory.item.controller.DispatchController;
import com.inventory.item.controller.ReceptionController;
import com.inventory.item.controller.WarehouseController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author ignis
 */
@Component
public class PermissionSetup {

    public List<Permission> createPermissions(Organization organization, List<Resource> resources) {

        List<Permission> permissions = resources.stream()
                .map(resource -> new Permission(resource, organization))
                .collect(Collectors.toList());

        return permissions;
    }

    public List<Resource> createResources() {

        List<Resource> resources = new ArrayList<>();

        List<Method> methods = Arrays.asList(ItemController.class.getDeclaredMethods());
        resources.addAll(createResources(ItemController.class, methods));

        methods = Arrays.asList(DispatchController.class.getDeclaredMethods());
        resources.addAll(createResources(DispatchController.class, methods));

        methods = Arrays.asList(ReceptionController.class.getDeclaredMethods());
        resources.addAll(createResources(ReceptionController.class, methods));

        methods = Arrays.asList(ReportController.class.getDeclaredMethods());
        resources.addAll(createResources(ReportController.class, methods));

        methods = Arrays.asList(WarehouseController.class.getDeclaredMethods());
        resources.addAll(createResources(WarehouseController.class, methods));

        return resources;
    }
    
    public List<Resource> createResources(Class controller, List<Method> methods) {
        
        String domain = ((RequestMapping) controller.getAnnotation(RequestMapping.class)).value()[0].replace("/", "");
        
        List<Resource> resources = methods.stream().map(Method::getName).distinct()
                .map(methodName -> new Resource(methodName, domain)).collect(Collectors.toList());

        return resources;
    }
}
