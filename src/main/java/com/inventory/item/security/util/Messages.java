/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.util;

/**
 *
 * @author ignis
 */
public class Messages {
    
    public final static String USERACCOUNT_ID_NOT_FOUND = "Usuario valido";
    public final static String ROLE_NOT_FOUND = "El role {0} no existe en el sistem";
    public final static String ROLE_DUPLICATED = "El role {0} ya existe en el sistem";
    public final static String USERACCOUNT_EMAIL_NOT_FOUND = "{0} no es un usuario valido";
    public final static String USERACCOUNT_EMAIL_DUPLICATED = "Ya existe un usuario con el email {0}";
    public final static String ORGANIZATION_NOT_FOUND = "La organizacion con id {0} no se encuentra en el sistema";
    public final static String UNAUTHORIZED = "Recurso denegado. El usuario no cuenta con permisos para realizar esta accion";
}
