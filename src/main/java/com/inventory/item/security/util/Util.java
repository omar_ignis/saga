/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.util;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;
import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;

/**
 *
 * @author ignis
 */
public class Util {

    public static UserAccount copy(UserAccount userAccount, UserAccount result) {

        result.setName(userAccount.getName());
        result.setEmail(userAccount.getEmail());
        result.setPassword(userAccount.getPassword());
        result.setUsername(userAccount.getUsername());
        result.setPaternalLastName(userAccount.getPaternalLastName());
        result.setMaternalLastName(userAccount.getMaternalLastName());

        return result;
    }

    public static List<Permission> getDeletedPermissions(List<Permission> permissions) {

        List<Permission> list = permissions
                .stream()
                .filter(permission -> permission.isDeleted() == true)
                .collect(Collectors.toList());

        return list;
    }

    public static List<Permission> getNotDeletedPermissions(List<Permission> permissions) {

        List<Permission> list = permissions
                .stream()
                .filter(permission -> permission.isDeleted() == false)
                .collect(Collectors.toList());

        return list;
    }

    public static List<Permission> getNewRolePermissions(List<Permission> currentPermissions, List<Permission> requestedPermissions) {

        Map<Long, Permission> requestedPermissionsMap = requestedPermissions
                .stream()
                .collect(Collectors.toMap(Permission::getId, permission -> permission));
        
        Map<Long, Permission> currentPermissionsMap = currentPermissions
                .stream()
                .collect(Collectors.toMap(Permission::getId, permission -> permission));
        
        Map<Long, Permission> map = requestedPermissionsMap
                .entrySet().stream()
		.filter(element -> !currentPermissionsMap.containsKey(element.getKey()))
		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        
       List<Permission> list = map.values().stream().collect(Collectors.toList());
        
        return list;
    }
    
    public static List<Role> getDeletedRoles(List<Role> roles){
        
        List<Role> list = roles
                .stream()
                .filter(role -> role.isDeleted() == true)
                .collect(Collectors.toList());

        return list;
    }
    
    public static List<Role> getNotDeletedRoles(List<Role> roles) {

        List<Role> list = roles
                .stream()
                .filter(role -> role.isDeleted() == false)
                .collect(Collectors.toList());

        return list;
    }
    
    public static List<Role> getNewUserRoles(List<Role> currentRoles, List<Role> requestedRoles) {

        Map<String, Role> requestedRoleMap = requestedRoles
                .stream()
                .collect(Collectors.toMap(Role::toString, role -> role));
        
        Map<String, Role> currentRoleMap = currentRoles
                .stream()
                .collect(Collectors.toMap(Role::toString, role -> role));
        
        Map<String, Role> map = requestedRoleMap
                .entrySet().stream()
		.filter(element -> !currentRoleMap.containsKey(element.getKey()))
		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        
       List<Role> list = map.values().stream().collect(Collectors.toList());
        
        return list;
    }
}
