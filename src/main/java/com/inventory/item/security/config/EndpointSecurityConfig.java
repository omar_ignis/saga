/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 *
 * @author ignis
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EndpointSecurityConfig extends GlobalMethodSecurityConfiguration {

    private final PermissionEvaluator permissionEvaluatorImpl;

    @Autowired
    public EndpointSecurityConfig(PermissionEvaluator permissionEvaluatorImpl) {
        this.permissionEvaluatorImpl = permissionEvaluatorImpl;
    }

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {

        var expressionHandler = new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(permissionEvaluatorImpl);

        return expressionHandler;
    }
}
