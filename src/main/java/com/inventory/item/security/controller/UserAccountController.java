/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.controller;

import java.util.List;
import javax.validation.Valid;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.service.SecurityService;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author ignis
 */
@CrossOrigin
@RestController
@RequestMapping("/admin")
public class UserAccountController {

    private final SecurityService securityServiceImpl;

    @Autowired
    public UserAccountController(SecurityService securityServiceImpl) {
        this.securityServiceImpl = securityServiceImpl;
    }
    
    //Users

    @PostMapping("/organizations/{organizationId}/users")
    @PreAuthorize("hasPermission('admin','createUser')")
    public ResponseEntity<UserAccount> createUser(@PathVariable Long organizationId, @Valid @RequestBody UserAccount userAccount) {

        var result = securityServiceImpl.createUserAccount(organizationId, userAccount);

        return ResponseEntity.ok(result);
    }

    @PutMapping("/organizations/{organizationId}/users")
    @PreAuthorize("#userAccount.email == authentication.principal.username or  hasPermission('admin','updateUser')")
    public ResponseEntity<UserAccount> updateUser(@PathVariable Long organizationId, @Valid @RequestBody UserAccount userAccount) {
        
        var result = securityServiceImpl.updateUserAccount(organizationId, userAccount);
        
        return ResponseEntity.ok(result);
    }
    
    @PutMapping("/organizations/{organizationId}/users/{userId}")
    @PreAuthorize("hasPermission('admin','updateUserRoles')")
    public ResponseEntity<UserAccount> updateUserRoles(@PathVariable Long organizationId, @PathVariable Long userId, @Valid @RequestBody List<Role> roles){
        
        var result = securityServiceImpl.updateUserRoles(organizationId, userId, roles);
        
        return ResponseEntity.ok(result);
    }
    
    @PutMapping("/organizations/{organizationId}/users/{userId}/enable/{enable}")
    @PreAuthorize("hasPermission('admin','enableUser')")
    public ResponseEntity<UserAccount> enableUser(@PathVariable Long organizationId, @PathVariable Long userId, @PathVariable boolean enable){
        
        var result = securityServiceImpl.enableUserAcccount(organizationId, userId, enable);
        
        return ResponseEntity.ok(result);
    }
    
    //Security
    @PostMapping("/organizations/{organizationId}/roles/{name}")
    @PreAuthorize("hasPermission('admin','createRole')")
    public ResponseEntity<Role> createRole(@PathVariable Long organizationId, @PathVariable String name){
        
        var result = securityServiceImpl.createRole(name, organizationId);
        
        return ResponseEntity.ok(result);
    }
    
    @PutMapping("/organizations/{organizationId}/roles/{name}")
    @PreAuthorize("hasPermission('admin','updateRole')")
    public ResponseEntity<Role> updateRole(@PathVariable Long organizationId, @PathVariable String name, @Valid @RequestBody List<Permission> permissions){
        
        var result = securityServiceImpl.updateRole(name, organizationId, permissions);
        
        return ResponseEntity.ok(result);
    }
}
