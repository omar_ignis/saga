/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author ignis
 */
@Controller
public class IndexController {
    
    @GetMapping("/")
    public String showIndex(){
        return "index";
    }
    
    @GetMapping("/login")
    public String showLogin(){
        return "login";
    }
}
