/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service.impl;

import java.io.Serializable;
import com.inventory.item.security.service.AuthorizationService;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.access.PermissionEvaluator;

/**
 *
 * @author ignis
 */
@Service
public class PermissionEvaluatorImpl implements PermissionEvaluator {
    
    private AuthorizationService authorizationServiceImpl;

    @Autowired
    public PermissionEvaluatorImpl(AuthorizationService authorizationServiceImpl) {
        this.authorizationServiceImpl = authorizationServiceImpl;
    }
    
    @Override
    public boolean hasPermission(Authentication authentication, Object domain, Object resource) {

        if ((authentication == null || domain == null) || !(resource instanceof String)) {
            return false;
        }
        
        User user = (User) authentication.getPrincipal();
        String resourceName = resource.toString();
        String resourceDomain = domain.toString();
        
        return authorizationServiceImpl.hasUserPermission(user, resourceName, resourceDomain);
    }

    @Override
    public boolean hasPermission(Authentication a, Serializable srlzbl, String string, Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
