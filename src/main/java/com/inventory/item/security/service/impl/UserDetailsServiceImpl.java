/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.repository.UserAccountRepository;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 *
 * @author ignis
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    
    private final UserAccountRepository userAccountRepository;

    @Autowired
    public UserDetailsServiceImpl(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        
        UserAccount userAccount = userAccountRepository.findByEmail(email);
        
        if(userAccount == null) 
            throw new UsernameNotFoundException("User not found for email : " + email); 
        
        User user = new User(userAccount.getEmail(), userAccount.getPassword(), userAccount.getRoles());
        
        return user; 
    }
    
}
