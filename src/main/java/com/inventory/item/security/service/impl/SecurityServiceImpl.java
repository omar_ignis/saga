/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service.impl;

import java.util.List;
import com.inventory.item.security.util.Util;
import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;
import com.inventory.item.security.service.SecurityService;
import com.inventory.item.security.validation.SecurityValidation;

import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.ResourceRepository;
import com.inventory.item.security.repository.PermissionRepository;
import com.inventory.item.security.repository.UserAccountRepository;
import com.inventory.item.security.repository.OrganizationRepository;


import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ignis
 */
@Service
public class SecurityServiceImpl implements SecurityService {
    
    private final RoleRepository roleRepository;

    private final SecurityValidation securityValidation;
    
    private final ResourceRepository resourceRepository;

    private final PermissionRepository permissionRepository;
    
    private final UserAccountRepository userAccountRepository;
    
    private final OrganizationRepository organizationRepository;
    
    
    @Autowired
    public SecurityServiceImpl(
            RoleRepository roleRepository,
            SecurityValidation securityValidation,
            ResourceRepository resourceRepository,
            PermissionRepository permissionRepository,
            UserAccountRepository userAccountRepository,
            OrganizationRepository organizationRepository) {

        this.roleRepository = roleRepository;
        this.securityValidation = securityValidation;
        this.resourceRepository = resourceRepository;
        this.permissionRepository = permissionRepository;
        this.userAccountRepository = userAccountRepository;
        this.organizationRepository = organizationRepository;
    }

    @Override
    public List<Resource> createResources(List<Resource> resources) {

        var list = resourceRepository.saveAll(resources);

        return list;
    }
    
    @Override
    public List<Permission> createPermissions(List<Permission> permissions) {
        
        permissions = permissionRepository.saveAll(permissions);
        
        return permissions;
    }

    @Override
    public Organization createOrganization(String name, String description) {

        var organization = new Organization(name, description);

        organization = organizationRepository.save(organization);

        return organization;
    }

    @Override
    public Role createRole(String name, Long organizationId) {

        var organization = securityValidation.validateOrganization(organizationId);
        securityValidation.validateRoleBeforeCreate(name, organizationId);
        
        var role = new Role(name, organization);
        
        role = roleRepository.save(role);
        
        return role;
    }

    @Override
    public Role updateRole(String name, Long organizationId, List<Permission> permissions) {

        securityValidation.validateOrganization(organizationId);
        var role = securityValidation.validateRoleBeforeUpdate(name, organizationId);
        
        var removedPermissions = Util.getDeletedPermissions(permissions);

        if (!removedPermissions.isEmpty()) {
            role.removePermissions(removedPermissions);
        }

        var requestedPermissions = Util.getNotDeletedPermissions(permissions);

        var newRolePermissions = Util.getNewRolePermissions(role.getPermissions(), requestedPermissions);

        if (!newRolePermissions.isEmpty()) {
            role.addPermissions(newRolePermissions);
        }

        var result = roleRepository.save(role);
        return result;
    }

    @Override
    public UserAccount createUserAccount(Long organizationId, UserAccount userAccount) {
        
        var organization = securityValidation.validateOrganization(organizationId);
        securityValidation.validateUserAccountBeforeCreate(userAccount.getUsername(), userAccount.getEmail());
        
        var result = Util.copy(userAccount, new UserAccount());
        result.setOrganization(organization);
        result = userAccountRepository.save(result);
        
        return result;
    }

    @Override
    public UserAccount enableUserAcccount(Long organizationId, Long userId, boolean enable) {
        
        securityValidation.validateOrganization(organizationId);
        var userAccount = securityValidation.validateUserAccount(userId);
        userAccount.setActive(enable);
        userAccount = userAccountRepository.save(userAccount);
        
        return userAccount;
    }

    @Override
    public UserAccount updateUserAccount(Long organizationId, UserAccount userAccount) {
        
        securityValidation.validateOrganization(organizationId);
        
        var result = securityValidation
                .validateUserAccountBeforeUpdate(userAccount.getId(), userAccount.getEmail());
        
        result = Util.copy(userAccount, result);
        result = userAccountRepository.save(result);
        return result;
    }

    @Override
    public UserAccount updateUserRoles(Long organizationId, Long userId, List<Role> roles) {

        securityValidation.validateOrganization(organizationId);
        var userAccount = securityValidation.validateUserAccount(userId);

        var removedRoles = Util.getDeletedRoles(roles);

        if (!removedRoles.isEmpty()) {
            userAccount.removeRoles(removedRoles);
        }

        var requestedRoles = Util.getNotDeletedRoles(roles);

        var newRequestedRoles = Util.getNewUserRoles(userAccount.getRoles(), requestedRoles);

        if (!newRequestedRoles.isEmpty()) {
            userAccount.addRoles(newRequestedRoles);
        }

        var result = userAccountRepository.save(userAccount);

        return result;
    }
}
