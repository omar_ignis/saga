/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.service.impl;

import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.repository.RoleRepository;
import com.inventory.item.security.repository.UserAccountRepository;

import org.springframework.security.core.userdetails.User;
import com.inventory.item.security.service.AuthorizationService;
import com.inventory.item.security.exception.UnauthorizedException;

/**
 *
 * @author ignis
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private final RoleRepository roleRepository;
    
    private final UserAccountRepository userAccountRepository;

    @Autowired
    public AuthorizationServiceImpl(
            RoleRepository roleRepository, 
            UserAccountRepository userAccountRepository) {
        
        this.roleRepository = roleRepository;
        this.userAccountRepository = userAccountRepository;
    }
    
    @Override
    public boolean hasUserPermission(User user, String resourceName, String resourceDomain) {
        
        UserAccount userAccount = userAccountRepository.findByEmail(user.getUsername());
        
        Set<String> userRoles = getUserRoles(userAccount);
        Set<String> resourceRoles = getRolesByResourceAndOrganization(
                userAccount.getOrganization().getId(), resourceName, resourceDomain);
        
        return !Collections.disjoint(userRoles, resourceRoles);
    }

    @Override
    public Set<String> getUserRoles(UserAccount user) {

        if (user.getRoles().isEmpty()) {
            throw new UnauthorizedException();
        }
        
        Set<String> roles = user.getRoles().stream()
                .map(Role::getRoleName).collect(Collectors.toSet());

        return roles;
    }

    @Override
    public Set<String> getRolesByResourceAndOrganization(Long organization, String resourceName, String resourceDomain) {

        List<Role> roles = roleRepository
                .findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(
                        organization, resourceName, resourceDomain);

        if (roles.isEmpty()) {
            throw new UnauthorizedException();
        }

        Set<String> set = roles.stream().map(Role::getRoleName).collect(Collectors.toSet());

        return set;
    }

}
