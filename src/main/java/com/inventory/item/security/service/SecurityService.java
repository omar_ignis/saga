/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.security.service;

import java.util.List;
import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.Resource;
import com.inventory.item.security.entity.Permission;
import com.inventory.item.security.entity.UserAccount;
import com.inventory.item.security.entity.Organization;

/**
 *
 * @author ignis
 */
public interface SecurityService {

    public Role createRole(String name, Long organizationId);
    
    public List<Resource> createResources(List<Resource> resources);
    
    public List<Permission> createPermissions(List<Permission> permissions);

    public Organization createOrganization(String name, String description);
    
    public Role updateRole(String name, Long organizationId, List<Permission> list);

    public UserAccount createUserAccount(Long organizationId, UserAccount userAccount);
    
    public UserAccount updateUserAccount(Long organizationId, UserAccount userAccount);
    
    public UserAccount updateUserRoles(Long organizationId, Long userId, List<Role> roles);
    
    public UserAccount enableUserAcccount(Long organizationId, Long userId, boolean enable);
    
}
