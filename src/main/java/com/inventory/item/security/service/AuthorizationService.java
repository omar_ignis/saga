/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.security.service;

import java.util.Set;
import com.inventory.item.security.entity.UserAccount;
import org.springframework.security.core.userdetails.User;
/**
 *
 * @author ignis
 */
public interface AuthorizationService {

    Set<String> getUserRoles(UserAccount user);

    boolean hasUserPermission(User user, String resourceName, String resourceDomain);

    Set<String> getRolesByResourceAndOrganization(Long organization, String resourceName, String resourceDomain);

}
