/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import java.util.List;
import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 *
 * @author ignis
 */
@Entity
public class Permission implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private boolean active;

    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="resource_name", referencedColumnName = "name"),
        @JoinColumn(name="resource_domain", referencedColumnName = "domain")
    })
    private Resource resource;

    @ManyToOne
    @JoinColumn
    private Organization organization;

    @ManyToMany(mappedBy = "permissions")
    List<Role> roles;
    
    @Transient
    private boolean deleted;

    public Permission() {
        this.active = true;
    }

    public Permission(Resource resource, Organization organization) {

        this.active = true;
        this.resource = resource;
        this.organization = organization;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "Permission{" + "id=" + id + ", resource=" + resource
                + ", organization=" + organization + ", roles=" + roles + ", active=" + active + '}';
    }

}
