/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.FetchType;
import javax.persistence.Transient;
import javax.persistence.JoinColumn;
import javax.persistence.EmbeddedId;
import javax.persistence.ManyToMany;

import com.inventory.item.security.entity.key.RoleKey;
import org.springframework.security.core.GrantedAuthority;
/**
 *
 * @author ignis
 */
@Entity
public class Role implements GrantedAuthority{
    
    @EmbeddedId
    private RoleKey key;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="role_auth",
            joinColumns={
                @JoinColumn(name="role_name"),
                @JoinColumn(name="organization_id"),
            },
            inverseJoinColumns = {
                @JoinColumn(name="permission_id"),
            })
    private List<Permission> permissions;
    
    @ManyToMany(mappedBy = "roles")
    private List<UserAccount> users;
    
    @Transient
    private boolean deleted;

    public Role() {
        permissions = new ArrayList();
    }

    public Role(RoleKey key) {
        this.key = key;
        permissions = new ArrayList();
    }
    
    public Role(String name, Organization organization) {
        this.key = new RoleKey(name, organization);
        permissions = new ArrayList();
    }

    public RoleKey getKey() {
        return key;
    }

    public void setKey(RoleKey key) {
        this.key = key;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
    
    public void addPermission(Permission element) {
        
        this.permissions.add(element);
    }
    
    public void addPermissions(List<Permission> list){
        if(!list.isEmpty()){
            this.permissions.addAll(list);
        }
    }
    
    public void removePermissions(List<Permission> list){
        this.permissions.removeAll(list);
    }

    public List<UserAccount> getUsers() {
        return users;
    }

    public void setUsers(List<UserAccount> users) {
        this.users = users;
    }
    
    public String getRoleName(){
        return key.getName();
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    
    @Override
    public String toString() {
        return "Role{" + "key={name=" + key.getName() + ", organizationId={" + key.getOrganization().getId() + "}}";
    }

    @Override
    public String getAuthority() {
        return this.key.getName();
    }
    
}
