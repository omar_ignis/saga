/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

/**
 *
 * @author ignis
 */
@Entity
public class UserAccount extends Person{
    
    @Column(unique = true)
    private String username;
    
    @Column(unique = true)
    private String email;
    
    @Column
    private String password;
    
    @Column
    private boolean active;
    
    @ManyToOne
    @JoinColumn
    private Organization organization;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_role",
            joinColumns = {
                @JoinColumn(name="user_id")
            },
            inverseJoinColumns = {
                @JoinColumn(name="role_name", referencedColumnName = "name"),
                @JoinColumn(name="organization_id", referencedColumnName = "organization_id")
                    
            })
    private List<Role> roles;
    
    public UserAccount() {
        this.roles = new ArrayList();
    }

    public UserAccount(String name, String paternalLastName, String maternalLastName, 
            String username, String email, String password, boolean active) {
        this.roles = new ArrayList();
        super.setName(name);
        super.setPaternalLastName(paternalLastName);
        super.setMaternalLastName(maternalLastName);
        this.username = username;
        this.email = email;
        this.password = password;
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    
    public void addRole(Role role){
        this.roles.add(role);
    }
    
    public void addRoles(List<Role> list) {
        if (!list.isEmpty()) {
            this.roles.addAll(list);
        }
    }
    
    public void removeRoles(List<Role> list){
        this.roles.removeAll(list);
    }
    
    @Override
    public String toString() {
        return "User{" + super.toString() + "username=" + username + ", email=" + 
                email + ", password=" + password + ", active=" + active + '}';
    }
    
    
}
