/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/licensedefault.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.annotations.Type;

/**
 *
 * @author ignis
 */
@Entity
public class OAuthAccessToken {
    @Id
    @Column
    private String tokenId;
    
    @Lob
    @Column
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] token;
    
    @Column
    private String authenticationId;
    
    @Column
    private String userName;
    
    @Column
    private String clientId;
    
    @Lob
    @Column
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] authentication;
    
    @Column
    private String refreshToken;

    public OAuthAccessToken() {
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public void setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "OAuthAccessToken{" + "tokeId=" + tokenId + ", token=" + token
                + ", authenticationId=" + authenticationId + ", username=" + userName
                + ", clientId=" + clientId + ", authentication=" + authentication
                + ", refreshToke=" + refreshToken + '}';
    }
}
