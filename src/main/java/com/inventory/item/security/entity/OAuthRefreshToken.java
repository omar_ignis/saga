/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/licensedefault.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.annotations.Type;

/**
 *
 * @author ignis
 */
@Entity
public class OAuthRefreshToken {

    @Id
    @Column
    private String tokenId;

    @Lob
    @Column
    @Type(type = "org.hibernate.type.BinaryType")
    private byte[] token;

    @Lob
    @Column
    @Type(type = "org.hibernate.type.BinaryType")
    private String authentication;

    public OAuthRefreshToken() {
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    @Override
    public String toString() {
        return "OAuthRefreshToken{" + "tokeId=" + tokenId + ", token=" + token
                + ", authenticationId=" + authentication + '}';
    }
}
