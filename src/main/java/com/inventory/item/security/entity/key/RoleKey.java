/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity.key;

import java.io.Serializable;
import com.inventory.item.security.entity.Organization;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

/**
 *
 * @author ignis
 */
@Embeddable
public class RoleKey implements Serializable{
    
    @Column
    private String name;
    
    @ManyToOne
    @JoinColumn
    private Organization organization;

    public RoleKey() {
    }

    public RoleKey(String name, Organization organization) {
        this.name = name;
        this.organization = organization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "{" + "name=" + name + ", organization=" + organization + '}';
    }
    
}
