/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity.key;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
/**
 *
 * @author ignis
 */
@Embeddable
public class ResourceKey implements Serializable {
    
    @Column
    private String name;
    
    @Column
    private String domain;

    public ResourceKey() {
    }

    public ResourceKey(String name, String domain) {
        this.name = name;
        this.domain = domain;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "ResourceKey{" + "name=" + name + ", domain=" + domain + '}';
    }
    
}
