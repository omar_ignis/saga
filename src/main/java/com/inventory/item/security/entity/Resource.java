/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.security.entity;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;

import com.inventory.item.security.entity.key.ResourceKey;
import java.io.Serializable;

/**
 *
 * @author ignis
 */
@Entity
public class Resource implements Serializable{

    @EmbeddedId
    private ResourceKey key;

    public Resource() {
    }

    public Resource(ResourceKey key) {
        this.key = key;
    }

    public Resource(String name, String domain) {
        this.key = new ResourceKey(name, domain);
    }

    public ResourceKey getKey() {
        return key;
    }

    public void setKey(ResourceKey key) {
        this.key = key;
    }

    public String getResourceName() {

        return this.key == null ? null : this.key.getName();
    }
    
    public String getResourceDomain() {

        return this.key == null ? null : this.key.getDomain();
    }

    @Override
    public String toString() {
        return "Resource{" + "key=" + key + '}';
    }

}
