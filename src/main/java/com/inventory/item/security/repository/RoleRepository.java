/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.security.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.inventory.item.security.entity.Role;
import com.inventory.item.security.entity.key.RoleKey;

/**
 *
 * @author ignis
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, RoleKey> {
    
    Optional<Role> findByKeyNameAndKeyOrganizationId(String name, Long id);
    
    List<Role> findByKeyOrganizationIdAndPermissionsResourceKeyNameAndPermissionsResourceKeyDomain(Long organization, String resource, String domain);
}
