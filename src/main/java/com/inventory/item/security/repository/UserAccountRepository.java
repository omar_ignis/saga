/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.security.repository;


import com.inventory.item.security.entity.UserAccount;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author ignis
 */
@Repository
public interface UserAccountRepository extends JpaRepository <UserAccount, Long>{
    
    UserAccount findByEmail(String email);
}
