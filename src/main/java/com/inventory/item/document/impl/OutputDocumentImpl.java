/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.document.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.util.Util;
import com.inventory.item.util.Constants;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.OutputDetail;
import com.inventory.item.entity.OutputDocument;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.repository.OutputDocumentRepository;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.inventory.item.document.InventoryDocument;
/**
 *
 * @author ignis
 */
@Component
public class OutputDocumentImpl implements InventoryDocument {
    
    private final OutputDocumentRepository outputDocumentRepository;

    @Autowired
    public OutputDocumentImpl(OutputDocumentRepository outputDocumentRepository) {
        this.outputDocumentRepository = outputDocumentRepository;
    }
    
    @Override
    public String getNextId() {
        
        String sequencePrefix = Constants.OUTPUT_PREFIX;
        Long id = outputDocumentRepository.getNextId();
        return Util.buildRefNbr(sequencePrefix, id, Constants.REF_NBR_LENGTH);
    }

    @Override
    public DocumentType getType() {
        
        return DocumentType.OUTPUT;
    }

    @Override
    public <T extends Document> T release(T document) {
        document.setStatus(Status.RELEASED);
        return document;
    }

    @Override
    public <S extends Detail> S createDetail(S detail) {
        
        OutputDetail outputDetail = Util.copy((OutputDetail) detail, new OutputDetail());
        return (S) outputDetail;
    }

    @Override
    public <S extends Detail> S updateDetail(S detail) {
        
        OutputDetail outputDetail = Util.copy((OutputDetail) detail, new OutputDetail());
        outputDetail.setId(detail.getId());
        return (S) outputDetail;
    }

    @Override
    public <T extends Document> T createDocument(T document) {
        
        OutputDocument outputDocument = Util.copy((OutputDocument) document, new OutputDocument());
        String id = getNextId();
        outputDocument.setId(id);
        return (T) outputDocument;
    }

    @Override
    public <T extends Document> T updateDocument(T document) {
        
        OutputDocument outputDocument = Util.copy((OutputDocument) document, new OutputDocument());
        outputDocument.setId(document.getId());
        return (T) outputDocument;
    }

    @Override
    public <T extends Document, S extends Detail> List<S> updateDetails(T document, List<S> details) {
        
        OutputDocument outputDocument = (OutputDocument) document;
        List<OutputDetail> list = new ArrayList();
        OutputDetail detail = null;
        for (OutputDetail element : (List<OutputDetail>) details) {

            if (element.getId() != null) {
                detail = updateDetail(element);
            } else {
                detail = createDetail(element);
            }

            detail.setOutputDocument(outputDocument);
            list.add(detail);
        }

        return (List<S>) list;
    }

    @Override
    public <T extends Document, S extends Detail> List<S> generateDetails(T document, List<S> details) {
        
        OutputDocument outputDocument = (OutputDocument) document;
        List<OutputDetail> list = new ArrayList();
        
        for(OutputDetail element: (List<OutputDetail>) details){
            OutputDetail detail = createDetail(element);
            detail.setOutputDocument(outputDocument);
            list.add(detail);
        }
        
        return (List<S>) list;
    }
    
}
