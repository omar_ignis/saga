/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.document.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.util.Util;
import com.inventory.item.util.Constants;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.SalesReturnDetail;
import com.inventory.item.entity.SalesReturnDocument;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.repository.SalesReturnDocumentRepository;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.inventory.item.document.InventoryDocument;

/**
 *
 * @author ignis
 */
@Component
public class SaleReturnDocumentImpl implements InventoryDocument {
    
    private final SalesReturnDocumentRepository salesReturnDocumentRepository;

    @Autowired
    public SaleReturnDocumentImpl(SalesReturnDocumentRepository salesReturnDocumentRepository) {
        this.salesReturnDocumentRepository = salesReturnDocumentRepository;
    }

    @Override
    public DocumentType getType() {
        return DocumentType.SALES_RETURN;
    }

    @Override
    public String getNextId() {
        String sequencePrefix = Constants.RETURN_FOR_SELLING_PREFIX;
        Long id = salesReturnDocumentRepository.getNextId();
        return Util.buildRefNbr(sequencePrefix, id, Constants.REF_NBR_LENGTH);
    }

    @Override
    public <T extends Document> T release(T document) {
        document.setStatus(Status.RELEASED);
        return document;
    }
    
    @Override
    public <T extends Document> T createDocument(T document) {
        SalesReturnDocument salesReturnDocument = Util.copy((SalesReturnDocument) document, new SalesReturnDocument());
        String id = getNextId();
        salesReturnDocument.setId(id);
        return (T) salesReturnDocument;
    }
    
    @Override
    public <T extends Document> T updateDocument(T document) {
        SalesReturnDocument salesReturnDocument = Util.copy((SalesReturnDocument) document, new SalesReturnDocument());
        salesReturnDocument.setId(document.getId());
        return (T) salesReturnDocument;
    }

    @Override
    public <S extends Detail> S createDetail(S detail) {
        SalesReturnDetail salesReturnDetail = Util.copy((SalesReturnDetail) detail, new SalesReturnDetail());
        return (S) salesReturnDetail;
    }
    
    @Override
    public <S extends Detail> S updateDetail(S detail) {
        SalesReturnDetail salesReturnDetail = Util.copy((SalesReturnDetail) detail, new SalesReturnDetail());
        salesReturnDetail.setId(detail.getId());
        return (S) salesReturnDetail;
    }
    
    @Override
    public <T extends Document, S extends Detail> List<S> updateDetails(T document, List<S> details) {
        SalesReturnDocument salesReturnDocument = (SalesReturnDocument) document;
        List<SalesReturnDetail> list = new ArrayList();
        SalesReturnDetail detail = null;
        for (SalesReturnDetail element : (List<SalesReturnDetail>) details) {

            if (element.getId() != null) {
                detail = updateDetail(element);
            } else {
                detail = createDetail(element);
            }

            detail.setSalesReturnDocument(salesReturnDocument);
            list.add(detail);
        }

        return (List<S>) list;
    }
    
    @Override
    public <T extends Document, S extends Detail> List<S> generateDetails(T document, List<S> details) {
        
        SalesReturnDocument salesReturnDocument = (SalesReturnDocument) document;
        List<SalesReturnDetail> list = new ArrayList();
        
        for(SalesReturnDetail element: (List<SalesReturnDetail>) details){
            SalesReturnDetail detail = createDetail(element);
            detail.setSalesReturnDocument(salesReturnDocument);
            list.add(detail);
        }
        
        return (List<S>) list;
    }
}
