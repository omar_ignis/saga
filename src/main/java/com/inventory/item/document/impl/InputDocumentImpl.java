/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.document.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.util.Util;
import com.inventory.item.util.Constants;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.InputDetail;
import com.inventory.item.entity.InputDocument;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.repository.InputDocumentRepository;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.inventory.item.document.InventoryDocument;

/**
 *
 * @author ignis
 */

@Component
public class InputDocumentImpl implements InventoryDocument {

    private final InputDocumentRepository inputDocumentRepository;

    @Autowired
    public InputDocumentImpl(InputDocumentRepository inputDocumentRepository) {
        this.inputDocumentRepository = inputDocumentRepository;
    }

    @Override
    public DocumentType getType() {
        return DocumentType.INPUT;
    }

    @Override
    public String getNextId() {
        String sequencePrefix = Constants.INPUT_PREFIX;
        Long id = inputDocumentRepository.getNextId();
        return Util.buildRefNbr(sequencePrefix, id, Constants.REF_NBR_LENGTH);
    }

    @Override
    public <T extends Document> T release(T document) {
        document.setStatus(Status.RELEASED);
        return document;
    }
    
    @Override
    public <T extends Document> T createDocument(T document) {
        InputDocument inputDocument = Util.copy((InputDocument) document, new InputDocument());
        String id = getNextId();
        inputDocument.setId(id);
        return (T) inputDocument;
    }
    
    @Override
    public <T extends Document> T updateDocument(T document) {
        InputDocument inputDocument = Util.copy((InputDocument) document, new InputDocument());
        inputDocument.setId(document.getId());
        return (T) inputDocument;
    }

    @Override
    public <S extends Detail> S createDetail(S detail) {
        InputDetail inputDetail = Util.copy((InputDetail) detail, new InputDetail());
        return (S) inputDetail;
    }
    
    @Override
    public <S extends Detail> S updateDetail(S detail) {
        InputDetail inputDetail = Util.copy((InputDetail) detail, new InputDetail());
        inputDetail.setId(detail.getId());
        return (S) inputDetail;
    }
    
    @Override
    public <T extends Document, S extends Detail> List<S> updateDetails(T document, List<S> details) {
        InputDocument inputDocument = (InputDocument) document;
        List<InputDetail> list = new ArrayList();
        InputDetail detail = null;
        for (InputDetail element : (List<InputDetail>) details) {

            if (element.getId() != null) {
                detail = updateDetail(element);
            } else {
                detail = createDetail(element);
            }

            detail.setInputDocument(inputDocument);
            list.add(detail);
        }

        return (List<S>) list;
    }
    
    @Override
    public <T extends Document, S extends Detail> List<S> generateDetails(T document, List<S> details) {
        
        InputDocument inputDocument = (InputDocument) document;
        List<InputDetail> list = new ArrayList();
        
        for(InputDetail element: (List<InputDetail>) details){
            InputDetail detail = createDetail(element);
            detail.setInputDocument(inputDocument);
            list.add(detail);
        }
        
        return (List<S>) list;
    }
    
}
