/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.document.impl;

import java.util.List;
import java.util.ArrayList;

import com.inventory.item.util.Util;
import com.inventory.item.util.Constants;

import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.entity.PurchaseReturnDetail;
import com.inventory.item.entity.PurchaseReturnDocument;

import com.inventory.item.enums.Status;
import com.inventory.item.enums.DocumentType;
import com.inventory.item.repository.PurchaseReturnDocumentRepository;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.inventory.item.document.InventoryDocument;

/**
 *
 * @author ignis
 */
@Component
public class PurchaseReturnDocumentImpl implements InventoryDocument {
    
    private final PurchaseReturnDocumentRepository purchaseReturnDocumentRepository;

    @Autowired
    public PurchaseReturnDocumentImpl(
            PurchaseReturnDocumentRepository purchaseReturnDocumentRepository) {
        
        this.purchaseReturnDocumentRepository = purchaseReturnDocumentRepository;
    }
    
    @Override
    public String getNextId() {
        
        String sequencePrefix = Constants.RETURN_FOR_PURCHASE_PREFIX;
        Long id = purchaseReturnDocumentRepository.getNextId();
        return Util.buildRefNbr(sequencePrefix, id, Constants.REF_NBR_LENGTH);
    }

    @Override
    public DocumentType getType() {
        
        return DocumentType.PURCHASE_RETURN;
    }

    @Override
    public <T extends Document> T release(T document) {
        document.setStatus(Status.RELEASED);
        return document;
    }

    @Override
    public <S extends Detail> S createDetail(S detail) {
        
        PurchaseReturnDetail purchaseReturnDetail = 
                Util.copy((PurchaseReturnDetail) detail, new PurchaseReturnDetail());
        return (S) purchaseReturnDetail;
    }

    @Override
    public <S extends Detail> S updateDetail(S detail) {
        
        PurchaseReturnDetail purchaseReturnDetail = 
                Util.copy((PurchaseReturnDetail) detail, new PurchaseReturnDetail());
        purchaseReturnDetail.setId(detail.getId());
        return (S) purchaseReturnDetail;
    }

    @Override
    public <T extends Document> T createDocument(T document) {
        
        PurchaseReturnDocument purchaseReturnDocument = 
                Util.copy((PurchaseReturnDocument) document, new PurchaseReturnDocument());
        String id = getNextId();
        purchaseReturnDocument.setId(id);
        return (T) purchaseReturnDocument;
    }

    @Override
    public <T extends Document> T updateDocument(T document) {
        
        PurchaseReturnDocument purchaseReturnDocument = 
                Util.copy((PurchaseReturnDocument) document, new PurchaseReturnDocument());
        purchaseReturnDocument.setId(document.getId());
        return (T) purchaseReturnDocument;
    }

    @Override
    public <T extends Document, S extends Detail> List<S> updateDetails(T document, List<S> details) {
        
        PurchaseReturnDocument purchaseReturnDocument = (PurchaseReturnDocument) document;
        List<PurchaseReturnDetail> list = new ArrayList();
        PurchaseReturnDetail detail = null;
        for (PurchaseReturnDetail element : (List<PurchaseReturnDetail>) details) {

            if (element.getId() != null) {
                detail = updateDetail(element);
            } else {
                detail = createDetail(element);
            }

            detail.setPurchaseReturnDocument(purchaseReturnDocument);
            list.add(detail);
        }

        return (List<S>) list;
    }

    @Override
    public <T extends Document, S extends Detail> List<S> generateDetails(T document, List<S> details) {
        
        PurchaseReturnDocument purchaseReturnDocument = (PurchaseReturnDocument) document;
        List<PurchaseReturnDetail> list = new ArrayList();
        
        for(PurchaseReturnDetail element: (List<PurchaseReturnDetail>) details){
            PurchaseReturnDetail detail = createDetail(element);
            detail.setPurchaseReturnDocument(purchaseReturnDocument);
            list.add(detail);
        }
        
        return (List<S>) list;
    }
    
}
