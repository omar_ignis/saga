/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.inventory.item.document;

import java.util.List;
import com.inventory.item.entity.Detail;
import com.inventory.item.entity.Document;
import com.inventory.item.enums.DocumentType;

/**
 *
 * @author ignis
 */
public interface InventoryDocument {
    
    public String getNextId();
    
    public DocumentType getType();
    
    public <T extends Document> T release(T document);
    
    public <S extends Detail> S createDetail(S detail);
    
    public <S extends Detail> S updateDetail(S detail);
    
    public <T extends Document> T createDocument(T document);
    
    public <T extends Document> T updateDocument(T document);
    
    public <T extends Document, S extends Detail> List<S> updateDetails(T document, List<S> details);
    
    public <T extends Document, S extends Detail> List<S> generateDetails(T document, List<S> details);
}
