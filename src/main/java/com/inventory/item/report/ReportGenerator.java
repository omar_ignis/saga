/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory.item.report;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.io.InputStream;

import com.inventory.item.model.RowSheet;
import org.springframework.stereotype.Component;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ClassPathResource;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author ignis
 */
@Component
public class ReportGenerator {

    public final String basePath = "";
    public final String reportName = "OD5160.jrxml";

    private JasperPrint prepareReport(List<RowSheet>  list) throws Exception {

        var beanCollectionDataSource = new JRBeanCollectionDataSource(list);
        Map<String, Object> parameters = new HashMap<>();
        var compileReport = JasperCompileManager
                .compileReport(getReportFilePath(reportName));

        var jasperPrint = JasperFillManager.fillReport(compileReport, parameters, beanCollectionDataSource);

        return jasperPrint;
    }

    public byte[] getReportAsByArray(List<RowSheet> list) throws Exception {

        var jasperPrint = prepareReport(list);
        byte data[] = JasperExportManager.exportReportToPdf(jasperPrint);

        return data;
    }

    public void generateReport(List<RowSheet> list) throws Exception {

        var jasperPrint = prepareReport(list);
        JasperExportManager.exportReportToPdfFile(jasperPrint, "barcodes.pdf");

    }

    private InputStream getReportFilePath(String name) throws Exception {

        Resource resource = new ClassPathResource("reports/" + name);

        return resource.getInputStream();
    }
}
